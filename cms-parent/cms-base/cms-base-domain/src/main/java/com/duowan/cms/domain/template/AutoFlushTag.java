package com.duowan.cms.domain.template;

import java.util.Date;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.dto.template.AutoFlushTagInfo;

/**
 * 
 * 记录需要自动刷新的tag（一般由定时任务插入最近有修改的tag的记录，由另一个定时任务取出tag，刷新对应的模板和tag页）
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
public class AutoFlushTag implements DomainObject {
    
    private static final long serialVersionUID = -7038036284544075204L;
    
    /**
     * 唯一标识
     */
    private Long id;
    /**
     * tag名字
     */
    private String tagName;

    /**
     * 频道ID
     */
    private String channelId;
    
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 刷新时间
     */
    private Date flushTime;
    
    public AutoFlushTag(AutoFlushTagInfo autoFlushTagInfo) {
        this.createTime = new Date();
        update(autoFlushTagInfo);
    }
    
    public AutoFlushTag() {
    }
    
    public AutoFlushTag update(AutoFlushTagInfo autoFlushTagInfo){
        this.channelId = autoFlushTagInfo.getChannelId();
        this.tagName = autoFlushTagInfo.getTagName();
        this.flushTime = autoFlushTagInfo.getFlushTime();
        return this;
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTagName() {
        return tagName;
    }
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
    public String getChannelId() {
        return channelId;
    }
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getFlushTime() {
        return flushTime;
    }
    public void setFlushTime(Date flushTime) {
        this.flushTime = flushTime;
    }
    
}
