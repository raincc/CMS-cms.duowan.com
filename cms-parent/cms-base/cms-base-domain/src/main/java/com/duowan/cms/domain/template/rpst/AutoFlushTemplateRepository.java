package com.duowan.cms.domain.template.rpst;

import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.template.AutoFlushTemplate;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;
import com.duowan.cms.dto.template.AutoFlushTemplateSearchCriteria;

public interface AutoFlushTemplateRepository extends DomainObjectRepository<AutoFlushTemplate, AutoFlushTemplateInfo, AutoFlushTemplateSearchCriteria> {

	/**
	 * 根据主键获取刷新模板对象
	 * @param id
	 * @return
	 */
	public AutoFlushTemplate getById(String id);
	/**
	 * 根据频道id和模板id获取刷新模板对象
	 * @param channelId
	 * @param templateId
	 * @return
	 */
	public AutoFlushTemplate getByChannelIdAndTemplateId(String channelId , Long templateId);
}
