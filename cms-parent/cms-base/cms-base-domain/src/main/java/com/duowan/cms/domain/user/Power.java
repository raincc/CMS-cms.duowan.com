package com.duowan.cms.domain.user;

import java.util.HashMap;
import java.util.Map;

import com.duowan.cms.common.domain.DomainObject;

/**
 * 权限
 */
public class Power  implements DomainObject {
    
    private static final long serialVersionUID = -3241652927716737179L;
    /**
     * 权限说明
     * 所有权限： "添加文章", "文章修删", "文章搜看","上传文件到频道图片域名", 
     *                  "上传文件到频道根域名", "", "", "模板添加", "模板修删", "模板搜看", "",
     *                  "投票添加", "投票修删", "投票搜看", "", "添加关键字", "关键字修删", "关键字搜看", 
     *                  "", "", "", "", "", "tag添加", "tag修删", "tag搜看", "热词管理", "回复管理", "抓取管理", "用户搜看","用户修删加"
     */
    
    private static Map<String,String> allPowerMap = new HashMap<String,String>();
    
    static {
        //allPowerMap.put("1", "角色");
        allPowerMap.put("1", "添加文章");
        allPowerMap.put("2", "文章修删");
        allPowerMap.put("3", "文章搜看");
        allPowerMap.put("4", "上传文件到频道图片域名");
        allPowerMap.put("5", "上传文件到频道根域名");
        allPowerMap.put("6", "最终文章页模板增删改");
        allPowerMap.put("7", "");
        allPowerMap.put("8", "模板添加");
        allPowerMap.put("9", "模板修删");
        allPowerMap.put("10", "模板搜看");
        allPowerMap.put("11", "");
        allPowerMap.put("12", "投票添加");
        allPowerMap.put("13", "投票修删");
        allPowerMap.put("14", "投票搜看");
        allPowerMap.put("15", "");
//        allPowerMap.put("16", "添加关键字");
//        allPowerMap.put("17", "关键字修删");
//        allPowerMap.put("18", "关键字搜看");
        allPowerMap.put("19", "");
        allPowerMap.put("20", "");
        allPowerMap.put("21", "");
        allPowerMap.put("22", "");
        allPowerMap.put("23", "");
        allPowerMap.put("24", "tag添加");
        allPowerMap.put("25", "tag修删");
        allPowerMap.put("26", "tag搜看");
        allPowerMap.put("27", "热词管理");
        allPowerMap.put("28", "回复管理");
        allPowerMap.put("29", "复制/粘贴最终文章页模板的权限");
        allPowerMap.put("30", "用户搜看");
        allPowerMap.put("31", "用户修删加");
    }
    
    public static Map<String,String> getAllPowerMap(){
        return allPowerMap;
    }
    
    public final static int length = allPowerMap.size();
    
    /**
     * 权限的唯一标识
     */
    String id;
    /**
     * 权限名字
     */
    String name;
    
    public Power(String id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}
