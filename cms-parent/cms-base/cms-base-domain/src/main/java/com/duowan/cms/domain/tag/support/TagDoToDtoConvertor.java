/**
 * 
 */
package com.duowan.cms.domain.tag.support;

import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.tag.Tag;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.tag.TagInfo;

/**
 * Tag的dto转换器
 * 
 * @author yzq
 * 
 */
public class TagDoToDtoConvertor extends
        AbstractDoToDtoConvertor<Tag, TagInfo> {
    
    private TagDoToDtoConvertor() {
    }
    
    private static TagDoToDtoConvertor instance;
    
    static {
        instance = new TagDoToDtoConvertor();
        DoToDtoConvertorFactory.register(Tag.class, instance);
    }
    
    public static TagDoToDtoConvertor getInstance() {
        return instance;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public TagInfo do2Dto(Tag tag) {
        
        if (tag == null)
            return null;
        
        TagInfo tagInfo = new TagInfo();
        tagInfo.setName(tag.getName());
        tagInfo.setChannelInfo((ChannelInfo)DoToDtoConvertorFactory.getConvertor(Channel.class).do2Dto(tag.getChannel()));
        tagInfo.setId(tag.getId());
        tagInfo.setParentName(tag.getParentName());
        tagInfo.setTree(tag.getTree());
        tagInfo.setUpdateTime(tag.getUpdateTime());
        tagInfo.setCategory(tag.getCategory());
        tagInfo.setRelatedTemplateCategory(tag.getRelatedTemplateCategory());
        return tagInfo;
    }

}
