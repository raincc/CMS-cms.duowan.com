/**
 * 
 */
package com.duowan.cms.domain.hotword.rpst;

import java.util.List;

import com.duowan.cms.domain.hotword.HotWord;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.dto.hotword.HotWordInfo;
import com.duowan.cms.dto.hotword.HotWordSearchCriteria;

/**
 * @author yzq
 *
 */
public interface HotWordRepository extends DomainObjectRepository<HotWord,  HotWordInfo,  HotWordSearchCriteria> {

    public List<HotWordInfo> getAll();
    
    public List<HotWordInfo> getByChannelId(String channelId);
    
    public HotWord getByChannelIdAndName(String channelId, String name);
}
