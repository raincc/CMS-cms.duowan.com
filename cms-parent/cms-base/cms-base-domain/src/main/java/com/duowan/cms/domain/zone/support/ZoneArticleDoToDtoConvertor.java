/**
 * 
 */
package com.duowan.cms.domain.zone.support;

import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.zone.ZoneArticle;
import com.duowan.cms.dto.zone.ZoneArticleInfo;

/**
 * ZoneArticle的dto转换器
 * 
 * @author yzq
 * 
 */
public class ZoneArticleDoToDtoConvertor extends AbstractDoToDtoConvertor<ZoneArticle, ZoneArticleInfo> {

    private ZoneArticleDoToDtoConvertor() {
    }

    private static ZoneArticleDoToDtoConvertor instance;

    static {
        instance = new ZoneArticleDoToDtoConvertor();
        DoToDtoConvertorFactory.register(ZoneArticle.class, instance);
    }

    public static ZoneArticleDoToDtoConvertor getInstance() {
        return instance;
    }

    public ZoneArticleInfo do2Dto(ZoneArticle zoneArticle) {

        if (zoneArticle == null)
            return null;
        
        ZoneArticleInfo zoneArticleInfo = new ZoneArticleInfo();
        zoneArticleInfo.setArticleId(zoneArticle.getArticleId());
        zoneArticleInfo.setChannelId(zoneArticle.getChannelId());
        zoneArticleInfo.setCreateTime(zoneArticle.getCreateTime());
        zoneArticleInfo.setStatus(zoneArticle.getStatus());
        zoneArticleInfo.setTag(zoneArticle.getTag());
        zoneArticleInfo.setTitle(zoneArticle.getTitle());
        zoneArticleInfo.setUpdateTime(zoneArticle.getUpdateTime());
        zoneArticleInfo.setZoneUserName(zoneArticle.getZoneUserName());

        return zoneArticleInfo;
    }
}
