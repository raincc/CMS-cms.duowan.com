/**
 * 
 */
package com.duowan.cms.domain.template.support;

import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.template.Template;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.template.TemplateInfo;

/**
 * Tag的dto转换器
 * 
 * @author yzq
 * 
 */
public class TemplateDoToDtoConvertor extends
        AbstractDoToDtoConvertor<Template, TemplateInfo> {
    
    private TemplateDoToDtoConvertor() {
    }
    
    private static TemplateDoToDtoConvertor instance;
    
    static {
        instance = new TemplateDoToDtoConvertor();
        DoToDtoConvertorFactory.register(Template.class, instance);
    }
    
    public static TemplateDoToDtoConvertor getInstance() {
        return instance;
    }
    
    @Override
    public TemplateInfo do2Dto(Template template) {
        
        if (template == null)
            return null;
        
        TemplateInfo templateInfo = new TemplateInfo();
        templateInfo.setId(template.getId());
        templateInfo.setChannelInfo((ChannelInfo)DoToDtoConvertorFactory.getConvertor(Channel.class).do2Dto(template.getChannel()));
        templateInfo.setName(template.getName());
        templateInfo.setDigest(template.getDigest());
        templateInfo.setCreateUserId(template.getCreateUserId());
        templateInfo.setLastUpdateUserId(template.getLastUpdateUserId());
        templateInfo.setUserIp(template.getUserIp());
        templateInfo.setProxyIp(template.getProxyIp());
        templateInfo.setPostTime(template.getPostTime());
        templateInfo.setUpdateTime(template.getUpdateTime());
        templateInfo.setContent(template.getContent());
        templateInfo.setUpdateTime2(template.getUpdateTime2());
        templateInfo.setContent2(template.getContent2());
        templateInfo.setUpdateTime3(template.getUpdateTime3());
        templateInfo.setContent3(template.getContent3());
        templateInfo.setRelatedtag(template.getRelatedtag());
        templateInfo.setLogs(template.getLogs());
        templateInfo.setAlias(template.getAlias());
        //templateInfo.setCooperate(template.getCooperate());
        templateInfo.setCode(template.getCode());
        templateInfo.setParserTime(template.getParserTime());
        templateInfo.setTemplateStatus(template.getTemplateStatus());
        templateInfo.setTemplateCategory(template.getTemplateCategory());
        return templateInfo;
    }

}
