/**
 * 
 */
package com.duowan.cms.domain.channel.support;

import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * Channel的dto转换器
 * 
 * @author yzq
 * 
 */
public class ChannelDoToDtoConvertor extends
        AbstractDoToDtoConvertor<Channel, ChannelInfo> {
    
    private ChannelDoToDtoConvertor() {
    }
    
    private static ChannelDoToDtoConvertor instance;
    
    static {
        instance = new ChannelDoToDtoConvertor();
        DoToDtoConvertorFactory.register(Channel.class, instance);
    }
    
    public static ChannelDoToDtoConvertor getInstance() {
        return instance;
    }
    
    public ChannelInfo do2Dto(Channel channel) {
        
        if (channel == null)
            return null;
        
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setId(channel.getId());
        channelInfo.setName(channel.getName());
        channelInfo.setDomain(channel.getDomain());
        channelInfo.setArticleFilePath(channel.getArticleFilePath());
        channelInfo.setPicDomain(channel.getPicDomain());
        channelInfo.setPicFilePath(channel.getPicFilePath());
        channelInfo.setImportant(channel.isImportant());
        channelInfo.setUpline(channel.isUpline());
        channelInfo.setCategory(channel.getCategory());
        
        return channelInfo;
    }

}
