package com.duowan.cms.domain.article;

import java.util.Date;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.util.DateUtil;

/**
 * 用于文章统计，统计编辑的工作量
 * @author Administrator
 *
 */
public class ArticleCount implements DomainObject{

    private static final long serialVersionUID = -1573635756585866523L;

    private String channelId;
    
    private String userId;
    
    private Date date;
    
    private Integer count;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    
    public String getDateStr(){
        if(null == this.date)
            return "";
        return DateUtil.format(this.date, "yyyy-MM-dd");
    }
    
}
