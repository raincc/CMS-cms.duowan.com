/**
 * 
 */
package com.duowan.cms.domain.article.support;

import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.article.Article4TagList;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * Article4TagList的dto转换器
 * 
 * @author yzq
 * 
 */
public class Article4TagListDoToDtoConvertor extends AbstractDoToDtoConvertor<Article4TagList, Article4TagListInfo> {

    private Article4TagListDoToDtoConvertor() {
    }

    private static Article4TagListDoToDtoConvertor instance;

    static {
        instance = new Article4TagListDoToDtoConvertor();
        DoToDtoConvertorFactory.register(Article4TagList.class, instance);
    }

    public static Article4TagListDoToDtoConvertor getInstance() {
        return instance;
    }

    // ChannelInfo channelInfo =
    // (ChannelInfo)DoToDtoConvertorFactory.getConvertor(Channel.class).do2Dto(article.getChannel());

    @SuppressWarnings("unchecked")
    @Override
    public Article4TagListInfo do2Dto(Article4TagList a) {
        if (a == null)
            return null;

        Article4TagListInfo at = new Article4TagListInfo();
        at.setId(a.getId());
        at.setTag(a.getTag());
        ChannelInfo channelInfo = (ChannelInfo) DoToDtoConvertorFactory.getConvertor(Channel.class).do2Dto(a.getChannel());
        at.setChannelInfo(channelInfo);
        at.setTitle(a.getTitle());
        at.setSubtitle(a.getSubtitle());
        at.setDigest(a.getDigest());
        at.setEmptyLink(a.getEmptyLink());
        at.setPictureUrl(a.getPictureUrl());
        at.setSource(a.getSource());
        at.setAuthor(a.getAuthor());
        at.setUserId(a.getUserId());
        at.setDayNum(a.getDayNum());
        at.setPower(a.getPower());
        at.setPublishTime(a.getPublishTime());
        at.setLastUpdateTime(a.getUpdateTime());
        at.setIsHeadPic(a.getIsHeadPic());
        at.setSonTag(a.getSonTag());
        at.setSonTagId(a.getSonTagId());
        at.setDiy1(a.getDiy1());
        at.setDiy2(a.getDiy2());
        at.setDiy3(a.getDiy3());
        at.setDiy4(a.getDiy4());
        at.setDiy5(a.getDiy5());
        at.setDelete(a.isDelete());
        at.setUrlOnLine(a.getUrlOnLine());
        at.setUrlOnCms(a.getUrlOnCms());
        at.setTitleColor(a.getTitleColor());
        at.setStatus(a.getStatus());
        return at;
    }

}
