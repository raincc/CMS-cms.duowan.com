package com.duowan.cms.domain.article.rpst;

import java.util.Date;
import java.util.List;

import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.article.Article4TagList;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.Article4TagListSearchCriteria;

/**
 * 文章仓库
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：yzq
 * <br> 版本：1.0
 * <br> 创建时间：2012-10-5
 * <br>==========================
 */
public interface Article4TagListRepository extends DomainObjectRepository<Article4TagList, Article4TagListInfo, Article4TagListSearchCriteria> {
    
    public Article4TagList getByPrimaryKey(String channelId, String tag, Long articleId);
    /**
     * 获取channelId频道下的articleId的文章列表
     */
    public List<Article4TagListInfo> getByChannelIdAndArticleId(String channelId, Long articleId);
    /**
     * 根据文章标题，获取channelId频道下的文章列表数
     */
    public int getCountByTitle(String channelId, String title);
    
    /**
     * 获取channelId频道下time时间内更新过的文章tag（一般是通过修改文章的关联tags , 所引起的变化的tags）,返回多个tag，使用逗号分隔，如tag1,tag2,tag3
     * @param time
     * @return
     */
    public List<String> getRecentEffectTagsInArticle(String channelId , int minute);
    /**
     * 更新标签名称
     */
    public void updateTagName(String channelId , String originalTagName , String newTagName);
    /**
     * 根据频道id和文章id批量更新文章列表数据
     */
    public void updateByChannelIdAndArticleId(Article4TagList article4TagList);
   
    
    /**
     * 获取文章数，根据频道id与tag
     * @param channelId
     * @param tag
     * @return
     */
    public Integer getArticleCountByChannelIdAndTag(String channelId, String tag);
    
    /**
     * 逻辑删除，即将isdelete字段设为1(true)
     * @param channelId
     * @param articleId
     */
    public void logicDeleteByChannelIdAndArticleId(String channelId, Long articleId);
    
    /**
     * 物理删除，即从库中删除
     * @param channelId
     * @param articleId
     */
    public void deleteByChannelIdAndArticleId(String channelId, Long articleId);

    /**
     * 批量保存，若记录已经存在，则更新记录所有字段
     * @param channelId
     * @param list
     */
    public void saveList(String channelId, List<Article4TagList> list);
    
    /**
     * 创建list_${channelId}表
     * @param channelId
     */
    public void createTableByChannelId(String channelId);
    
    /**
     * 更改文章发表时间
     * @param channelId
     * @param articleId
     * @param newPublishTime
     * @param dayNum
     */
    public void updatePublishTime(String channelId, Long articleId, Date newPublishTime, int newDayNum);
    
}