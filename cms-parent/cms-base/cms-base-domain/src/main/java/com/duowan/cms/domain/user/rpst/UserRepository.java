package com.duowan.cms.domain.user.rpst;

import com.duowan.cms.domain.user.User;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserSearchCriteria;

public interface UserRepository extends DomainObjectRepository<User, UserInfo, UserSearchCriteria> {
	
	public User getById(String userId);
	
	public User getByMail(String mail);
	
	public User getByUserIdAndPassword(String userId, String password);
	
	public User getByUdbName(String udbName);
	
	public void updateName(String oldName, String newName);
	
}
