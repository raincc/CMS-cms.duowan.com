/**
 * 
 */
package com.duowan.cms.domain.template;

import java.util.Date;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;

/**
 * 需要自动刷新的模板(由后台管理员录入的模板，一般是排行榜模板(系统本身不知道数据是否变化了，所以定时获取并刷新) )
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-3-1
 * <br>==========================
 */
public class AutoFlushTemplate implements DomainObject {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 唯一标识
     */
    private Long id;
    /**
     * 模板ID
     */
    private Long templateId ;
    /**
     * 频道id
     */
    private Channel channel;
    /**
     * 刷新间隔时间（单位秒）
     */
    private int interval;
    /**
     * 上一次的成功刷新时间
     */
    private Date lastFlushTime;
    /**
     * 描述
     */
    private String description;
    
    public AutoFlushTemplate(){}
    
    public AutoFlushTemplate(AutoFlushTemplateInfo autoFlushTemplateInfo){
    	this.id = autoFlushTemplateInfo.getId();
    	this.interval = autoFlushTemplateInfo.getInterval();
    	this.description = autoFlushTemplateInfo.getDescription();
    	if (autoFlushTemplateInfo.getTemplateId() != null) {
			this.templateId = autoFlushTemplateInfo.getTemplateId();
		}
    	if (autoFlushTemplateInfo.getLastFlushTime() != null) {
    		this.lastFlushTime = autoFlushTemplateInfo.getLastFlushTime();
		}
    	if (autoFlushTemplateInfo.getChannelInfo() != null) {
    		this.channel = new Channel(autoFlushTemplateInfo.getChannelInfo());
		}
    }
    
  //******************普通的getter和setter************************    
    
    
    public Channel getChannel() {
		return channel;
	}
	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public int getInterval() {
        return interval;
    }
    public void setInterval(int interval) {
        this.interval = interval;
    }
    public Date getLastFlushTime() {
        return lastFlushTime;
    }
    public void setLastFlushTime(Date lastFlushTime) {
        this.lastFlushTime = lastFlushTime;
    }
    
}
