package com.duowan.cms.domain.article.rpst;

import com.duowan.cms.domain.article.ArticleGameLib;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;

public interface ArticleGameLibRepository extends DomainObjectRepository<ArticleGameLib, ArticleInfo, ArticleSearchCriteria> {

	public ArticleGameLib  getArticleGameLib(String channelId , long articleId );
}
