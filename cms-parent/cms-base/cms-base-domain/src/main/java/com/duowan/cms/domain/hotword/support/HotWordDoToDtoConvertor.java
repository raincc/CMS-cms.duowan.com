/**
 * 
 */
package com.duowan.cms.domain.hotword.support;

import com.duowan.cms.domain.hotword.HotWord;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.hotword.HotWordInfo;

/**
 * HotWord的dto转换器
 * 
 * @author yzq
 * 
 */
public class HotWordDoToDtoConvertor extends
        AbstractDoToDtoConvertor<HotWord, HotWordInfo> {
    
    private HotWordDoToDtoConvertor() {
    }
    
    private static HotWordDoToDtoConvertor instance;
    
    static {
        instance = new HotWordDoToDtoConvertor();
        DoToDtoConvertorFactory.register(HotWord.class, instance);
    }
    
    public static HotWordDoToDtoConvertor getInstance() {
        return instance;
    }
    
    @Override
    public HotWordInfo do2Dto(HotWord hotWord) {
        
        if (hotWord == null)
            return null;
        
        HotWordInfo hotWordInfo = new HotWordInfo();
        hotWordInfo.setChannelId(hotWord.getChannelId());
        hotWordInfo.setName(hotWord.getName());
        hotWordInfo.setHref(hotWord.getHref());

        return hotWordInfo;
    }

}
