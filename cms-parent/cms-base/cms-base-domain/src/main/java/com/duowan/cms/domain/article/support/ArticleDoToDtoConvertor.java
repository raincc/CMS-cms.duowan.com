/**
 * 
 */
package com.duowan.cms.domain.article.support;

import com.duowan.cms.domain.article.Article;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * Article的dto转换器
 * 
 * @author coolcooldee
 * 
 */
public class ArticleDoToDtoConvertor extends
        AbstractDoToDtoConvertor<Article, ArticleInfo> {
    
    private ArticleDoToDtoConvertor() {
    }
    
    private static ArticleDoToDtoConvertor instance;
    
    static {
        instance = new ArticleDoToDtoConvertor();
        DoToDtoConvertorFactory.register(Article.class, instance);
    }
    
    public static ArticleDoToDtoConvertor getInstance() {
        return instance;
    }
    
    @SuppressWarnings("unchecked")
    public ArticleInfo do2Dto(Article article) {
        
        if (article == null)
            return null;
        
        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setId(article.getId());
        ChannelInfo channelInfo = (ChannelInfo)DoToDtoConvertorFactory.getConvertor(Channel.class).do2Dto(article.getChannel());
        articleInfo.setChannelInfo(channelInfo);
        articleInfo.setTitle(article.getTitle());
        articleInfo.setSubtitle(article.getSubtitle());
        articleInfo.setDigest(article.getDigest());
        articleInfo.setPictureUrl(article.getPictureUrl());
        //articleInfo.setTagPictureUrl(article.getTagPictureUrl());
        articleInfo.setSource(article.getSource());
        articleInfo.setAuthor(article.getAuthor());
        articleInfo.setPublishTime(article.getPublishTime());
        articleInfo.setPrePublishTime(article.getPrePublishTime());
        articleInfo.setLastUpdateTime(article.getLastUpdateTime());
        articleInfo.setTags(article.getTags());
        articleInfo.setUserId(article.getUserId());
        articleInfo.setLastUpdateUserId(article.getLastUpdateUserId());
        articleInfo.setUserIp(article.getUserIp());
        articleInfo.setProxyIp(article.getProxyIp());
        articleInfo.setStatus(article.getStatus());
        articleInfo.setLogs(article.getLogs());
        articleInfo.setContent(article.getContent());
        articleInfo.setDayNum(article.getDayNum());
        articleInfo.setPower(article.getPower());
        articleInfo.setThreadId(article.getThreadId());
        articleInfo.setTemplateId(article.getTemplateId());
        articleInfo.setRealtags(article.getRealtags());
        articleInfo.setIsHeadPic(article.getIsHeadPic());
        articleInfo.setCatchUrl(article.getCatchUrl());
        articleInfo.setUrlOnCms(article.getUrlOnCms());
        articleInfo.setUrlOnLine(article.getUrlOnLine());
        //articleInfo.setEmptyLink(article.getEmptyLink());
        articleInfo.setDiy1(article.getDiy1());
        articleInfo.setDiy2(article.getDiy2());
        articleInfo.setDiy3(article.getDiy3());
        articleInfo.setDiy4(article.getDiy4());
        articleInfo.setDiy5(article.getDiy5());
        articleInfo.setIsShowAllPage(article.isShowAllPage() ? "on" : null);
        articleInfo.setIsSyncWap(article.isSyncWap() ? "on" : null);
        articleInfo.setTitleColor(article.getTitleColor());
        return articleInfo;
    }
    
    public static void main(String[] args) {
        System.out.println(ArticleDoToDtoConvertor.getInstance().do2Dto(null));
    }
    
}
