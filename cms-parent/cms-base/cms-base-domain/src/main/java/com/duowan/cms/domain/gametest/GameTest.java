package com.duowan.cms.domain.gametest;

import java.util.Date;

import com.duowan.cms.common.domain.DomainObject;

/**
 * 
 * @author yzq
 *
 */
public class GameTest  implements DomainObject{
    
    /**
     * 
     */
    private static final long serialVersionUID = 4990923745502320903L;

    private Integer id;
    
    /**
     * 时间
     */
    private Date time;
    /**
     * 游戏名称
     */
    private String gameName;
    /**
     * 游戏名链接
     */
    private String gameNameUrl;
    /**
     * 阶段
     */
    private String state;
    /**
     * 阶段链接
     */
    private String stateUrl;
    /**
     * 下载地点
     */
    private String download;
    /**
     * 下载链接
     */
    private String downloadUrl;
    /**
     * 截图描述
     */
    private String cutPictureDesc;
    /**
     * 截图链接
     */
    private String cutPictureUrl;
    /**
     * 视频描述
     */
    private String videoDesc;
    /**
     * 视频链接
     */
    private String videoUrl;
    /**
     * 运营/开发公司名称
     */
    private String company;
    /**
     * 运营/开发公司名称链接
     */
    private String companyUrl;
    /**
     * 多玩专区名称
     */
    private String duowanChannel;
    /**
     * 多玩专区链接
     */
    private String duowanChannelUrl;
    /**
     * 帐号描述
     */
    private String accountDesc;
    /**
     * 账号链接
     */
    private String accountUrl;
    /**
     * 评论描述
     */
    private String commentDesc;
    /**
     * 评论链接
     */
    private String commentUrl;
    /**
     * 自定义参数
     */
    private String diy;
    /**
     * 主页检查
     */
    private String indexCheck;
    /**
     * 公测检查
     */
    private String pubCheck;
    /**
     * 权重
     */
    private Integer power;
    /**
     * 免费游戏
     */
    private String freeGame;
    /**
     * 已付费
     */
    private String hasPay;
    /**
     * 国外游戏
     */
    private String abroad;
    
    private Integer ctype;
    
    private String fxcompany;
    
    private String fxcompanyurl;
    
    private String gameType;
    
    private String gameTypeUrl;
    
    //1是封测，2是内测，3是公测 add by zqm-首页测试游戏列表状态自动填写功能
    private String gstatus;

    //****************************普通的getter和setter*******************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameNameUrl() {
        return gameNameUrl;
    }

    public void setGameNameUrl(String gameNameUrl) {
        this.gameNameUrl = gameNameUrl;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateUrl() {
        return stateUrl;
    }

    public void setStateUrl(String stateUrl) {
        this.stateUrl = stateUrl;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getCutPictureDesc() {
        return cutPictureDesc;
    }

    public void setCutPictureDesc(String cutPictureDesc) {
        this.cutPictureDesc = cutPictureDesc;
    }

    public String getCutPictureUrl() {
        return cutPictureUrl;
    }

    public void setCutPictureUrl(String cutPictureUrl) {
        this.cutPictureUrl = cutPictureUrl;
    }

    public String getVideoDesc() {
        return videoDesc;
    }

    public void setVideoDesc(String videoDesc) {
        this.videoDesc = videoDesc;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyUrl() {
        return companyUrl;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getDuowanChannel() {
        return duowanChannel;
    }

    public void setDuowanChannel(String duowanChannel) {
        this.duowanChannel = duowanChannel;
    }

    public String getDuowanChannelUrl() {
        return duowanChannelUrl;
    }

    public void setDuowanChannelUrl(String duowanChannelUrl) {
        this.duowanChannelUrl = duowanChannelUrl;
    }

    public String getAccountDesc() {
        return accountDesc;
    }

    public void setAccountDesc(String accountDesc) {
        this.accountDesc = accountDesc;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {
        this.accountUrl = accountUrl;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }

    public String getCommentUrl() {
        return commentUrl;
    }

    public void setCommentUrl(String commentUrl) {
        this.commentUrl = commentUrl;
    }

    public String getDiy() {
        return diy;
    }

    public void setDiy(String diy) {
        this.diy = diy;
    }

    public String getIndexCheck() {
        return indexCheck;
    }

    public void setIndexCheck(String indexCheck) {
        this.indexCheck = indexCheck;
    }

    public String getPubCheck() {
        return pubCheck;
    }

    public void setPubCheck(String pubCheck) {
        this.pubCheck = pubCheck;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public String getFreeGame() {
        return freeGame;
    }

    public void setFreeGame(String freeGame) {
        this.freeGame = freeGame;
    }

    public String getHasPay() {
        return hasPay;
    }

    public void setHasPay(String hasPay) {
        this.hasPay = hasPay;
    }

    public String getAbroad() {
        return abroad;
    }

    public void setAbroad(String abroad) {
        this.abroad = abroad;
    }

    public Integer getCtype() {
        return ctype;
    }

    public void setCtype(Integer ctype) {
        this.ctype = ctype;
    }

    public String getFxcompany() {
        return fxcompany;
    }

    public void setFxcompany(String fxcompany) {
        this.fxcompany = fxcompany;
    }

    public String getFxcompanyurl() {
        return fxcompanyurl;
    }

    public void setFxcompanyurl(String fxcompanyurl) {
        this.fxcompanyurl = fxcompanyurl;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getGameTypeUrl() {
        return gameTypeUrl;
    }

    public void setGameTypeUrl(String gameTypeUrl) {
        this.gameTypeUrl = gameTypeUrl;
    }

    public String getGstatus() {
        return gstatus;
    }

    public void setGstatus(String gstatus) {
        this.gstatus = gstatus;
    }
}
/**
游戏测试表数据库结构
time,gname,gnameurl,state,jdurl,download,downloadurl,cutpic,cutpicurl,video,videourl,company,companyurl,area,areaurl,account,accounturl,comment,commenturl,zidingyi,checkindex,checkpub,power,freegame,haspay,abroad

时间 游戏名称,游戏名链接,阶段,阶段链接,下载地点,下载链接,截图描述,截图链接,视频描述,视频链接, 运营/开发公司名称,运营/开发公司名称链接,多玩专区名称,多玩专区链接,帐号描述,账号链接, 评论描述,评论链接,自定义参数,主页检查,公测检查,权重,免费游戏,已付费,国外游戏

CREATE TABLE `gametestinfo` (
  `time` date NOT NULL,
  `gname` varchar(50) NOT NULL default '',
  `gnameurl` varchar(250) default NULL,
  `state` varchar(24) NOT NULL,
  `stateurl` varchar(250) default NULL,
  `download` varchar(12) default NULL,
  `downloadurl` varchar(250) default NULL,
  `cutpic` varchar(12) default NULL,
  `cutpicurl` varchar(250) default NULL,
  `video` varchar(12) default NULL,
  `videourl` varchar(250) default NULL,
  `company` varchar(50) default NULL,
  `companyurl` varchar(250) default NULL,
  `area` varchar(12) NOT NULL,
  `areaurl` varchar(250) default NULL,
  `account` varchar(12) default NULL,
  `accounturl` varchar(250) default NULL,
  `comment` varchar(12) NOT NULL,
  `commenturl` varchar(250) default NULL,
  `zidingyi` varchar(12) default NULL,
  `checkindex` char(3) default NULL,
  `power` tinyint(4) default '1',
  `checkpub` char(3) default NULL,
  `freegame` char(3) default NULL,
  `haspay` char(3) default NULL,
  `abroad` char(3) default NULL,
  `ctype` tinyint(4) default '1',
  `fxcompany` varchar(50) default NULL,
  `fxcompanyurl` varchar(250) default NULL,
  `gametype` varchar(20) default NULL,
  `gametypeurl` varchar(250) default NULL,
  `gametestid` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`gametestid`),
  KEY `time` (`time`),
  KEY `gname` (`gname`),
  KEY `state` (`state`),
  KEY `company` (`company`),
  KEY `zidingyi` (`zidingyi`),
  KEY `checkindex` (`checkindex`),
  KEY `power` (`power`),
  KEY `checkpub` (`checkpub`),
  KEY `freegame` (`freegame`),
  KEY `haspay` (`haspay`),
  KEY `abroad` (`abroad`),
  KEY `gametype` (`gametype`),
  KEY `ctype` (`ctype`),
  KEY `fxcompany` (`fxcompany`)
) ENGINE=MyISAM AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;
*/
