package com.duowan.cms.domain.zone;

import com.duowan.cms.common.domain.DomainObject;
/**
 * 有爱用户
 * @author yzq
 *
 * -- Table "zone_user" DDL
CREATE TABLE `zone_user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userName` varchar(255) NOT NULL,
  `nickName` varchar(255) default NULL,
  `defaultTag` varchar(255) default NULL,
  `channelId` varchar(45) NOT NULL,
  `isDel` char(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_index` (`userName`,`channelId`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
 */
public class ZoneUser  implements DomainObject {

    private static final long serialVersionUID = -3208759001974428572L;
    
    private Long id;
    private String channelId;
    private String userName;
    private String nickName;
    private String defaultTag;
    private String isDel;
    
    public ZoneUser(){}
    
    public ZoneUser(String userName){
        this.userName = userName;
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getChannelId() {
        return channelId;
    }
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getDefaultTag() {
        return defaultTag;
    }
    public void setDefaultTag(String defaultTag) {
        this.defaultTag = defaultTag;
    }
    public String getIsDel() {
        return isDel;
    }
    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }
    
    
}
