package com.duowan.cms.domain.channel;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * 频道
 * 
 * @author coolcooldee
 *
 */
public class Channel  implements DomainObject{
 
    private static final long serialVersionUID = -5269263658059304926L;

    /**
     * 频道的唯一标识
     */
    String id;
    
    /**
     * 频道的名称
     */
    String name;
    
    /**
     * 频道的域名(外网的访问域名)
     */
    String domain;
    
    /**
     * 频道文章的存放地址( 由编辑发表的文章，发布系统生成的静态页面 )
     */
    String articleFilePath;
    
    /**
     * 图片域名(外网的访问域名)
     */
    String picDomain;
    
    /**
     * 频道图片的存放地址 （由用户上传的图片，包括发表文章的上传和文件管理器的上传）
     */
    String picFilePath;
    
    /**
     * 是否重点专区
     */
    private boolean important;
    
    private boolean upline;
    
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getArticleFilePath() {
        return articleFilePath;
    }

    public void setArticleFilePath(String articleFilePath) {
        this.articleFilePath = articleFilePath;
    }

    public String getPicDomain() {
        return picDomain;
    }

    public void setPicDomain(String picDomain) {
        this.picDomain = picDomain;
    }

    public String getPicFilePath() {
        return picFilePath;
    }

    public void setPicFilePath(String picFilePath) {
        this.picFilePath = picFilePath;
    }
    
    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public Channel(){
    }
    
    public Channel(String id){
        this.id = id;
    }
    
    public boolean isUpline() {
        return upline;
    }

    public void setUpline(boolean upline) {
        this.upline = upline;
    }
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Channel(ChannelInfo channelInfo){
        this.id = channelInfo.getId();
        this.domain = channelInfo.getDomain();
        this.picDomain = channelInfo.getPicDomain();
        this.picFilePath = channelInfo.getPicFilePath();
        this.articleFilePath = channelInfo.getArticleFilePath();
        this.update(channelInfo);
    }
    
    public Channel update(ChannelInfo channelInfo){
        this.name = channelInfo.getName();
        this.important = channelInfo.isImportant();
        this.upline = channelInfo.isUpline();
        this.category = channelInfo.getCategory();
        return this;
    }
}
