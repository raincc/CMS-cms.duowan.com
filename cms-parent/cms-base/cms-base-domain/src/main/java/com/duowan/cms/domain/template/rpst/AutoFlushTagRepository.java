/**
 * 
 */
package com.duowan.cms.domain.template.rpst;

import java.util.List;

import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.template.AutoFlushTag;
import com.duowan.cms.dto.template.AutoFlushTagInfo;
import com.duowan.cms.dto.template.AutoFlushTagSearchCriteria;


/**
 * 自动刷新标签的仓库
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
public interface AutoFlushTagRepository extends DomainObjectRepository<AutoFlushTag, AutoFlushTagInfo, AutoFlushTagSearchCriteria> {
    
    /**
     * 获取所有未刷新的tag列表（即刷新时间flushTime为空）
     * @return
     */
    public List<AutoFlushTagInfo> listUnFlushTag();
    
    /**
     * 批量更新
     * @param autoFlushTagInfos
     */
    public void batcUpdate(List<AutoFlushTagInfo> autoFlushTagInfos);
    
    /**
     * 根据id获取
     * @param id
     * @return
     */
    public AutoFlushTag getById(Long id);
}
