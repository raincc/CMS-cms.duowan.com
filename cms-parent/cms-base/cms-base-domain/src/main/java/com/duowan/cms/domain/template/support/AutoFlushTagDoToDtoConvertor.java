/**
 * 
 */
package com.duowan.cms.domain.template.support;

import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.template.AutoFlushTag;
import com.duowan.cms.dto.template.AutoFlushTagInfo;

/**
 * 接口或类的说明
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
public class AutoFlushTagDoToDtoConvertor extends AbstractDoToDtoConvertor<AutoFlushTag, AutoFlushTagInfo> {

    private AutoFlushTagDoToDtoConvertor() {
    }
    
    private static AutoFlushTagDoToDtoConvertor instance;
    
    static {
        instance = new AutoFlushTagDoToDtoConvertor();
        DoToDtoConvertorFactory.register(AutoFlushTag.class, instance);
    }
    
    public static AutoFlushTagDoToDtoConvertor getInstance() {
        return instance;
    }
    
    @Override
    public AutoFlushTagInfo do2Dto(AutoFlushTag autoFlushTag) {
        
        if (autoFlushTag == null){
             return null;
        }
        AutoFlushTagInfo autoFlushTagInfo = new AutoFlushTagInfo(autoFlushTag.getChannelId(),  autoFlushTag.getTagName());
        autoFlushTagInfo.setId(autoFlushTag.getId());
        autoFlushTagInfo.setCreateTime(autoFlushTag.getCreateTime());
        autoFlushTagInfo.setFlushTime(autoFlushTag.getFlushTime());
        return autoFlushTagInfo;
    }

}
