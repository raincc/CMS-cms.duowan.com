package com.duowan.cms.domain.user;

import java.util.Date;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserStatus;

/**
 * 用户领域类
 * @author coolcooldee
 * @version 1.0
 * @created 04-九月-2012 13:50:17
 */
public class User  implements DomainObject{

	/**
     * 
     */
    private static final long serialVersionUID = -3056256175864982240L;
    /**
	 * 用户唯一标识
	 */
	private String userId;
	/**
	 * 用户密码
	 */
	private String password;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 最后修改时间
	 */
	private Date lastModifyTime;
	/**
	 * 用户操作机器的ip地址
	 */
	private String userIp;
	/**
	 * 用户操作机器代理IP
	 */
	private String proxyIp;
	/**
	 * 用户状态
	 */
	private UserStatus userStatus;
	/**
	 * 邮箱地址
	 */
	private String mail;
	
	/**
	 * 是否管理员（指的是系统管理员，与具体某个专区的管理员权限不同）
	 */
	private boolean admin;
	
	private String udbName;
	
	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getProxyIp() {
        return proxyIp;
    }

    public void setProxyIp(String proxyIp) {
        this.proxyIp = proxyIp;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String value) {
        this.userStatus = UserStatus.getInstance(value);
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

	public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    
    public String getUdbName() {
        return udbName;
    }

    public void setUdbName(String udbName) {
        this.udbName = udbName;
    }

    public User(UserInfo userInfo){
	    this.userId = userInfo.getUserId();
	    this.createTime = new Date();
	    this.userStatus = UserStatus.NORMAL;
	    this.update(userInfo);
	}
	
	public User(){
	    
	}
	
	public void update(UserInfo userInfo){
	    this.lastModifyTime = new Date();
        this.mail = userInfo.getMail();
        this.proxyIp = userInfo.getProxyIp();
        this.userIp= userInfo.getUserIp();
        this.password = userInfo.getPassword();
        if(!StringUtil.isEmpty(userInfo.getUdbName())){
            this.udbName = userInfo.getUdbName();
        }else{
            this.udbName = null;
        }
	}
	

	/**
	 * 使用户冻结
	 */
	public void freeze(){
	    this.userStatus = UserStatus.FREEZED;
	    this.lastModifyTime = new Date();
	}

	/**
     * 使用户解冻
     */
	public void unfreeze(){
        this.userStatus = UserStatus.NORMAL;
        this.lastModifyTime = new Date();
    }
	
	/**
	 * 验证密码
	 */
	public boolean verifyPassword(String password){
		return this.password.equals(password);
	}

}