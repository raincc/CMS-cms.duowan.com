/**
 * 
 */
package com.duowan.cms.domain.user.support;

import com.duowan.cms.domain.user.Power;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.user.PowerInfo;

/**
 * 权限DO和DTO转换器
 * @author coolcooldee
 */
public class PowerDoToDtoConvertor extends AbstractDoToDtoConvertor<Power, PowerInfo> {

    private PowerDoToDtoConvertor() {
    }

    private static PowerDoToDtoConvertor instance;

    static {
        instance = new PowerDoToDtoConvertor();
        DoToDtoConvertorFactory.register(Power.class, instance);
    }

    public static PowerDoToDtoConvertor getInstance() {
        return instance;
    }

    public PowerInfo do2Dto(Power power) {
        if (power == null)
            return null;
        PowerInfo powerInfo = new PowerInfo(power.getId(), power.getName());
        return powerInfo;
    }
}
