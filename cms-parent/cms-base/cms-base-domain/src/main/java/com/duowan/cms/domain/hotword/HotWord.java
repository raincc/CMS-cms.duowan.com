package com.duowan.cms.domain.hotword;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.dto.hotword.HotWordInfo;

public class HotWord implements DomainObject{

    private static final long serialVersionUID = 4229855203042300729L;

    private String channelId;
    
    private String name;
    
    private String href;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
    
    public HotWord(HotWordInfo hotWordInfo){
        this.update(hotWordInfo);
    }
    
    public HotWord() {
    }
    
    public void update(HotWordInfo hotWordInfo){
        this.channelId = hotWordInfo.getChannelId();
        this.name = hotWordInfo.getName();
        this.href = hotWordInfo.getHref();
    }
    
}
