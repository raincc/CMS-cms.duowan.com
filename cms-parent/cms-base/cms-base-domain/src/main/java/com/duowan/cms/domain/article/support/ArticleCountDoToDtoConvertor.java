package com.duowan.cms.domain.article.support;

import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.article.ArticleCount;
import com.duowan.cms.dto.article.ArticleCountInfo;

/**
 * ArticleCount的dto转换器
 * 
 */
public class ArticleCountDoToDtoConvertor extends
        AbstractDoToDtoConvertor<ArticleCount, ArticleCountInfo> {
    
    private ArticleCountDoToDtoConvertor() {
    }
    
    private static ArticleCountDoToDtoConvertor instance;
    
    static {
        instance = new ArticleCountDoToDtoConvertor();
        DoToDtoConvertorFactory.register(ArticleCount.class, instance);
    }
    
    public static ArticleCountDoToDtoConvertor getInstance() {
        return instance;
    }
    
    public ArticleCountInfo do2Dto(ArticleCount articleCount) {
        
        if (articleCount == null)
            return null;
        
        ArticleCountInfo articleCountInfo = new ArticleCountInfo();
        articleCountInfo.setDate(articleCount.getDate());
        articleCountInfo.setChannelId(articleCount.getChannelId());
        articleCountInfo.setUserId(articleCount.getUserId());
        articleCountInfo.setCount(articleCount.getCount());
        
        return articleCountInfo;
    }
    
    public static void main(String[] args) {
    }
    
}
