package com.duowan.cms.domain.template.support;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import com.duowan.cms.dto.template.TemplateCategory;
@MappedTypes(value={TemplateCategory.class})
public class TemplateCategoryTypeHandler implements TypeHandler<TemplateCategory> {

    @Override
    public void setParameter(PreparedStatement ps, int i, TemplateCategory parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.getValue());
    }

    @Override
    public TemplateCategory getResult(ResultSet rs, String columnName) throws SQLException {
        return TemplateCategory.getInstance(rs.getString(columnName));
    }

    @Override
    public TemplateCategory getResult(ResultSet rs, int columnIndex) throws SQLException {
        return TemplateCategory.getInstance(rs.getString(columnIndex));
    }

    @Override
    public TemplateCategory getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return TemplateCategory.getInstance(cs.getString(columnIndex));
    }

    

}
