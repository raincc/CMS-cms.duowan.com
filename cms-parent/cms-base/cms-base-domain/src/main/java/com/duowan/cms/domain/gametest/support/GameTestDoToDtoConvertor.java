/**
 * 
 */
package com.duowan.cms.domain.gametest.support;

import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.gametest.GameTest;
import com.duowan.cms.dto.gametest.GameTestInfo;

/**
 * GameTest的dto转换器
 * 
 * @author yzq
 * 
 */
public class GameTestDoToDtoConvertor extends
        AbstractDoToDtoConvertor<GameTest, GameTestInfo> {
    
    private GameTestDoToDtoConvertor() {
    }
    
    private static GameTestDoToDtoConvertor instance;
    
    static {
        instance = new GameTestDoToDtoConvertor();
        DoToDtoConvertorFactory.register(GameTest.class, instance);
    }
    
    public static GameTestDoToDtoConvertor getInstance() {
        return instance;
    }
    
    public GameTestInfo do2Dto(GameTest gameTest) {
        
        if (gameTest == null)
            return null;
        
        GameTestInfo gameTestInfo = new GameTestInfo();
        gameTestInfo.setId(gameTest.getId());
        gameTestInfo.setTime(gameTest.getTime());
        gameTestInfo.setGameName(gameTest.getGameName());
        gameTestInfo.setGameNameUrl(gameTest.getGameNameUrl());
        gameTestInfo.setState(gameTest.getState());
        gameTestInfo.setStateUrl(gameTest.getStateUrl());
        gameTestInfo.setDownload(gameTest.getDownload());
        gameTestInfo.setDownloadUrl(gameTest.getDownloadUrl());
        gameTestInfo.setCutPictureDesc(gameTest.getCutPictureDesc());
        gameTestInfo.setCutPictureUrl(gameTest.getCutPictureUrl());
        gameTestInfo.setVideoDesc(gameTest.getVideoDesc());
        gameTestInfo.setVideoUrl(gameTest.getVideoUrl());
        gameTestInfo.setCompany(gameTest.getCompany());
        gameTestInfo.setCompanyUrl(gameTest.getCompanyUrl());
        gameTestInfo.setDuowanChannel(gameTest.getDuowanChannel());
        gameTestInfo.setDuowanChannelUrl(gameTest.getDuowanChannelUrl());
        gameTestInfo.setAccountDesc(gameTest.getAccountDesc());
        gameTestInfo.setAccountUrl(gameTest.getAccountUrl());
        gameTestInfo.setCommentDesc(gameTest.getCommentDesc());
        gameTestInfo.setCommentUrl(gameTest.getCommentUrl());
        gameTestInfo.setDiy(gameTest.getDiy());
        gameTestInfo.setIndexCheck(gameTest.getIndexCheck());
        gameTestInfo.setPubCheck(gameTest.getPubCheck());
        gameTestInfo.setPower(gameTest.getPower());
        gameTestInfo.setFreeGame(gameTest.getFreeGame());
        gameTestInfo.setHasPay(gameTest.getHasPay());
        gameTestInfo.setAbroad(gameTest.getAbroad());
        gameTestInfo.setCtype(gameTest.getCtype());
        gameTestInfo.setFxcompany(gameTest.getFxcompany());
        gameTestInfo.setFxcompanyurl(gameTest.getFxcompanyurl());
        gameTestInfo.setGameType(gameTest.getGameType());
        gameTestInfo.setGameTypeUrl(gameTest.getGameTypeUrl());
        gameTestInfo.setGstatus(gameTest.getGstatus());
        
        return gameTestInfo;
    }

}
