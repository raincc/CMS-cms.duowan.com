/**
 * 
 */
package com.duowan.cms.domain.tag.rpst;

import java.util.List;

import com.duowan.cms.domain.tag.Tag;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.dto.tag.TagInfo;
import com.duowan.cms.dto.tag.TagSearchCriteria;

/**
 * Tag 仓库
 * @author coolcooldee yzq
 *
 */
public interface TagRepository extends DomainObjectRepository<Tag, TagInfo, TagSearchCriteria>  {
    /**
     * 在某个频道下，根据tag名字获取Tag
     * @param channelId
     * @param name
     * @return
     */
    public Tag getByChannelIdAndName(String channelId, String name);
    
    /**
     * 得到祖宗树
     * @param channelId
     * @param name
     * @return
     */
    public String getTreeByChannelIdAndName(String channelId, String name);
    
    /**
     * 获取某频道某tag的后代，不包含自身
     * @param channelId
     * @param parent
     * @return
     */
    @Deprecated
    public List<TagInfo> getDescendant(String channelId, String parent);
    
    /**
     * 获取某频道某tag的后代，不包含自身
     * @param channelId
     * @param parent
     * @return
     */
    public List<TagInfo> getDescendantIncludeSelf(String channelId, String parent);
    
    
    /**
     * 获取某tag的子tag列表
     * @param channelId
     * @param parent
     * @return
     */
    public List<TagInfo> getChildren(String channelId, String parent);
    
    /**
     * 获取某tag的子tag(排除特殊tag)列表
     * @param channelId
     * @param parent
     * @return
     */
    public List<TagInfo> getChildrenExcludeSpecial(String channelId, String parent);
    
    /**
     * 获取某频道下所有tag(排除特殊tag)列表
     * @param channelId
     * @param parent
     * @return
     */
    public List<TagInfo> getTagsExcludeSpecial(String channelId);
    

    /**
     * 获取某频道下所有特殊tag列表
     * @param channelId
     * @return
     */
    public List<TagInfo> getSpecialTags(String channelId);
    
    /**
     * 获取某频道下整体tag树，包括所有子孙tag（需要按照“tree”属性排序，方便页面展示）
     * @return
     */
    public List<TagInfo> getAllTagsInChannel(String channelId);
    
    /**
     * 用于标签粘贴:将某频道下的originalTag标签移动到parent下
     * @param channelId 频道id
     * @param srcTag 源标签，将要用于粘贴标签
     * @param destTag 目标标签，源标签将粘贴于目标标签下，成为目标标签的子标签
     */
    public void moveTag(Tag srcTag, Tag destTag);
    
    /**
     * 修改标签名
     */
    public void updateTagName(String channelId, String originalTagName , String newTagName);
    /**
     * 更新标签关联的模版名称
     * @param channelId
     * @param tagName
     * @param templateName          
     */
    public void updateTemplateName(String channelId, String tagName, String templateName) ;
    
}
