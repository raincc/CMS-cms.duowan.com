package com.duowan.cms.domain.article;

import java.util.Date;

import org.apache.log4j.Logger;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.util.IdManager;
import com.duowan.cms.common.util.PathUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.dto.article.Article4TagListInfo;

/**
 * 用于列表展示的文章信息（主要用于tag的列表）
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-26
 * <br>==========================
 */
public class Article4TagList  implements DomainObject {
    
    private static final long serialVersionUID = -3790228418321280514L;
    
    public static Logger logger = Logger.getLogger(Article4TagList.class);
    
    public static final int EMPTYLINK_PRE = 1;
    public static final int EMPTYLINK_NORMAL = 0;
    
    /**
     * 唯一标识符
     */
    private Long id;
    /**
     * 频道id
     */
    private Channel channel;
    /**
     * 对应一个tag(只对应一个tag)
     */
    private String tag;
    /**
     * 标题
     */
    private String title;
    /**
     * 子标题
     */
    private String subtitle;
    /**
     * 摘要
     */
    private String digest;
    /**
     * 空链接地址
     */
    private String emptyLink;
    /**
     * 头图图片地址
     */
    private String pictureUrl;
    /**
     * 来源
     */
    private String source;
    /**
     * 作者
     */
    private String author;
    /**
     * 发布者ID
     */
    private String userId;
    /**
     * 天数
     */
    private Integer dayNum;
    /**
     * 文章权重，一般用于排序
     */
    private Integer power;
    /**
     * 发布时间（精确到秒）
     */
    private Date publishTime;
    
    /**
     * 更新时间
     */
    private Date updateTime;
    
    /**
     * 是否有头图
     */
    private String isHeadPic;
    
    /**
     * 子标签
     */
    private String sonTag;
    
    /**
     * 子标签id
     */
    private String sonTagId;
    
    /**
     * 自定义内容1
     */
    private String diy1;
    /**
     * 自定义内容2
     */
    private String diy2;
    /**
     * 自定义内容3
     */
    private String diy3;
    /**
     * 自定义内容4
     */
    private String diy4;
    /**
     * 自定义内容5
     */
    private String diy5;
    
    /**
     * 是否已经删除
     */
    private boolean delete;
    
    /**
     * 标题颜色
     */
    private String titleColor;
    
    /**
     * 状态：0 普通,1 预发布
     */
    private int status;

    /**
     * 发布器服务器上的url.
     * 使用到的地方:点击滚动文章列表，具体文章的标题旁的"空心五角星"访问的url
     * 举例:urlOnCms的格式类似http://cms.duowan.com/wow/1209/211821276072.html这样的链接
     */
    public String getUrlOnCms() {
        return  PathUtil.getArticleOnCmsUrl(this.getChannel().getId(), id);
    }

    public String getTitleColor() {
		return titleColor;
	}

	public void setTitleColor(String titleColor) {
		this.titleColor = titleColor;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getEmptyLink() {
        return emptyLink;
    }

    public void setEmptyLink(String emptyLink) {
        this.emptyLink = emptyLink;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getDayNum() {
        return dayNum;
    }

    public void setDayNum(Integer dayNum) {
        this.dayNum = dayNum;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsHeadPic() {
        return isHeadPic;
    }

    public void setIsHeadPic(String isHeadPic) {
        this.isHeadPic = isHeadPic;
    }
    
    public String getSonTag() {
        return sonTag;
    }

    public void setSonTag(String sonTag) {
        this.sonTag = sonTag;
    }

    public String getSonTagId() {
        return sonTagId;
    }

    public void setSonTagId(String sonTagId) {
        this.sonTagId = sonTagId;
    }

    public String getDiy1() {
        return diy1;
    }

    public void setDiy1(String diy1) {
        this.diy1 = diy1;
    }

    public String getDiy2() {
        return diy2;
    }

    public void setDiy2(String diy2) {
        this.diy2 = diy2;
    }

    public String getDiy3() {
        return diy3;
    }

    public void setDiy3(String diy3) {
        this.diy3 = diy3;
    }

    public String getDiy4() {
        return diy4;
    }

    public void setDiy4(String diy4) {
        this.diy4 = diy4;
    }

    public String getDiy5() {
        return diy5;
    }
    
    public void setDiy5(String diy5) {
        this.diy5 = diy5;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
    
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Article4TagList(){}
    
    public Article4TagList(Article4TagListInfo a){
        
        this.id = a.getId();
        this.dayNum = IdManager.id2DayNum(this.id);
        this.publishTime = new Date();
        if (!StringUtil.isEmpty(a.getUserId())) {
        	this.userId = a.getUserId();
		}
        this.update(a);
    }
    
    //更新某tag列表下的文章
    public void update(Article4TagListInfo a){
        this.channel = new Channel(a.getChannelInfo());
        this.tag = a.getTag();
        this.title = a.getTitle();
        this.subtitle = a.getSubtitle();
        this.digest = a.getDigest();
        if(!StringUtil.isEmpty( a.getEmptyLink() )){
        	this.emptyLink = a.getEmptyLink();
        }
        //被调用：Article4TagListServiceImpl:updateArticle4TagListInfo更新发表时间
        if(null != a.getPublishTime()){
            this.publishTime = a.getPublishTime();
        }
        
        this.pictureUrl = a.getPictureUrl();
        this.source = a.getSource();
        this.author = a.getAuthor();
        this.power = a.getPower();
        this.updateTime = new Date();
        this.isHeadPic = StringUtil.isEmpty(a.getPictureUrl()) ? "no" : "yes";
        
        this.diy1 = a.getDiy1();
        this.diy2 = a.getDiy2();
        this.diy3 = a.getDiy3();
        this.diy4 = a.getDiy4();
        this.diy5 = a.getDiy5();
        this.delete = a.isDelete();
        this.titleColor = a.getTitleColor();
        this.status = a.getStatus();
    }
    
    public void resetDayNum() {
        // 该方法只是改变文章实例的状态，外部调用之后需要调用仓库层将之持久化
        this.dayNum = IdManager.id2DayNum(this.id);
    }
    
    public String getUrlOnLine(){
        return PathUtil.getArticleOnlineUrl(this.getChannel().getDomain(), id);
    }
    

}

/*
-- Table "list_1000y" DDL

CREATE TABLE `list_1000y` (
  `tag` varchar(20) NOT NULL,
  `articleid` bigint(13) unsigned NOT NULL,
  `title` varchar(46) NOT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `digest` varchar(250) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `picurl` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `author` varchar(30) DEFAULT NULL,
  `userid` char(18) DEFAULT NULL,
  `daynum` int(11) DEFAULT NULL,
  `power` tinyint(4) DEFAULT NULL,
  `posttime` datetime DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `ispic` char(3) DEFAULT 'no',
  `sontag` varchar(20) DEFAULT NULL,
  `sontagid` bigint(13) unsigned DEFAULT NULL,
  `diy1` varchar(2000) DEFAULT NULL,
  `diy2` varchar(2000) DEFAULT NULL,
  `diy3` varchar(2000) DEFAULT NULL,
  `isdelete` tinyint(4) DEFAULT '0',
  `diy4` varchar(2000) DEFAULT NULL,
  `diy5` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`tag`,`articleid`),
  KEY `tag` (`tag`,`daynum`,`power`),
  KEY `updatetime` (`updatetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
暂时sontag和sontagid未用到
*/
