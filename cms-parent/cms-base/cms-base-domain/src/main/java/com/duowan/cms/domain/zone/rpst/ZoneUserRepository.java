/**
 * 
 */
package com.duowan.cms.domain.zone.rpst;

import java.util.List;

import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.zone.ZoneUser;
import com.duowan.cms.dto.zone.ZoneUserInfo;
import com.duowan.cms.dto.zone.ZoneUserSearchCriteria;

/**
 * @author yzq
 *
 */
public interface ZoneUserRepository extends DomainObjectRepository<ZoneUser,  ZoneUserInfo,  ZoneUserSearchCriteria> {
    
    public ZoneUser getById(Long id);
    
    public ZoneUser getByChannelIdAndUserName(String channelId, String userName);
    
    public List<ZoneUserInfo> getByChannelId(String channelId);
}
