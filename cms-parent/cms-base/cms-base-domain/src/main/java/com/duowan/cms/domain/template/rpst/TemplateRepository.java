package com.duowan.cms.domain.template.rpst;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.template.Template;
import com.duowan.cms.dto.template.SimpleTemplateInfo;
import com.duowan.cms.dto.template.TemplateCategory;
import com.duowan.cms.dto.template.TemplateInfo;
import com.duowan.cms.dto.template.TemplateSearchCriteria;

public interface TemplateRepository extends DomainObjectRepository<Template, TemplateInfo, TemplateSearchCriteria> {
    /**
     * 在专区下，根据模板ID获取模板
     * @param channelId
     * @param templateId
     * @return
     */
    public Template getByChannelIdAndTemplateId(String channelId, Long templateId);
    
    /**
     * 在专区下，根据模板名字获取模板
     * @param channelId
     * @param templateName
     * @return
     */
    public Template getByChannelIdAndTemplateName(String channelId, String templateName);
    
    /**
     * 把旧频道的所有模板批量插入其他频道
     * @param oldChannelId
     * @param newTemplateInfo 提供channelId , userIp , proxyIp , createUserId ,lastUpdateUserId等信息
     */
    public void insertAll( String oldChannelId , Template newTemplate );
    
    /**
     *  根据查询条件获取模板列表(慎用，结果集过大的时候，为了不影响性能，请使用pageSearch分页查询)
     * @param searchCriteria
     * @return
     */
    //public List<TemplateInfo> listSearch(TemplateSearchCriteria searchCriteria);
    
    /**
     * 根据channelid和类别获取模板，用于刷tag
     * @param channelId
     * @param category
     * @return
     */
    public List<TemplateInfo> getByChannelIdAndCategoryOrderByCooperater(String channelId, TemplateCategory category);

    /**
     * 分页查询，用于显示
     * @param searchCriteria
     * @return
     */
    public Page<SimpleTemplateInfo> pageSearch4Show(TemplateSearchCriteria searchCriteria);

}
