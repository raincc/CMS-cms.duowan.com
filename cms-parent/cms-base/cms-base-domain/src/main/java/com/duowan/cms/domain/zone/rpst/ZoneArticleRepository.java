/**
 * 
 */
package com.duowan.cms.domain.zone.rpst;

import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.zone.ZoneArticle;
import com.duowan.cms.dto.zone.ZoneArticleInfo;
import com.duowan.cms.dto.zone.ZoneArticleSearchCriteria;

/**
 * @author yzq
 *
 */
public interface ZoneArticleRepository extends DomainObjectRepository<ZoneArticle,  ZoneArticleInfo,  ZoneArticleSearchCriteria> {
    
    public ZoneArticle getByChannelIdAndArticleId(String channelId, Long articleId);
}
