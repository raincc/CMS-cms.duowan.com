package com.duowan.cms.domain.article;

import java.util.Date;

import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.common.domain.DomainObject;

/*
 * 
@Description:
用途：记录文章与游戏库中的相关游戏的关系，一篇文章对应一个游戏，
通过游戏名从游戏库获取名片信息，同时把文章的信息发到游戏库
文章与库相关游戏表：频道id,文章id,相关游戏（游戏中文名)
*
 CREATE TABLE `ku_article_game` (
  `channelid` char(18) NOT NULL,
  `articleid` bigint(13) unsigned NOT NULL,
  `relategame` char(32) NOT NULL,
  `diy1` char(8) ,
  `diy2` char(8) ,
  PRIMARY KEY  (`channelid`,`articleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
* @author hc
*/
public class ArticleGameLib  implements DomainObject{

    private static final long serialVersionUID = 5151491814436126993L;

    /**
     * 频道id
     */
    private Channel channel;
    
    private Article article;
    /**
     * 相关游戏
     */
    private String relategame;
    /**
     * 自定义1
     */
    private String diy1;
    /**
     * 自定义2
     */
    private String diy2;
    /**
     * 更新时间
     */
    private Date updateTime;
    

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public String getRelategame() {
		return relategame;
	}

	public void setRelategame(String relategame) {
		this.relategame = relategame;
	}

	public String getDiy1() {
		return diy1;
	}

	public void setDiy1(String diy1) {
		this.diy1 = diy1;
	}

	public String getDiy2() {
		return diy2;
	}

	public void setDiy2(String diy2) {
		this.diy2 = diy2;
	}
    
    
    
}
