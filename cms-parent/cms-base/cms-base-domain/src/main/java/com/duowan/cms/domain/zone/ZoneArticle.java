package com.duowan.cms.domain.zone;

import java.util.Date;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.dto.zone.ZoneArticleStatus;

/**
 * 有爱文章
 * @author yzq
 * 
 * -- Table "zone_article" DDL

CREATE TABLE `zone_article` (
  `articleId` bigint(13) unsigned NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  `zoneUserName` varchar(255) NOT NULL,
  `channelId` varchar(255) NOT NULL,
  `status` char(1) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `title` varchar(255) default NULL,
  PRIMARY KEY  (`articleId`,`channelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
public class ZoneArticle implements DomainObject {

    private static final long serialVersionUID = 1753105677543037292L;
    
    
    private Long articleId;
    private String tag;
    private Date createTime;
    private Date updateTime;
    private String zoneUserName;
    private String channelId;
    private ZoneArticleStatus status;//文章同步有爱的状态：开始,check,成功,失败
    private String title;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getZoneUserName() {
        return zoneUserName;
    }

    public void setZoneUserName(String zoneUserName) {
        this.zoneUserName = zoneUserName;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public ZoneArticleStatus getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = ZoneArticleStatus.getInstance(status);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
