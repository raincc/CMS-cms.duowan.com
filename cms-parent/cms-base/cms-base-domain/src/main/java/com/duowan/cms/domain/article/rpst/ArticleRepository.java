package com.duowan.cms.domain.article.rpst;

import java.util.Date;
import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.article.Article;
import com.duowan.cms.domain.article.ArticleCount;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;
import com.duowan.cms.dto.article.SimpleArticleInfo;

/**
 * 文章仓库
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-10-5
 * <br>==========================
 */
public interface ArticleRepository extends DomainObjectRepository<Article, ArticleInfo, ArticleSearchCriteria> {
    /**
     * 根据文章ID获取某个频道下的文章实体
     * 
     * @param channelId 频道ID
     * @param articleId 文章ID
     * @return
     */
    public Article getByChannelIdAndArticleId(String channelId, Long articleId);
    
    /**
     * 该标题的文章是否在该频道下存在，返回true表示存在，反之
     * @param title
     * @return
     */
    public Article getByTitle(String channelId, String title);
    
    /**
     *  根据查询条件获取文章列表(慎用，结果集过大的时候，为了不影响性能，请使用pageSearch分页查询)
     * @param searchCriteria
     * @return
     */
    //public List<ArticleInfo> listSearch(ArticleSearchCriteria searchCriteria);
    
    public void createTableByChannelId(String channelId);
    /**
     *  更新标签名称
     */
    public void updateTagName(String channelId ,String originalTagName , String newTagName);
    
    public List<ArticleCount> getAllUserCount(String channelId, int day);
    
    public List<ArticleCount> getAllUserCount(String channelId, Date date);
    /**
     * 得到某用户今天发了多少文章
     * @param channelId
     * @param userId
     * @return
     */
    public Integer getUserCountToday(String channelId, String userId);
    
    /**
     * 分页查询，用于显示
     * @param searchCriteria
     * @return
     */
    public Page<SimpleArticleInfo> pageSearch4Show(ArticleSearchCriteria searchCriteria);
}