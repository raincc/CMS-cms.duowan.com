package com.duowan.cms.domain.user.rpst;

import java.io.Serializable;
import java.util.List;

import com.duowan.cms.domain.user.UserPower;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.dto.user.UserPowerInfo;
import com.duowan.cms.dto.user.UserPowerSearchCriteria;

public interface UserPowerRepository extends DomainObjectRepository<UserPower, UserPowerInfo, UserPowerSearchCriteria>{
    /**
     * 获取某人某频道的权限
     * @param userId
     * @param channelId
     * @return
     */
    public UserPower getByUserIdAndChannelId(String userId, String channelId);
    
    /**
     * 找出某人所有的权限
     * @param userId
     * @return
     */
    public List<UserPowerInfo> getAllPowerByUserId(String userId);
    
    /**
     * 根据ID批量删除
     * @param ids
     * @return
     */
    public int deleteByIds(List<Serializable> ids);
    
    /**
     * 根据userID删除
     * @param userId
     * @return
     */
    public void deleteByUserId(String userId);
    
    /**
     * 把当前的频道的权限信息应用到该用户的其他所有频道
     * @param userPower
     */
    public void updateUserPowerInAllChannles(UserPower userPower);
    
    /**
     * 批量保存
     * @param list
     */
    public void saveList(List<UserPower> list);
}
