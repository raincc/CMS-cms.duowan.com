package com.duowan.cms.domain.article.rpst;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.article.ArticleCount;
import com.duowan.cms.dto.article.ArticleCountInfo;
import com.duowan.cms.dto.article.ArticleCountSearchCriteria;

/**
 * 文章统计仓库
 */
public interface ArticleCountRepository extends DomainObjectRepository<ArticleCount, ArticleCountInfo, ArticleCountSearchCriteria> {
    
    /**
     * @param dateStr 格式："yyyy-MM-dd"
     * @param channelId
     * @param userId
     * @return
     */
    public ArticleCount getByPrimaryKey(String dateStr, String channelId, String userId);
    
    /**
     * 对频道进行统计
     * @param searchCriteria
     * @return
     */
    public Page<ArticleCountInfo> channelStatistics(ArticleCountSearchCriteria searchCriteria);
    
    /**
     * 对用户进行统计
     * @param searchCriteria
     * @return
     */
    public Page<ArticleCountInfo> userStatistics(ArticleCountSearchCriteria searchCriteria);
}