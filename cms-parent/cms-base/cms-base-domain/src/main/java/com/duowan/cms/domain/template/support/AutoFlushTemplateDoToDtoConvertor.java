/**
 * 
 */
package com.duowan.cms.domain.template.support;

import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.template.AutoFlushTemplate;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;

/**
 * AutoFlushTemplate的dto转换器
 * 
 * @author yzq
 * 
 */
public class AutoFlushTemplateDoToDtoConvertor extends
        AbstractDoToDtoConvertor<AutoFlushTemplate, AutoFlushTemplateInfo> {
    
    private AutoFlushTemplateDoToDtoConvertor() {
    }
    
    private static AutoFlushTemplateDoToDtoConvertor instance;
    
    static {
        instance = new AutoFlushTemplateDoToDtoConvertor();
        DoToDtoConvertorFactory.register(AutoFlushTemplate.class, instance);
    }
    
    public static AutoFlushTemplateDoToDtoConvertor getInstance() {
        return instance;
    }
    
    @Override
    public AutoFlushTemplateInfo do2Dto(AutoFlushTemplate template) {
        
        if (template == null){
        	 return null;
        }
        AutoFlushTemplateInfo templateInfo = new AutoFlushTemplateInfo();
        templateInfo.setId(template.getId());
        templateInfo.setTemplateId(template.getTemplateId());
        templateInfo.setDescription(template.getDescription());
        templateInfo.setInterval(template.getInterval());
        templateInfo.setLastFlushTime(template.getLastFlushTime());
        templateInfo.setChannelInfo((ChannelInfo)DoToDtoConvertorFactory.getConvertor(Channel.class).do2Dto(template.getChannel()));
        return templateInfo;
    }

}
