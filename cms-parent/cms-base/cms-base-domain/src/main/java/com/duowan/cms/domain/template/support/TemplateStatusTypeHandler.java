package com.duowan.cms.domain.template.support;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import com.duowan.cms.dto.template.TemplateStatus;
@MappedTypes(value={TemplateStatus.class})
public class TemplateStatusTypeHandler implements TypeHandler<TemplateStatus> {

    @Override
    public void setParameter(PreparedStatement ps, int i, TemplateStatus parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.getValue());
    }

    @Override
    public TemplateStatus getResult(ResultSet rs, String columnName) throws SQLException {
        return TemplateStatus.getInstance(rs.getString(columnName));
    }

    @Override
    public TemplateStatus getResult(ResultSet rs, int columnIndex) throws SQLException {
        return TemplateStatus.getInstance(rs.getString(columnIndex));
    }

    @Override
    public TemplateStatus getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return TemplateStatus.getInstance(cs.getString(columnIndex));
    }

    

}
