/**
 * 
 */
package com.duowan.cms.domain.gametest.rpst;

import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.domain.gametest.GameTest;
import com.duowan.cms.dto.gametest.GameTestInfo;
import com.duowan.cms.dto.gametest.GameTestSearchCriteria;

/**
 * @author yzq
 *
 */
public interface GameTestRepository extends DomainObjectRepository<GameTest, GameTestInfo,  GameTestSearchCriteria> {
   
}
