/**
 * 
 */
package com.duowan.cms.domain.user.support;

import com.duowan.cms.domain.user.User;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.user.UserInfo;

/**
 * User的dto转换器
 * 
 * @author yzq
 * 
 */
public class UserDoToDtoConvertor extends AbstractDoToDtoConvertor<User, UserInfo> {

    private UserDoToDtoConvertor() {
    }

    private static UserDoToDtoConvertor instance;

    static {
        instance = new UserDoToDtoConvertor();
        DoToDtoConvertorFactory.register(User.class, instance);
    }

    public static UserDoToDtoConvertor getInstance() {
        return instance;
    }

    public UserInfo do2Dto(User user) {

        if (user == null)
            return null;

        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getUserId());
        userInfo.setCreateTime(user.getCreateTime());
        userInfo.setLastModifyTime(user.getLastModifyTime());
        userInfo.setUserStatus(user.getUserStatus());
        userInfo.setUserIp(user.getUserIp());
        userInfo.setProxyIp(user.getProxyIp());
        userInfo.setMail(user.getMail());
        userInfo.setAdmin(user.isAdmin());
        userInfo.setUdbName(user.getUdbName());
        return userInfo;
    }
}
