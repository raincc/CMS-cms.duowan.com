/**
 * 
 */
package com.duowan.cms.domain.tag;

import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.dto.tag.TagCategory;
import com.duowan.cms.dto.tag.TagInfo;
import com.duowan.cms.dto.template.TemplateCategory;

/**
 * Tag
 * 
 * @author coolcooldee
 */
@Alias("Tag")
public class Tag implements DomainObject {

	private static final long serialVersionUID = -6299876099437847764L;

	public static final String[] DEFAULT_SPECIAL_TAGS = { "头图", "头条", "二条", "重点", "要闻", "专题", "图片" };
	/**
	 * 名字（与频道ID构成唯一标识）
	 */
	private String name;

	/**
	 * 频道ID
	 */
	private Channel channel;

	/**
	 * 用于生成tag页面URL的名字（非唯一标识）
	 */
	private Long id;

	/**
	 * 父tag
	 */
	private String parentName;

	/**
	 * 层级结构（包括自身和所有父tag，中间使用“，”隔开）
	 */
	private String tree;

	/**
	 * 创建/更新时间
	 */
	private Date updateTime;

	/**
	 * 标签类别
	 */
	private TagCategory category;

	/**
	 * 标签所关联的模板类别，只有两种：标签，标签图
	 */
	private TemplateCategory relatedTemplateCategory;

	public Tag() {
	}

	public Tag(TagInfo tagInfo) {
	    this.id = tagInfo.getId();
		this.name = tagInfo.getName();
		this.channel = new Channel(tagInfo.getChannelInfo());
		this.category = tagInfo.getCategory();
		this.parentName = tagInfo.getParentName();
		this.tree = tagInfo.getTree();
		this.updateTime = tagInfo.getUpdateTime();
		this.relatedTemplateCategory = tagInfo.getRelatedTemplateCategory();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getTree() {
		return tree;
	}

	public void setTree(String tree) {
		this.tree = tree;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public TagCategory getCategory() {
		return category;
	}

	public void setCategory(String categoryStr) {
	    if(!StringUtil.isEmpty(categoryStr)){
	        try{
	            this.category = TagCategory.getInstance(categoryStr);
	        }catch(IllegalArgumentException ex){
	            
	        }
	    }
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TemplateCategory getRelatedTemplateCategory() {
		return relatedTemplateCategory;
	}

	public void setRelatedTemplateCategory(String relatedTemplateCategory) {
		if (!StringUtil.isEmpty(relatedTemplateCategory)) {
		    try{
		        this.relatedTemplateCategory = TemplateCategory.getInstance(relatedTemplateCategory);
		    }catch(IllegalArgumentException ex){
		        this.relatedTemplateCategory = null;
		    }
		}
	}

}
