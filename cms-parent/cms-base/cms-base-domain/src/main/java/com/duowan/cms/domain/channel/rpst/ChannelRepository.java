/**
 * 
 */
package com.duowan.cms.domain.channel.rpst;

import java.util.List;

import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.common.repository.DomainObjectRepository;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.channel.ChannelSearchCriteria;

/**
 * @author coolcooldee
 *
 */
public interface ChannelRepository extends DomainObjectRepository< Channel,  ChannelInfo,  ChannelSearchCriteria> {
    /**
     * 根据ID获取频道信息
     * @param id
     * @return
     */
    public Channel getById(String id);
    
    /**
     * 根据name获取频道信息
     * @param name
     * @return
     */
    public Channel getByName(String name);
    
    /**
     * 获取所有的频道
     * @return
     */
    public List<ChannelInfo> getAll();
}
