/**
 * 
 */
package com.duowan.cms.domain.zone.support;

import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.zone.ZoneUser;
import com.duowan.cms.dto.zone.ZoneUserInfo;

/**
 * ZoneUser的dto转换器
 * 
 * @author yzq
 * 
 */
public class ZoneUserDoToDtoConvertor extends AbstractDoToDtoConvertor<ZoneUser, ZoneUserInfo> {

    private ZoneUserDoToDtoConvertor() {
    }

    private static ZoneUserDoToDtoConvertor instance;

    static {
        instance = new ZoneUserDoToDtoConvertor();
        DoToDtoConvertorFactory.register(ZoneUser.class, instance);
    }

    public static ZoneUserDoToDtoConvertor getInstance() {
        return instance;
    }

    public ZoneUserInfo do2Dto(ZoneUser zoneUser) {

        if (zoneUser == null)
            return null;

        ZoneUserInfo zoneUserInfo = new ZoneUserInfo();
        zoneUserInfo.setChannelId(zoneUser.getChannelId());
        zoneUserInfo.setDefaultTag(zoneUser.getDefaultTag());
        zoneUserInfo.setId(zoneUser.getId());
        zoneUserInfo.setIsDel(zoneUser.getIsDel());
        zoneUserInfo.setNickName(zoneUser.getNickName());
        zoneUserInfo.setUserName(zoneUser.getUserName());

        return zoneUserInfo;
    }
}
