/**
 * 
 */
package com.duowan.cms.domain.user.support;

import com.duowan.cms.domain.user.Power;
import com.duowan.cms.domain.user.User;
import com.duowan.cms.domain.user.UserPower;
import com.duowan.cms.common.service.AbstractDoToDtoConvertor;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserPowerInfo;

/**
 * User的dto转换器
 * 
 * @author yzq
 * 
 */
public class UserPowerDoToDtoConvertor extends AbstractDoToDtoConvertor<UserPower, UserPowerInfo> {

    private UserPowerDoToDtoConvertor() {
    }

    private static UserPowerDoToDtoConvertor instance;

    static {
        instance = new UserPowerDoToDtoConvertor();
        DoToDtoConvertorFactory.register(UserPower.class, instance);
    }

    public static UserPowerDoToDtoConvertor getInstance() {
        return instance;
    }

    public UserInfo doToDto(User user) {

        if (user == null)
            return null;

        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getUserId());
        userInfo.setCreateTime(user.getCreateTime());
        userInfo.setUserStatus(user.getUserStatus());
        userInfo.setUserIp(user.getUserIp());
        userInfo.setProxyIp(user.getProxyIp());
        userInfo.setMail(user.getMail());

        return userInfo;
    }

    @SuppressWarnings("unchecked")
    @Override
    public UserPowerInfo do2Dto(UserPower userPower) {
        if (userPower == null)
            return null;
        UserPowerInfo userPowerInfo = new UserPowerInfo();
        userPowerInfo.setUserId(userPower.getUserId());
        if (userPower.getChannel() != null) {
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setId(userPower.getChannel().getId());
            channelInfo.setName(userPower.getChannel().getName());
            userPowerInfo.setChannelInfo(channelInfo);
        }
        userPowerInfo.setCreateTime(userPower.getCreateTime());
        userPowerInfo.setLastModifyTime(userPower.getLastModifyTime());
        userPowerInfo.setAdminId(userPower.getAdminId());
        userPowerInfo.setUserRole(userPower.getUserRole());
        if (userPower.getOwnPower() != null) {
            userPowerInfo.setOwnPowers(DoToDtoConvertorFactory.getConvertor(Power.class).dos2Dtos(userPower.getOwnPower()));
        }
        if (userPower.getAdded() != null) {
            userPowerInfo.setAdded(DoToDtoConvertorFactory.getConvertor(Power.class).dos2Dtos(userPower.getAdded()));
        }
        if (userPower.getRemoved() != null) {
            userPowerInfo.setRemoved(DoToDtoConvertorFactory.getConvertor(Power.class).dos2Dtos(userPower.getRemoved()));
        }
        return userPowerInfo;

    }
}
