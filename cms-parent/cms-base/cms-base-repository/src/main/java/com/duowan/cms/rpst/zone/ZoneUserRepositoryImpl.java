package com.duowan.cms.rpst.zone;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.zone.ZoneUser;
import com.duowan.cms.domain.zone.rpst.ZoneUserRepository;
import com.duowan.cms.dto.zone.ZoneUserInfo;
import com.duowan.cms.dto.zone.ZoneUserSearchCriteria;

public class ZoneUserRepositoryImpl extends AbstractDomainObjectRepository<ZoneUser, ZoneUserInfo, ZoneUserSearchCriteria> implements ZoneUserRepository {

    protected Class<ZoneUser> getDomainObjectClass() {
        return ZoneUser.class;
    }

    @Override
    public ZoneUser getById(Long id) {
        return selectOne(getPrefix() + ".getById", id);
    }

    @Override
    public ZoneUser getByChannelIdAndUserName(String channelId, String userName) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        map.put("userName", userName);
        return selectOne(getPrefix() + ".getByChannelIdAndUserName", map);
    }

    @Override
    public List<ZoneUserInfo> getByChannelId(String channelId) {
        return selectList(getPrefix() + ".getByChannelId", channelId);
    }
}
