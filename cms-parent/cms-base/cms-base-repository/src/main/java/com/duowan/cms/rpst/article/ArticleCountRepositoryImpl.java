package com.duowan.cms.rpst.article;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.common.repository.SQLParameterPair;
import com.duowan.cms.common.repository.SQLParameterPair.SQLOperateType;
import com.duowan.cms.domain.article.ArticleCount;
import com.duowan.cms.domain.article.rpst.ArticleCountRepository;
import com.duowan.cms.dto.article.ArticleCountInfo;
import com.duowan.cms.dto.article.ArticleCountSearchCriteria;

/**
 * @author yzq
 * @version 1.0
 * @created 04-九月-2012 10:55:04
 */
public class ArticleCountRepositoryImpl extends AbstractDomainObjectRepository<ArticleCount, ArticleCountInfo, ArticleCountSearchCriteria> implements ArticleCountRepository {

    protected Class<ArticleCount> getDomainObjectClass() {
        return ArticleCount.class;
    }
    
    @Override
    public ArticleCount getByPrimaryKey(String dateStr, String channelId, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("dateStr", dateStr);
        map.put("channelId", channelId);
        map.put("userId", userId);
        return selectOne(getPrefix() + ".getByPrimaryKey", map);
    }

    @Override
    public Page<ArticleCountInfo> channelStatistics(ArticleCountSearchCriteria searchCriteria) {
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("search", searchCriteria);
        Integer offset = 0;
        if(null != searchCriteria.getPageSize()){
            offset = (searchCriteria.getPageNo() - 1) * searchCriteria.getPageSize();
            map.put("offset", offset);
        }
        List<ArticleCountInfo> list = selectList(getPrefix() + ".channelStatistics", map);
        Integer totalCount = 0;
        totalCount = (Integer)queryObject(new SQLParameterPair(getPrefix() + ".channelStatistics" + "-count", map, SQLOperateType.SELECT_ONE));
        
        return new Page<ArticleCountInfo>(offset, searchCriteria.getPageSize(), totalCount, list);
    }
    
    @Override
    public Page<ArticleCountInfo> userStatistics(ArticleCountSearchCriteria searchCriteria) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("search", searchCriteria);
        Integer offset = 0;
        if(null != searchCriteria.getPageSize()){
            offset = (searchCriteria.getPageNo() - 1) * searchCriteria.getPageSize();
            map.put("offset", offset);
        }
        List<ArticleCountInfo> list = selectList(getPrefix() + ".userStatistics", map);
        Integer totalCount = 0;
        totalCount = (Integer)queryObject(new SQLParameterPair(getPrefix() + ".userStatistics" + "-count", map, SQLOperateType.SELECT_ONE));
        
        return new Page<ArticleCountInfo>(offset, searchCriteria.getPageSize(), totalCount, list);
    }
    
    
}
