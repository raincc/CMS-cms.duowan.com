package com.duowan.cms.rpst.article;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.common.repository.SQLParameterPair;
import com.duowan.cms.common.repository.SQLParameterPair.SQLOperateType;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.domain.article.Article4TagList;
import com.duowan.cms.domain.article.rpst.Article4TagListRepository;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.Article4TagListSearchCriteria;

/**
 * @author yzq
 * @version 1.0
 * @created 2012-12-26 
 */
public class Article4TagListRepositoryImpl extends AbstractDomainObjectRepository<Article4TagList, Article4TagListInfo, Article4TagListSearchCriteria> implements Article4TagListRepository {

    protected Class<Article4TagList> getDomainObjectClass() {
        return Article4TagList.class;
    }
    
    @Override
	public int getCountByTitle(String channelId, String title) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("title", title);
        return selectCount(getPrefix() + ".getCountByTitle", map);
	}

	/**
     * 更新标签名称
     */
    @Override
    public void updateTagName(String channelId ,String originalTagName , String newTagName){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("originalTagName", originalTagName);
        map.put("newTagName", newTagName);
        updateOne(new SQLParameterPair(getPrefix() + ".updateTagName", map, SQLOperateType.UPDATE));
    }
    
    /**
     * 根据频道id和文章id批量更新文章列表数据
     */    
    @Override
	public void updateByChannelIdAndArticleId(Article4TagList article4TagList) {
    	 updateOne(new SQLParameterPair(getPrefix() + ".updateByChannelIdAndArticleId",  article4TagList, SQLOperateType.UPDATE));
	}
    



	@Override
	public List<Article4TagListInfo> getByChannelIdAndArticleId(String channelId, Long articleId) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("articleId", articleId);
        return selectList(getPrefix() + ".getByChannelIdAndArticleId", map);
	}

	@Override
    public Article4TagList getByPrimaryKey(String channelId, String tag, Long articleId) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("tag", tag);
        map.put("articleId", articleId);
        return selectOne(getPrefix() + ".getByPrimaryKey", map);
    }

    @Override
    public Integer getArticleCountByChannelIdAndTag(String channelId, String tag) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        map.put("tag", tag);

        return selectCount(getPrefix() + ".getArticleCountByChannelIdAndTag", map);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getRecentEffectTagsInArticle(String channelId, int minute) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("updatetime", DateUtil.getBeforeTime(minute));
        return (List<String>) queryObject(new SQLParameterPair(getPrefix() + ".getRecentEffectTagsInArticle", map, SQLOperateType.SELECT_LIST));
    }
    
    @Override
    public void logicDeleteByChannelIdAndArticleId(String channelId, Long articleId) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("articleId", articleId);
        updateOne(new SQLParameterPair(getPrefix() + ".logicDeleteByChannelIdAndArticleId", map, SQLOperateType.UPDATE));
    }
    
    @Override
    public void deleteByChannelIdAndArticleId(String channelId, Long articleId) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("articleId", articleId);
        updateOne(new SQLParameterPair(getPrefix() + ".deleteByChannelIdAndArticleId", map, SQLOperateType.DELETE));
    }
    
    @Override
    public void saveList(String channelId, List<Article4TagList> list) {
        if(StringUtil.isEmpty(channelId) || null == list || list.isEmpty())
            return ;
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("list", list);
        updateOne(new SQLParameterPair(getPrefix() + ".saveList", map, SQLOperateType.INSERT));
    }
    
    @Override
    public void createTableByChannelId(String channelId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        updateOne(new SQLParameterPair(getPrefix() + ".createTableByChannelId",  map, SQLOperateType.UPDATE));
    }
    
    @Override
    public void updatePublishTime(String channelId, Long articleId, Date newPublishTime, int newDayNum) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("articleId", articleId);
        map.put("newPublishTime", newPublishTime);
        map.put("newDayNum", newDayNum);
        
        updateOne(new SQLParameterPair(getPrefix() + ".updatePublishTime", map, SQLOperateType.UPDATE));
    }

}
