package com.duowan.cms.rpst.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.common.repository.SQLParameterPair;
import com.duowan.cms.common.repository.SQLParameterPair.SQLOperateType;
import com.duowan.cms.domain.user.User;
import com.duowan.cms.domain.user.rpst.UserRepository;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserSearchCriteria;

public class UserRepositoryImpl extends AbstractDomainObjectRepository<User, UserInfo, UserSearchCriteria> implements UserRepository {
    
    protected Class<User> getDomainObjectClass() {
        return User.class;
    }
    
    @Override
    public User getById(String userId) {
        return selectOne(getPrefix() + ".getById", userId);
    }
    
    @Override
    public User getByMail(String mail) {
        return selectOne(getPrefix() + ".getByMail", mail);
    }

    @Override
    public User getByUserIdAndPassword(String userId, String password) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("userId", userId);
        map.put("password", password);
        return selectOne(getPrefix() + ".getByUserIdAndPassword", map);
    }
    
    @Override
    public User getByUdbName(String udbName) {
        return selectOne(getPrefix() + ".getByUdbName", udbName);
    }

    @Override
    public void updateName(String oldName, String newName) {
        
        List<SQLParameterPair> list = new ArrayList<SQLParameterPair>();
        
        Map<String, String> map = new HashMap<String, String>();
        map.put("oldName", oldName);
        map.put("newName", newName);
        //更新user
        list.add(new SQLParameterPair(getPrefix() + ".updateName-1", map, SQLOperateType.UPDATE));
        //更新userpower
        list.add(new SQLParameterPair(getPrefix() + ".updateName-2", map, SQLOperateType.UPDATE));
        //更新ArticleCount(文章统计)
        list.add(new SQLParameterPair(getPrefix() + ".updateName-3", map, SQLOperateType.UPDATE));
        updateList(list);
    }
    
}
