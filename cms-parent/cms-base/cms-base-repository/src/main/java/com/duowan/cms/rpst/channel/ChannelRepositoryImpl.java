package com.duowan.cms.rpst.channel;

import java.util.List;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.channel.rpst.ChannelRepository;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.channel.ChannelSearchCriteria;

public class ChannelRepositoryImpl extends AbstractDomainObjectRepository<Channel, ChannelInfo, ChannelSearchCriteria> implements ChannelRepository {

    @Override
    protected Class<Channel> getDomainObjectClass() {
        return Channel.class;
    }

    @Override
    public Channel getById(String id) {

        return selectOne(getDomainObjectClass().getName() + ".getById", id);
    }

    @Override
    public List<ChannelInfo> getAll() {

        return selectList(getDomainObjectClass().getName() + ".getAll", null);
    }

    @Override
    public Channel getByName(String name) {
        return selectOne(getDomainObjectClass().getName() + ".getByName", name);
    }

}
