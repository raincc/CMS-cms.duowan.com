package com.duowan.cms.rpst.template;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.template.AutoFlushTemplate;
import com.duowan.cms.domain.template.rpst.AutoFlushTemplateRepository;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;
import com.duowan.cms.dto.template.AutoFlushTemplateSearchCriteria;

public class AutoFlushTemplateRepositoryImpl  extends AbstractDomainObjectRepository<AutoFlushTemplate, AutoFlushTemplateInfo, AutoFlushTemplateSearchCriteria> implements AutoFlushTemplateRepository {

	
	@Override
	protected Class<AutoFlushTemplate> getDomainObjectClass() {
		// TODO Auto-generated method stub
		return AutoFlushTemplate.class;
	}

	@Override
	public AutoFlushTemplate getById(String id) {
	       Map<String, Object> map = new HashMap<String, Object>();
	        map.put("id", id);

	        return selectOne(getPrefix() + ".getById", map);
	}

	@Override
	public AutoFlushTemplate getByChannelIdAndTemplateId(String channelId, Long templateId) {
	       Map<String, Object> map = new HashMap<String, Object>();
	        map.put("channelId", channelId);
	        map.put("templateId", templateId);
	        return selectOne(getPrefix() + ".getByChannelIdAndTemplateId", map);
	}
	
	

}
