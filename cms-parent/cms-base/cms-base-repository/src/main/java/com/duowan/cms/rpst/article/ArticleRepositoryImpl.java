package com.duowan.cms.rpst.article;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.common.repository.SQLParameterPair;
import com.duowan.cms.common.repository.SQLParameterPair.SQLOperateType;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.domain.article.Article;
import com.duowan.cms.domain.article.ArticleCount;
import com.duowan.cms.domain.article.rpst.ArticleRepository;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;
import com.duowan.cms.dto.article.SimpleArticleInfo;

/**
 * @author yzq
 * @version 1.0
 * @created 04-九月-2012 10:55:04
 */
public class ArticleRepositoryImpl extends AbstractDomainObjectRepository<Article, ArticleInfo, ArticleSearchCriteria> implements ArticleRepository {

    protected Class<Article> getDomainObjectClass() {
        return Article.class;
    }

    @Override
    public void updateTagName(String channelId, String originalTagName, String newTagName) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("originalTagName", originalTagName + ",");
        map.put("newTagName", newTagName + ",");
        updateOne(new SQLParameterPair(getPrefix() + ".updateTagName", map, SQLOperateType.UPDATE));
    }

    @Override
    public void delete(Article object) {
        List<SQLParameterPair> list = new ArrayList<SQLParameterPair>();
        list.add(new SQLParameterPair(getPrefix() + ".delete", object, SQLOperateType.DELETE));
        //list.add(new SQLParameterPair(getPrefix() + ".delete-list", object, SQLOperateType.DELETE));
        updateList(list);
    }

    @Override
    public Article getByChannelIdAndArticleId(String channelId, Long articleId) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("articleId", articleId);

        return selectOne(getPrefix() + ".getByChannelIdAndArticleId", map);
    }

    @Override
    public Article getByTitle(String channelId, String title) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("title", title);

        return selectOne(getPrefix() + ".getByTitle", map);
    }

    @Override
    public void createTableByChannelId(String channelId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        updateOne(new SQLParameterPair(getPrefix() + ".createTableByChannelId", map, SQLOperateType.UPDATE));
    }

    @Override
    public List<ArticleCount> getAllUserCount(String channelId, int day) {

        return getAllUserCount(channelId, DateUtil.getDate(day));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ArticleCount> getAllUserCount(String channelId, Date date) {

        String dateStr = DateUtil.format(date, "yyyy-MM-dd");

        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        map.put("start", dateStr);
        map.put("end", dateStr + " 23:59:59");

        List<ArticleCount> list = (List<ArticleCount>) queryObject(new SQLParameterPair(getPrefix() + ".getAllUserCount", map, SQLOperateType.SELECT_LIST));

        for (ArticleCount a : list) {
            a.setChannelId(channelId);
            a.setDate(date);
        }

        return list;
    }

    @Override
    public Integer getUserCountToday(String channelId, String userId) {
        String dateStr = DateUtil.format(new Date(), "yyyy-MM-dd");

        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        map.put("start", dateStr);
        map.put("end", dateStr + " 23:59:59");
        map.put("userId", userId);

        Integer count = (Integer) queryObject(new SQLParameterPair(getPrefix() + ".getUserCountToday", map, SQLOperateType.SELECT_ONE));

        return count;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Page<SimpleArticleInfo> pageSearch4Show(ArticleSearchCriteria searchCriteria) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("search", searchCriteria);
        Integer offset = 0;
        if (null != searchCriteria.getPageSize()) {
            offset = (searchCriteria.getPageNo() - 1) * searchCriteria.getPageSize();
            map.put("offset", offset);
        }

        List<SimpleArticleInfo> list = (List<SimpleArticleInfo>) queryObject(new SQLParameterPair(getPrefix() + ".pagedSearch4Show", map, SQLOperateType.SELECT_LIST));

        Integer totalCount = (Integer) queryObject(new SQLParameterPair(getPrefix() + ".pagedSearch-count", map, SQLOperateType.SELECT_ONE));

        return new Page<SimpleArticleInfo>(offset, searchCriteria.getPageSize(), totalCount, list);
    }

}
