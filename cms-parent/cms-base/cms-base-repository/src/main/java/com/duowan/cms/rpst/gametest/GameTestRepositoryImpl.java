package com.duowan.cms.rpst.gametest;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.gametest.GameTest;
import com.duowan.cms.domain.gametest.rpst.GameTestRepository;
import com.duowan.cms.dto.gametest.GameTestInfo;
import com.duowan.cms.dto.gametest.GameTestSearchCriteria;

public class GameTestRepositoryImpl extends AbstractDomainObjectRepository<GameTest, GameTestInfo, GameTestSearchCriteria> implements GameTestRepository {

    @Override
    protected Class<GameTest> getDomainObjectClass() {
        return GameTest.class;
    }


}
