package com.duowan.cms.rpst.zone;

import java.util.HashMap;
import java.util.Map;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.zone.ZoneArticle;
import com.duowan.cms.domain.zone.rpst.ZoneArticleRepository;
import com.duowan.cms.dto.zone.ZoneArticleInfo;
import com.duowan.cms.dto.zone.ZoneArticleSearchCriteria;

public class ZoneArticleRepositoryImpl extends AbstractDomainObjectRepository<ZoneArticle, ZoneArticleInfo, ZoneArticleSearchCriteria> implements ZoneArticleRepository {

    protected Class<ZoneArticle> getDomainObjectClass() {
        return ZoneArticle.class;
    }

    @Override
    public ZoneArticle getByChannelIdAndArticleId(String channelId, Long articleId) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("articleId", articleId);
        return selectOne(getPrefix() + ".getByChannelIdAndArticleId", map);
    }

}
