package com.duowan.cms.rpst.template;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.common.repository.SQLParameterPair;
import com.duowan.cms.common.repository.SQLParameterPair.SQLOperateType;
import com.duowan.cms.domain.template.Template;
import com.duowan.cms.domain.template.rpst.TemplateRepository;
import com.duowan.cms.dto.template.SimpleTemplateInfo;
import com.duowan.cms.dto.template.TemplateCategory;
import com.duowan.cms.dto.template.TemplateInfo;
import com.duowan.cms.dto.template.TemplateSearchCriteria;

public class TemplateRepositoryImpl extends AbstractDomainObjectRepository<Template, TemplateInfo, TemplateSearchCriteria> implements TemplateRepository {

    @Override
    protected Class<Template> getDomainObjectClass() {
        return Template.class;
    }

    @Override
    public Template getByChannelIdAndTemplateId(String channelId, Long templateId) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("templateId", templateId);
        return selectOne(getPrefix() + ".getByChannelIdAndTemplateId", map);
    }

    @Override
    public Template getByChannelIdAndTemplateName(String channelId, String templateName) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        map.put("templateName", templateName);
        return selectOne(getPrefix() + ".getByChannelIdAndTemplateName", map);
    }

    @Override
    public void insertAll(String oldChannelId, Template newTemplate) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", oldChannelId);
        map.put("template", newTemplate);

        updateOne(new SQLParameterPair(getPrefix() + ".insertAll", map, SQLOperateType.INSERT));
    }


    @Override
    public List<TemplateInfo> getByChannelIdAndCategoryOrderByCooperater(String channelId, TemplateCategory category) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("channelId", channelId);
        map.put("category", category);
        return selectList(getPrefix() + ".getByChannelIdAndCategoryOrderByCooperater", map);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Page<SimpleTemplateInfo> pageSearch4Show(TemplateSearchCriteria searchCriteria) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("search", searchCriteria);
        Integer offset = 0;
        if(null != searchCriteria.getPageSize()){
            offset = (searchCriteria.getPageNo() - 1) * searchCriteria.getPageSize();
            map.put("offset", offset);
        }
        
        List<SimpleTemplateInfo> list = (List<SimpleTemplateInfo>)queryObject(new SQLParameterPair(getPrefix() + ".pagedSearch4Show", map, SQLOperateType.SELECT_LIST));
        
        Integer totalCount = (Integer) queryObject(new SQLParameterPair(getPrefix() + ".pagedSearch-count", map, SQLOperateType.SELECT_ONE));
        
        return new Page<SimpleTemplateInfo>(offset, searchCriteria.getPageSize(), totalCount, list);
    }

}
