/**
 * 
 */
package com.duowan.cms.rpst.template;

import java.util.List;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.template.AutoFlushTag;
import com.duowan.cms.domain.template.rpst.AutoFlushTagRepository;
import com.duowan.cms.dto.template.AutoFlushTagInfo;
import com.duowan.cms.dto.template.AutoFlushTagSearchCriteria;

/**
 * 接口或类的说明
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
public class AutoFlushTagRepositoryImpl extends AbstractDomainObjectRepository<AutoFlushTag, AutoFlushTagInfo, AutoFlushTagSearchCriteria> implements AutoFlushTagRepository {

    @Override
    public List<AutoFlushTagInfo> listUnFlushTag() {
        return selectList(getPrefix() + ".listUnFlushTag", null);
    }

    @Override
    protected Class<AutoFlushTag> getDomainObjectClass() {
        return AutoFlushTag.class;
    }

    @Override
    public AutoFlushTag getById(Long id) {
        return selectOne(getPrefix() + ".getById", id);
    }

    @Override
    public void batcUpdate(List<AutoFlushTagInfo> autoFlushTagInfos) {
       for(AutoFlushTagInfo autoFlushTagInfo : autoFlushTagInfos){
           AutoFlushTag autoFlushTag = getById(autoFlushTagInfo.getId());
           autoFlushTag.update(autoFlushTagInfo);
           update(autoFlushTag);
       }
    }


}
