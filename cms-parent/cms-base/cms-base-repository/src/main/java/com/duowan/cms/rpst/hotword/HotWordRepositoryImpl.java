package com.duowan.cms.rpst.hotword;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.hotword.HotWord;
import com.duowan.cms.domain.hotword.rpst.HotWordRepository;
import com.duowan.cms.dto.hotword.HotWordInfo;
import com.duowan.cms.dto.hotword.HotWordSearchCriteria;

/**
 * @author yzq
 * @version 1.0
 * @created 2012-11-16 15:07:00
 */
public class HotWordRepositoryImpl extends AbstractDomainObjectRepository<HotWord, HotWordInfo, HotWordSearchCriteria> implements HotWordRepository {


    protected Class<HotWord> getDomainObjectClass() {
        return HotWord.class;
    }

    @Override
    public List<HotWordInfo> getAll() {
        return selectList(getDomainObjectClass().getName() + ".getAll", null);
    }

    @Override
    public List<HotWordInfo> getByChannelId(String channelId) {
        return selectList(getDomainObjectClass().getName() + ".getByChannelId", channelId);
    }

    @Override
    public HotWord getByChannelIdAndName(String channelId, String name) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("channelId", channelId);
        map.put("name", name);
        return selectOne(getDomainObjectClass().getName() + ".getByChannelIdAndName", map);
    }

}
