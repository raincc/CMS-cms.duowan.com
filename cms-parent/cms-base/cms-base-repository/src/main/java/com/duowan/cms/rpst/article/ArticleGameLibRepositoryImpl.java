package com.duowan.cms.rpst.article;

import java.util.HashMap;
import java.util.Map;

import com.duowan.cms.common.repository.AbstractDomainObjectRepository;
import com.duowan.cms.domain.article.ArticleGameLib;
import com.duowan.cms.domain.article.rpst.ArticleGameLibRepository;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;

public class ArticleGameLibRepositoryImpl extends
			AbstractDomainObjectRepository<ArticleGameLib, ArticleInfo, ArticleSearchCriteria>
			implements ArticleGameLibRepository {

	@Override
	protected Class<ArticleGameLib> getDomainObjectClass() {
		return ArticleGameLib.class;
	}

	@Override
	public ArticleGameLib getArticleGameLib(String channelId, long articleId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("channelId", channelId);
		map.put("articleId", articleId);

		return selectOne(getPrefix() + ".getArticleGameLib", map);
	}

}
