package com.duowan.cms.rpst;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.domain.hotword.HotWord;
import com.duowan.cms.domain.hotword.rpst.HotWordRepository;
import com.duowan.cms.domain.hotword.support.HotWordDoToDtoConvertor;
import com.duowan.cms.dto.hotword.HotWordInfo;

public class HotWordRepositoryImplTest {

    BeanFactory factory;

    HotWordRepository hotWordRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        hotWordRepository = (HotWordRepository) factory.getBean("hotWordRepository");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSave() {

    }

    @Test
    public void testUpdate() {

    }

    @Test
    public void testPagedSearch() {

    }

    @Test
    public void testGetAll() {
        try {
            Class.forName(HotWordDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        long start = System.currentTimeMillis();
        List<HotWordInfo> list = hotWordRepository.getAll();
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(list.size());

        int count = 0;
        for (HotWordInfo hw : list) {
            System.out.println(hw.getChannelId() + ":" + hw.getName() + ":" + hw.getHref());
            if (++count > 10)
                break;
        }
    }

    @Test
    public void testGetByChannelId() {
        try {
            Class.forName(HotWordDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        long start = System.currentTimeMillis();
        List<HotWordInfo> list = hotWordRepository.getByChannelId("lol");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(list.size());

        for (HotWordInfo hw : list) {
            System.out.println(hw.getChannelId() + ":" + hw.getName() + ":" + hw.getHref());
        }
    }

    @Test
    public void testGetByChannelIdAndName() {
        long start = System.currentTimeMillis();
        HotWord hw = hotWordRepository.getByChannelIdAndName("lol", "LOL");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");

        System.out.println(hw.getChannelId() + ":" + hw.getName() + ":" + hw.getHref());
    }

}
