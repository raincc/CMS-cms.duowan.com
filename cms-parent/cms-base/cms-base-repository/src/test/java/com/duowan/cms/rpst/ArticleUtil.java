package com.duowan.cms.rpst;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ArticleUtil {

	public static long BASE_TIMESTAMP = 1136044800000L;// - InfoConfig.get_base_timestamp();

	public static long getArticleId() {
		
		return System.currentTimeMillis() - BASE_TIMESTAMP;
	}
	
	/**
	 * 返回年月日时分秒
	 * 
	 * @param articleid
	 * @return
	 */
	public static String getArticleTime(long articleid) {
		String pattern = "yyyy-MM-dd HH:mm:ss";
		Date date = new Date();
		date.setTime(articleid + BASE_TIMESTAMP);
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}

	/**
	 * 返回年月
	 * 
	 * @param articleid
	 * @return
	 */
	public static String getArticleYM(long articleid) {
		String pattern = "yyMM";
		Date date = new Date();
		date.setTime(articleid + BASE_TIMESTAMP);
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}

	/**
	 * 返回年月
	 * 
	 * @param articleid
	 * @return
	 */
	public static String getArticleYYYY(long articleid) {
		String pattern = "yyyy";
		Date date = new Date();
		date.setTime(articleid + BASE_TIMESTAMP);
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}

	/**
	 * 返回日
	 * 
	 * @param articleid
	 * @return
	 */
	public static String getArticleD(long articleid) {
		String pattern = "dd";
		Date date = new Date();
		date.setTime(articleid + BASE_TIMESTAMP);
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}
	
	/**
	 * 把文章id，转换成天数
	 * 
	 * @param articleid
	 * @return
	 */
	public static int getDaynum(long articleId) {
		return (int) ((articleId) / (24 * 60 * 60 * 1000));
	}
	
	public static void test() {
	    
	}
	
	
	public static void main(String[] args) {
        
	    System.out.println("hello");
    }
	
}
