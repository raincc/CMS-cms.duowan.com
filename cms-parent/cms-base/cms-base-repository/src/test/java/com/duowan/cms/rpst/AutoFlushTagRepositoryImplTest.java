package com.duowan.cms.rpst;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.domain.template.AutoFlushTag;
import com.duowan.cms.domain.template.rpst.AutoFlushTagRepository;
import com.duowan.cms.domain.template.support.AutoFlushTagDoToDtoConvertor;
import com.duowan.cms.dto.template.AutoFlushTagInfo;
import com.duowan.cms.dto.template.AutoFlushTagSearchCriteria;
import com.duowan.test.yzq.util.TypeUtil;

public class AutoFlushTagRepositoryImplTest {

    BeanFactory factory;

    AutoFlushTagRepository autoFlushTagRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        autoFlushTagRepository = (AutoFlushTagRepository) factory.getBean("autoFlushTagRepository");
        
        try {
            Class.forName(AutoFlushTagDoToDtoConvertor.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() {
        // save
        AutoFlushTag a = getAutoFlushTag();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        autoFlushTagRepository.save(a);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        AutoFlushTag a2 = autoFlushTagRepository.getById(a.getId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertTrue(TypeUtil.isSameObject(a, a2));

        // update
        a2.setFlushTime(new Date());
        long update = System.currentTimeMillis();
        autoFlushTagRepository.update(a2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        AutoFlushTag a3 = autoFlushTagRepository.getById(a2.getId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertTrue(TypeUtil.isSameObject(a2, a3));

        // delete
        long delete = System.currentTimeMillis();
        autoFlushTagRepository.delete(a3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        AutoFlushTag a4 = autoFlushTagRepository.getById(a3.getId());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(a4);
    }
    
    @Test
    public void testPageSearch(){
        
        AutoFlushTagSearchCriteria searchCriteria = new AutoFlushTagSearchCriteria();
        //searchCriteria.setChannelId("ceshi");
        searchCriteria.setIsFlushed(false);
        
        Page<AutoFlushTagInfo> page = autoFlushTagRepository.pageSearch(searchCriteria);
        
        System.out.println(page.getTotalCount() + ":" + page.getResult().size());
        
    }
    
    @Test
    public void testListUnFlushTag(){
        List<AutoFlushTagInfo> list = autoFlushTagRepository.listUnFlushTag();
        System.out.println(list.size());
        for(AutoFlushTagInfo a : list){
            System.out.println(a.getId() + ":" + a.getChannelId() + ":" + a.getTagName() + ":" + a.getCreateTime() + ":" + a.getFlushTime());
        }
    }
    
    
    
    private AutoFlushTag getAutoFlushTag(){
        AutoFlushTag a = new AutoFlushTag();
        
        Date now = new Date();
        
        a.setChannelId("ceshi");
        a.setTagName("test");
        a.setCreateTime(now);
        return a;
    }
    
}
