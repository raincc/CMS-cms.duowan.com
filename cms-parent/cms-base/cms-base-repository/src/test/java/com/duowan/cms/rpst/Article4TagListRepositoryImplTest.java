package com.duowan.cms.rpst;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.util.IdManager;
import com.duowan.cms.domain.article.Article4TagList;
import com.duowan.cms.domain.article.rpst.Article4TagListRepository;
import com.duowan.cms.domain.article.support.Article4TagListDoToDtoConvertor;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.channel.rpst.ChannelRepository;
import com.duowan.cms.domain.channel.support.ChannelDoToDtoConvertor;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.Article4TagListSearchCriteria;
import com.duowan.test.yzq.util.TypeUtil;

public class Article4TagListRepositoryImplTest {

    BeanFactory factory;

    Article4TagListRepository article4TagListRepository;

    ChannelRepository channelRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        article4TagListRepository = (Article4TagListRepository) factory.getBean("article4TagListRepository");

        channelRepository = (ChannelRepository) factory.getBean("channelRepository");

        try {
            Class.forName(Article4TagListDoToDtoConvertor.class.getName());
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() {
        // save
        Article4TagList article4TagList = getArticle4TagList();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        article4TagListRepository.save(article4TagList);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        Article4TagList article4TagList2 = article4TagListRepository.getByPrimaryKey(article4TagList.getChannel().getId(), article4TagList.getTag(), article4TagList.getId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getByPrimaryKey cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, TypeUtil.isSameObject(article4TagList, article4TagList2));

        // update
        article4TagList2.setDelete(true);
        article4TagList2.setUpdateTime(new Date());
        long update = System.currentTimeMillis();
        article4TagListRepository.update(article4TagList2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        Article4TagList article4TagList3 = article4TagListRepository.getByPrimaryKey(article4TagList2.getChannel().getId(), article4TagList2.getTag(), article4TagList2.getId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getByPrimaryKey cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, TypeUtil.isSameObject(article4TagList2, article4TagList3));

        // delete
        long delete = System.currentTimeMillis();
        article4TagListRepository.delete(article4TagList3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        Article4TagList article4TagList4 = article4TagListRepository.getByPrimaryKey(article4TagList3.getChannel().getId(), article4TagList3.getTag(), article4TagList3.getId());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(article4TagList4);
    }

    private Article4TagList getArticle4TagList() {
        Article4TagList a = new Article4TagList();
        Channel channel = channelRepository.getById("1000y");
        a.setChannel(channel);
        a.setId(IdManager.generateId());
        a.setTag("yzq");
        a.setTitle("hello");
        a.setAuthor("helloworld");
        a.setUserId("dw_hello");
        a.setPublishTime(new Date());
        a.setUpdateTime(new Date());
        a.setSource("www.duowan.com");
        
        a.setSonTag("yzq_son");
        a.setSonTagId("112233");

        return a;
    }

    public void testPagedSearch() {

        Article4TagListSearchCriteria searchCriteria = new Article4TagListSearchCriteria();
        long start = System.currentTimeMillis();
        Page<Article4TagListInfo> page = article4TagListRepository.pageSearch(searchCriteria);
        System.out.println("pageSearch cost time:" + (System.currentTimeMillis() - start) + "ms");
        List<Article4TagListInfo> list = page.getData();
        for (Article4TagListInfo article4TagListInfo : list) {
            System.out.println(article4TagListInfo.isDelete());
        }

        System.out.println(page.getTotalCount());
        System.out.println(page.getResult().size());
    }
    
    @Test
    public void testGetByChannelIdAndArticleId(){
        List<Article4TagListInfo> list = article4TagListRepository.getByChannelIdAndArticleId("ceshi", 224089133119L);
        System.out.println(list.size());
        for(Article4TagListInfo a : list){
            System.out.println(a.isDelete());
        }
    }

}
