package com.duowan.test.yzq.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CreateSQLScript20130410 {

    private static final String FILE_NAME = "springConfig/time.txt";
    private static final String FILE_PATH = Thread.currentThread().getContextClassLoader().getResource(FILE_NAME).getFile();
    
    private String basePath;
    private String systemLine;

    @Before
    public void setUp() throws Exception {
        
        basePath = "G:/yangzhuangqiu/workProject/new_cms_fabu/cmsfabu/docs/数据库整理/";
        systemLine = System.getProperty("line.separator");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGenerate() {

        BufferedReader bufReader = null;
        StringBuffer sb = new StringBuffer();
        String line = null;
        try {
            bufReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(FILE_PATH), "UTF8"));
            while ((line = bufReader.readLine()) != null) {
                if (!line.startsWith("==") && !line.trim().equals("")) {
                    String str = line.substring(0, line.indexOf("."));
                    sb.append("update article_").append(str).append(" set threadid=articleid where threadid is NULL;").append(systemLine);
                }
            }
            writeFile(basePath+"commentbug.sql", sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeBufReader(bufReader);
        }
    }
    
    private static void closeBufReader(BufferedReader bufReader) {
        if (bufReader != null) {
            try {
                bufReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeFile(String path, String content) {
        try {
            OutputStream os = FileUtils.openOutputStream(new File(path));
            IOUtils.write(content, os, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    

}
