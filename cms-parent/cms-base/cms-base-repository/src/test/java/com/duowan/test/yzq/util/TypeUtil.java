package com.duowan.test.yzq.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.duowan.cms.common.util.DateUtil;

public class TypeUtil {

    private static List<String> typeList;

    static {
        typeList = new ArrayList<String>();
        // 包装类型
        typeList.add("class java.lang.Byte");
        typeList.add("class java.lang.Character");
        typeList.add("class java.lang.Short");
        typeList.add("class java.lang.Integer");
        typeList.add("class java.lang.Long");
        typeList.add("class java.lang.Boolean");
        typeList.add("class java.lang.Float");
        typeList.add("class java.lang.Double");
        // 原生类型
        typeList.add("byte");
        typeList.add("char");
        typeList.add("short");
        typeList.add("int");
        typeList.add("long");
        typeList.add("float");
        typeList.add("double");
        typeList.add("boolean");

        // 常用类型
        typeList.add("class java.util.Date");
        typeList.add("class java.lang.String");

    }

    public static boolean isSameObject(Object o1, Object o2) {

        if (o1.getClass() != o2.getClass())
            return false;

        Class<?> cls = o1.getClass();
        Field[] fields = cls.getDeclaredFields();

        for (Field field : fields) {
            String name = field.getName();
            String methodName = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
            Method method = null;
            try {
                method = cls.getMethod(methodName);
                Object o1_value = method.invoke(o1);
                Object o2_value = method.invoke(o2);
                return isEquals(o1_value, o2_value);
            } catch (NoSuchMethodException e) {
                System.out.println("NoSuchMethod Exception: " + methodName + " not found!");
                continue;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private static boolean isEquals(Object o1, Object o2) {
        
        if (null == o1 && null == o2)
            return true;
        if (null == o1 || null == o2)
            return false;
        
        if (!typeList.contains(o1.getClass().toString())) 
            return isSameObject(o1, o2);
        
        if((o1 instanceof Date) && (o2 instanceof Date)) {
            String d1 = DateUtil.convertDate2Str((Date)o1);
            String d2 = DateUtil.convertDate2Str((Date)o2);
            return d1.equals(d2);
        }
        if(!o1.equals(o2))
            return false;
        return true;
    }

    public static void main(String[] args) {

        Class<TestType> clazz = TestType.class;
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            String type = field.getType().toString();
            System.out.println(type);
            System.out.println("===============================================");
        }
        
        System.out.println(String.class.getName());

    }
}
