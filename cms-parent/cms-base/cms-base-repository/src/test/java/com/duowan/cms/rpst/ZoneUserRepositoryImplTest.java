package com.duowan.cms.rpst;


import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.domain.zone.ZoneUser;
import com.duowan.cms.domain.zone.rpst.ZoneUserRepository;
import com.duowan.cms.domain.zone.support.ZoneUserDoToDtoConvertor;
import com.duowan.cms.dto.zone.ZoneUserInfo;
import com.duowan.cms.dto.zone.ZoneUserSearchCriteria;

/**
 * -- Table "zone_user" DDL
CREATE TABLE `zone_user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userName` varchar(255) NOT NULL,
  `nickName` varchar(255) default NULL,
  `defaultTag` varchar(255) default NULL,
  `channelId` varchar(45) NOT NULL,
  `isDel` char(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_index` (`userName`,`channelId`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
 *
 */
public class ZoneUserRepositoryImplTest {

    BeanFactory factory;

    ZoneUserRepository zoneUserRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        zoneUserRepository = (ZoneUserRepository) factory.getBean("zoneUserRepository");
        
        try {
            Class.forName(ZoneUserDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD(){
        
        // save 
        ZoneUser zoneUser = getZoneUser();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        zoneUserRepository.save(zoneUser);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        ZoneUser zoneUser2 = zoneUserRepository.getById(zoneUser.getId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameZoneUser(zoneUser, zoneUser2));
        
        // update
        String nickName = "hello" + System.currentTimeMillis();
        zoneUser2.setNickName(nickName);
        long update = System.currentTimeMillis();
        zoneUserRepository.update(zoneUser2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        ZoneUser zoneUser3 = zoneUserRepository.getById(zoneUser2.getId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameZoneUser(zoneUser2, zoneUser3));
        
        // delete
        long delete = System.currentTimeMillis();
        zoneUserRepository.delete(zoneUser3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        ZoneUser zoneUser4 = zoneUserRepository.getById(zoneUser3.getId());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(zoneUser4);
        
    }

    @Test
    public void testPagedSearch() {

        ZoneUserSearchCriteria searchCriteria = new ZoneUserSearchCriteria();
        searchCriteria.setChannelId("lol");
        searchCriteria.setUserName("test");
        searchCriteria.setIsDel("0");
        
        long start = System.currentTimeMillis();
        Page<ZoneUserInfo> page = zoneUserRepository.pageSearch(searchCriteria);
        System.out.println("pageSearch cost time : " + (System.currentTimeMillis() - start) + "ms");
        System.out.println("=======================================================");
        System.out.println("count : " + page.getTotalCount());
        List<ZoneUserInfo> data = page.getData();
        for(ZoneUserInfo user : data) {
            System.out.println(user.getChannelId() + ":" + user.getUserName() + ":" + user.getNickName() + ":" + user.getDefaultTag());
            break;
        }
    }
    
    @Test
    public void testGetByChannelId() {
        
        String channelId = "wow";
        
        List<ZoneUserInfo> list = zoneUserRepository.getByChannelId(channelId);
        
        for(ZoneUserInfo zui: list) {
            System.out.println(zui.getUserName());
        }
    }


    private ZoneUser getZoneUser() {

        ZoneUser zoneUser = new ZoneUser();
        //zoneUser.setId(1L); auto_increment
        zoneUser.setChannelId("lol");
        zoneUser.setUserName("test" + System.currentTimeMillis());
        zoneUser.setIsDel("0");
        
        return zoneUser;
    }

    
    private boolean isSameZoneUser(ZoneUser z1, ZoneUser z2){
        
        if(!isEquals(z1.getId(), z2.getId()))
            return false;
        if(!isEquals(z1.getChannelId(), z2.getChannelId()))
            return false;
        if(!isEquals(z1.getUserName(), z2.getUserName()))
            return false;
        if(!isEquals(z1.getNickName(), z2.getNickName()))
            return false;
        if(!isEquals(z1.getDefaultTag(), z2.getDefaultTag()))
            return false;
        if(!isEquals(z1.getIsDel(), z2.getIsDel()))
            return false;
        return true;
    }
    
    private boolean isEquals(Object o1, Object o2) {
        if(null == o1 && null == o2)
            return true;
        if(null == o1 || null == o2 || !o1.equals(o2))
            return false;
        return true;
    }
    
}
