package com.duowan.cms.rpst;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.domain.article.Article;
import com.duowan.cms.domain.article.ArticleGameLib;
import com.duowan.cms.domain.article.rpst.ArticleGameLibRepository;
import com.duowan.cms.domain.article.rpst.ArticleRepository;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.channel.rpst.ChannelRepository;

public class ArticleGameLibRepositoryImplTest {
	
	 BeanFactory factory;
	 ArticleGameLibRepository articleGameLibRepository;
	 ArticleRepository articleRepository;
	 ChannelRepository channelRepository; 
	 
	@Before
	public void setUp() throws Exception {
		factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");
		articleGameLibRepository = (ArticleGameLibRepository) factory.getBean("articleGameLibRepository");
        articleRepository = (ArticleRepository) factory.getBean("articleRepository");
        channelRepository = (ChannelRepository) factory.getBean("channelRepository");
	}

	@Test
	public void testSave() {
		String channelId = "1000y";
		String articleId = "74013023522";
		
		Article article = articleRepository.getByChannelIdAndArticleId(channelId, Long.parseLong(articleId));
		Channel channel = channelRepository.getById(channelId);
		ArticleGameLib articleGameLib = new ArticleGameLib();
		articleGameLib.setChannel(channel);
		articleGameLib.setArticle(article);
		articleGameLib.setRelategame("游戏2");
		articleGameLibRepository.save(articleGameLib);
	}
	
	@Test
	public void testGetArticleGameLib() {
		ArticleGameLib articleGameLib = articleGameLibRepository.getArticleGameLib("3ds", Long.parseLong("207401717417"));
		Assert.assertNotNull(articleGameLib);
	}
	

}
