package com.duowan.cms.rpst;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.util.ChineseCharactersUtil;
import com.duowan.cms.domain.user.UserPower;
import com.duowan.cms.domain.user.rpst.UserPowerRepository;
import com.duowan.cms.domain.user.support.PowerDoToDtoConvertor;
import com.duowan.cms.domain.user.support.UserPowerDoToDtoConvertor;
import com.duowan.cms.dto.user.UserPowerInfo;
import com.duowan.cms.dto.user.UserPowerSearchCriteria;

public class UserPowerRepositoryImplTest {
    
    BeanFactory factory;
    
    UserPowerRepository userPowerRepository;
    
    @Before
    public void setUp() throws Exception {
        
        factory = new ClassPathXmlApplicationContext(
                "springConfig/applicationContext.xml");
        
        userPowerRepository = (UserPowerRepository)factory.getBean("userPowerRepository");
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testSave() {
        
        //userPowerRepository.save(getUserPower());
        
    }
    
    @Test
    public void testUpdate() {
        
        UserPower userPower = getUserPower();
        userPower.setUserId("多玩yy62209");
        userPower.setAdminId("tmp");
        userPower.setLastModifyTime(new Date());
        
        
        //userPowerRepository.update(userPower);
        
    }
    
    @Test
    public void testDelete() {
        
        UserPower userPower = getUserPower();
        userPower.setUserId("多玩yy62209");
        
        //userPowerRepository.delete(userPower);
    }
    
    @Test
    public void testPagedSearch() {

        UserPowerSearchCriteria searchCriteria = new UserPowerSearchCriteria();
        
        try {
            Class.forName(UserPowerDoToDtoConvertor.class.getName());
            Class.forName(PowerDoToDtoConvertor.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //searchCriteria.setUserId("杨");

        searchCriteria.setChannelId("lol");
       // searchCriteria.setBeginLastModifyTime(getTime("2012-09-10 11:30:00"));
        //searchCriteria.setTitle("wo");
        //searchCriteria.setChannelId("12fy");
       // searchCriteria.setStatus(ArticleStatus.NORMAL);
        //searchCriteria.setBeginTime(getTime("2012-08-07 10:00:00"));
        //searchCriteria.setEndTime(getTime("2012-09-03 11:11:11"));
        //searchCriteria.setKeyword("teststesttest");
        //searchCriteria.setTag("test");
        long start = System.currentTimeMillis();
        Page<UserPowerInfo> page = userPowerRepository.pageSearch(searchCriteria);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(page.getTotalCount());
        System.out.println(page.getResult().size());
        System.out.println("=======================================================");
        for(UserPowerInfo upi : page.getData()){
            System.out.println(upi.getLastModifyTimeStr());
        }
    }
    
    @Test
    public void testGetByUserIdAndChannelId() {
        long start = System.currentTimeMillis();
        UserPower userPower = userPowerRepository.getByUserIdAndChannelId("杨壮秋", "1000y");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(userPower.getChannel().getName());
        System.out.println(userPower.getUserId() + ":" + userPower.getChannel().getId() + ":" + userPower.getValue() + ":" + userPower.getAdminId());
    }
    
    @Test
    public void testGetUserPowerInfoByUserId() {
        
        try {
            Class.forName(UserPowerDoToDtoConvertor.class.getName());
            Class.forName(PowerDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        userPowerRepository.getByUserIdAndChannelId("杨壮秋", "1000y");
        userPowerRepository.getByUserIdAndChannelId("杨壮秋", "12fy");
        userPowerRepository.getByUserIdAndChannelId("杨壮秋", "wow");
        userPowerRepository.getByUserIdAndChannelId("杨壮秋", "lol");
        long start = System.currentTimeMillis();
        List<UserPowerInfo> list = userPowerRepository.getAllPowerByUserId("杨壮秋");
        //userPowerRepository.getAllPowerByUserId("黄均杨");
        //userPowerRepository.getAllPowerByUserId("丘斯迪");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        for(UserPowerInfo upi : list) {
            String channelName = upi.getChannelInfo().getName();
            String channelId = upi.getChannelInfo().getId();
            System.out.println(channelId + ":" + ChineseCharactersUtil.String2Alpha(channelName) + ":" + channelName);
        }
    }
    
    public void testGetAllPowerByUserId(){
        userPowerRepository.getAllPowerByUserId("杨壮秋");
    }
    
    
    
    private UserPower getUserPower() {
        UserPowerInfo userPowerInfo = new UserPowerInfo();
        String rand = String.valueOf(System.currentTimeMillis());
        String yy = rand.substring(2 * rand.length() / 3);
        userPowerInfo.setUserId("多玩yy" + yy);
        //多玩yy98830
        userPowerInfo.setUserId("多玩yy98830");
        userPowerInfo.setCreateTime(new Date());
        userPowerInfo.setLastModifyTime(new Date());
        userPowerInfo.setAdminId("dw_admin" + new Random().nextInt(3));
        userPowerInfo.setChannelId("tx2");
        UserPower userPower = new UserPower(userPowerInfo);
        return userPower;
    }
    
    
}
