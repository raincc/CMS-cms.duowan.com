package com.duowan.test.yzq.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.duowan.cms.common.util.StringUtil;

public class MyUtil {

    public static Date getTime(String dateStr, String formatTime) {
        
        if(StringUtil.isEmpty(formatTime)){
            formatTime = "yyyy-MM-dd HH:mm:ss";
        }
        
        DateFormat format = new SimpleDateFormat(formatTime);
        Date date = null;

        try {
            date = format.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }
    
    public static void main(String[] args) {
        
        //System.out.println(DateUtil.convertDate2Str(new Date(getTime("2012-12-31", "yyyy-MM-dd").getTime() + 24 * 60 * 60 * 1000L)));
        String a = ",a,";
        String[] arr = a.split(",");
        System.out.println(arr.length);
        for(String t : arr){
            System.out.println(t + " ++++++++++++");
        }
    }
}
