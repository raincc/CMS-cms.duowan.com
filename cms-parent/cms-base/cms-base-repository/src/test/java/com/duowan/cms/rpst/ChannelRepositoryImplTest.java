package com.duowan.cms.rpst;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.channel.rpst.ChannelRepository;
import com.duowan.cms.domain.channel.support.ChannelDoToDtoConvertor;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.test.yzq.util.ChannelInfoConfigUtil;
import com.duowan.test.yzq.util.TypeUtil;

public class ChannelRepositoryImplTest {

    BeanFactory factory;

    ChannelRepository channelRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        channelRepository = (ChannelRepository) factory.getBean("channelRepository");
    }

    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testCRUD(){
        
        // save 
        Channel channel = getChannel();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        channelRepository.save(channel);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        Channel channel2 = channelRepository.getById(channel.getId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, TypeUtil.isSameObject(channel, channel2));
        
        // update
        channel2.setImportant(true);
        long update = System.currentTimeMillis();
        channelRepository.update(channel2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        Channel channel3 = channelRepository.getById(channel2.getId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, TypeUtil.isSameObject(channel2, channel3));
        
        // delete
        long delete = System.currentTimeMillis();
        channelRepository.delete(channel3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        Channel channel4 = channelRepository.getById(channel3.getId());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(channel4);
        
    }

    @Test
    public void testSave() {

        List<Channel> list = listChannel();
        System.out.println(list.size());
        // for(Channel channel : list) {

        // channelRepository.save(channel);
        // }

    }

    @Test
    public void testUpdate() {

        // Article article = getArticle();
        // article.setId(210789640125L);
        // article.setTitle("hellworld");

        // articleRepository.update(article);
    }

    @Test
    public void testPagedSearch() {

        /*
         * ArticleSearchCriteria searchCriteria = new ArticleSearchCriteria();
         * 
         * try { Class.forName(ArticleDoToDtoConvertor.class.getName()); } catch
         * (ClassNotFoundException e) { e.printStackTrace(); }
         * searchCriteria.setPageNo(1); searchCriteria.setPageSize(5);
         * searchCriteria.setTitle("wo"); searchCriteria.setChannelId("12fy");
         * searchCriteria.setStatus(ArticleStatus.NORMAL);
         * searchCriteria.setBeginTime(getTime("2012-08-07 10:00:00"));
         * searchCriteria.setEndTime(getTime("2012-09-03 11:11:11"));
         * searchCriteria.setKeyword("teststesttest");
         * searchCriteria.setTag("test");
         * 
         * Page<ArticleInfo> page =
         * channelRepository.pageSearch(searchCriteria);
         * 
         * System.out.println(page.getTotalCount());
         * System.out.println(page.getResult().size());
         */
    }

    @Test
    public void testGetById() {

        Channel channel = channelRepository.getById("lol");
        System.out.println(channel.getName());
        System.out.println(channel.isImportant());
        System.out.println(channel.isUpline());
    }

    @Test
    public void testGetAll() {

        try {
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        long start = System.currentTimeMillis();
        List<ChannelInfo> list = channelRepository.getAll();
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(list.size());

        for (ChannelInfo channelInfo : list) {

            System.out.println(channelInfo.getId() + ":" + channelInfo.getName() + ":" + channelInfo.getDomain());
        }
    }

    @Test
    public void testGetByName() {
        long start = System.currentTimeMillis();
        Channel channel = channelRepository.getByName("英雄联盟");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(channel.getId());

    }

    private List<Channel> listChannel() {

        String[][] cs = ChannelInfoConfigUtil.readChannelinfo();
        List<Channel> list = new ArrayList<Channel>();
        for (int i = 0; i < cs.length; i++) {
            if (cs[i][0] != null && cs[i][0].trim() != "") {
                Channel channel = new Channel();
                channel.setId(cs[i][0]);
                channel.setName(cs[i][1]);
                channel.setDomain(cs[i][2]);
                channel.setArticleFilePath(cs[i][3]);
                channel.setPicDomain(cs[i][4]);
                channel.setPicFilePath(cs[i][5]);
                list.add(channel);
            }

        }
        return list;
    }
    
    private Channel getChannel(){
        Channel channel = new Channel();
        long time = System.currentTimeMillis();
        channel.setId("dw_" + time);
        channel.setName("dw_test" + time);
        channel.setDomain("dw_test.duowan.com");
        channel.setPicDomain("pic.duowan.com");
        channel.setArticleFilePath("/data/www.duowan.com/dw_test");
        channel.setPicFilePath("/pic/www.duowan.com/dw_test");
        
        return channel;
    }
}
