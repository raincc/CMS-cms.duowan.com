package com.duowan.cms.rpst;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.domain.user.User;
import com.duowan.cms.domain.user.rpst.UserRepository;
import com.duowan.cms.domain.user.support.UserDoToDtoConvertor;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserSearchCriteria;
import com.duowan.cms.dto.user.UserStatus;

public class UserRepositoryImplTest {

    BeanFactory factory;

    UserRepository userRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        userRepository = (UserRepository) factory.getBean("userRepository");
        
        try {
            Class.forName(UserDoToDtoConvertor.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() {
        // save
        User user = getUser();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        userRepository.save(user);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        User user2 = userRepository.getById(user.getUserId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameUser(user, user2));

        // update
        String userIp = "192.168.0.1";
        user2.setUserIp(userIp);
        long update = System.currentTimeMillis();
        userRepository.update(user2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        User user3 = userRepository.getById(user2.getUserId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameUser(user2, user3));

        // delete
        long delete = System.currentTimeMillis();
        userRepository.delete(user3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        User user4 = userRepository.getById(user3.getUserId());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(user4);
    }
    
    @Test
    public void testGetByMail(){
        User user = userRepository.getByMail("yangzhuangqiu@yy.com");
        Assert.assertNotNull(user);
    }
    
    @Test
    public void testGetByUdbName(){
        User user = userRepository.getByUdbName("dw_yangzhuangqiu");
        System.out.println(user.getUserId() + ":" + user.getUserStatus());
    }
    
    @Test
    public void getSystemAdmins(){
        
        UserSearchCriteria searchCriteria = new UserSearchCriteria();
        searchCriteria.setPageSize(null);
        searchCriteria.setAdmin(true);
        List<UserInfo> admins = userRepository.listSearch(searchCriteria);
        for(UserInfo u : admins){
            System.out.println(u.getUserId());
        }
    }


    @Test
    public void testPagedSearch() {

        UserSearchCriteria searchCriteria = new UserSearchCriteria();

        Page<UserInfo> page = userRepository.pageSearch(searchCriteria);
        List<UserInfo> list = page.getData();
        for(UserInfo userInfo : list) {
            //lastModifyTime
            System.out.println(userInfo.getCreateTimeStr() + ":" + userInfo.getLastModifyTime());
        }
        

        //System.out.println(page.getTotalCount());
       // System.out.println(page.getResult().size());
    }

    @Test
    public void testGetUserById() {
        User user = userRepository.getById("杨壮秋");
        Assert.assertNotNull(user);
        Assert.assertEquals("杨壮秋", user.getUserId());
        System.out.println(user.getPassword());
    }

    @Test
    public void testIsExistUser() {
        Assert.assertEquals(true, null != userRepository.getByUserIdAndPassword("杨壮秋", "1"));
    }
    
    @Test
    public void testUpdateName() {
        
        String oldName = "yzq";
        String newName = "杨壮秋";
        
        User user = userRepository.getById(oldName);
        User user2 = userRepository.getById(newName);
        if(user != null && user2 == null){
            userRepository.updateName(oldName, newName);
        }
        
    }
    
    @Test
    public void testUpdatePassword(){
        
        User user = userRepository.getById("test_yzq");
        String now = System.currentTimeMillis() + "";
        String newPass = "dw_" + now.substring(now.length() / 3);
        user.setPassword(newPass);
        userRepository.update(user);
        Assert.assertEquals(true, null != userRepository.getByUserIdAndPassword("test_yzq", newPass));
    }
    
    

    private User getUser() {
        User user = new User();
        user.setUserId("yy_" + System.currentTimeMillis());
        user.setPassword(Math.random() + "yy");
        user.setCreateTime(new Date());
        user.setLastModifyTime(new Date());
        user.setUserIp("127.0.0.1");
        user.setUserStatus(UserStatus.NORMAL.getValue());
        user.setMail("xxx@yy.com");
        user.setAdmin(false);
        return user;
    }
    
    private boolean isSameUser(User u1, User u2) {
        
        if(!isEquals(u1.getUserId(), u2.getUserId()))
            return false;
        //if(!isEquals(u1.getPassword(), u2.getPassword()))
            //return false;
        if(!isEquals(DateUtil.convertDate2Str(u1.getCreateTime()), DateUtil.convertDate2Str(u2.getCreateTime())))
            return false;
        if(!isEquals(DateUtil.convertDate2Str(u1.getLastModifyTime()), DateUtil.convertDate2Str(u2.getLastModifyTime())))
            return false;
        if(!isEquals(u1.getUserIp(), u2.getUserIp()))
            return false;
        if(!isEquals(u1.getProxyIp(), u2.getProxyIp()))
            return false;
        if(!isEquals(u1.getUserStatus(), u2.getUserStatus()))
            return false;
        if(!isEquals(u1.getMail(), u2.getMail()))
            return false;
        if(u1.isAdmin() != u2.isAdmin())
            return false;
        
        return true;
    }
    
    private boolean isEquals(Object o1, Object o2) {
        if(null == o1 && null == o2)
            return true;
        if(null == o1 || null == o2 || !o1.equals(o2))
            return false;
        return true;
    }

}
