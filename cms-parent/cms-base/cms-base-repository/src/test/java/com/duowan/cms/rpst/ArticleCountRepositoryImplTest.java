package com.duowan.cms.rpst;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.domain.article.ArticleCount;
import com.duowan.cms.domain.article.rpst.ArticleCountRepository;
import com.duowan.cms.domain.article.rpst.ArticleRepository;
import com.duowan.cms.domain.article.support.ArticleCountDoToDtoConvertor;
import com.duowan.cms.domain.channel.support.ChannelDoToDtoConvertor;
import com.duowan.cms.dto.article.ArticleCountInfo;
import com.duowan.cms.dto.article.ArticleCountSearchCriteria;

public class ArticleCountRepositoryImplTest {

    BeanFactory factory;

    ArticleCountRepository articleCountRepository;
    
    ArticleRepository articleRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        articleCountRepository = (ArticleCountRepository) factory.getBean("articleCountRepository");

        articleRepository = (ArticleRepository) factory.getBean("articleRepository");
        
        Class.forName(ArticleCountDoToDtoConvertor.class.getName());
        Class.forName(ChannelDoToDtoConvertor.class.getName());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() {
        // save
        ArticleCount articleCount = getArticleCount();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        articleCountRepository.save(articleCount);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        ArticleCount articleCount2 = articleCountRepository.getByPrimaryKey(articleCount.getDateStr(), articleCount.getChannelId(), articleCount.getUserId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSame(articleCount, articleCount2));

        // update
        articleCount2.setCount((int) Math.random() * 100);
        long update = System.currentTimeMillis();
        articleCountRepository.update(articleCount2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        ArticleCount articleCount3 = articleCountRepository.getByPrimaryKey(articleCount2.getDateStr(), articleCount2.getChannelId(), articleCount2.getUserId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getById cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSame(articleCount2, articleCount3));

        // delete
        long delete = System.currentTimeMillis();
        articleCountRepository.delete(articleCount3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        ArticleCount articleCount4 = articleCountRepository.getByPrimaryKey(articleCount3.getDateStr(), articleCount3.getChannelId(), articleCount3.getUserId());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(articleCount4);

    }

    @Test
    public void testListSearch() {
        ArticleCountSearchCriteria searchCriteria = new ArticleCountSearchCriteria();
        searchCriteria.setUserId("杨壮秋");
        List<ArticleCountInfo> list = articleCountRepository.listSearch(searchCriteria);
        for (ArticleCountInfo a : list) {
            System.out.println(a.getDate() + ":" + a.getCount());
        }
    }
    
    @Test
    public void testPageSearch(){
        ArticleCountSearchCriteria searchCriteria = new ArticleCountSearchCriteria();
        searchCriteria.setUserId("曾庆明");
        articleCountRepository.pageSearch(searchCriteria);
        searchCriteria.setUserId("杨壮秋");
        long start = System.currentTimeMillis();
        List<ArticleCountInfo> list = articleCountRepository.pageSearch(searchCriteria).getData();
        System.out.println("cost times:" + (System.currentTimeMillis() - start) + "ms");
        for (ArticleCountInfo a : list) {
            System.out.println(/*a.getChannelInfo().getName() + */":" + a.getDate() + ":" + a.getCount());
        }
    }
    
    @Test
    public void testGetAllUserCount() {
        
        List<ArticleCount> list = articleRepository.getAllUserCount("ceshi", 0);

        for(ArticleCount a : list) {
            System.out.println(a.getChannelId() + ":" + a.getDateStr() + ":" + a.getUserId() + ":" + a.getCount());
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    private ArticleCount getArticleCount() {
        ArticleCount articleCount = new ArticleCount();
        articleCount.setDate(new Date());
        articleCount.setChannelId("lol");
        articleCount.setUserId("杨壮秋");
        articleCount.setCount(10);
        return articleCount;
    }

    private boolean isSame(ArticleCount a1, ArticleCount a2) {

        if (!isEquals(a1.getDateStr(), a2.getDateStr()))
            return false;
        if (!isEquals(a1.getChannelId(), a2.getChannelId()))
            return false;
        if (!isEquals(a1.getUserId(), a2.getUserId()))
            return false;
        if (!isEquals(a1.getCount(), a2.getCount()))
            return false;

        return true;
    }

    private boolean isEquals(Object o1, Object o2) {
        if (null == o1 && null == o2)
            return true;
        if (null == o1 || null == o2 || !o1.equals(o2))
            return false;
        return true;
    }

}
