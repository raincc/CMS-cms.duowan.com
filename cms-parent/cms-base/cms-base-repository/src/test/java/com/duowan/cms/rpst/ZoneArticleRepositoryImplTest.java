package com.duowan.cms.rpst;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.domain.zone.ZoneArticle;
import com.duowan.cms.domain.zone.rpst.ZoneArticleRepository;
import com.duowan.cms.domain.zone.support.ZoneArticleDoToDtoConvertor;
import com.duowan.cms.dto.zone.ZoneArticleInfo;
import com.duowan.cms.dto.zone.ZoneArticleSearchCriteria;
import com.duowan.cms.dto.zone.ZoneArticleStatus;

public class ZoneArticleRepositoryImplTest {

    BeanFactory factory;

    ZoneArticleRepository zoneArticleRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        zoneArticleRepository = (ZoneArticleRepository) factory.getBean("zoneArticleRepository");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() {

        // save
        ZoneArticle zoneArticle = getZoneArticle();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        zoneArticleRepository.save(zoneArticle);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        ZoneArticle zoneArticle2 = zoneArticleRepository.getByChannelIdAndArticleId(zoneArticle.getChannelId(), zoneArticle.getArticleId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getByChannelIdAndArticleId cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameZoneArticle(zoneArticle, zoneArticle2));

        // update
        String title = "hello" + System.currentTimeMillis();
        zoneArticle2.setTitle(title);
        long update = System.currentTimeMillis();
        zoneArticleRepository.update(zoneArticle2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        ZoneArticle zoneArticle3 = zoneArticleRepository.getByChannelIdAndArticleId(zoneArticle2.getChannelId(), zoneArticle2.getArticleId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getByChannelIdAndArticleId cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameZoneArticle(zoneArticle2, zoneArticle3));

        // delete
        long delete = System.currentTimeMillis();
        zoneArticleRepository.delete(zoneArticle3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        ZoneArticle zoneArticle4 = zoneArticleRepository.getByChannelIdAndArticleId(zoneArticle3.getChannelId(), zoneArticle3.getArticleId());
        System.out.println("getByChannelIdAndArticleId cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(zoneArticle4);

    }

    @Test
    public void testPagedSearch() {

        try {
            Class.forName(ZoneArticleDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        ZoneArticleSearchCriteria searchCriteria = new ZoneArticleSearchCriteria();
        searchCriteria.setChannelId("lol");
        searchCriteria.setZoneUserName("lolspecial_dw");
        searchCriteria.setStatus(ZoneArticleStatus.FAIL.getValue());
        
        long start = System.currentTimeMillis();
        Page<ZoneArticleInfo> page = zoneArticleRepository.pageSearch(searchCriteria);
        System.out.println("pageSearch cost time : " + (System.currentTimeMillis() - start) + "ms");
        System.out.println("=======================================================");
        System.out.println("count : " + page.getTotalCount());
        List<ZoneArticleInfo> data = page.getData();
        for (ZoneArticleInfo zai : data) {
            System.out.println(zai.getChannelId() + "##" + zai.getArticleId() + "##" + zai.getTitle() + "##" + zai.getTag() + "##" + zai.getZoneUserName() + "##" + zai.getStatus()
                    + "##" + DateUtil.convertDate2Str(zai.getCreateTime()) + "##" + DateUtil.convertDate2Str(zai.getUpdateTime()));
        }
    }

    private ZoneArticle getZoneArticle() {

        ZoneArticle zoneArticle = new ZoneArticle();
        zoneArticle.setChannelId("tl8");
        zoneArticle.setArticleId(218034660261L);
        zoneArticle.setTitle("天山谢谢你陪我五年的时光" + System.currentTimeMillis());
        zoneArticle.setTag("天龙八部3,");
        zoneArticle.setZoneUserName("o营养o快线o");
        zoneArticle.setStatus(ZoneArticleStatus.FAIL.getValue());
        try {
            zoneArticle.setCreateTime(DateUtil.convertStr2Date("2012-11-28 13:13:18"));
            zoneArticle.setUpdateTime(DateUtil.convertStr2Date("2012-11-28 13:20:01"));
        } catch (ParseException e) {
            zoneArticle.setCreateTime(new Date());
            zoneArticle.setUpdateTime(new Date());
            e.printStackTrace();
        }

        return zoneArticle;
    }

    private boolean isSameZoneArticle(ZoneArticle z1, ZoneArticle z2) {

        if (!isEquals(z1.getArticleId(), z2.getArticleId()))
            return false;
        if (!isEquals(z1.getChannelId(), z2.getChannelId()))
            return false;
        if (!isEquals(z1.getCreateTime(), z2.getCreateTime()))
            return false;
        if (!isEquals(z1.getStatus(), z2.getStatus()))
            return false;
        if (!isEquals(z1.getTag(), z2.getTag()))
            return false;
        if (!isEquals(z1.getTitle(), z2.getTitle()))
            return false;
        if (!isEquals(z1.getUpdateTime(), z2.getUpdateTime()))
            return false;
        if (!isEquals(z1.getZoneUserName(), z2.getZoneUserName()))
            return false;
        return true;
    }

    private boolean isEquals(Object o1, Object o2) {
        if (null == o1 && null == o2)
            return true;
        if (null == o1 || null == o2 || !o1.equals(o2))
            return false;
        return true;
    }

}
