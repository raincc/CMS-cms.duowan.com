package com.duowan.cms.rpst;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.util.IdManager;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.channel.rpst.ChannelRepository;
import com.duowan.cms.domain.channel.support.ChannelDoToDtoConvertor;
import com.duowan.cms.domain.template.Template;
import com.duowan.cms.domain.template.rpst.TemplateRepository;
import com.duowan.cms.domain.template.support.TemplateDoToDtoConvertor;
import com.duowan.cms.dto.template.SimpleTemplateInfo;
import com.duowan.cms.dto.template.TemplateCategory;
import com.duowan.cms.dto.template.TemplateInfo;
import com.duowan.cms.dto.template.TemplateSearchCriteria;
import com.duowan.test.yzq.util.TypeUtil;

public class TemplateRepositoryImplTest {

    BeanFactory factory;

    TemplateRepository templateRepository;

    ChannelRepository channelRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        templateRepository = (TemplateRepository) factory.getBean("templateRepository");

        channelRepository = (ChannelRepository) factory.getBean("channelRepository");

        try {
            Class.forName(TemplateDoToDtoConvertor.class.getName());
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() {

        // save
        Template template = getTemplate();
        System.out.println("id:" + template.getId());
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        templateRepository.save(template);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        Template template2 = templateRepository.getByChannelIdAndTemplateId(template.getChannel().getId(), template.getId());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getByChannelIdAndName cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, TypeUtil.isSameObject(template, template2));

        // update
        template2.setCode("GBK");
        template2.setName("helloworld");
        template2.setUpdateTime(new Date());
        long update = System.currentTimeMillis();
        templateRepository.update(template2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        Template template3 = templateRepository.getByChannelIdAndTemplateId(template2.getChannel().getId(), template2.getId());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getByChannelIdAndName cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, TypeUtil.isSameObject(template2, template3));

        // delete
        long delete = System.currentTimeMillis();
        templateRepository.delete(template3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        Template template4 = templateRepository.getByChannelIdAndTemplateId(template3.getChannel().getId(), template3.getId());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(template4);

    }

    @Test
    public void testPagedSearch() {
        templateRepository.getByChannelIdAndTemplateId("ceshi", 170895055658L);
        TemplateSearchCriteria searchCriteria = new TemplateSearchCriteria();

        searchCriteria.setChannelId("wow");
        // searchCriteria.setName("1003");
        // searchCriteria.setCreateUserId("胡兰丽");
        // searchCriteria.setTemplateCategory(TemplateCategory.TOPIC);
        // searchCriteria.setAlias("/s/i10/page.css");
        // searchCriteria.setLastUpdateUserId("李文峰");

        //searchCriteria.addRelateTags("YD解说,小楼解说");
        //searchCriteria.setName("暴雪");
        searchCriteria.setTemplateCategory(TemplateCategory.FINAL_ARTICLE);
        long start = System.currentTimeMillis();
        System.out.println("start:========================");
        Page<TemplateInfo> page = templateRepository.pageSearch(searchCriteria);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");

        System.out.println(page.getResult().size());
        System.out.println(page.getTotalCount());
        for (TemplateInfo info : page.getResult())
            //info.getUrlOnLine() + 
            System.out.println(info.getId() + ":" + info.getChannelId() + ":" + info.getName() + ":" + info.getDigest() + ":" + info.getTemplateCategory() + ":"
                    + info.getTemplateStatus());
        templateRepository.getByChannelIdAndTemplateId("ceshi", 170895055658L);

    }
    @Test
    public void testPagedSearch4Show() {
        templateRepository.getByChannelIdAndTemplateId("ceshi", 170895055658L);
        TemplateSearchCriteria searchCriteria = new TemplateSearchCriteria();

        searchCriteria.setChannelId("wow");
        // searchCriteria.setName("1003");
        // searchCriteria.setCreateUserId("胡兰丽");
        // searchCriteria.setTemplateCategory(TemplateCategory.TOPIC);
        // searchCriteria.setAlias("/s/i10/page.css");
        // searchCriteria.setLastUpdateUserId("李文峰");

        //searchCriteria.addRelateTags("YD解说,小楼解说");
        //searchCriteria.setName("暴雪");
        searchCriteria.setTemplateCategory(TemplateCategory.FINAL_ARTICLE);
        long start = System.currentTimeMillis();
        System.out.println("start:========================");
        Page<SimpleTemplateInfo> page = templateRepository.pageSearch4Show(searchCriteria);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");

        System.out.println(page.getResult().size());
        System.out.println(page.getTotalCount());
        for (SimpleTemplateInfo info : page.getResult())
            //info.getUrlOnLine() + 
            System.out.println(info.getId() + ":" + info.getChannelId() + ":" + info.getName() + ":" + info.getDigest() + ":" + info.getTemplateCategory() + ":"
                    + info.getTemplateStatus());
        templateRepository.getByChannelIdAndTemplateId("ceshi", 170895055658L);

    }

    @Test
    public void testGetByTemplateIdAndChannelId() {

        long start = System.currentTimeMillis();
        Template t = templateRepository.getByChannelIdAndTemplateId("ceshi", 170895055658L);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(t.getChannel().getId() + ":" + t.getName() + ":" + t.getContent());
    }

    @Test
    public void testGetByChannelIdAndTemplateName() {

        long start = System.currentTimeMillis();
        Template t = templateRepository.getByChannelIdAndTemplateName("qn", "配置文件");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(t.getChannel().getId() + ":" + t.getContent());
    }

    @Test
    public void testInsertAll() throws Exception {

        // String channelId = "ceshi5";

        Template template = new Template(new TemplateInfo());
        template.setChannel(getChannel("ceshi4"));
        template.setCreateUserId("杨壮秋");
        template.setUserIp("127.0.0.1");
        // template.setProxyIp("127.0.0.1");
        // template.setLogs("aaa");

        // templateRepository.insertAll(channelId, template);
    }

    @Test
    public void testGetByChannelIdAndCategoryOrderByCooperater() {

        long start = System.currentTimeMillis();
        List<TemplateInfo> list = templateRepository.getByChannelIdAndCategoryOrderByCooperater("lol", TemplateCategory.FINAL_ARTICLE);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("===========================================");
        for (TemplateInfo t : list) {
            System.out.println(t.getCooperate() + ":" + t.getTemplateCategory() + ":" + t.getTemplateStatus());
            System.out.println("==========================================");
        }
    }

    private Template getTemplate() {

        Template template = new Template();
        template.setChannel(getChannel("1000y"));
        template.setId(IdManager.generateId());
        template.setName("test");
        template.setContent("$aaa");
        template.setCode("utf-8");

        return template;
    }

    private Channel getChannel(String channelId) {

        return channelRepository.getById(channelId);
    }

}
