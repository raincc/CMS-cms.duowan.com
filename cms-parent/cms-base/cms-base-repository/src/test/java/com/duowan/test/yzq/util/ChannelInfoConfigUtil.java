package com.duowan.test.yzq.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;



public class ChannelInfoConfigUtil {
    
    private static Logger log = Logger.getLogger(ChannelInfoConfigUtil.class);
    private static final String FILE_NAME = "springConfig/channelinfo20130418.txt";
    private static final String FILE_PATH = Thread.currentThread().getContextClassLoader().getResource(FILE_NAME).getFile();
    private static final int CHANNEL_MAX_SIZE = 800;// 频道信息可能的最大数目
    private static final int CHANNEL_INFO_COLUMN = 8;// 频道信息的字段数
    
    private static final int MAX_LINE = 800;
    
    public static String[][] readChannelinfo() {
        log.info("read file：" + FILE_PATH);
        String[][] channelInfoOrg = new String[CHANNEL_MAX_SIZE][CHANNEL_INFO_COLUMN];
        BufferedReader bufReader = null;
        int channelIndex = 0;
        String line = null;
        try {
            bufReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(FILE_PATH), "UTF8"));
            while ((line = bufReader.readLine()) != null) {
                if (!line.startsWith("#") && !line.trim().equals("")) {
                    String[] strings = line.split(",");
                    if (strings.length == CHANNEL_INFO_COLUMN) {
                        for (int i = 0; i < CHANNEL_INFO_COLUMN; i++) {
                            channelInfoOrg[channelIndex][i] = strings[i].trim();
                        }
                        channelIndex++;
                        if (channelIndex == CHANNEL_MAX_SIZE) {
                            log.error("配置的频道信息超过预计的最大值：" + CHANNEL_MAX_SIZE
                                    + " 请修改最大值。");
                            break;
                        }
                    } else {
                        logWarn(line, strings);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeBufReader(bufReader);
        }
        if (channelIndex > 0) {
            return channelInfoOrg;
        } else if (channelIndex == 0) {
            log.error("没有读取到频道的配置信息，请检查配置是否正确!");
        }
        return null;
    }
    
    private static void closeBufReader(BufferedReader bufReader) {
        if (bufReader != null) {
            try {
                bufReader.close();
            } catch (IOException e) {
                log.warn("关闭文件出错：", e);
            }
        }
    }
    
    private static void logWarn(String line, String[] strings) {
        StringBuffer sbf = new StringBuffer("strings.length=");
        sbf.append(strings.length).append(" content=");
        for (int i = 0; i < strings.length; i++) {
            sbf.append(strings[i]).append(",");
        }
        log.warn("此行配置有误(非8项信息)，将忽略此项配置。请检查：line=" + line);
        log.warn(sbf.toString());
    }
    
    public static String[] readFile2Array (String filePath) {
        
        log.info("read file：" + filePath);
        String[] info = new String[MAX_LINE];
        BufferedReader bufReader = null;
        int index = 0;
        String line = null;
        try {
            bufReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(filePath), "UTF8"));
            while ((line = bufReader.readLine()) != null) {
                if (!line.trim().equals("")) {
                    info[index] = line;
                    index ++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeBufReader(bufReader);
        }
        
        return info;
        
    }
    
    public static void main(String[] args) {
        //System.out.println(readChannelinfo()[0][1]);
        String[][] info = readChannelinfo();
        for(String[] line : info){
            if(line != null)
                System.out.println(line[0]);
        }
    }
}
