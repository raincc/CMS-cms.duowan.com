package com.duowan.cms.rpst;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.util.IdManager;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.domain.article.rpst.ArticleRepository;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.channel.support.ChannelDoToDtoConvertor;
import com.duowan.cms.domain.tag.Tag;
import com.duowan.cms.domain.tag.rpst.TagRepository;
import com.duowan.cms.domain.tag.support.TagDoToDtoConvertor;
import com.duowan.cms.dto.tag.TagCategory;
import com.duowan.cms.dto.tag.TagInfo;
import com.duowan.cms.dto.tag.TagSearchCriteria;
import com.duowan.cms.dto.template.TemplateCategory;
import com.duowan.test.yzq.util.MyUtil;

public class TagRepositoryImplTest {

    BeanFactory factory;

    TagRepository tagRepository;

    ArticleRepository articleRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        articleRepository = (ArticleRepository) factory.getBean("articleRepository");
        tagRepository = (TagRepository) factory.getBean("tagRepository");

        try {
            Class.forName(TagDoToDtoConvertor.class.getName());
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() throws ParseException {

        // save
        Tag tag = getTag();
        System.out.println("====================================================");
        long save = System.currentTimeMillis();
        tagRepository.save(tag);
        long saveEnd = System.currentTimeMillis();
        System.out.println("save cost time : " + (saveEnd - save) + "ms");
        System.out.println("====================================================");
        Tag tag2 = tagRepository.getByChannelIdAndName(tag.getChannel().getId(), tag.getName());
        long queryEnd = System.currentTimeMillis();
        System.out.println("getByChannelIdAndName cost time : " + (queryEnd - saveEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameTag(tag, tag2));

        // update
        tag2.setCategory(TagCategory.USER_CONTRIBUTE.getValue());
        tag2.setRelatedTemplateCategory(null);
        tag2.setUpdateTime(new Date());
        long update = System.currentTimeMillis();
        tagRepository.update(tag2);
        long updateEnd = System.currentTimeMillis();
        System.out.println("update cost time : " + (updateEnd - update) + "ms");
        System.out.println("====================================================");
        Tag tag3 = tagRepository.getByChannelIdAndName(tag2.getChannel().getId(), tag2.getName());
        long queryEnd2 = System.currentTimeMillis();
        System.out.println("getByChannelIdAndName cost time : " + (queryEnd2 - updateEnd) + "ms");
        System.out.println("====================================================");
        Assert.assertEquals(true, isSameTag(tag2, tag3));

        // delete
        long delete = System.currentTimeMillis();
        tagRepository.delete(tag3);
        long deleteEnd = System.currentTimeMillis();
        System.out.println("delete cost time : " + (deleteEnd - delete) + "ms");
        System.out.println("====================================================");
        Tag tag4 = tagRepository.getByChannelIdAndName(tag3.getChannel().getId(), tag3.getName());
        System.out.println("getById cost time : " + (System.currentTimeMillis() - deleteEnd) + "ms");
        Assert.assertNull(tag4);
    }

    private Tag getTag() {
        Tag tag = new Tag();
        tag.setChannel(getChannel("lol"));
        tag.setName("yzqtest_yzq");
        tag.setParentName("测试");
        tag.setTree(",测试,yzqtest,");
        tag.setCategory(TagCategory.NORMAL.getValue());
        tag.setUpdateTime(new Date());
        Long id = IdManager.generateId();
        System.out.println("tag id : " + id);
        tag.setId(id);
        tag.setRelatedTemplateCategory(TemplateCategory.TAG_VIEW.getValue());
        return tag;
    }

    private Channel getChannel(String channelId) {
        Channel channel = new Channel();
        channel.setId(channelId);
        return channel;
    }

    private boolean isSameTag(Tag t1, Tag t2) {
        
        if(!isEquals(t1.getId(), t2.getId()))
            return false;
        if(!isEquals(t1.getChannel().getId(), t2.getChannel().getId()))
            return false;
        if(!isEquals(t1.getName(), t2.getName()))
            return false;
        if(!isEquals(t1.getParentName(), t2.getParentName()))
            return false;
        if(!isEquals(t1.getRelatedTemplateCategory(), t2.getRelatedTemplateCategory()))
            return false;
        if(!isEquals(t1.getTree(), t2.getTree()))
            return false;
        if(!isEquals(t1.getUpdateTime(), t2.getUpdateTime()));
        
        return true;
    }
    
    private boolean isEquals(Object o1, Object o2) {
        if(null == o1 && null == o2)
            return true;
        if(null == o1 || null == o2 || !o1.equals(o2))
            return false;
        return true;
    }

    @Test
    public void testPagedSearch() {

        TagSearchCriteria searchCriteria = new TagSearchCriteria();
        searchCriteria.setChannelId("1000y");
        searchCriteria.setUpdateTime(MyUtil.getTime("2012-10-18 00:00:00", null));

        Page<TagInfo> page = tagRepository.pageSearch(searchCriteria);
        System.out.println(page.getTotalCount());
        System.out.println(page.getResult().size());

    }

    @Test
    public void testGetByChannelIdAndName() {
        System.out.println("================================================");
        long time = System.currentTimeMillis();
        Tag tag = tagRepository.getByChannelIdAndName("12fy", "装备");
        System.out.println("all time:" + (System.currentTimeMillis() - time) + "ms");
        System.out.println("================================================");
        Channel c = tag.getChannel();
        System.out.println("channel info:" + c.getId() + "##" + c.getName() + "##" + c.getDomain() + "##" + c.getPicDomain() + "##" + c.getPicFilePath() + "##" + c.getArticleFilePath());
        System.out.println(c.getId() + ":" + tag.getName() + ":" + tag.getParentName() + ":" + tag.getTree() + ":" + tag.getCategory() + ":" + tag.getUpdateTime()
                + ":" + tag.getId() + ":" + tag.getRelatedTemplateCategory());
    }

    @Test
    public void testGetChildren() {
        System.out.println("================================================");
        long search = System.currentTimeMillis();
        List<TagInfo> list = tagRepository.getChildren("12fy", "系统玩法");
        System.out.println("update all time : " + (System.currentTimeMillis() - search) + "ms");
        System.out.println("================================================");
        for (TagInfo tag : list) {
            System.out.println(tag.getChannelId() + ":" + tag.getName() + ":" + tag.getParentName() + ":" + tag.getTree() + ":" + tag.getCategory() + ":"
                    + tag.getUpdateTime());
        }
        System.out.println("size:" + list.size());
    }

    @Test
    public void testTagTrees2Json() {

        long time = System.currentTimeMillis();
        System.out.println(getTagTrees2Json("wow", "").toJSONString());
        System.out.println("all time:" + (System.currentTimeMillis() - time) + "ms");
    }

    @SuppressWarnings("unchecked")
    private JSONArray getTagTrees2Json(String channelId, String tag) {
        JSONArray jarray = new JSONArray();
        String son = "";
        List<String> temp_list = new ArrayList<String>();
        if (null == tag)
            tag = "";
        List<TagInfo> list = tagRepository.getChildrenExcludeSpecial(channelId, tag);
        for (TagInfo tagInfo : list) {
            son = tagInfo.getName();
            List<TagInfo> sonList = tagRepository.getChildren(channelId, son);
            if (null == sonList || sonList.isEmpty()) {
                temp_list.add(son);
            } else {
                JSONObject jobj = new JSONObject();
                jobj.put(son, getTagTrees2Json(channelId, son));
                jarray.add(jobj);
            }
        }
        jarray.addAll(temp_list);
        return jarray;
    }

    @Test
    public void testTagTreees2Json2() {
        System.out.println("================================================");
        long start = System.currentTimeMillis();
        List<TagInfo> list = tagRepository.getTagsExcludeSpecial("wow");
        System.out.println(list.size());
        System.out.println("=====query db cost time:" + (System.currentTimeMillis() - start) + "ms");
        Map<String, List<TagInfo>> map = new HashMap<String, List<TagInfo>>();
        for (TagInfo tag : list) {
            // if(!"".equals(tag.getName().trim()))
            map.put(tag.getName(), getson(tag.getName(), list));
        }
        System.out.println(map.size());

        List<TagInfo> sonList = getson("", list);
        System.out.println(tagTreesList2Json(sonList, map).toJSONString());
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("================================================");
    }

    private List<TagInfo> getson(String parent, List<TagInfo> list) {
        List<TagInfo> temp_list = new ArrayList<TagInfo>();
        for (TagInfo tag : list) {
            if (tag.getParentName().equals(parent)) {
                temp_list.add(tag);
            }
        }
        return temp_list;
    }

    @SuppressWarnings("unchecked")
    private JSONArray tagTreesList2Json(List<TagInfo> list, Map<String, List<TagInfo>> map) {
        JSONArray jarray = new JSONArray();

        String son = "";

        List<String> temp_list = new ArrayList<String>();

        for (TagInfo tagInfo : list) {

            son = tagInfo.getName();
            List<TagInfo> tags = map.get(son);

            if (tags.isEmpty() || StringUtil.isEmpty(son)) {
                temp_list.add(son);
            } else {
                JSONObject jobj = new JSONObject();
                jobj.put(son, tagTreesList2Json(tags, map));
                jarray.add(jobj);
            }
        }
        jarray.addAll(temp_list);
        return jarray;
    }

    @Test
    public void testGetSepcial() {

        System.out.println("================================================");
        long time = System.currentTimeMillis();
        List<TagInfo> list = tagRepository.getSpecialTags("lol");
        System.out.println("all time:" + (System.currentTimeMillis() - time) + "ms");
        System.out.println("================================================");
        for (TagInfo tag : list) {
            System.out.println(tag.getName() + ":" + tag.getCategory());
        }
    }

    @Test
    public void testGetAllTagsInChannel() {
        try {
            Class.forName(TagDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("================================================");
        long start = System.currentTimeMillis();
        List<TagInfo> list = tagRepository.getAllTagsInChannel("lol");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("================================================");
        System.out.println("size:" + list.size());
        for (TagInfo tag : list) {
            System.out.println(tag.getName() + "#" + tag.getChannelInfo().getId() + "#" + tag.getParentName() + "#" + tag.getTree() + "#" + formatDateTime(tag.getUpdateTime())
                    + "#" + tag.getCategory().getValue());
        }
    }

    @Test
    public void testUpdateTagName() {
        System.out.println("================================================");
        long start = System.currentTimeMillis();
        tagRepository.updateTagName("lol", "yzq", "英雄视频");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("================================================");
    }

    @Test
    public void testMoveTag() {

        Tag srcTagInfo = new Tag();
        srcTagInfo.setChannel(getChannel("lol"));
        srcTagInfo.setName("英雄视频");
        srcTagInfo.setParentName("国服资讯");
        srcTagInfo.setTree(",国服资讯,英雄视频,");

        Tag destTagInfo = new Tag();
        destTagInfo.setChannel(getChannel("lol"));
        destTagInfo.setName("游戏视频");
        destTagInfo.setParentName("");
        destTagInfo.setTree(",游戏视频,");

        long start = System.currentTimeMillis();
        // tagRepository.moveTag(srcTagInfo, destTagInfo);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
    }

    private static String formatDateTime(Date date) {

        String pattern = "yyyy-MM-dd HH:mm:ss";

        DateFormat format = new SimpleDateFormat(pattern);

        return format.format(date);
    }

    @Test
    public void testGetDescendant() {
        System.out.println("================================================");
        long start = System.currentTimeMillis();
        @SuppressWarnings("deprecation")
        List<TagInfo> list = tagRepository.getDescendant("lol", "游戏视频");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("================================================");
        System.out.println("size:" + list.size());
        for (TagInfo tag : list) {
            System.out.println(tag.getName() + "======" + tag.getChannelId() + "======" + tag.getParentName());
        }
    }

    @Test
    public void testGetDescendantIncludeSelf() {

        String channelId = "lol";
        String seachTag = "游戏视频";

        System.out.println("================================================");
        long start2 = System.currentTimeMillis();
        List<TagInfo> list2 = tagRepository.getDescendantIncludeSelf(channelId, seachTag);
        System.out.println("all time:" + (System.currentTimeMillis() - start2) + "ms");
        System.out.println("================================================");
        System.out.println("size:" + list2.size());
        for (TagInfo tag : list2) {
            System.out.println(tag.getName() + "======" + tag.getChannelId() + "======" + tag.getParentName());
        }
    }
    
    @Test
    public void testGetTreeByChannelIdAndName(){
        String tree = tagRepository.getTreeByChannelIdAndName("1000y", "图片");
        System.out.println(tree);
    }

}
