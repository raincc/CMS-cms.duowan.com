package com.duowan.cms.rpst;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.domain.gametest.rpst.GameTestRepository;
import com.duowan.cms.domain.gametest.support.GameTestDoToDtoConvertor;
import com.duowan.cms.dto.gametest.GameTestInfo;
import com.duowan.cms.dto.gametest.GameTestSearchCriteria;
import com.duowan.cms.dto.gametest.GameTestSearchCriteria.GameTestOrderBy;

public class GameTestRepositoryImplTest {

    BeanFactory factory;

    GameTestRepository gameTestRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        gameTestRepository = (GameTestRepository) factory.getBean("gameTestRepository");
        
        try {
            Class.forName(GameTestDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCRUD() {

        

    }


    @Test
    public void testPagedSearch() {

        GameTestSearchCriteria searchCriteria = new GameTestSearchCriteria();
        searchCriteria.addOrderBy(GameTestOrderBy.TIME_DESC);
        Page<GameTestInfo> page = gameTestRepository.pageSearch(searchCriteria);

        System.out.println(page.getTotalCount());
        System.out.println(page.getResult().size());
        
        for(GameTestInfo g : page.getData()){
            System.out.println(g.getTime() + ":" + g.getState());
        }

    }
    
    @Test
    public void testListSearch(){
        GameTestSearchCriteria searchCriteria = new GameTestSearchCriteria();
        searchCriteria.setAbroad("on");
        searchCriteria.setTimeStart("2008-11-25");
        searchCriteria.setTimeEnd("2013-04-23");
        searchCriteria.setCtype(1);
        searchCriteria.addOrderBy(GameTestOrderBy.TIME_ASC).addOrderBy(GameTestOrderBy.POWER_DESC);
        
        List<GameTestInfo> list = gameTestRepository.listSearch(searchCriteria);
        
        for(GameTestInfo g : list){
            System.out.println(g.getState() + ":" + g.getAccountDesc());
        }
    }

    
}
