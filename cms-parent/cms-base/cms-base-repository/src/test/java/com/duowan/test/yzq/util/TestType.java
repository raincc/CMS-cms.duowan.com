package com.duowan.test.yzq.util;

public class TestType {

    private byte my_byte;
    private char my_char;
    private short my_short;
    private int my_int;
    private long my_long;
    private float my_float;
    private double my_double;
    private boolean my_boolean;
    
    private byte[] my_byte_array;
    private char[] my_char_array;
    private short[] my_short_array;
    private int[] my_int_array;
    private long[] my_long_array;
    private float[] my_float_array;
    private double[] my_double_array;
    private boolean[] my_boolean_array;
    
    private Byte my_Byte;
    private Character my_Character;
    private Short my_Short;
    private Integer my_Integer;
    private Long my_Long;
    private Float my_Float;
    private Double my_Double;
    private Boolean my_Boolean;
    
    private Byte[] my_Byte_array;
    private Character[] my_Character_array;
    private Short[] my_Short_array;
    private Integer[] my_Integer_array;
    private Long[] my_Long_array;
    private Float[] my_Float_array;
    private Double[] my_Double_array;
    private Boolean[] my_Boolean_array;
    public byte getMy_byte() {
        return my_byte;
    }
    public void setMy_byte(byte my_byte) {
        this.my_byte = my_byte;
    }
    public char getMy_char() {
        return my_char;
    }
    public void setMy_char(char my_char) {
        this.my_char = my_char;
    }
    public short getMy_short() {
        return my_short;
    }
    public void setMy_short(short my_short) {
        this.my_short = my_short;
    }
    public int getMy_int() {
        return my_int;
    }
    public void setMy_int(int my_int) {
        this.my_int = my_int;
    }
    public long getMy_long() {
        return my_long;
    }
    public void setMy_long(long my_long) {
        this.my_long = my_long;
    }
    public float getMy_float() {
        return my_float;
    }
    public void setMy_float(float my_float) {
        this.my_float = my_float;
    }
    public double getMy_double() {
        return my_double;
    }
    public void setMy_double(double my_double) {
        this.my_double = my_double;
    }
    public boolean isMy_boolean() {
        return my_boolean;
    }
    public void setMy_boolean(boolean my_boolean) {
        this.my_boolean = my_boolean;
    }
    public byte[] getMy_byte_array() {
        return my_byte_array;
    }
    public void setMy_byte_array(byte[] my_byte_array) {
        this.my_byte_array = my_byte_array;
    }
    public char[] getMy_char_array() {
        return my_char_array;
    }
    public void setMy_char_array(char[] my_char_array) {
        this.my_char_array = my_char_array;
    }
    public short[] getMy_short_array() {
        return my_short_array;
    }
    public void setMy_short_array(short[] my_short_array) {
        this.my_short_array = my_short_array;
    }
    public int[] getMy_int_array() {
        return my_int_array;
    }
    public void setMy_int_array(int[] my_int_array) {
        this.my_int_array = my_int_array;
    }
    public long[] getMy_long_array() {
        return my_long_array;
    }
    public void setMy_long_array(long[] my_long_array) {
        this.my_long_array = my_long_array;
    }
    public float[] getMy_float_array() {
        return my_float_array;
    }
    public void setMy_float_array(float[] my_float_array) {
        this.my_float_array = my_float_array;
    }
    public double[] getMy_double_array() {
        return my_double_array;
    }
    public void setMy_double_array(double[] my_double_array) {
        this.my_double_array = my_double_array;
    }
    public boolean[] getMy_boolean_array() {
        return my_boolean_array;
    }
    public void setMy_boolean_array(boolean[] my_boolean_array) {
        this.my_boolean_array = my_boolean_array;
    }
    public Byte getMy_Byte() {
        return my_Byte;
    }
    public void setMy_Byte(Byte my_Byte) {
        this.my_Byte = my_Byte;
    }
    public Character getMy_Character() {
        return my_Character;
    }
    public void setMy_Character(Character my_Character) {
        this.my_Character = my_Character;
    }
    public Short getMy_Short() {
        return my_Short;
    }
    public void setMy_Short(Short my_Short) {
        this.my_Short = my_Short;
    }
    public Integer getMy_Integer() {
        return my_Integer;
    }
    public void setMy_Integer(Integer my_Integer) {
        this.my_Integer = my_Integer;
    }
    public Long getMy_Long() {
        return my_Long;
    }
    public void setMy_Long(Long my_Long) {
        this.my_Long = my_Long;
    }
    public Float getMy_Float() {
        return my_Float;
    }
    public void setMy_Float(Float my_Float) {
        this.my_Float = my_Float;
    }
    public Double getMy_Double() {
        return my_Double;
    }
    public void setMy_Double(Double my_Double) {
        this.my_Double = my_Double;
    }
    public Boolean getMy_Boolean() {
        return my_Boolean;
    }
    public void setMy_Boolean(Boolean my_Boolean) {
        this.my_Boolean = my_Boolean;
    }
    public Byte[] getMy_Byte_array() {
        return my_Byte_array;
    }
    public void setMy_Byte_array(Byte[] my_Byte_array) {
        this.my_Byte_array = my_Byte_array;
    }
    public Character[] getMy_Character_array() {
        return my_Character_array;
    }
    public void setMy_Character_array(Character[] my_Character_array) {
        this.my_Character_array = my_Character_array;
    }
    public Short[] getMy_Short_array() {
        return my_Short_array;
    }
    public void setMy_Short_array(Short[] my_Short_array) {
        this.my_Short_array = my_Short_array;
    }
    public Integer[] getMy_Integer_array() {
        return my_Integer_array;
    }
    public void setMy_Integer_array(Integer[] my_Integer_array) {
        this.my_Integer_array = my_Integer_array;
    }
    public Long[] getMy_Long_array() {
        return my_Long_array;
    }
    public void setMy_Long_array(Long[] my_Long_array) {
        this.my_Long_array = my_Long_array;
    }
    public Float[] getMy_Float_array() {
        return my_Float_array;
    }
    public void setMy_Float_array(Float[] my_Float_array) {
        this.my_Float_array = my_Float_array;
    }
    public Double[] getMy_Double_array() {
        return my_Double_array;
    }
    public void setMy_Double_array(Double[] my_Double_array) {
        this.my_Double_array = my_Double_array;
    }
    public Boolean[] getMy_Boolean_array() {
        return my_Boolean_array;
    }
    public void setMy_Boolean_array(Boolean[] my_Boolean_array) {
        this.my_Boolean_array = my_Boolean_array;
    }
   
}
