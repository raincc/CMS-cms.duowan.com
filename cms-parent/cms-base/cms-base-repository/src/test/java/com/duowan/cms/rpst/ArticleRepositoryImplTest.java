package com.duowan.cms.rpst;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.domain.article.Article;
import com.duowan.cms.domain.article.rpst.Article4TagListRepository;
import com.duowan.cms.domain.article.rpst.ArticleRepository;
import com.duowan.cms.domain.article.support.ArticleDoToDtoConvertor;
import com.duowan.cms.domain.channel.Channel;
import com.duowan.cms.domain.channel.rpst.ChannelRepository;
import com.duowan.cms.domain.channel.support.ChannelDoToDtoConvertor;
import com.duowan.cms.domain.tag.support.TagDoToDtoConvertor;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;
import com.duowan.cms.dto.article.ArticleSearchCriteria.ArticleOrderBy;
import com.duowan.cms.dto.article.ArticleStatus;
import com.duowan.cms.dto.article.SimpleArticleInfo;

public class ArticleRepositoryImplTest {

    BeanFactory factory;

    ArticleRepository articleRepository;
    
    ChannelRepository channelRepository; 
    
    Article4TagListRepository article4TagListRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        articleRepository = (ArticleRepository) factory.getBean("articleRepository");
        
        channelRepository = (ChannelRepository) factory.getBean("channelRepository");
        
        article4TagListRepository = (Article4TagListRepository) factory.getBean("article4TagListRepository");
        
        try {
            Class.forName(ArticleDoToDtoConvertor.class.getName());
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSave() {
        
        try {
            Class.forName(TagDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Article a = getArticle();
        System.out.println(a.getId());

        long start = System.currentTimeMillis();
        articleRepository.save(a);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");

    }
    
    @Test
    public void testCreateTableByChannelId(){
        
        articleRepository.createTableByChannelId("yzq");
    }

    @Test
    public void testUpdate() {
        
        try {
            Class.forName(TagDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("===============================================");
        long start = System.currentTimeMillis();
        Article article = articleRepository.getByChannelIdAndArticleId("12ha", 217347328953L);//214499248032L
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("article tags:" + article.getTags());
        System.out.println("===============================================");
        article.setTags("bb,重点");
        article.setLastUpdateTime(new Date());
        
        long update = System.currentTimeMillis();
        articleRepository.update(article);
        System.out.println("all time:" + (System.currentTimeMillis() - update) + "ms");
        System.out.println("===============================================");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
    }



    @Test
    public void testPagedSearch() {
        articleRepository.getByChannelIdAndArticleId("wow", 221589420615L);
        ArticleSearchCriteria searchCriteria = new ArticleSearchCriteria();

        // searchCriteria.setTitle("wo");
        searchCriteria.setChannelId("wow");
        searchCriteria.setPageNo(3);
        // searchCriteria.setStatus(ArticleStatus.NORMAL);
        //searchCriteria.setPublishTimeStart(getTime("2012-08-07 10:00:00"));
        //searchCriteria.setPublishTimeEnd(getTime("2012-09-03 11:11:11"));
        //searchCriteria.addTags("视频");
        searchCriteria.setTitle("的");
        searchCriteria.addStatus(ArticleStatus.NORMAL).addStatus(ArticleStatus.NO_TAG).addStatus(ArticleStatus.CATCHED).addStatus(ArticleStatus.USER_DELIVERY);
        //searchCriteria.addOrderBy(ArticleOrderBy.DAY_NUM_DESC).addOrderBy(ArticleOrderBy.POWER_DESC).addOrderBy(ArticleOrderBy.UPDATE_TIME_DESC);
        searchCriteria.addOrderBy(ArticleOrderBy.UPDATE_TIME_DESC);
        //searchCriteria.setUpdateTime(getTime("2012-08-07 11:11:11"));
        // searchCriteria.setKeyword("teststesttest");
        // searchCriteria.setTag("test");

        long start = System.currentTimeMillis();
        Page<ArticleInfo> page = articleRepository.pageSearch(searchCriteria);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");

        System.out.println(page.getTotalCount());
        for(ArticleInfo a : page.getData()){
            System.out.println(a.getId() + ":" + a.getChannelInfo().getId() + ":" + a.getTitle() + ":" + a.isShowAllPage());
        }
    }
    
    @Test
    public void testPagedSearch4Show() {
        articleRepository.getByChannelIdAndArticleId("wow", 221589420615L);
        ArticleSearchCriteria searchCriteria = new ArticleSearchCriteria();

        // searchCriteria.setTitle("wo");
        searchCriteria.setChannelId("wow");
        searchCriteria.setPageNo(3);
        // searchCriteria.setStatus(ArticleStatus.NORMAL);
        //searchCriteria.setPublishTimeStart(getTime("2012-08-07 10:00:00"));
        //searchCriteria.setPublishTimeEnd(getTime("2012-09-03 11:11:11"));
        //searchCriteria.addTags("视频");
        searchCriteria.setTitle("的");
        searchCriteria.addStatus(ArticleStatus.NORMAL).addStatus(ArticleStatus.NO_TAG).addStatus(ArticleStatus.CATCHED).addStatus(ArticleStatus.USER_DELIVERY);
        //searchCriteria.addOrderBy(ArticleOrderBy.DAY_NUM_DESC).addOrderBy(ArticleOrderBy.POWER_DESC).addOrderBy(ArticleOrderBy.UPDATE_TIME_DESC);
        searchCriteria.addOrderBy(ArticleOrderBy.UPDATE_TIME_DESC);
        //searchCriteria.setUpdateTime(getTime("2012-08-07 11:11:11"));
        // searchCriteria.setKeyword("teststesttest");
        // searchCriteria.setTag("test");

        long start = System.currentTimeMillis();
        Page<SimpleArticleInfo> page = articleRepository.pageSearch4Show(searchCriteria);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");

        System.out.println(page.getTotalCount());
        for(SimpleArticleInfo a : page.getData()){
            System.out.println(a.getId() + ":" + a.getTitle());
        }
    }

    @Test
    public void testGetByChannelIdAndArticleId() {

        long start = System.currentTimeMillis();//217347328953
        Article article = articleRepository.getByChannelIdAndArticleId("wow", 221589420615L);//214499248032L
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        Channel channel = article.getChannel();
        System.out.println(channel.getId() + ":" + channel.getName() + ":" + channel.getDomain() + ":" + channel.getArticleFilePath() + ":" + channel.getPicDomain() + ":"
                + channel.getPicFilePath());
        
        System.out.println("========================================");
        System.out.println(article.getTags());
        article.setTags(article.getTags() + "新闻故事,");
        
        article4TagListRepository.logicDeleteByChannelIdAndArticleId(article.getChannel().getId(), article.getId());
        article4TagListRepository.saveList(article.getChannel().getId(), article.toArticle4TagList());
    }

    @Test
    public void testGetByTitle() {

        Article a = articleRepository.getByTitle("hero", "《英雄OL》公测前瞻——灵物篇");

        System.out.println(a == null);
        System.out.println(a.getId() + ":" + a.getChannel().getId());

    }
    
    public Article getArticle() {

        Article article = new Article();
        long articleId = ArticleUtil.getArticleId();
        article.setId(articleId);
        Channel c = new Channel();
        c.setId("12ha");
        article.setChannel(c);
        article.setTitle("title" + System.currentTimeMillis());
        article.setSubtitle("subtitleTest" + System.currentTimeMillis());
        article.setDigest("digest" + System.currentTimeMillis());
        article.setPictureUrl("www.duowan.com");
        article.setSource("hello.duowan.com");
        article.setAuthor("yy");
        article.setPublishTime(new Date());
        // article.setPrePublishTime(new Date());
        article.setLastUpdateTime(new Date());
        article.setTags("test,重点,专题");
        article.setUserId("hello");
        article.setLastUpdateUserId("hello");
        article.setUserIp("127.0.0.1");
        article.setProxyIp("127.0.0.1");
        // article.setStatus(ArticleStatus.NORMAL);
        article.setLogs("add");
        article.setContent("teststesttestteststesttestteststesttestteststesttestteststesttest");
        article.setDayNum(ArticleUtil.getDaynum(articleId));
        article.setPower(60);
        article.setThreadId(articleId);
        // article.setTemplateId(templateId);
        article.setRealtags("test");
        article.setIsHeadPic("no");
        article.setDiy1("diy1");
        article.setDiy2("diy2");
        article.setDiy3("diy3");
        article.setCatchUrl("http://www.yy.com");

        return article;
    }

    private Date getTime(String dateStr) {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;

        try {
            date = format.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }
    
    
    
    @Test
    public void testListDB(){
        
        try {
            Class.forName(ArticleDoToDtoConvertor.class.getName());
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ArticleSearchCriteria searchCriteria = new ArticleSearchCriteria();
        searchCriteria.setChannelId("lol");
        searchCriteria.addTags("COS锦集");
        searchCriteria.setPowerFrom(60);
        searchCriteria.setPowerTo(61);
        searchCriteria.setPageNo(1);
        searchCriteria.setPageSize(600);
        long start = System.currentTimeMillis();
        for(int i = 0; i < 10; i++){
            List<ArticleInfo> list = articleRepository.listSearch(searchCriteria);
            System.out.println(list.size());
        }
        
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        
    }
    
    
    @Test
    public void testNewSave(){
        
        try {
            Class.forName(ArticleDoToDtoConvertor.class.getName());
            Class.forName(ChannelDoToDtoConvertor.class.getName());
            Class.forName(TagDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        long id = 204050330565L;
        
        Article article = articleRepository.getByChannelIdAndArticleId("lol", 1351850984527L);
        
        article.setId(id);
        article.setTitle("yzq" + article.getTitle());
        System.out.println(article.getTags());
        long start = System.currentTimeMillis();
        articleRepository.save(article);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        
    }
    
    
    @Test
    public void testGetArticleCountByChannelIdAndTag() {
        long start = System.currentTimeMillis();
        int count = article4TagListRepository.getArticleCountByChannelIdAndTag("qn", "PK心得");
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("count:" + count);
    }
    
    @Test
    public void testTime(){
        System.out.println(System.currentTimeMillis());
    }
    
    
    @Test
    public void testGetRecentEffectTagsInArticle(){
        long start = System.currentTimeMillis();
        List<String> list = article4TagListRepository.getRecentEffectTagsInArticle("qn", 600);
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("count:" + list.size());
        for(String str : list) {
            System.out.println(str);
        }
    }
    

}
