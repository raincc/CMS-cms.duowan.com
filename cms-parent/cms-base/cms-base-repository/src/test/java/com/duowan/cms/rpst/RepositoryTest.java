package com.duowan.cms.rpst;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.domain.channel.rpst.ChannelRepository;
import com.duowan.cms.domain.channel.support.ChannelDoToDtoConvertor;
import com.duowan.cms.dto.channel.ChannelInfo;

public class RepositoryTest {

    BeanFactory factory;

    ChannelRepository channelRepository;

    @Before
    public void setUp() throws Exception {

        factory = new ClassPathXmlApplicationContext("springConfig/applicationContext.xml");

        channelRepository = (ChannelRepository) factory.getBean("channelRepository");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSave() {

    }

    @Test
    public void testAlterTable() {

        String driver = "com.mysql.jdbc.Driver";
        // URL指向要访问的数据库名scutcs
        String url = "jdbc:mysql://172.19.103.15:6301/cms";
        // MySQL配置时的用户名
        String user = "clientuser";
        // Java连接MySQL配置时的密码
        String password = "Kv2X3s7e2aDqT5x";

        Statement st = null;
        Connection conn = null;
        try {
            // 加载驱动程序
            Class.forName(driver);
            // 连续数据库
            conn = DriverManager.getConnection(url, user, password);
            if (!conn.isClosed())
                System.out.println("Succeeded connecting to the Database!");
            // statement用来执行SQL语句
            st = conn.createStatement();
            
            String sql_prefix = "alter table "; 
            String sql_suffix = " add(showAllPage BOOLEAN default FALSE,syncQa BOOLEAN default FALSE,syncWap BOOLEAN default FALSE,syncZone BOOLEAN default FALSE);";
            for (ChannelInfo channel : getAllChannel()) {
                if (!StringUtil.isEmpty(channel.getId())) {
                    String tableName = "article_" + channel.getId();
                    st.addBatch(sql_prefix + tableName + sql_suffix);
                }
            }

            int[] a = st.executeBatch();
            int count = 0;
            for (int tmp : a) {
                if (tmp > 0) {
                    count++;
                }
            }
            System.out.println("all count" + count);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (st != null)
                    st.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
    
    @Test
    public void testJDBC() {
        String driver = "com.mysql.jdbc.Driver";
        // URL指向要访问的数据库名scutcs
        String url = "jdbc:mysql://172.19.103.15:6301/cms?zeroDateTimeBehavior=convertToNull";
        // MySQL配置时的用户名
        String user = "clientuser";
        // Java连接MySQL配置时的密码
        String password = "Kv2X3s7e2aDqT5x";

        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            // 加载驱动程序
            Class.forName(driver);
            // 连续数据库
            conn = DriverManager.getConnection(url, user, password);
            if (!conn.isClosed())
                System.out.println("Succeeded connecting to the Database!");
            // statement用来执行SQL语句
            String sql = "select * from article_12ha where articleid=217347328953";
            ps = conn.prepareStatement(sql);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                String author = rs.getString("author");
                System.out.println(author);
                String order = rs.getString("ordertime");
                System.out.println(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(rs != null) 
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
    
    @Test
    public void testJDBC228() {
        String driver = "com.mysql.jdbc.Driver";
        // URL指向要访问的数据库名scutcs
        String url = "jdbc:mysql://119.97.153.228:6301/fabu?zeroDateTimeBehavior=convertToNull";
        // MySQL配置时的用户名
        String user = "root";
        // Java连接MySQL配置时的密码
        String password = "7N2ziKYCi5UFdVh9";

        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            // 加载驱动程序
            Class.forName(driver);
            // 连续数据库
            conn = DriverManager.getConnection(url, user, password);
            if (!conn.isClosed())
                System.out.println("Succeeded connecting to the Database!");
            // statement用来执行SQL语句 a.updateuserid, a.userip, a.proxyip, , a.logs , a.threadid, a.templateid, a.realtags,
            String sql = 
                
                "select count(articleid) from article_wow where ( status = '0' or status = '2' or status = '3' or status = '4' ) and title like concat('%', '的', '%')";
                
//                "select a.articleid, a.title, a.subtitle, a.picurl, " +
//                       "a.source, a.author, a.ordertime, a.posttime, a.updatetime, " +
//                       "a.tags, a.userid, a.status, a.daynum, a.power," +
//                       "a.ispic, c.id,c.name,c.domain,c.article_file_path,c.pic_domain,c.pic_file_path from article_wow a, channel c where  a.title like concat('%', '的', '%') and c.id = 'wow'  limit 0, 25";
//            "select " +  
//        "a.articleid, a.title, a.subtitle, a.picurl, a.source, a.author, a.ordertime, a.posttime, a.updatetime, a.tags, a.userid, a.updateuserid, a.userip, a.proxyip, a.status, a.daynum, a.power, a.threadid, a.templateid, a.ispic, a.catchurl, " +
//        "c.id,c.name,c.domain,c.article_file_path,c.pic_domain,c.pic_file_path " + 
//        "from article_wow a, channel c " + 
//          "where  a.title like concat('%', '的', '%') and c.id = 'wow' limit 0, 25";
                //"select count(*) from article_wow where title like concat('%', '的', '%')";
                            //"select * from article_wow a, channel c where  a.title like concat('%', '的', '%') and c.id = 'wow'  limit 0, 25";
                //"select channelid,templateid,templatename,status,sort,userid,posttime,updatetime,digest,parsertime,alias,cooperate from template  order by templatename asc limit 0,25";
                //"select * from template t where  t.channelid = 'wow' limit 0, 25";
            long start = System.currentTimeMillis();
            ps = conn.prepareStatement(sql);
            
            rs = ps.executeQuery();
            
            long end = System.currentTimeMillis();
            System.out.println("====================================");
            System.out.println("cost time:" + (end - start) + "ms");
            System.out.println("====================================");
            while (rs.next()) {
                String channelid = rs.getString("articleid");
                System.out.println(channelid);
                String templateid = rs.getString("title");
                System.out.println(templateid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(rs != null) 
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    @Test
    public void testUtil() {
        try {
            System.out.println(DateUtil.convertDate2Str(DateUtil.convertStr2Date("0000-00-00 00:00:01")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testGetAll() {

        try {
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        long start = System.currentTimeMillis();
        List<ChannelInfo> list = channelRepository.getAll();
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(list.size());

        for (ChannelInfo channelInfo : list) {

            System.out.println(channelInfo.getId() + ":" + channelInfo.getName() + ":" + channelInfo.getDomain());
        }
    }

    private List<ChannelInfo> getAllChannel() {
        try {
            Class.forName(ChannelDoToDtoConvertor.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        long start = System.currentTimeMillis();
        List<ChannelInfo> list = channelRepository.getAll();
        System.out.println("all time:" + (System.currentTimeMillis() - start) + "ms");
        System.out.println(list.size());

        return list;
    }

}
