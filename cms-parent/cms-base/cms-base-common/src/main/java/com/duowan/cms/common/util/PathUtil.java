/**
 * 
 */
package com.duowan.cms.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.duowan.cms.common.dto.UrlInfo;
import com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder;

/**
 * 管理路径的工具类
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-10-24
 * <br>==========================
 */
public abstract class PathUtil {

    public final static String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     * 获取模板的绝对路径（例如：/data2/www/cms.duowan.com/wow/1201/m_190747785453_6.html）
     * @param channelId
     * @param templateId
     * @param alias
     * @param pageNo
     * @return
     */
    public static String getAbsoluteTemplatePath(String baseArticleFilePath, Long templateId, String alias, int pageNo) {
        // String url = channelInfo.getArticleFilePath();
        String url = baseArticleFilePath;
        if (StringUtil.isEmpty(alias)) { // 判断别名是否为空
            url += "/" + IdManager.formatIdToYM(templateId);
            url += "/m_" + templateId + ".html";
        } else {
            //可能是css，js
//            if (alias.lastIndexOf(".html") == -1) { // 别名没有以.html结尾
//                alias += ".html";
//            }
            if (alias.indexOf("/") != -1) {
                url += alias;
            } else {
                url += "/" + alias;
            }
        }
        // 处理有分页的情况
        if (pageNo > 1) {
            url = url.replace(".html", "") + "_" + pageNo + ".html";
        }
        return url;
    }
    
    public static String getAbsolutePublicTemplatePath(String baseFilePath, String channelId, Long templateId, String alias, int pageNo){
        
        String url = baseFilePath;
        if(StringUtil.isEmpty(alias)){
            url += "/template/" + channelId + "/" + IdManager.formatIdToYM(templateId) + "/m_" + templateId + ".html";
        }else{
            if (alias.indexOf("/") != -1) {
                url += "/template/" + channelId + alias;
            } else {
                url += "/template/" + channelId + "/" + alias;
            }
        }
        //处理有分页的情况
        if (pageNo > 1) {
            url = url.replace(".html", "") + "_" + pageNo + ".html";
        }
        
        return url;
    }

    /**
     * 获取文章的的绝对路径（例如：/data2/www/cms.duowan.com/wow/1201/190747785453_6.html）
     */
    public static String getAbsoluteArticlePath(String baseArticleFilePath, Long articleId, int pageNo) {
        String url = baseArticleFilePath;
        url += "/" + IdManager.formatIdToYM(articleId);
        if (pageNo == 0) {
            url += "/" + articleId + "_all.html";
        } else if (pageNo > 1) {
            url += "/" + articleId + "_" + pageNo + ".html";
        } else {
            url += "/" + articleId + ".html";
        }
        return url;
    }

    /**
     * 获取文章Tag的绝对路径（例如：/data2/www/cms.duowan.com/wow/tag/190747785453_6.html）
     */
    public static String getAbsoluteTagPath(String baseArticleFilePath, Long tagId, int pageNo) {
        String url = baseArticleFilePath + "/tag/";
        if (pageNo > 1) {
            url += tagId + "_" + pageNo + ".html";
        } else {
            url += tagId + ".html";
        }
        return url;
    }

    /**
     * 获取“普通模板”包含的地址（例如：/1201/m_190747785453.html 或者 /s/index.html）
     * @param channelId
     * @param templateId
     * @param alias
     * @return
     */
    public static String getTemplateInclude(String channelId, Long templateId, String alias) {
        String url = "";
        if (StringUtil.isEmpty(alias)) // 判断别名是否为空
            url += "/" + IdManager.formatIdToYM(templateId) + "/m_" + templateId + ".html";
        else if (alias.indexOf("/") != -1)
            url += alias;
        return url;
    }

    /**
     * 获取“普通模板”的相对路径（例如：/qn/1201/m_190747785453.html）
     * @param channelId 频道ID
     * @param templateId 模板ID
     * @param alias 模板别名
     * @return
     */
    public static String getTemplatePath(String channelId, Long templateId, String alias) {
        return "/" + channelId + getTemplateInclude(channelId, templateId, alias);
    }

    /**
     * 获取“普通模板”的相对路径, 不带别名( 例如：/1201/m_190747785453.html）
     */
    public static String getTemplatePath(String channelId, Long templateId) {
        return getTemplatePath(channelId, templateId, "");
    }

    /**
     * 获取“公共模板”的包含地址（例如：/public/template/qn/1201/c_190747785453.html）
     * @param channelId
     * @param templateId
     * @return
     */
    public static String getPublicTemplateInclude(String channelId, Long templateId, String alias) {
        return "/public/template/" + channelId + getTemplateInclude(channelId, templateId, alias);
    }

    /**
     * 获取“公共模板”的相对路径，和获取公共模板的包含地址结果一样（例如：/public/template/qn/1201/c_190747785453.html）
     * @param channelId
     * @param templateId
     * @return
     */
    public static String getPublicTemplatePath(String channelId, Long templateId, String alias) {
        return "/public/template" + getTemplatePath(channelId, templateId, alias);
    }

    /**
     * 获取文章评论的相对路径（例如：/1201/c_190747785453.html）
     * @param articleId
     * @return
     */
    // public static String getCommentHtmlIncludePath(Long articleId){
    // return "/" + IdManager.formatIdToYM(articleId) + "/c_" + articleId +
    // ".html";
    // }

    /**
     * 获取文章在发布器上的URL
     * @param channelId
     * @param templateId 
     * @param alias
     * @return
     */
    public static String getTemplateOnCmsUrl(String channelId, Long templateId, String alias) {
        StringBuffer url = new StringBuffer();
        url.append(getDomainOncms(channelId));
        if (StringUtil.isEmpty(alias)) // 判断别名是否为空
            url.append("/" + IdManager.formatIdToYM(templateId) + "/m_" + templateId + ".html");
        else if (alias.indexOf("/") != -1)
            url.append(alias);
        return url.toString();
    }

    /**
     * 获取文章在外网的URL
     * @param channelId
     * @param templateId 
     * @param alias
     * @return
     */
    public static String getTemplateOnlineUrl(String domain, Long templateId, String alias) {
        StringBuffer url = new StringBuffer(domain);
        if (StringUtil.isEmpty(alias)) // 判断别名是否为空
            url.append("/" + IdManager.formatIdToYM(templateId) + "/m_" + templateId + ".html");
        else if (alias.indexOf("/") != -1)
            url.append(alias);
        return url.toString();
    }

    /**
     * 获取文章在发布器的URL
     * @param channelInfo
     * @param articleId
     * @return
     */
    public static String getArticleOnCmsUrl(String channelId, Long articleId) {
        return getDomainOncms(channelId) + "/" + IdManager.formatIdToYM(articleId) + "/" + articleId + ".html";
    }

    /**
     * 获取专区在内网的域名
     * @param channelId
     * @return
     */
    public static String getDomainOncms(String channelId) {
        return "http://" + channelId + ".webpreview.duowan.com";//+ PropertiesHolder.get("domain", "cms.duowan.com").replace("http://", "");
    }

    /**
     * 获取文章在外网的URL
     * @param channelInfo
     * @param id
     * @return
     */
    public static String getArticleOnlineUrl(String domain, Long articleId) {
        return domain + "/" + IdManager.formatIdToYM(articleId) + "/" + articleId + ".html";
    }

    public static String getTagOnCmsUrl(String channelId, long tagId) {
        return getDomainOncms(channelId) + "/tag/" + tagId + ".html";
    }

    public static String getTagOnlineUrl(String domain, Long tagId) {
        return domain + "/tag/" + tagId + ".html";
    }

    /**
     * 得到文章图片的存储路径
     * @param channelId
     * @param articleId
     * @return
     */
    public static String getPicPath(String channelPicBasePath, long articleId, String picName) {
        return getPicBasePath(channelPicBasePath, articleId) + picName;
    }

    /**
     * 获取文章里用到图片的存放路径(即是图片的服务器路径)
     */
    public static String getPicBasePath(String channelPicBasePath, long articleId) {
        if (!channelPicBasePath.endsWith("/")) {
            channelPicBasePath += "/";
        }
        return channelPicBasePath + IdManager.formatIdToYM(articleId) + "/" + articleId + "/";
    }
    /**
     * 获取图片在发布器服务器的URL
     */
    public static String getPicUrlOnCms(String channelId , Long articleId , String fileName){
    	return PathUtil.getDomainOncms(channelId) + "/"+IdManager.formatIdToYM(articleId) + "/" +articleId +"/" + fileName;
    }
    /**
     * 获取图片在外网的URL
     */
    public static String getPicUrlOnline(String channelPicDomain, Long articleId , String picName){
        if (StringUtil.isEmpty(picName))
            return null;
        if (!channelPicDomain.endsWith("/")) {
            return channelPicDomain + "/"+IdManager.formatIdToYM(articleId) + "/" +articleId +"/" + picName;
        }
    	return channelPicDomain +IdManager.formatIdToYM(articleId) + "/" +articleId +"/" + picName;
    }

//    /**
//     * 根据客户上传的图片名称得到图片的访问URL 
//     */
//	public static String getPictureUrl(String channelId , Long articleId , String picturePath){
//		String channelPicBasePicDomain = PathUtil.getDomainOncms(channelId);
//		String picName = getPicName(picturePath);
//		return PathUtil.getArticleImgUrl(channelPicBasePicDomain, articleId, picName);
//	}
	
/**
  * 根据图片路径得到图片名称
  * 
  * @return
  */
 public static String getPicName(String picturePath) {
     if (StringUtil.isEmpty(picturePath)) {
         return "";
     }
     picturePath = picturePath.replace("\\", FileUtil.FILE_SEPARATOR);
     if (picturePath.lastIndexOf(FileUtil.FILE_SEPARATOR) > -1) {
         return picturePath.substring(picturePath.lastIndexOf(FileUtil.FILE_SEPARATOR), picturePath
                     .length());
     }
     return picturePath;
 }
    
    
    /**
     * 得到文章图片的访问URL
     * @param channelId
     * @param articleId
     * @return
     */
//    public static String getArticleImgUrl(String channelPicBasePicDomain, long articleId, String picName) {
//        if (StringUtil.isEmpty(picName))
//            return null;
//        if (!channelPicBasePicDomain.endsWith("/")) {
//            channelPicBasePicDomain += "/";
//        }
//        return channelPicBasePicDomain + IdManager.formatIdToYM(articleId) + "/" + articleId + "/" + picName;
//    }
    
    

    /**
     * 得到web频道的域名
     * @return
     */
    // public static String getWebChannelDomain(){
    // return "http://web.duowan.com";
    // }

    /**
     * 将id转成url形式：获得相对路径
     * @param id
     * @return
     */
    public static String getId2Url(Long id) {
        return "/" + IdManager.formatIdToYM(id) + "/" + id + ".html";
    }

    /**
     * 根据输入的静态的文章或者模板的url，解析出类型和id
     * 
     * @param url
     * @return
     */

    public static UrlInfo parser(String url) {
        int type = 0;
        Long id = 0L;
        if (StringUtil.isEmpty(url))
            return null;

        Pattern pattern = Pattern.compile("/(([\\w])_)?([^\\./]+).html$", Pattern.CASE_INSENSITIVE);

        Matcher m = pattern.matcher(url);
        // 2是类型，3是id
        if (m.find()) {
            if (!StringUtil.isEmpty(m.group(2)))
                type = UrlInfo.TYPE_TEMPLATE;
            else
                type = UrlInfo.TYPE_ARTICLE;
            id = Long.valueOf(m.group(3));
        }

        return new UrlInfo(type, id);
    }

    public static void main(String[] args) {
        String url = "http://xx.duowan.com/1212/218730291582.html";// [,218730291582]
        // url = "http://xx.duowan.com/1110/m_183723384293.html";//
        // [m,183723384293]
        // url = "http://xx.duowan.com/xinfa/index.html";//[, index]
        // url = "http://xx.duowan.com/s/fwz/sl.html";//[, sl]
        // url = "http://xx.duowan.com/tag/104164432508.html";//[, 104164432508]
        UrlInfo info = parser(url);
        System.out.println(info.getType() + "-----" + info.getId());
    }

}
