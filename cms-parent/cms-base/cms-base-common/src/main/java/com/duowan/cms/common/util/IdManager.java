/**
 * 
 */
package com.duowan.cms.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * ID管理器，根据当前的时间戳，产生ID（用于发布器的文章ID，图片ID等）
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-10-31
 * <br>==========================
 */
public abstract class IdManager {
    
    /*
     *  base time point 2006-01-01 00:00:00
     */
    public  static final long BASE_TIMESTAMP = 1136044800000l;
    
    /**
     * 根据时间戳，生成一个新的ID
     * @return
     */
    public static long generateId(){
        return System.currentTimeMillis() - BASE_TIMESTAMP ;
    }
    
    public static long generateIdByDate(Date date){
        if(null == date)
            return generateId();
        return date.getTime() - BASE_TIMESTAMP;
    }
    /**
     * 把ID还原回 "年月" 的格式
     * @param id
     * @return
     */
    public static String formatIdToYM(Long id){
        String pattern = "yyMM";
        Date date = new Date();
        date.setTime(id + BASE_TIMESTAMP);
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }
    
    public static int id2DayNum(Long id){
        if(null == id)
            return 0;
        return (int)(id / (24 * 60 * 60 * 1000));
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(IdManager.generateId());
        
    }
    
}
