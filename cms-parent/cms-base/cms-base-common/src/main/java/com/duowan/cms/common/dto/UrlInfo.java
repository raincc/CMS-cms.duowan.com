/**
 * 
 */
package com.duowan.cms.common.dto;

/**
 * 文章或者模板静态页面连接的信息
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-11
 * <br>==========================
 */
public class UrlInfo {
    
    public final static int TYPE_ARTICLE = 1;
    
    public final static int TYPE_TEMPLATE = 2;
    
    /**
     * 链接类型，0表示文章，1表示模板
     */
    private int type;
    
    
    /**
     * 文章/模板 ID
     */
    private Long id;
    
    private UrlInfo(){};
    
    public UrlInfo(int type, Long id) {
        this.type = type;
        this.id = id;
    }

    
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
