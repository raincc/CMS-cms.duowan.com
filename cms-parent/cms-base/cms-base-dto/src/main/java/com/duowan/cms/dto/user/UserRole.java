package com.duowan.cms.dto.user;


/**
 * 用户角色
 * @author coolcooldee
 * @version 1.0
 * @created 10-九月-2012 13:45:55
 */
public enum UserRole implements UserRoleInterface {

    /**
     * 管理员(a)
     */
    ADMINISTRATOR {
        public String getDisplay() {
            return "管理员";
        }
        public String getValue() {
            return "a";
        }
        public String getExactValue() {
            return "1111111111111111111111111111111";
        }
        public int getPowerValue() {
            return 9;
        }
    },
    /**
     * 美术组
     */
    ARTIST {
        public String getDisplay() {
            return "美术组";
        }
        public String getValue() {
            return "d";
        }
        public String getExactValue() {
            return "1111111111111111111111111111100";
        }
        @Override
        public int getPowerValue() {
            return 6;
        }
    },
    /**
     * 主编辑(h)
     */
    MAIN_EDITOR {
        public String getDisplay() {
            return "主编辑";
        }
        public String getValue() {
            return "h";
        }
        public String getExactValue() {
            return "1111111111111111111111111111111";
        }
        @Override
        public int getPowerValue() {
            return 7;
        }
    },
    /**
     * 普通编辑(o)
     */
    EDITOR {
        public String getDisplay() {
            return "编辑";
        }
        public String getValue() {
            return "o";
        }
        public String getExactValue() {
            return "1110111111111111111111111111101";
        }
        @Override
        public int getPowerValue() {
            return 5;
        }
    },
    /**
     * 实习生/兼职
     */
    BEGINNER {
        public String getDisplay() {
            return "实习生/兼职";
        }
        public String getValue() {
            return "b";
        }
        public String getExactValue() {
            return "1100000000000000000000000000000";
        }
        @Override
        public int getPowerValue() {
            return 1;
        }
    };

    public static UserRole getInstance(String value) throws IllegalArgumentException {

        if ("a".equals(value))
            return ADMINISTRATOR;
        else if ("h".equals(value))
            return MAIN_EDITOR;
        else if ("o".equals(value))
            return EDITOR;
        else if ("d".equals(value))
            return ARTIST;
        else if ("b".equals(value))
            return BEGINNER;
        else
            throw new IllegalArgumentException("Argument value is illegal");
    }
}