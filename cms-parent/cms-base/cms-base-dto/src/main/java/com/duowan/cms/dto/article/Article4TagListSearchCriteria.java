/**
 * 
 */
package com.duowan.cms.dto.article;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.duowan.cms.common.dto.EnumInterface;
import com.duowan.cms.common.dto.PageSearchCriteria;
import com.duowan.cms.common.util.StringUtil;

/**
 * 文章查询条件
 */
public class Article4TagListSearchCriteria extends PageSearchCriteria {

    private static final long serialVersionUID = 4075628384996692628L;

    /**
     * 按照频道ID查询
     */
    private String channelId;

    /**
     * 按照文章标题查询
     */
    private String title;

    /**
     * 作者
     */
    private String author;
    /**
     * 发布者ID/编辑
     */
    private String userId;
    /**
     * 来源
     */
    private String source;

    /**
     * 是否有头图
     */
    private String isHeadPic;


    /**
     * 权重区间 下限
     */
    private Integer powerFrom;

    /**
     * 权重区间 上限
     */
    private Integer powerTo;
    /**
     * 最后更新时间的开始时间
     */
    private Date updateTimeStart;
    /**
     * 最后更新时间的结束时间
     */
    private Date updateTimeEnd;

    /**
     * 发布时间的开始时间（针对于查询区间的开始时间）
     */
    private Date publishTimeStart;

    /**
     *  发布时间的结束时间（针对于查询区间的结束时间）
     */
    private Date publishTimeEnd;
    
    private boolean delete = false;
    
    private int status = 0;

    /**
     * 标签列表
     */
    private List<String> tagList;

    /**
     * 排序字段集
     */
    //private List<Article4TagListOrderBy> orderByList;
    
    private List<String> orderByStrList;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getIsHeadPic() {
        return isHeadPic;
    }

    public void setIsHeadPic(String isHeadPic) {
        this.isHeadPic = isHeadPic;
    }


    public Integer getPowerFrom() {
        return powerFrom;
    }

    public void setPowerFrom(Integer powerFrom) {
        this.powerFrom = powerFrom;
    }

    public Integer getPowerTo() {
        return powerTo;
    }

    public void setPowerTo(Integer powerTo) {
        this.powerTo = powerTo;
    }

    public Date getUpdateTimeStart() {
        return updateTimeStart;
    }

    public void setUpdateTimeStart(Date updateTimeStart) {
        this.updateTimeStart = updateTimeStart;
    }

    public Date getUpdateTimeEnd() {
        return updateTimeEnd;
    }

    public void setUpdateTimeEnd(Date updateTimeEnd) {
        this.updateTimeEnd = updateTimeEnd;
    }

    public List<String> getTagList() {
        return tagList;
    }
    
    public Date getPublishTimeStart() {
        return publishTimeStart;
    }

    public void setPublishTimeStart(Date publishTimeStart) {
        this.publishTimeStart = publishTimeStart;
    }

    public Date getPublishTimeEnd() {
        return publishTimeEnd;
    }

    public void setPublishTimeEnd(Date publishTimeEnd) {
        this.publishTimeEnd = publishTimeEnd;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
    
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Article4TagListSearchCriteria addTags(String tags) {
        if (!StringUtil.isEmpty(tags)) {
            for (String str : tags.split(",")) {
                if (!StringUtil.isEmpty(str)) {
                    if (null == this.tagList) {
                        this.tagList = new ArrayList<String>();
                    }
                    this.tagList.add(str);
                }
            }
        }
        return this;
    }

    public Article4TagListSearchCriteria addTags(String... tags) {
        for (String str : tags) {
            addTags(str);
        }
        return this;
    }

    public Article4TagListSearchCriteria addTags(List<String> tags) {
        for (String str : tags) {
            addTags(str);
        }
        return this;
    }

    public List<String> getOrderByStrList() {
        
        if(null == orderByStrList || orderByStrList.isEmpty()){
            return null;
        }
        
        return orderByStrList;
    }
    public static enum Article4TagListOrderBy implements EnumInterface {
        DAY_NUM_DESC {
            @Override
            public String getDisplay() {
                return "按字段daynum(距2006-01-01的天数)降序排序";
            }

            @Override
            public String getValue() {
                return "daynum desc";
            }
        },
        DAY_NUM_ASC {
            @Override
            public String getDisplay() {
                return "按字段daynum(距2006-01-01的天数)升序排序";
            }

            @Override
            public String getValue() {
                return "daynum asc";
            }
        },
        POWER_DESC {
            @Override
            public String getDisplay() {
                return "按字段power(权重)降序排序";
            }

            @Override
            public String getValue() {
                return "power desc";
            }

        },
        UPDATE_TIME_DESC {
            @Override
            public String getDisplay() {
                return "按字段updatetime(更新时间)字段降序排序";
            }

            @Override
            public String getValue() {
                return "updatetime desc";
            }
        },POST_TIME_DESC {
            @Override
            public String getDisplay() {
                return "按字段posttime(发布时间)字段降序排序";
            }

            @Override
            public String getValue() {
                return "posttime desc";
            }
        };

    }

    public Article4TagListSearchCriteria addOrderBy(Article4TagListOrderBy orderBy) {
        if (null != orderBy) {
            if (this.orderByStrList == null) {
                this.orderByStrList = new ArrayList<String>();
            }
            this.orderByStrList.add(orderBy.getValue());
        }
        return this;
    }

    

}
