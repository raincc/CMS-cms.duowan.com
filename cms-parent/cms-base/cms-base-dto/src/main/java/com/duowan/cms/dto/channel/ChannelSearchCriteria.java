/**
 * 
 */
package com.duowan.cms.dto.channel;

import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * @author coolcooldee
 *
 */
public class ChannelSearchCriteria  extends PageSearchCriteria {
    
    private static final long serialVersionUID = 3513726768600303473L;

    private String id;
    
    private String name;
    
    private Boolean important;
    
    private Boolean upline;
    
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getImportant() {
        return important;
    }

    public void setImportant(Boolean important) {
        this.important = important;
    }

    public Boolean getUpline() {
        return upline;
    }

    public void setUpline(Boolean upline) {
        this.upline = upline;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
