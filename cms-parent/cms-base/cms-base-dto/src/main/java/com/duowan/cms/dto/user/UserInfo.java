package com.duowan.cms.dto.user;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.DateUtil;


/**
 * 用户
 *
 */
public class UserInfo  implements DataTransferObject{

    /**
     * 用户唯一标识
     */
    private String userId;
    /**
     * 密码
     */
    private String password;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后修改时间
     */
    private Date lastModifyTime;
    /**
     * 用户操作机器的ip地址
     */
    private String userIp;
    /**
     * 用户操作机器代理IP
     */
    private String proxyIp;
    /**
     * 用户状态
     */
    private UserStatus userStatus;
    /**
     * 邮箱地址
     */
    private String mail;
    
    /**
     * 是否管理员（指的是系统管理员，与具体某个专区的管理员权限不同）
     */
    private boolean admin;
    
    private String udbName;
    
    /**
     * 个人所拥有的所有频道的权限信息（暂时未使用）
     */
    private List <UserPowerInfo> allChannelPowerInfo;
    
    private UserPowerInfo singleChannelPowerInfo;

	public List<UserPowerInfo> getAllChannelPowerInfo() {
		return allChannelPowerInfo;
	}
	public void setAllChannelPowerInfo(List<UserPowerInfo> allChannelPowerInfo) {
		this.allChannelPowerInfo = allChannelPowerInfo;
	}
	public UserPowerInfo getSingleChannelPowerInfo() {
		return singleChannelPowerInfo;
	}
	public void setSingleChannelPowerInfo(UserPowerInfo singleChannelPowerInfo) {
		this.singleChannelPowerInfo = singleChannelPowerInfo;
	}
	public String getUserId() {
        return userId;
    }
    public String getCreateTimeStr() {
        return DateUtil.format(createTime, DateUtil.defaultDateTimePatternStr) ;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLastModifyTimeStr() {
    	if(lastModifyTime == null){
    		return "";
    	}
    	
        return DateUtil.format(lastModifyTime, DateUtil.defaultDateTimePatternStr) ;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getProxyIp() {
        return proxyIp;
    }

    public void setProxyIp(String proxyIp) {
        this.proxyIp = proxyIp;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    public boolean isAdmin() {
        return admin;
    }
    public boolean getIsAdmin() {
        return admin;
    }
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    public String getUdbName() {
        return udbName;
    }
    public void setUdbName(String udbName) {
        this.udbName = udbName;
    }
    
}
