/**
 * 
 */
package com.duowan.cms.dto.article;

import java.util.Date;

import com.duowan.cms.common.dto.PageSearchCriteria;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.worklog.WorkLogSearchCriteria;

/**
 * 文章查询条件
 */
public class ArticleCountSearchCriteria extends PageSearchCriteria {

    private static final long serialVersionUID = 1452572430216941290L;
    /**
     * 按照频道ID查询
     */
    private String channelId;
    /**
     * 发布者ID/编辑
     */
    private String userId;
    /**
     * 查询特定某天
     */
    private Date date;
    
    /**
     * 查询特定时间段的开始时间
     */
    private Date startDate;
    /**
     * 查询特定时间段的结束时间
     */
    private Date endDate;
    
    public ArticleCountSearchCriteria updateByWorkLogSearchCriteria(WorkLogSearchCriteria workLogSearchCriteria){
        if(!StringUtil.isEmpty(workLogSearchCriteria.getUserId())){
            this.userId = workLogSearchCriteria.getUserId();
        }
    	this.startDate = workLogSearchCriteria.getStartDate();
    	this.endDate = workLogSearchCriteria.getEndDate();
    	this.setPageNo(workLogSearchCriteria.getPageNo());
    	this.setPageSize(workLogSearchCriteria.getPageSize());
    	return this;
    }
    

    public String getStartDateStr(){
        if(null == this.startDate)
            return null;
        return DateUtil.format(this.startDate, "yyyy-MM-dd");
    }
    
    public String getEndDateStr(){
        if(null == this.endDate)
            return null;
        return DateUtil.format(this.endDate, "yyyy-MM-dd");
    }
    
    
    public String getDateStr(){
        if(null == this.date)
            return null;
        return DateUtil.format(this.date, "yyyy-MM-dd");
    }
    
//******************普通的getter和setter************************    
    public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getChannelId() {
        return channelId;
    }
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    public String getUserId() {
    	if (StringUtil.isEmpty(userId)) {
			return null;
		}
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
}
