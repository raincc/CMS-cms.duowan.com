package com.duowan.cms.dto.worklog;

import java.util.Date;

import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.dto.article.ArticleCountInfo;
import com.duowan.cms.dto.channel.ChannelInfo;

public class WorkLogInfo {

    private String channelId;
    
    private ChannelInfo channelInfo;
    
    private String userId;
    
    private Date date;
    
    private Integer count;
    
    public WorkLogInfo updateByArticleCountInfo(ArticleCountInfo articleCountInfo){
    	this.channelId = articleCountInfo.getChannelId();
    	this.userId = articleCountInfo.getUserId();
    	this.date = articleCountInfo.getDate();
    	this.count = articleCountInfo.getCount();
    	return this;
    }
    
    public String getDateStr() {
    	if(date == null){
    		return "";
    	}
        return DateUtil.format(date, DateUtil.defaultDatePatternStr);
    }
    
    
    
//*********************普通的getter和setter***************************************
    
	public String getChannelId() {
		return channelId;
	}

	public ChannelInfo getChannelInfo() {
		return channelInfo;
	}

	public void setChannelInfo(ChannelInfo channelInfo) {
		this.channelInfo = channelInfo;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
    
    
}
