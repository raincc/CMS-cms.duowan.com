package com.duowan.cms.dto.user;

import com.duowan.cms.common.dto.DataTransferObject;

public class PowerInfo implements DataTransferObject {
    
    private static final long serialVersionUID = 1L;
    
    /**
     * 权限的唯一标识
     */
    String id;
    /**
     * 权限名字
     */
    String name;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public PowerInfo(String id, String name) {
        this.id = id;
        this.name = name;
    }
    
}
