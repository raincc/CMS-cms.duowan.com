package com.duowan.cms.dto.zone;

import com.duowan.cms.common.dto.DataTransferObject;

public class ZoneUserInfo  implements DataTransferObject {

    private static final long serialVersionUID = -4459363484777726476L;
    private Long id;
    private String channelId;
    private String userName;
    private String nickName;
    private String defaultTag;
    private String isDel;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getChannelId() {
        return channelId;
    }
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getDefaultTag() {
        return defaultTag;
    }
    public void setDefaultTag(String defaultTag) {
        this.defaultTag = defaultTag;
    }
    public String getIsDel() {
        return isDel;
    }
    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }
    
    
}
