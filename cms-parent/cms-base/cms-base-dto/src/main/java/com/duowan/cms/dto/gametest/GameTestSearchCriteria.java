/**
 * 
 */
package com.duowan.cms.dto.gametest;

import java.util.ArrayList;
import java.util.List;

import com.duowan.cms.common.dto.EnumInterface;
import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * @author yzq
 *
 */
public class GameTestSearchCriteria  extends PageSearchCriteria {
    /**
     * 
     */
    private static final long serialVersionUID = -3663436916151468075L;

    private Integer ctype;
    
    private String timeStart;
    
    private String timeEnd;
    
    private String abroad;
    
    private Integer powerFrom;
    
    private Integer powerTo;
    
    private String hasPay;
    
    private String freeGame;
    
    private String pubCheck;
    
    private String state;
    
    private String indexCheck;
    
    private String abroadNot;
    
    private List<GameTestOrderBy> orderByList;

    public Integer getCtype() {
        return ctype;
    }

    public void setCtype(Integer ctype) {
        this.ctype = ctype;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getAbroad() {
        return abroad;
    }

    public void setAbroad(String abroad) {
        this.abroad = abroad;
    }
    
    public Integer getPowerFrom() {
        return powerFrom;
    }

    public void setPowerFrom(Integer powerFrom) {
        this.powerFrom = powerFrom;
    }

    public Integer getPowerTo() {
        return powerTo;
    }

    public void setPowerTo(Integer powerTo) {
        this.powerTo = powerTo;
    }

    public String getHasPay() {
        return hasPay;
    }

    public void setHasPay(String hasPay) {
        this.hasPay = hasPay;
    }

    public String getFreeGame() {
        return freeGame;
    }

    public void setFreeGame(String freeGame) {
        this.freeGame = freeGame;
    }

    public String getPubCheck() {
        return pubCheck;
    }

    public void setPubCheck(String pubCheck) {
        this.pubCheck = pubCheck;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIndexCheck() {
        return indexCheck;
    }

    public void setIndexCheck(String indexCheck) {
        this.indexCheck = indexCheck;
    }
    
    public String getAbroadNot() {
        return abroadNot;
    }

    public void setAbroadNot(String abroadNot) {
        this.abroadNot = abroadNot;
    }

    public List<GameTestOrderBy> getOrderByList() {
        if(null == orderByList || orderByList.isEmpty())
            return null;
        return orderByList;
    }

    public void setOrderByList(List<GameTestOrderBy> orderByList) {
        this.orderByList = orderByList;
    }

    public GameTestSearchCriteria addOrderBy(GameTestOrderBy gameTestOrderBy) {

        if (null != gameTestOrderBy) {
            if (this.orderByList == null) {
                this.orderByList = new ArrayList<GameTestOrderBy>();
            }
            this.orderByList.add(gameTestOrderBy);
        }
        return this;
    }

    public static enum GameTestOrderBy implements EnumInterface {
        TIME_DESC {
            @Override
            public String getDisplay() {
                return "按字段time降序排序";
            }

            @Override
            public String getValue() {
                return "time desc";
            }
        },
        TIME_ASC {
            @Override
            public String getDisplay() {
                return "按字段time升序排序";
            }

            @Override
            public String getValue() {
                return "time asc";
            }
        },
        POWER_DESC {
            @Override
            public String getDisplay() {
                return "按字段power降序排序";
            }

            @Override
            public String getValue() {
                return "power desc";
            }

        },
        ID_DESC {
            @Override
            public String getDisplay() {
                return "按字段gametestid降序排序";
            }

            @Override
            public String getValue() {
                return "gametestid desc";
            }
        };

    }
    
}
