package com.duowan.cms.dto.worklog;

import java.text.ParseException;
import java.util.Date;

import com.duowan.cms.common.dto.PageSearchCriteria;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;

public class WorkLogSearchCriteria extends PageSearchCriteria {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6501240594794669870L;

	/*
	 * 频道名
	 */
	private String channelName;
    /**
     * 用户名
     */
    private String userId;

    /**
     * 查询开始时间的字符串形式
     */
    private String startDateStr;
    /**
     * 查询结束时间的字符串形式
     */
    private String endDateStr;
    /**
     * 文章数
     */
    private Integer count;

	public Date getStartDate() {
		if (StringUtil.isEmpty(startDateStr)) {
			return null;
		}
		try {
			return DateUtil.parse(startDateStr, "yyyy-MM-dd");
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	public Date getEndDate() {
		if (StringUtil.isEmpty(endDateStr)) {
			return null;
		}
		try {
			return DateUtil.parse(endDateStr, "yyyy-MM-dd");
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//******************普通的getter和setter************************    
	
	public String getChannelName() {
		return channelName;
	}

	public String getStartDateStr() {
	    if(StringUtil.isEmpty(startDateStr))
	        return "";
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
	    if(StringUtil.isEmpty(endDateStr))
            return "";
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getUserId() {
	    if(StringUtil.isEmpty(userId))
            return "";
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
    
    
}
