package com.duowan.cms.dto.article;

/**
 * 文章状态
 * 
 * @author coolcooldee
 * @version 1.0
 * @created 04-九月-2012 11:44:15
 */
public enum ArticleStatus {
    /**
     * 普通
     */
    NORMAL {
    // public String getDisplay() {
    // return "普通文章";
    // }

    // public String getValue() {
    // return "0";
    // }

    },
    /**
     * 预定发布
     */
    PER_PUBLISH {
    // public String getDisplay() {
    // return "预定发布的文章";
    // }
    // public String getValue() {
    // return "1";
    // }

    },
    /**
     * 未打标签的
     */
    NO_TAG {
    // public String getDisplay() {
    // return "未打标签的文章";
    // }
    // public String getValue() {
    // return "2";
    // }

    },
    /**
     * 抓取的
     */
    CATCHED {
    // public String getDisplay() {
    // return "抓取的文章";
    // }
    // public String getValue() {
    // return "3";
    // }

    },
    /**
     * 用户投递
     */
    USER_DELIVERY {
    // public String getDisplay() {
    // return "用户投递的文章";
    // }
    // public String getValue() {
    // return "4";
    // }

    },
    /**
     * 已删除
     */
    DELETED {
    // public String getDisplay() {
    // return "已删除的文章";
    // }
    // public String getValue() {
    // return "99";
    // }

    };

    public static ArticleStatus getInstance(String value) throws IllegalArgumentException {
        if ("0".equals(value))
            return NORMAL;
        else if ("1".equals(value))
            return PER_PUBLISH;
        else if ("2".equals(value))
            return NO_TAG;
        else if ("3".equals(value))
            return CATCHED;
        else if ("4".equals(value))
            return USER_DELIVERY;
        else if ("99".equals(value))
            return DELETED;
        else
            throw new IllegalArgumentException("Argument value is illegal");
    }

    public String getDisplay() {
        switch (this) {
        case NORMAL:
            return "普通文章";
        case PER_PUBLISH:
            return "预定发布的文章";
        case NO_TAG:
            return "未打标签的文章";
        case CATCHED:
            return "抓取的文章";
        case USER_DELIVERY:
            return "用户投递的文章";
        case DELETED:
            return "已删除的文章";
        default:
            return "未知状态的文章";
        }
    }

    public String getValue() {
        switch (this) {
        case NORMAL:
            return "0";
        case PER_PUBLISH:
            return "1";
        case NO_TAG:
            return "2";
        case CATCHED:
            return "3";
        case USER_DELIVERY:
            return "4";
        case DELETED:
            return "99";
        default:
            return "-1";
        }
    }
    
    public static void main(String[] args) {
        System.out.println(ArticleStatus.CATCHED.getDisplay());
    }

}