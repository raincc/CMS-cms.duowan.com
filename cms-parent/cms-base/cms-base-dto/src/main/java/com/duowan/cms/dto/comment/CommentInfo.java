/**
 * 
 */
package com.duowan.cms.dto.comment;

import com.duowan.cms.common.dto.DataTransferObject;

/**
 * 评论信息
 * @author coolcooldee
 */
public class CommentInfo  implements DataTransferObject {
    /**
     * 唯一标识
     */
    long threadId;
    
    /**
     * 所在频道
     */
    String channelId;
    
    /**
     * 关联的文章的标题
     */
    String articleTitle;
    
    /**
     * 评论数
     */
    private int count;

    public long getThreadId() {
        return threadId;
    }

    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }
    
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getString(String key){
        
        if("replys".equals(key)){
            return count + "";
        }
        
        return "";
    }
    
}
