package com.duowan.cms.dto.hotword;

import com.duowan.cms.common.dto.DataTransferObject;

/**
 * 
 */
public class HotWordInfo implements DataTransferObject {

    /**
     * 
     */
    private static final long serialVersionUID = -2523421390959632675L;

    private String channelId;

    private String name;

    private String href;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
