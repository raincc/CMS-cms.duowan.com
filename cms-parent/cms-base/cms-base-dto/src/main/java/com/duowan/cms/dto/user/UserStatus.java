package com.duowan.cms.dto.user;

import com.duowan.cms.common.dto.EnumInterface;

/**
 * 用户状态
 * 
 * @author coolcooldee
 * @version 1.0
 * @created 04-九月-2012 13:50:17
 */
public enum UserStatus implements EnumInterface {
    /**
     * 正常
     */
    NORMAL {
        @Override
        public String getDisplay() {
            return "正常";
        }
        
        @Override
        public String getValue() {
            return "1";
        }
    },
    /**
     * 被冻结
     */
    FREEZED {
        @Override
        public String getDisplay() {
            return "冻结";
        }
        
        @Override
        public String getValue() {
            return "0";
        }
    };
    
    public static UserStatus getInstance(String value)
            throws IllegalArgumentException {
        if ("0".equals(value))
            return FREEZED;
        else if ("1".equals(value))
            return NORMAL;
        else
            throw new IllegalArgumentException("Argument value is illegal");
    }
    
}