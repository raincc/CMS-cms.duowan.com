package com.duowan.cms.dto.article;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * 文章
 *  alter 2013-03-07: 多问(Y世界),有爱项目关闭 
 */
public class ArticleInfo implements DataTransferObject {

    private static final long serialVersionUID = -2721061530305732410L;

    /**
     * 唯一标识符
     */
    private Long id;

    /**
     * 频道id
     */
    private ChannelInfo channelInfo;

    /**
     * 标题
     */
    private String title;
    /**
     * 标题颜色
     */
    private String titleColor;
    /**
     * 子标题
     */
    private String subtitle;
    /**
     * 摘要
     */
    private String digest;
    /**
     * 图片地址
     */
    private String pictureUrl;
    /**
     * 标签图图片地址
     */
    private String tagPictureUrl;
    /**
     * 来源
     */
    private String source;
    /**
     * 作者
     */
    private String author;
    /**
     * 发布时间（精确到秒）
     */
    private Date publishTime;
    /**
     * 预定发布时间（精确到秒）
     */
    private Date prePublishTime;
    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;
    /**
     * 标签列表（多个标签使用“，”分割），所有的标签
     */
    private String tags;
    /**
     * 发布者ID
     */
    private String userId;
    /**
     * 最后修改者ID
     */
    private String lastUpdateUserId;
    /**
     * 用户IP地址
     */
    private String userIp;
    /**
     * 用户使用的代理IP地址
     */
    private String proxyIp;
    /**
     * 文章的状态(0普通，1预定* 2未加tag,3抓取,4用户投稿,99删除)
     */
    private ArticleStatus status;
    /**
     * 文章的操作记录
     */
    private String logs;
    /**
     * 详细内容
     */
    private String content;
    /**
     * 天数
     */
    private Integer dayNum;
    /**
     * 文章权重，一般用于排序
     */
    private Integer power = 60;
    /**
     * 对应的评论帖子ID
     */
    private Long threadId;
    /**
     * 模板ID
     */
    private Long templateId;
    /**
     * 相关联的标签，所有标签中去除特殊标签
     */
    private String realtags;
    /**
     * 是否有头图
     */
    private String isHeadPic;
    /**
     * 自定义内容1
     */
    private String diy1;
    /**
     * 自定义内容2
     */
    private String diy2;
    /**
     * 自定义内容3
     */
    private String diy3;
    /**
     * 自定义内容4
     */
    private String diy4;
    /**
     * 自定义内容5
     */
    private String diy5;
    /**
     * 文章抓取的URL
     */
    private String catchUrl;
    /**
     * 正式环境的url:外网访问的url. 使用到的地方:点击滚动文章列表，具体文章的标题旁的"实心五角星"访问的url
     * 举例:urlOnLine的格式类似http://wow.duowan.com/1209/211821276072.html这样的链接
     */
    private String urlOnLine;

    /**
     * 发布器服务器上的url. 使用到的地方:点击滚动文章列表，具体文章的标题旁的"空心五角星"访问的url
     * 举例:urlOnCms的格式类似http://cms.duowan.com/wow/1209/211821276072.html这样的链接
     */
    private String urlOnCms;

    /**
     * 是否需要评论（用于接收表单）
     */
    private boolean needComment;

    /**
     * 评论URL（用于接收表单）
     */
    private String commentUrl;


    /**
     * 是否同步到Y世界
     */
    //private String isSyncQa;
    /**
     * 同步到wap资讯
     */
    private String isSyncWap;
    /**
     * 同步到有爱
     */
    //private String isSyncZone;
    /**
     * 有爱用户id
     */
    private String zoneUserId;
    /**
     * 相关游戏
     */
    private String relateGame;

    /**
     * 是否需要单页显示全部
     */
    private String isShowAllPage;
    /**
     * 是否修改所有tag
     */
    private String isChangeAllTags;
    
    
    public boolean isChangeAllTags() {
        return "on".equals(isChangeAllTags);
    }
    
    /**
     * 是否推送到wap，如果页面勾上，就返回true;否则返回false;
     */
    public boolean isSyncWap() {
        return "on".equals(isSyncWap);
    }


    /**
     * 是否使用下拉分页，如果页面勾上，就返回true;否则返回false;
     */
    public boolean isShowAllPage() {
        return "on".equals(isShowAllPage);
    }

    public boolean getHasPrePublishTime() {
        if (this.prePublishTime == null) {
            return false;
        }
        return true;
    }

    public String getPrePublishTimeStr() {
    	if(prePublishTime == null){
    		return "";
    	}
        return DateUtil.format(prePublishTime, DateUtil.defaultDateTimePatternStr);
    }

    public boolean getIsEmptyStatus(){
    	return status==null;
    }

    /**
     * 判断自定义4是否为空
     */
    public boolean getIsEmptyDiy4() {
        return StringUtil.isEmpty(diy4);
    }

    /**
     * 判断自定义5是否为空
     */
    public boolean getIsEmptyDiy5() {
        return StringUtil.isEmpty(diy5);
    }
    
    public boolean getIsTagsEmpty(){
    	return StringUtil.isEmpty(tags);
    }

    public String[] getTagArr() {
        if ("".equals(tags) || tags == null) {
            return new String[] {};
        }
        return tags.split(",");
    }
    
    public String getPublishTimeStr() {
    	if(publishTime == null){
    		return "";
    	}
        return DateUtil.format(publishTime, DateUtil.defaultDateTimePatternStr);
    }
    
    /**
     * 对应模板语法中的 文章的getString()语法
     * @return
     */
    public String getString(String key) {
        if ("picurl".equals(key)) {
            return this.getPictureUrl();
        } else if ("title".equals(key)) {
            return this.getTitle();
        } else if ("articleid".equals(key)) {
            return this.getId().toString();
        } else if ("subtitle".equals(key)) {
            return this.getSubtitle();
        } else if ("digest".equals(key)) {
            return this.getDigest();
        } else if ("picurl".equals(key)) {
            return this.getPictureUrl();
        } else if ("source".equals(key)) {
            return this.getSource();
        } else if ("author".equals(key)) {
            return this.getAuthor();
        } else if ("ordertime".equals(key)) {
            if (this.getPrePublishTime() == null)
                return null;
            return DateUtil.format(this.getPrePublishTime(), "MM-dd");
        } else if ("posttime".equals(key)) {
            return DateUtil.format(this.getPublishTime(), "MM-dd");
        } else if ("updatetime".equals(key)) {
            return DateUtil.format(this.getLastUpdateTime(), "MM-dd");
        } else if ("tags".equals(key)) {
            return this.getTags();
        } else if ("userid".equals(key)) {
            return this.getUserId();
        } else if ("userip".equals(key)) {
            return this.getUserIp();
        } else if ("proxyip".equals(key)) {
            return this.getProxyIp();
        } else if ("status".equals(key)) {
            return this.getStatus().getDisplay();
        } else if ("content".equals(key)) {
            return this.getContent();
        } else if ("daynum".equals(key)) {
            return this.getDayNum() + "";
        } else if ("power".equals(key)) {
            return this.getPower() + "";
        } else if ("templateid".equals(key)) {
            return this.getTemplateId() + "";
        } else if ("realtags".equals(key)) {
            return this.getRealtags();
        } else if ("diy1".equals(key)) {
            return this.getDiy1();
        } else if ("diy2".equals(key)) {
            return this.getDiy2();
        } else if ("diy3".equals(key)) {
            return this.getDiy3();
        } else if ("diy4".equals(key)) {
            return this.getDiy4();
        } else if ("diy5".equals(key)) {
            return this.getDiy5();
        } else if ("tag".equals(key)) {
            return this.getTags();
        } else if ("ispic".equals(key)) {
            return this.isHeadPic;
        } else if ("sontag".equals(key)) {
            // return this.isHeadPic;
        } else if ("sontagid".equals(key)) {
            // return this.isHeadPic;
        } else if ("updateuserid".equals(key)) {
            return this.getLastUpdateUserId();
        } else if ("logs".equals(key)) {
            return this.getLogs() + "";
        } else if ("catchurl".equals(key)) {
            return this.getCatchUrl() + "";
        } else if ("threadid".equals(key)) {
            return this.getThreadId() + "";
        }else if("titleColor".equals(key)){
        	return this.getTitleColor() + "";
        }
        return null;
    }

    public String getLong(String key) {
        if ("daynum".equals(key)) {
            return this.getDayNum() + "";
        } else if ("articleid".equals(key)) {
            return this.getId() + "";
        } else if ("templateid".equals(key)) {
            return this.getTemplateId() + "";
        }
        return null;
    }

    public String getInt(String key) {
        if ("power".equals(key)) {
            return this.getPower() + "";
        } else if ("status".equals(key)) {
            return this.getStatus().getValue();
        }
        return null;
    }

    
  //******************普通的getter和setter************************

    
    public String getIsChangeAllTags() {
		return isChangeAllTags;
	}

	public String getTitleColor() {
		return titleColor;
	}

	public void setTitleColor(String titleColor) {
		this.titleColor = titleColor;
	}

	public void setIsChangeAllTags(String isChangeAllTags) {
		this.isChangeAllTags = isChangeAllTags;
	}


    public String getIsSyncWap() {
        return isSyncWap;
    }

    public void setIsSyncWap(String isSyncWap) {
        this.isSyncWap = isSyncWap;
    }

	public String getZoneUserId() {
		return zoneUserId;
	}

	public void setZoneUserId(String zoneUserId) {
		this.zoneUserId = zoneUserId;
	}

    public String getDiy4() {
        return diy4;
    }

    public void setDiy4(String diy4) {
        this.diy4 = diy4;
    }

    public String getDiy5() {
        return diy5;
    }

    public void setDiy5(String diy5) {
        this.diy5 = diy5;
    }

    public String getUrlOnLine() {
        return this.urlOnLine;
    }

    public String getUrlOnCms() {
        return this.urlOnCms;
    }

    public void setUrlOnLine(String urlOnLine) {
        this.urlOnLine = urlOnLine;
    }

    public void setUrlOnCms(String urlOnCms) {
        this.urlOnCms = urlOnCms;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Date getPrePublishTime() {
        return prePublishTime;
    }

    public void setPrePublishTime(Date prePublishTime) {
        this.prePublishTime = prePublishTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public String getLastUpdateTimeStr() {
        return DateUtil.format(lastUpdateTime, DateUtil.defaultDateTimePatternStr);
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    public void setLastUpdateUserId(String lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getProxyIp() {
        return proxyIp;
    }

    public void setProxyIp(String proxyIp) {
        this.proxyIp = proxyIp;
    }

    public ArticleStatus getStatus() {
        return status;
    }

    public void setStatus(ArticleStatus status) {
        this.status = status;
    }

    public String getLogs() {
        return logs;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getDayNum() {
        return dayNum;
    }

    public void setDayNum(Integer dayNum) {
        this.dayNum = dayNum;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Long getThreadId() {
        return threadId;
    }

    public void setThreadId(Long threadId) {
        this.threadId = threadId;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getRealtags() {
        return realtags;
    }

    public void setRealtags(String realtags) {
        this.realtags = realtags;
    }

    public String getIsHeadPic() {
        return isHeadPic;
    }

    public void setIsHeadPic(String isHeadPic) {
        this.isHeadPic = isHeadPic;
    }

    public String getDiy1() {
        return diy1;
    }

    public void setDiy1(String diy1) {
        this.diy1 = diy1;
    }

    public String getDiy2() {
        return diy2;
    }

    public void setDiy2(String diy2) {
        this.diy2 = diy2;
    }

    public String getDiy3() {
        return diy3;
    }

    public void setDiy3(String diy3) {
        this.diy3 = diy3;
    }

    public String getCatchUrl() {
        return catchUrl;
    }

    public void setCatchUrl(String catchUrl) {
        this.catchUrl = catchUrl;
    }

    public boolean isNeedComment() {
        return needComment;
    }

    public void setNeedComment(boolean needComment) {
        this.needComment = needComment;
    }

    public String getCommentUrl() {
        return commentUrl;
    }

    public void setCommentUrl(String commentUrl) {
        this.commentUrl = commentUrl;
    }

    public String getTagPictureUrl() {
        return tagPictureUrl;
    }

    public void setTagPictureUrl(String tagPictureUrl) {
        this.tagPictureUrl = tagPictureUrl;
    }


    public String getRelateGame() {
        return relateGame;
    }

    public void setRelateGame(String relateGame) {
        this.relateGame = relateGame;
    }

    public String getIsShowAllPage() {
        return isShowAllPage;
    }

    public void setIsShowAllPage(String isShowAllPage) {
        this.isShowAllPage = isShowAllPage;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ArticleInfo other = (ArticleInfo) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }



}
