/**
 * 
 */
package com.duowan.cms.dto.channel;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.FileUtil;

/**
 * @author coolcooldee
 *
 */
public class ChannelInfo  implements DataTransferObject {
   
    private static final long serialVersionUID = 581409907912679078L;
    
    public static final String[] CATEGORY_LIST = {"重点", "二线", "页游", "其他"};

    /**
     * 频道的唯一标识
     */
    String id;
    
    /**
     * 频道的名称
     */
    String name;
    
    /**
     * 频道的域名
     */
    String domain;
    
    /**
     * 频道文章的存放地址 (上传文章的目录地址)
     */
    String articleFilePath;
    
    /**
     * 图片域名
     */
    String picDomain;
    
    /**
     * 频道图片的存放地址 (上传图片的目录地址)
     */
    String picFilePath;
    
    /**
     * 是否重点专区
     */
    private boolean important;
    
    /**
     * 是否上线（切外网）
     */
    private boolean upline;
    
    /**
     * 分类信息
     */
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getArticleFilePath() {
        return FileUtil.getFilePath(articleFilePath);
    }

    public void setArticleFilePath(String articleFilePath) {
        this.articleFilePath = articleFilePath;
    }

    public String getPicDomain() {
        return picDomain;
    }

    public void setPicDomain(String picDomain) {
        this.picDomain = picDomain;
    }

    public String getPicFilePath() {
        return FileUtil.getFilePath(picFilePath);
    }

    public void setPicFilePath(String picFilePath) {
        this.picFilePath = picFilePath;
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public boolean isUpline() {
        return upline;
    }

    public void setUpline(boolean upline) {
        this.upline = upline;
    }
    
    public String getCategory() {
        if(isCategoryInfo(category)){
            return category;
        }else{
            return "";
        }
    }

    public void setCategory(String category) {
        if(isCategoryInfo(category)){
            this.category = category;
        }
    }
    
    private boolean isCategoryInfo(String category){
        for(String str : CATEGORY_LIST){
            if(str.equals(category)){
                return true;
            }
        }
        return false;
    }

    
    
}
