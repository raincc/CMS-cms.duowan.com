/**
 * 
 */
package com.duowan.cms.dto.template;

import com.duowan.cms.common.dto.EnumInterface;

/**
 * 模板状态
 * @author coolcooldee
 *
 */
public enum TemplateStatus implements EnumInterface {

    /**
     * 普通
     */
    NORMAL {
        public String getDisplay() {
            return "普通模板";
        }

        public String getValue() {
            return "0";
        }
          
    },
    /**
     * 解析有误的模板
     */
    PASER_ERROR{
         public String getDisplay() {
                return "解析有误的模板";
            }   
        public String getValue() {
            return "98";
        }
    },
    /**
     * 已删除
     */
    DELETED{
         public String getDisplay() {
                return "已删除的模板";
            }   
        public String getValue() {
            return "99";
        }

    };
    
    public static TemplateStatus getInstance(String value)
            throws IllegalArgumentException {
        if ("0".equals(value)){
        	 return NORMAL;
        }  else if ("99".equals(value)){
        	  return DELETED;
        }else if ("98".equals(value)){
      	  return PASER_ERROR;
      }else
            throw new IllegalArgumentException("Argument value is illegal，模板类别不正确");
    }
    
}
