package com.duowan.cms.dto.tag;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.PathUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.template.TemplateCategory;

public class TagInfo implements DataTransferObject {

    private static final long serialVersionUID = 2136608846817928069L;

    /**
     * 名字
     */
    private String name;

    /**
     * 频道ID
     */
    private ChannelInfo channelInfo;

    /**
     * 非唯一标识，用于生成tag页面的名字
     */
    private Long id;

    /**
     * 父tag
     */
    private String parentName;

    /**
     * 层级结构
     */
    private String tree;

    /**
     * 创建/更新时间
     */
    private Date updateTime;

    /**
     * tag 类型
     */
    private TagCategory category;

    /**
     * 标签所关联的模板类别，只有两种：标签，标签图
     */
    private TemplateCategory relatedTemplateCategory;

    /**
     * 返回 urlOnLine 是否为空
     */
    public boolean getIsEmptyUrlOnLine() {
        return StringUtil.isEmpty(getUrlOnLine());
    }

    public boolean getIsEmptyUrlOnCms() {
        return StringUtil.isEmpty(getUrlOnCms());
    }

    /**
     * 正式环境的url:外网访问的url. 使用到的地方:标签列表中的具体标签的标题旁的"实心五角星"访问的url
     * 举例:urlOnLine的格式类似http://wow.duowan.com/tag/211821276072.html这样的链接
     */
    public String getUrlOnLine() {
        if (id != null)
            return PathUtil.getTagOnlineUrl(this.getChannelInfo().getDomain(), id);
        return "";
    }

    /**
    * 发布器服务器上的url. 使用到的地方:标签列表中的具体标签的标题旁的"空心五角星"访问的url
    * 举例:urlOnCms的格式类似http://cms.duowan.com/wow/tag/211821276072.html这样的链接
    */
    public String getUrlOnCms() {
        if (id != null )
            return PathUtil.getTagOnCmsUrl(this.getChannelInfo().getId(), id);
        return "";
    }


    /**
     * 标签的层级 
     */
    public int getTagDepth() {
        if (StringUtil.isEmpty(tree)) {
            return 1;
        }
        int a = tree.split(",").length;
        if (tree.startsWith(",")) {
            a = a - 1;
        }
        return a;
    }

    public String getName() {
        return name;
    }
    
    public boolean getRelatedTemplateCategoryExist(){
    	return relatedTemplateCategory != null;
    }

    public boolean getIsTagImage() {
        if (TemplateCategory.TAG_VIEW == this.relatedTemplateCategory)
            return true;
        return false;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }
    
    public String getChannelId(){
        if(null == channelInfo)
            return null;
        return channelInfo.getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getTree() {
        return tree;
    }

    public void setTree(String tree) {
        this.tree = tree;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public TagCategory getCategory() {
        return category;
    }

    public void setCategory(TagCategory category) {
        this.category = category;
    }

    public TemplateCategory getRelatedTemplateCategory() {
        return relatedTemplateCategory;
    }

    public void setRelatedTemplateCategory(TemplateCategory relatedTemplateCategory) {
        this.relatedTemplateCategory = relatedTemplateCategory;
    }

}
