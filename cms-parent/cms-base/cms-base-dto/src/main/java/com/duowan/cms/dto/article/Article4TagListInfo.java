package com.duowan.cms.dto.article;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * 文章
 */
public class Article4TagListInfo implements DataTransferObject {
    /**
     * 
     */
    private static final long serialVersionUID = -662866733377420524L;
    /**
     * 唯一标识符
     */
    private Long id;
    /**
     * 频道id
     */
    private ChannelInfo channelInfo;
    /**
     * 对应一个tag(只对应一个tag)
     */
    private String tag;
    /**
     * 标题
     */
    private String title;
    /**
     * 子标题
     */
    private String subtitle;
    /**
     * 摘要
     */
    private String digest;
    /**
     * 空链接地址
     */
    private String emptyLink;
    /**
     * 头图图片地址
     */
    private String pictureUrl;
    /**
     * 来源
     */
    private String source;
    /**
     * 作者
     */
    private String author;
    /**
     * 发布者ID
     */
    private String userId;
    /**
     * 天数
     */
    private Integer dayNum;
    /**
     * 正式环境的url:外网访问的url. 使用到的地方:点击滚动文章列表，具体文章的标题旁的"实心五角星"访问的url
     * 举例:urlOnLine的格式类似http://wow.duowan.com/1209/211821276072.html这样的链接
     */
    private String urlOnLine;

    /**
     * 发布器服务器上的url. 使用到的地方:点击滚动文章列表，具体文章的标题旁的"空心五角星"访问的url
     * 举例:urlOnCms的格式类似http://cms.duowan.com/wow/1209/211821276072.html这样的链接
     */
    private String urlOnCms;
    /**
     * 文章权重，一般用于排序
     */
    private Integer power  = 60;
    /**
     * 发布时间（精确到秒）
     */
    private Date publishTime;
    

    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;
    
    /**
     * 是否有头图
     */
    private String isHeadPic;
    
    /**
     * 子标签
     */
    private String sonTag;
    
    /**
     * 子标签id
     */
    private String sonTagId;
    
    /**
     * 自定义内容1
     */
    private String diy1;
    /**
     * 自定义内容2
     */
    private String diy2;
    /**
     * 自定义内容3
     */
    private String diy3;
    /**
     * 自定义内容4
     */
    private String diy4;
    /**
     * 自定义内容5
     */
    private String diy5;
    
    /**
     * 是否已经删除
     */
    private boolean delete;
    /**
     * 标题颜色
     */
    private String titleColor;
    
    private int status;
    
    /**
     * 是否更新所有标签
     */
    private String isChangeAllTags;
    /**
     * 流量统计
     */
    private long sum;
    /**
     * 评论数
     */
    private int commentCount;
    public Article4TagListInfo(){}
    public Article4TagListInfo(Article4TagListInfo article4TagListInfo){
        this.id = article4TagListInfo.getId();
        this.channelInfo = article4TagListInfo.getChannelInfo();
        this.tag = article4TagListInfo.getTag();
        this.title = article4TagListInfo.getTitle();
        this.subtitle = article4TagListInfo.getSubtitle();
        this.digest = article4TagListInfo.getDigest();
        this.emptyLink = article4TagListInfo.getEmptyLink();
        this.pictureUrl = article4TagListInfo.getPictureUrl();
        this.source = article4TagListInfo.getSource();
        this.author = article4TagListInfo.getAuthor();
        this.userId = article4TagListInfo.getUserId();
        this.dayNum = article4TagListInfo.getDayNum() ;
        this.urlOnCms = article4TagListInfo.getUrlOnCms();
        this.urlOnLine = article4TagListInfo.getUrlOnLine();
        this.power = article4TagListInfo.getPower();
        this.publishTime = article4TagListInfo.getPublishTime();
        this.lastUpdateTime = article4TagListInfo.getLastUpdateTime();
        this.isHeadPic = article4TagListInfo.getIsHeadPic();
        this.diy1 = article4TagListInfo.getDiy1();
        this.diy2 = article4TagListInfo.getDiy2();
        this.diy3 = article4TagListInfo.getDiy3();
        this.diy4 = article4TagListInfo.getDiy4();
        this.diy5 = article4TagListInfo.getDiy5();
        this.delete = article4TagListInfo.isDelete();
        this.titleColor = article4TagListInfo.getTitleColor();
        this.status = article4TagListInfo.getStatus();
        this.sum = article4TagListInfo.getSum();
    }

    /**
     * 判断该文章是否是空链接；
     * @return 是空链接返回true,否则返回false
     */
    public boolean getIsEmpltyLink() {
        return !StringUtil.isEmpty(emptyLink);
    }
    
    /**
     * 判断该文章是否是空链接；
     * @return 是空链接返回true,否则返回false
     */
    public boolean isEmpltyLink() {
        return !StringUtil.isEmpty(emptyLink);
    }
    public String getLastUpdateTimeStr() {
        if(null == lastUpdateTime)
            return "";
        return DateUtil.format(lastUpdateTime, DateUtil.defaultDateTimePatternStr);
    }
    //publishTime
    public String getPublishTimeStr() {
        if(null == publishTime)
            return "";
        return DateUtil.format(publishTime, DateUtil.defaultDateTimePatternStr);
    }
    
    /**
     * 判断自定义4是否为空
     */
    public boolean getIsEmpltyDiy4() {
        return StringUtil.isEmpty(diy4);
    }

    /**
     * 判断自定义5是否为空
     */
    public boolean getIsEmpltyDiy5() {
        return StringUtil.isEmpty(diy5);
    }
    /**
     * 是否更新到所有标签 
     */
    public boolean isChangeAllTags(){
    	return "on".equals(isChangeAllTags);
    }

 //******************普通的getter和setter**********************************************
    


	public String getIsChangeAllTags() {
		return isChangeAllTags;
	}
	public void setIsChangeAllTags(String isChangeAllTags) {
		this.isChangeAllTags = isChangeAllTags;
	}
	
	public String getUrlOnLine() {
		return urlOnLine;
	}
	
	public void setUrlOnLine(String urlOnLine) {
		this.urlOnLine = urlOnLine;
	}

	public String getUrlOnCms() {
		return urlOnCms;
	}

	public void setUrlOnCms(String urlOnCms) {
		this.urlOnCms = urlOnCms;
	}

	public String getTitleColor() {
		return titleColor;
	}

	public void setTitleColor(String titleColor) {
		this.titleColor = titleColor;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getEmptyLink() {
        return emptyLink;
    }

    public void setEmptyLink(String emptyLink) {
        this.emptyLink = emptyLink;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getDayNum() {
        return dayNum;
    }

    public void setDayNum(Integer dayNum) {
        this.dayNum = dayNum;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getIsHeadPic() {
        return isHeadPic;
    }

    public void setIsHeadPic(String isHeadPic) {
        this.isHeadPic = isHeadPic;
    }
    
    public String getSonTag() {
        return sonTag;
    }
    public void setSonTag(String sonTag) {
        this.sonTag = sonTag;
    }
    public String getSonTagId() {
        return sonTagId;
    }
    public void setSonTagId(String sonTagId) {
        this.sonTagId = sonTagId;
    }
    public String getDiy1() {
        return diy1;
    }

    public void setDiy1(String diy1) {
        this.diy1 = diy1;
    }

    public String getDiy2() {
        return diy2;
    }

    public void setDiy2(String diy2) {
        this.diy2 = diy2;
    }

    public String getDiy3() {
        return diy3;
    }

    public void setDiy3(String diy3) {
        this.diy3 = diy3;
    }

    public String getDiy4() {
        return diy4;
    }

    public void setDiy4(String diy4) {
        this.diy4 = diy4;
    }

    public String getDiy5() {
        return diy5;
    }

    public void setDiy5(String diy5) {
        this.diy5 = diy5;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
    
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    /**
     * 对应模板语法中的 文章的getString()语法
     * @return
     */
    public String getString(String key) {
        String result = "";
        if ("picurl".equals(key)) {
            result = this.getPictureUrl();
        } else if ("title".equals(key)) {
            result = this.getTitle();
        } else if ("articleid".equals(key)) {
            result = this.getId().toString();
        } else if ("subtitle".equals(key)) {
            result = this.getSubtitle();
        } else if ("digest".equals(key)) {
            result = this.getDigest();
        } else if ("url".equals(key)) {
            result = this.getEmptyLink();
        } else if ("source".equals(key)) {
            result = this.getSource();
        } else if ("author".equals(key)) {
            result = this.getAuthor();
        }  else if ("posttime".equals(key)) {
            result = DateUtil.format(this.getPublishTime(), "MM-dd");
        } else if ("updatetime".equals(key)) {
            result = DateUtil.format(this.getLastUpdateTime(), "MM-dd");
        }  else if ("userid".equals(key)) {
            result = this.getUserId();
        } else if ("daynum".equals(key)) {
            result = this.getDayNum() + "";
        } else if ("power".equals(key)) {
            result = this.getPower() + "";
        } else if ("diy1".equals(key)) {
            result = this.getDiy1();
        } else if ("diy2".equals(key)) {
            result = this.getDiy2();
        } else if ("diy3".equals(key)) {
            result = this.getDiy3();
        } else if ("diy4".equals(key)) {
            result = this.getDiy4();
        } else if ("diy5".equals(key)) {
            result = this.getDiy5();
        } else if ("ispic".equals(key)) {
            result = this.isHeadPic;
        } else if ("tag".equals(key)){
            result = this.tag;
        }else if ("sontag".equals(key)) {
            result = this.sonTag == null ? "" : this.sonTag;
        } else if ("sontagid".equals(key)) {
            result = this.sonTagId;
        }else if ("titlecolor".equals(key)) {
            result = this.titleColor;
        }

        return null == result ? "" : result;
    }

    public String getLong(String key) {
        if ("daynum".equals(key)) {
            return this.getDayNum() + "";
        } else if ("articleid".equals(key)) {
            return this.getId() + "";
        } 
        return null;
    }
    
    //对投票旧语法的支持
    public Long getArticleId() {
        return this.id;
    }
    
    public String getPostTime() {
        return DateUtil.convertDate2Str(this.publishTime);
    }
    
    public String getUrl() {
        return this.urlOnLine;
    }
    
    public String getChannelId() {
        return this.channelInfo.getId();
    }
    
    public long getSum() {
        return this.sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }
    
    public String getPicurl() {
        return this.pictureUrl;
    }
    public int getCommentCount() {
        return commentCount;
    }
    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }
    

}
