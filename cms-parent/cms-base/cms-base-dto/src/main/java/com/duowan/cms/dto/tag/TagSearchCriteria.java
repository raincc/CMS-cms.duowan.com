/**
 * 
 */
package com.duowan.cms.dto.tag;

import java.util.Date;

import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * @author coolcooldee
 *
 */
public class TagSearchCriteria extends PageSearchCriteria {
   
    private String channelId;
    
    private Date updateTime;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    
    
}
