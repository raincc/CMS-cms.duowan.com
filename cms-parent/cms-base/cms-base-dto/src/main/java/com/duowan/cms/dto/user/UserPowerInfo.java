package com.duowan.cms.dto.user;

import java.util.Date;
import java.util.List;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.ChineseCharactersUtil;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;

public class UserPowerInfo implements DataTransferObject {
    
    private static final long serialVersionUID = 7349957432569741192L;
    /**
     * 用户唯一标识
     */
    private String userId;
    /**
     * 频道
     */
    private ChannelInfo channelInfo;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后修改时间
     */
    private Date lastModifyTime;
    /**
     * 创建该用户的管理员ID
     */
    private String adminId;
    /**
     * 权限值（共32位，每位代表一种权限。0代表没有权限，1代表有权限。其中，第一位代表身份：1系统管理员，2主编，3编辑，4审核员）
     */
    // private String value;
    
    /**
     * 在该个专区下所拥有的权限
     */
    private List<PowerInfo> ownPowers;
    /**
     * 获取在该身份默认赋予的权限下，额外增加的权限
     */
    private List<PowerInfo> added;
    
    /**
     * 获取在该身份默认赋予的权限下，别另外去除的权限
     */
    private List<PowerInfo> removed;
    
    
    
    public List<PowerInfo> getAdded() {
        return added;
    }
    /**
     *	用户额外增加权限的个数 
     */
    public int getAddedPowerSize(){
    	return added.size();
    }
    /**
     * 用户额外减少权限的个数
     */
    public int getRemovedPowerSize(){
    	return removed.size();
    }
    /**
     * 用户额外增加的权限
     */
    public String getAddedPowerStr(){
    	StringBuilder strBuilder = new StringBuilder("");
    	for (PowerInfo powerInfo : added) {
    		strBuilder.append(powerInfo.getName());
    		strBuilder.append(" , ");
		}
    	return strBuilder.toString();
    }
    /**
     * 用户额外减少的权限
     */
    public String getRemovedPowerStr(){
    	StringBuilder strBuilder = new StringBuilder("");
    	for (PowerInfo powerInfo : removed) {
    		strBuilder.append(powerInfo.getName());
    		strBuilder.append(" , ");
		}
    	return strBuilder.toString();
    }
    
    /**
     * 得到本频道的排序字母
     */
    public String getSort(){
        if(null == channelInfo)
            return "";
        if (!StringUtil.isEmpty(channelInfo.getName())) {
			return String.valueOf(ChineseCharactersUtil.String2Alpha(channelInfo.getName()).charAt(0)).toUpperCase();
		}
        return  "";
    }

    public void setAdded(List<PowerInfo> added) {
        this.added = added;
    }

    public List<PowerInfo> getRemoved() {
        return removed;
    }

    public void setRemoved(List<PowerInfo> removed) {
        this.removed = removed;
    }

    private UserRole userRole;
    
    public String getUserId() {
        return userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getChannelId() {
        return this.getChannelInfo().getId();
    }
    
    public void setChannelId(String channelId) {
        this.getChannelInfo().setId(channelId) ;
    }
    
    public Date getCreateTime() {
        return createTime;
    }
    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public Date getLastModifyTime() {
        return lastModifyTime;
    }
    
    public String getLastModifyTimeStr() {
        return DateUtil.format(lastModifyTime,
                DateUtil.defaultDateTimePatternStr);
    }
    
    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }
    
    public String getAdminId() {
        return adminId;
    }
    
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }
    
    // public String getValue() {
    // return value;
    // }
    // public void setValue(String value) {
    // this.value = value;
    // }
    public UserRole getUserRole() {
        return userRole;
    }
    
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
    
    public List<PowerInfo> getOwnPowers() {
        return ownPowers;
    }
    
    public void setOwnPowers(List<PowerInfo> ownPowers) {
        this.ownPowers = ownPowers;
    }

    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }
    
}
