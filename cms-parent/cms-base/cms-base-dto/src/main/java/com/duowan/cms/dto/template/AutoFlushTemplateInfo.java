package com.duowan.cms.dto.template;

import java.util.Date;
import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.dto.channel.ChannelInfo;

public class AutoFlushTemplateInfo implements DataTransferObject  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 主键
     */
    private Long id;
    /**
     * 模板ID
     */
    private Long templateId ;
    /**
     * 频道id
     */
    private ChannelInfo channelInfo;
    /**
     * 刷新间隔时间（单位秒）
     */
    private int interval;
    /**
     * 上一次的成功刷新时间
     */
    private Date lastFlushTime;
    /**
     * 描述
     */
    private String description;
    
    
	public String getLastFlushTimeStr() {
		if(lastFlushTime == null){
			return "";
		}
		return DateUtil.format(lastFlushTime, DateUtil.defaultDateTimePatternStr);
	}
    
  //******************普通的getter和setter************************
    
	public Long getId() {
		return id;
	}
	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	public Date getLastFlushTime() {
		return lastFlushTime;
	}
	public void setLastFlushTime(Date lastFlushTime) {
		this.lastFlushTime = lastFlushTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ChannelInfo getChannelInfo() {
		return channelInfo;
	}
	public void setChannelInfo(ChannelInfo channelInfo) {
		this.channelInfo = channelInfo;
	}
}
