/**
 * 
 */
package com.duowan.cms.dto.zone;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;

/**
 * @author yzq
 */
public class ZoneArticleInfo  implements DataTransferObject {

    private static final long serialVersionUID = -2280413742986225231L;
   
    private Long articleId;
    private String tag;
    private Date createTime;
    private Date updateTime;
    private String zoneUserName;
    private String channelId;
    private ZoneArticleStatus status;
    private String title;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getZoneUserName() {
        return zoneUserName;
    }

    public void setZoneUserName(String zoneUserName) {
        this.zoneUserName = zoneUserName;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public ZoneArticleStatus getStatus() {
        return status;
    }

    public void setStatus(ZoneArticleStatus status) {
        this.status = status;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
}
