/**
 * 
 */
package com.duowan.cms.dto.tag;

import com.duowan.cms.common.dto.EnumInterface;

/**
 * 标签类别(0普通,1特殊,2用户投稿)
 * 
 * @author coolcooldee
 * 
 */
public enum TagCategory implements EnumInterface {
    /**
     * 普通
     */
    NORMAL {
        public String getDisplay() {
            return "普通";
        }
        
        public String getValue() {
            return "0";
        }
        
    },
    /**
     * 特殊
     */
    SPECIAL {
        public String getDisplay() {
            return "特殊";
        }
        
        public String getValue() {
            return "1";
        }
        
    },
    /**
     * 用户投稿
     */
    USER_CONTRIBUTE {
        public String getDisplay() {
            return "用户投稿";
        }
        
        public String getValue() {
            return "2";
        }
    };
    
    public static TagCategory getInstance(String value)
            throws IllegalArgumentException {
        
        if ("0".equals(value))
            return NORMAL;
        else if ("1".equals(value))
            return SPECIAL;
        else if ("2".equals(value))
            return USER_CONTRIBUTE;
        else
            throw new IllegalArgumentException("Argument value is illegal");
    }
    
}
