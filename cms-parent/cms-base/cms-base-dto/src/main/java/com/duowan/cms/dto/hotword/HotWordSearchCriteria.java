/**
 * 
 */
package com.duowan.cms.dto.hotword;

import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * 文章查询条件
 */
public class HotWordSearchCriteria extends PageSearchCriteria {

    /**
     * 
     */
    private static final long serialVersionUID = 3712578930365837958L;

    private String channelId;
    
    private String name;
    
    private String href;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
    
}
