package com.duowan.cms.dto.article;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.PathUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * 用于滚动文章展示
 * @author Administrator
 *
 */
public class SimpleArticleInfo implements DataTransferObject{
    /**
     * 
     */
    private static final long serialVersionUID = 3052223698860223943L;

    /**
     * 唯一标识符
     */
    private Long id;

    /**
     * 频道id
     */
    private ChannelInfo channelInfo;

    /**
     * 标题
     */
    private String title;
    /**
     * 来源
     */
    private String source;
    /**
     * 作者
     */
    private String author;
    /**
     * 发布时间（精确到秒）
     */
    private Date publishTime;
    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;
    
    private Date prePublishTime;
    /**
     * 标签列表（多个标签使用“，”分割），所有的标签
     */
    private String tags;
    /**
     * 发布者ID
     */
    private String userId;
    /**
     * 相关联的标签，所有标签中去除特殊标签
     */
    private String realtags;
    /**
     * 是否有头图
     */
    private String isHeadPic;
    /**
     * 图片地址
     */
    private String pictureUrl;
    /**
     * 正式环境的url:外网访问的url. 使用到的地方:点击滚动文章列表，具体文章的标题旁的"实心五角星"访问的url
     * 举例:urlOnLine的格式类似http://wow.duowan.com/1209/211821276072.html这样的链接
     */
    //private String urlOnLine;

    /**
     * 发布器服务器上的url. 使用到的地方:点击滚动文章列表，具体文章的标题旁的"空心五角星"访问的url
     * 举例:urlOnCms的格式类似http://cms.duowan.com/wow/1209/211821276072.html这样的链接
     */
    //private String urlOnCms;
    
    public boolean getIsTagsEmpty(){
        return StringUtil.isEmpty(tags);
    }

    public String[] getTagArr() {
        if ("".equals(tags) || tags == null) {
            return new String[] {};
        }
        return tags.split(",");
    }
    
    public String getPublishTimeStr() {
        if(publishTime == null){
            return "";
        }
        return DateUtil.format(publishTime, DateUtil.defaultDateTimePatternStr);
    }

    
  //******************普通的getter和setter************************

    public String getUrlOnLine() {
        return  PathUtil.getArticleOnlineUrl(this.getChannelInfo().getDomain(), id);
    }

    public String getUrlOnCms() {
        return PathUtil.getArticleOnCmsUrl(this.getChannelInfo().getId(), id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public String getLastUpdateTimeStr() {
        return DateUtil.format(lastUpdateTime, DateUtil.defaultDateTimePatternStr);
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
    public String getPrePublishTimeStr(){
        return DateUtil.format(prePublishTime, DateUtil.defaultDateTimePatternStr);
    }

    public Date getPrePublishTime() {
        return prePublishTime;
    }

    public void setPrePublishTime(Date prePublishTime) {
        this.prePublishTime = prePublishTime;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRealtags() {
        return realtags;
    }

    public void setRealtags(String realtags) {
        this.realtags = realtags;
    }

    public String getIsHeadPic() {
        return isHeadPic;
    }

    public void setIsHeadPic(String isHeadPic) {
        this.isHeadPic = isHeadPic;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
