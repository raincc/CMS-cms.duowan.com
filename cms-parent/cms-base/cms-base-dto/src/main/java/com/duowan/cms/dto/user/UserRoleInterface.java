package com.duowan.cms.dto.user;

import com.duowan.cms.common.dto.EnumInterface;

/**
 * 用户角色
 * @author coolcooldee
 * @version 1.0
 * @created 10-九月-2012 13:45:55
 */
public interface UserRoleInterface extends EnumInterface {
    
    /**
     * 获取角色准确的数值，对应UserPower中的value
     * @return
     */
    public abstract String getExactValue();
    
    public int getPowerValue();

}