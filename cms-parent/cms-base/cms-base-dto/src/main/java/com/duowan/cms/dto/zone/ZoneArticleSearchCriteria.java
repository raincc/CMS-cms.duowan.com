/**
 * 
 */
package com.duowan.cms.dto.zone;

import java.util.Date;

import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * @author yzq
 *
 */
public class ZoneArticleSearchCriteria  extends PageSearchCriteria {

    private static final long serialVersionUID = -4901512963658174320L;
    
    private String channelId;
    
    private String zoneUserName;
    
    private String tag;
    
    private String title;
    
    private String status;
    
    private Date updateTime;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getZoneUserName() {
        return zoneUserName;
    }

    public void setZoneUserName(String zoneUserName) {
        this.zoneUserName = zoneUserName;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    
}
