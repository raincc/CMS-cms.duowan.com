/**
 * 
 */
package com.duowan.cms.dto.gametest;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.DateUtil;

/**
 * @author yzq
 *
 */
public class GameTestInfo implements DataTransferObject {
    /**
     * 
     */
    private static final long serialVersionUID = 2532661216559255100L;

    private Integer id;

    /**
     * 时间
     */
    private Date time;
    
    private String timeStr;
    /**
     * 游戏名称
     */
    private String gameName;
    /**
     * 游戏名链接
     */
    private String gameNameUrl;
    /**
     * 阶段
     */
    private String state;
    /**
     * 阶段链接
     */
    private String stateUrl;
    /**
     * 下载地点
     */
    private String download;
    /**
     * 下载链接
     */
    private String downloadUrl;
    /**
     * 截图描述
     */
    private String cutPictureDesc;
    /**
     * 截图链接
     */
    private String cutPictureUrl;
    /**
     * 视频描述
     */
    private String videoDesc;
    /**
     * 视频链接
     */
    private String videoUrl;
    /**
     * 运营/开发公司名称
     */
    private String company;
    /**
     * 运营/开发公司名称链接
     */
    private String companyUrl;
    /**
     * 多玩专区名称
     */
    private String duowanChannel;
    /**
     * 多玩专区链接
     */
    private String duowanChannelUrl;
    /**
     * 帐号描述
     */
    private String accountDesc;
    /**
     * 账号链接
     */
    private String accountUrl;
    /**
     * 评论描述
     */
    private String commentDesc;
    /**
     * 评论链接
     */
    private String commentUrl;
    /**
     * 自定义参数
     */
    private String diy;
    /**
     * 主页检查
     */
    private String indexCheck;
    /**
     * 公测检查
     */
    private String pubCheck;
    /**
     * 权重
     */
    private Integer power;
    /**
     * 免费游戏
     */
    private String freeGame;
    /**
     * 已付费
     */
    private String hasPay;
    /**
     * 国外游戏
     */
    private String abroad;

    private Integer ctype;

    private String fxcompany;

    private String fxcompanyurl;

    private String gameType;

    private String gameTypeUrl;

    // 1是封测，2是内测，3是公测 add by zqm-首页测试游戏列表状态自动填写功能
    private String gstatus;
    
    //接口返回新增发号信息
    private int gameId;//表示特权游戏的id
    
    private String tqzxUrl;//表示游戏下最新活动的特权中心的url
    
    private String actState;//表示游戏下最新活动的状态，SEND-正在发号，BOOK-预定，TAO-淘号，JOIN-参与，空表示结束
    
    // ****************************普通的getter和setter*******************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    
    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameNameUrl() {
        return gameNameUrl;
    }

    public void setGameNameUrl(String gameNameUrl) {
        this.gameNameUrl = gameNameUrl;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateUrl() {
        return stateUrl;
    }

    public void setStateUrl(String stateUrl) {
        this.stateUrl = stateUrl;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getCutPictureDesc() {
        return cutPictureDesc;
    }

    public void setCutPictureDesc(String cutPictureDesc) {
        this.cutPictureDesc = cutPictureDesc;
    }

    public String getCutPictureUrl() {
        return cutPictureUrl;
    }

    public void setCutPictureUrl(String cutPictureUrl) {
        this.cutPictureUrl = cutPictureUrl;
    }

    public String getVideoDesc() {
        return videoDesc;
    }

    public void setVideoDesc(String videoDesc) {
        this.videoDesc = videoDesc;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyUrl() {
        return companyUrl;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getDuowanChannel() {
        return duowanChannel;
    }

    public void setDuowanChannel(String duowanChannel) {
        this.duowanChannel = duowanChannel;
    }

    public String getDuowanChannelUrl() {
        return duowanChannelUrl;
    }

    public void setDuowanChannelUrl(String duowanChannelUrl) {
        this.duowanChannelUrl = duowanChannelUrl;
    }

    public String getAccountDesc() {
        return accountDesc;
    }

    public void setAccountDesc(String accountDesc) {
        this.accountDesc = accountDesc;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {
        this.accountUrl = accountUrl;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }

    public String getCommentUrl() {
        return commentUrl;
    }

    public void setCommentUrl(String commentUrl) {
        this.commentUrl = commentUrl;
    }

    public String getDiy() {
        return diy;
    }

    public void setDiy(String diy) {
        this.diy = diy;
    }

    public String getIndexCheck() {
        return indexCheck;
    }

    public void setIndexCheck(String indexCheck) {
        this.indexCheck = indexCheck;
    }

    public String getPubCheck() {
        return pubCheck;
    }

    public void setPubCheck(String pubCheck) {
        this.pubCheck = pubCheck;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public String getFreeGame() {
        return freeGame;
    }

    public void setFreeGame(String freeGame) {
        this.freeGame = freeGame;
    }

    public String getHasPay() {
        return hasPay;
    }

    public void setHasPay(String hasPay) {
        this.hasPay = hasPay;
    }

    public String getAbroad() {
        return abroad;
    }

    public void setAbroad(String abroad) {
        this.abroad = abroad;
    }

    public Integer getCtype() {
        return ctype;
    }

    public void setCtype(Integer ctype) {
        this.ctype = ctype;
    }

    public String getFxcompany() {
        return fxcompany;
    }

    public void setFxcompany(String fxcompany) {
        this.fxcompany = fxcompany;
    }

    public String getFxcompanyurl() {
        return fxcompanyurl;
    }

    public void setFxcompanyurl(String fxcompanyurl) {
        this.fxcompanyurl = fxcompanyurl;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getGameTypeUrl() {
        return gameTypeUrl;
    }

    public void setGameTypeUrl(String gameTypeUrl) {
        this.gameTypeUrl = gameTypeUrl;
    }

    public String getGstatus() {
        return gstatus;
    }

    public void setGstatus(String gstatus) {
        this.gstatus = gstatus;
    }
    
    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getTqzxUrl() {
        return tqzxUrl;
    }

    public void setTqzxUrl(String tqzxUrl) {
        this.tqzxUrl = tqzxUrl;
    }

    public String getActState() {
        if("SEND".equalsIgnoreCase(actState) || "JOIN".equalsIgnoreCase(actState)){
            return "ling";
        }else if("BOOK".equalsIgnoreCase(actState)){
            return "yu";
        }else if("TAO".equalsIgnoreCase(actState)){
            return "tao";
        }else{
            return "wu";
        }
    }

    public void setActState(String actState) {
        this.actState = actState;
    }

    // ******************旧发布器语法**********************
    public String getString(String key) {
        String result = "";
        if ("time".equals(key)) {
            result = null != this.timeStr ? this.timeStr : DateUtil.format(this.time, "yyyy-MM-dd");
        } else if ("gname".equals(key)) {
            result = this.gameName;
        } else if ("gnameurl".equals(key)) {
            result = this.gameNameUrl;
        } else if ("state".equals(key)) {
            result = this.state;
        } else if ("stateurl".equals(key)) {
            result = this.stateUrl;
        } else if ("download".equals(key)) {
            result = this.download;
        } else if ("downloadurl".equals(key)) {
            result = this.downloadUrl;
        } else if ("cutpic".equals(key)) {
            result = this.cutPictureDesc;
        } else if ("cutpicurl".equals(key)) {
            result = this.cutPictureUrl;
        } else if ("video".equals(key)) {
            result = this.videoDesc;
        } else if ("videourl".equals(key)) {
            result = this.videoUrl;
        } else if ("company".equals(key)) {
            result = this.company;
        } else if ("companyurl".equals(key)) {
            result = this.companyUrl;
        } else if ("area".equals(key)) {
            result = this.duowanChannel;
        } else if ("areaurl".equals(key)) {
            result = this.duowanChannelUrl;
        } else if ("account".equals(key)) {
            result = this.accountDesc;
        } else if ("accounturl".equals(key)) {
            result = this.accountUrl;
        } else if ("comment".equals(key)) {
            result = this.commentDesc;
        } else if ("commenturl".equals(key)) {
            result = this.commentUrl;
        } else if ("zidingyi".equals(key)) {
            result = this.diy;
        } else if ("checkindex".equals(key)) {
            result = this.indexCheck;
        } else if ("power".equals(key)) {
            result = this.power + "";
        } else if ("checkpub".equals(key)) {
            result = this.pubCheck;
        }else if ("freegame".equals(key)) {
            result = this.freeGame;
        } else if ("haspay".equals(key)) {
            result = this.hasPay;
        } else if ("abroad".equals(key)) {
            result = this.abroad;
        } else if ("ctype".equals(key)) {
            result = this.ctype + "";
        } else if ("fxcompany".equals(key)) {
            result = this.fxcompany;
        }else if ("fxcompanyurl".equals(key)) {
            result = this.fxcompanyurl;
        } else if ("gametype".equals(key)) {
            result = this.gameType;
        } else if ("gametypeurl".equals(key)) {
            result = this.gameTypeUrl;
        } else if ("gametestid".equals(key)) {
            result = this.id + "";
        } else if ("gstatus".equals(key)) {
            result = this.gstatus;
        }

        return null == result ? "" : result;
    }

}
