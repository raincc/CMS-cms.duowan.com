/**
 * 
 */
package com.duowan.cms.dto.template;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;

/**
 * 接口或类的说明
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
public class AutoFlushTagInfo implements DataTransferObject  {

    private static final long serialVersionUID = -7362578871925534955L;
    
    /**
     * 唯一标识
     */
    private Long id;
    /**
     * tag名字
     */
    private String tagName;

    /**
     * 频道ID
     */
    private String channelId;
    
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 刷新时间
     */
    private Date flushTime;
    
    public AutoFlushTagInfo(String channelId, String tagName) {
        super();
        this.tagName = tagName;
        this.channelId = channelId;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTagName() {
        return tagName;
    }
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
    public String getChannelId() {
        return channelId;
    }
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getFlushTime() {
        return flushTime;
    }
    public void setFlushTime(Date flushTime) {
        this.flushTime = flushTime;
    }

}
