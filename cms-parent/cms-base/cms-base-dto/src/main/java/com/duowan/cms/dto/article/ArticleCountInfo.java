package com.duowan.cms.dto.article;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;

/**
 * 文章
 */
public class ArticleCountInfo implements DataTransferObject {

    private static final long serialVersionUID = 7349024897809827891L;
    
    private String channelId;

    private String userId;

    private Date date;

    private Integer count;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
