package com.duowan.cms.dto.template;

import com.duowan.cms.common.dto.PageSearchCriteria;

public class AutoFlushTemplateSearchCriteria extends PageSearchCriteria {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String channelId;

	private Long templateId;
	
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

	
}
