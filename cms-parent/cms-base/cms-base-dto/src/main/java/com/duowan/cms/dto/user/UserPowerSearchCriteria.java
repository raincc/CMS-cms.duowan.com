/**
 * 
 */
package com.duowan.cms.dto.user;

import java.util.Date;

import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * 用户查询条件
 * 
 */
public class UserPowerSearchCriteria extends PageSearchCriteria{
	
    private String userId;
    
    private String channelId;
    
    private Date beginCreateTime;
    
    private Date endCreateTime;
    
    private Date beginLastModifyTime;
    
    private Date endLastModifyTime;
    
    private String adminId;
    
    private String value;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Date getBeginCreateTime() {
        return beginCreateTime;
    }

    public void setBeginCreateTime(Date beginCreateTime) {
        this.beginCreateTime = beginCreateTime;
    }

    public Date getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(Date endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public Date getBeginLastModifyTime() {
        return beginLastModifyTime;
    }

    public void setBeginLastModifyTime(Date beginLastModifyTime) {
        this.beginLastModifyTime = beginLastModifyTime;
    }

    public Date getEndLastModifyTime() {
        return endLastModifyTime;
    }

    public void setEndLastModifyTime(Date endLastModifyTime) {
        this.endLastModifyTime = endLastModifyTime;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    
}
