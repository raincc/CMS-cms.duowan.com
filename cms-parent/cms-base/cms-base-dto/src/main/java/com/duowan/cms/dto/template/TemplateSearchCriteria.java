package com.duowan.cms.dto.template;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.duowan.cms.common.dto.PageSearchCriteria;
import com.duowan.cms.common.util.StringUtil;

public class TemplateSearchCriteria extends PageSearchCriteria {

    private static final long serialVersionUID = 5973344703495546079L;

    private String channelId;

    private String templateId;
    /**
     * 名字
     */
    private String name;
    /**
     * 模板别名(模糊匹配)
     */
    private String alias;
    /**
     * 模板别名(精确匹配)
     */
    private String exactAlias;
    /**
     * 创建模板用户ID
     */
    private String createUserId;

    /**
     * 最后修改模板用户ID
     */
    private String lastUpdateUserId;
    /**
     * 模板类别（栏目,专题,标签,最终文章,回复,关键字）
     */
    private TemplateCategory templateCategory;
    /**
     * 相关联的tags
     * 注：只要模板中的相关联的tags与查询中的relateTags中的其中一个tag匹配，即满足条件
     */
    private List<String> relateTags;
    
    /**
     * 关键字(文章内容里包含有此关键字)
     */
    private String keyword;



	public TemplateCategory getTemplateCategory() {
        return templateCategory;
    }

    public void setTemplateCategory(TemplateCategory templateCategory) {
        this.templateCategory = templateCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    public void setLastUpdateUserId(String lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public List<String> getRelateTags() {
        return relateTags;
    }

    public void setRelateTags(List<String> relateTags) {
        this.relateTags = relateTags;
    }
    
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getExactAlias() {
        return exactAlias;
    }

    public void setExactAlias(String exactAlias) {
        this.exactAlias = exactAlias;
    }

    public void addRelateTags(String relateTags) {
        if (!StringUtil.isEmpty(relateTags)) {
            for (String str : relateTags.split(",")) {
                if (!StringUtil.isEmpty(str)){
                    if(null == this.relateTags){
                        this.relateTags = new ArrayList<String>();
                    }
                    this.relateTags.add(str);
                }
                    
            }
        }
    }

}
