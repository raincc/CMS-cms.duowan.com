package com.duowan.cms.dto.template;

import java.util.Date;

import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.PathUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;

public class SimpleTemplateInfo implements DataTransferObject{

    private Long id;

    private ChannelInfo channelInfo;
    /**
     * 名字
     */
    private String name;

    /**
     * 说明
     */
    private String digest;

    /**
     * 创建模板用户ID
     */
    private String createUserId;

    /**
     * 初次发表时间
     */
    private Date postTime;

    /**
     * 上一次的更新时间
     */
    private Date updateTime;

    /**
     * 模板别名
     */
    private String alias;

    /**
     * 模板状态（普通，删除 , 解析有误的模板）
     */
    private TemplateStatus templateStatus;

    /**
     * 模板类别（栏目,专题,标签,最终文章,回复,关键字）
     */
    private TemplateCategory templateCategory;

    /**
     * 只有 "栏目","专题","资讯推广" 才显示链接
     */
    public boolean getHasUrlOnLine() {
        if (StringUtil.isEmpty(this.getUrlOnLine())
                    || (templateCategory != TemplateCategory.COLUMN
                            && templateCategory != TemplateCategory.TOPIC && templateCategory != TemplateCategory.INFO_PROMOTION)) {
            return false;
        }
        return true;
    }

    /**
     * 只有 "栏目","专题","资讯推广" 才显示链接
     */
    public boolean getHasUrlOnCms() {
        if (StringUtil.isEmpty(this.getUrlOnCms())
                    || (templateCategory != TemplateCategory.COLUMN
                                && templateCategory != TemplateCategory.TOPIC && templateCategory != TemplateCategory.INFO_PROMOTION)) {
            return false;
        }
        return true;
    }

    /**
     * 正式环境的url:外网访问的url. 使用到的地方:点击模板列表，具体模板的标题旁的"实心五角星"访问的url
     * 举例:urlOnLine的格式类似http://wow.duowan.com/1209/211821276072.html这样的链接
     */
    public String getUrlOnLine() {
        String url = PathUtil.getTemplateOnlineUrl(channelInfo.getDomain(), id, alias);
        if (StringUtil.isEmpty(url)) {
            return "/toPageNotFoundPage.do";
        }
        return url;
    }

    /**
     * 发布器服务器上的url. 使用到的地方:点击模板列表，具体模板的标题旁的"空心五角星"访问的url
     * 举例:urlOnCms的格式类似http://cms.duowan.com/wow/1209/211821276072.html这样的链接
     */
    public String getUrlOnCms() {
        String url = PathUtil.getTemplateOnCmsUrl(this.getChannelId(), id, alias);
        if (StringUtil.isEmpty(url)) {
            return "/toPageNotFoundPage.do";
        }
        return url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getChannelId() {
        if (null == channelInfo)
            return null;
        return channelInfo.getId();
    }

    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public String getUpdateTimeStr() {
        if (updateTime == null) {
            return "";
        }
        return DateUtil.format(updateTime, DateUtil.defaultDateTimePatternStr);
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public TemplateStatus getTemplateStatus() {
        return templateStatus;
    }

    public void setTemplateStatus(TemplateStatus templateStatus) {
        this.templateStatus = templateStatus;
    }

    public TemplateCategory getTemplateCategory() {
        return templateCategory;
    }

    public void setTemplateCategory(TemplateCategory templateCategory) {
        this.templateCategory = templateCategory;
    }
    
}
