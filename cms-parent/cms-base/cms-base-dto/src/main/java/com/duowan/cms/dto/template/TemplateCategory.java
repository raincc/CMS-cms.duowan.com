/**
 * 
 */
package com.duowan.cms.dto.template;

import com.duowan.cms.common.dto.EnumInterface;
import com.duowan.cms.common.util.StringUtil;

/**
 * 模板的类别（栏目,专题,标签,最终文章,回复,关键字）
 * @author coolcooldee
 *
 */
public enum TemplateCategory implements EnumInterface {
    /**
     * 栏目
     */
    COLUMN {
        public String getDisplay() {
            return "栏目";
        }
        
        public String getValue() {
            return "栏目";
        }
    },
    /**
     * 专题
     */
    TOPIC {
        public String getDisplay() {
            return "专题";
        }
        
        public String getValue() {
            return "专题";
        }
    },
    TAG {
        public String getDisplay() {
            return "标签";
        }
        
        public String getValue() {
            return "标签";
        }
    },
    TAG_VIEW {
        public String getDisplay() {
            return "标签图";
        }
        
        public String getValue() {
            return "标签图";
        }
    },
    FINAL_ARTICLE {
        public String getDisplay() {
            return "最终文章";
        }
        
        public String getValue() {
            return "最终文章";
        }
    },
    REPLY {
        public String getDisplay() {
            return "回复";
        }
        
        public String getValue() {
            return "回复";
        }
    },
    KEYWORD {
        public String getDisplay() {
            return "关键字";
        }
        
        public String getValue() {
            return "关键字";
        }
    },
    VOTE {
        public String getDisplay() {
            return "投票";
        }
        
        public String getValue() {
            return "投票";
        }
    },
    VOTE_RESULT {
        public String getDisplay() {
            return "投票结果";
        }
        
        public String getValue() {
            return "投票结果";
        }
    },
    PUBLIC {
        public String getDisplay() {
            return "公共";
        }
        
        public String getValue() {
            return "公共";
        }
    },
    NEW_VOTE_RESULT {
        public String getDisplay() {
            return "投票结果(新版)";
        }
        
        public String getValue() {
            return "投票结果(新版)";
        }
    },
    CONFIG {
        public String getDisplay() {
            return "配置文件";
        }
        
        public String getValue() {
            return "配置文件";
        }
    },
    INFO_PROMOTION {
        public String getDisplay() {
            return "资讯推广";
        }
        
        public String getValue() {
            return "资讯推广";
        }
    };
    
    public static TemplateCategory getInstance(String value)
            throws IllegalArgumentException {
        if ("栏目".equals(value))
            return COLUMN;
        else if ("专题".equals(value))
            return TOPIC;
        else if ("标签".equals(value))
            return TAG;
        else if ("标签图".equals(value))
            return TAG_VIEW;
        else if ("最终文章".equals(value))
            return FINAL_ARTICLE;
        else if ("回复".equals(value))
            return REPLY;
        else if ("关键字".equals(value))
            return KEYWORD;
        else if ("投票".equals(value))
            return VOTE;
        else if ("投票结果".equals(value))
            return VOTE_RESULT;
        else if ("公共".equals(value))
            return PUBLIC;
        else if ("投票结果(新版)".equals(value))
            return NEW_VOTE_RESULT;
        else if ("配置文件".equals(value))
            return CONFIG;
        else if ("资讯推广".equals(value))
            return INFO_PROMOTION;
        else
            throw new IllegalArgumentException("Argument value is illegal");
    }
    
}
