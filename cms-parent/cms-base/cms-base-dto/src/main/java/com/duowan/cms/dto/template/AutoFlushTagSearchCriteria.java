/**
 * 
 */
package com.duowan.cms.dto.template;

import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * 接口或类的说明
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
public class AutoFlushTagSearchCriteria extends PageSearchCriteria {

    private static final long serialVersionUID = 1178011904752736339L;

    private String channelId;
    
    private String tagName;
    
    /**
     * 是否刷新过,null则不查
     */
    private Boolean isFlushed;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Boolean getIsFlushed() {
        return isFlushed;
    }

    public void setIsFlushed(Boolean isFlushed) {
        this.isFlushed = isFlushed;
    }
    
    
    
}
