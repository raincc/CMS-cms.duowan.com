/**
 * 
 */
package com.duowan.cms.dto.user;

import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * 用户查询条件
 * 
 */
public class UserSearchCriteria extends PageSearchCriteria{
	
    /**
     * 
     */
    private static final long serialVersionUID = -1824020436041737475L;

    private String userId;
    
    private Boolean admin;
    
	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
    
}
