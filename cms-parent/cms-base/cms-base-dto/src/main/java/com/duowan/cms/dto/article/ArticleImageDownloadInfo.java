package com.duowan.cms.dto.article;

public class ArticleImageDownloadInfo {

    private String downloadUrl;
    
    private String storePath;
    
    //private String rsyncPath;

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }

//    public String getRsyncPath() {
//        return rsyncPath;
//    }
//
//    public void setRsyncPath(String rsyncPath) {
//        this.rsyncPath = rsyncPath;
//    }

    public ArticleImageDownloadInfo(String downloadUrl, String storePath ) {
        this.downloadUrl = downloadUrl;
        this.storePath = storePath;
        //this.rsyncPath = rsyncPath;
    }

    public ArticleImageDownloadInfo() {
    }
    
    
}
