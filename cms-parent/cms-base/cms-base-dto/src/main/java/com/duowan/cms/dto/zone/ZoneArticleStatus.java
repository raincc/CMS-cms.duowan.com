package com.duowan.cms.dto.zone;

import com.duowan.cms.common.dto.EnumInterface;

/**
 * 文章状态
 * 
 * @author yzq
 * @version 1.0
 * @created 04-九月-2012 11:44:15
 */
public enum ZoneArticleStatus implements EnumInterface {
	/**
	 * 待同步
	 */
	BEGIN {
		public String getDisplay() {
			return "待同步";
		}

		public String getValue() {
			return "1";
		}
	},
	/**
	 * 正在同步
	 */
	CHECK {
		public String getDisplay() {
			return "开始同步";
		}
		public String getValue() {
			return "2";
		}
	},
	/**
	 * 同步有爱成功状态
	 */
	SUCCESS {
		public String getDisplay() {
			return "同步成功";
		}
		public String getValue() {
			return "3";
		}
	},
	/**
	 * 同步有爱失败状态
	 */
	FAIL {
		public String getDisplay() {
			return "同步失败";
		}
		public String getValue() {
			return "4";
		}
	},
	/**
	 * 未知状态
	 */
	UNKNOW {
	    public String getDisplay() {
            return "未知状态";
        }
        public String getValue() {
            return "5";
        }
	};

	public static ZoneArticleStatus getInstance(String value)
			throws IllegalArgumentException {
		if ("1".equals(value))
			return BEGIN;
		else if ("2".equals(value))
			return CHECK;
		else if ("3".equals(value))
			return SUCCESS;
		else if ("4".equals(value))
			return FAIL;
		else
			return UNKNOW;
	}

}