import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.duowan.cms.common.util.ThreadUtil;


public class Test {
    //计数测试
    public volatile   static int count = 0;
    
    public static synchronized void inc()  
    {  
        System.out.println(count++);;  
    } 
    
    public static void main(String[] args) {
        final ThreadPoolExecutor threadPool = new ThreadPoolExecutor(100, 200, 1L, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(1),new ThreadPoolExecutor.CallerRunsPolicy());
        //final ExecutorService threadPool = Executors.newFixedThreadPool(100);
       //final ExecutorService threadPool = Executors.newCachedThreadPool(); //如果没有任务执行，1分钟后，自动释放，效果不错
        //final ExecutorService threadPool = Executors.newSingleThreadExecutor(); //
        //final ExecutorService threadPool = Executors.newScheduledThreadPool(100);
//        
//        try {
//            threadPool.awaitTermination(1, TimeUnit.SECONDS);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        final long begin = System.currentTimeMillis();
//        //System.out.println(begin);
//        
//        final CyclicBarrier barrier = new CyclicBarrier(500, new Runnable() {
//            @Override
//            public void run() {
//                long end = System.currentTimeMillis();
//                //System.out.println(count);
//                System.out.println("finished==========cost time :"+(end-begin)+"ms");
//            }
//        });   
//        
//        for(int i =0 ; i<5000; i++){
//            threadPool.execute(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e1) {
//                        // TODO Auto-generated catch block
//                        e1.printStackTrace();
//                    }
//                    inc();
//                    try {
//                        barrier.await();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    } catch (BrokenBarrierException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//        }
        
        
        System.out.println("====="+System.getProperty("user.dir"));
        
       // System.out.println(endTimes.get(endTimes.size()-1));
        
        
//        Producer p = new Producer();
//        Consumer c = new Consumer();
//        
//        p.start();
//        c.start();
//        
        
        
        
       // System.out.println("finished===="+threadPool.getLargestPoolSize()+"======cost time :"+(end-begin)+"ms");

       
        
        //System.out.println(threadPool.getCompletedTaskCount());
        //System.out.println(count);
       
    }
    
}
