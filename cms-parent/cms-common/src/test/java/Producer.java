
public class Producer extends Thread{
  private ProductList products = ProductList.getInstance();
  
  public void run(){
    int i = 0;
    
    while(!products.isFull()){
      synchronized(products){ // Get lock on product list
            if(products.isFull()){
                  System.out.println("List is full");
                  products.notify(); // Release the lock
            } else{
                  Product product = new Product(++i); // Produce a product
                  products.put(product);
                  System.out.println("Produced product " + product.getId());
                  products.notify(); // Release lock
                  try {
                      Thread.sleep(500);
                  } catch (InterruptedException e) {
                      // TODO Auto-generated catch block
                      e.printStackTrace();
                  }
            }
      } // Release the lock
    }
    
    System.out.println("finished...");
  }
}
