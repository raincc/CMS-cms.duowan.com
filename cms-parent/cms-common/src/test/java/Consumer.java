public class Consumer extends Thread {
    ProductList products = ProductList.getInstance();

    public void run() {
        while (true) {
            synchronized (products) {
                try {
                    Product product = null;
                    if (!(products.isEmpty())) {
                        product = products.take();
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.println("~~~~Consumed product " + product.getId()); // Get  the lock
                    } else {
                        products.wait(); // Wait for lock
                        System.out.println("List is empty");
                    }
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            } // Release the lock
        }
    }
}
