package com.duowan.cms.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

public class ProcessUtil {
	private static Logger logger = Logger.getLogger(ProcessUtil.class);
	private static String OS= System.getProperty("os.name").toLowerCase().startsWith("windows") ? "windows"
				: "linux";
	public static String getOS() {
		return OS;
	}
	public static int execute(String command){
	    return execute(command , "");
	}
	
	public static int execute(String command,  String directory){
		StringTokenizer st = new StringTokenizer(command);
		String[] cmdarray = new String[st.countTokens()];
	 	for (int i = 0; st.hasMoreTokens(); i++){
		    cmdarray[i] = st.nextToken();
	 	}
	 	List<String> cmdList = new ArrayList<String>(cmdarray.length);
		for (String cmd : cmdarray){
			cmdList.add(cmd);
	    }
		return execute(cmdList , directory);
	}
	
	public static int execute(String[] cmdarray){
		List<String> cmdList = new ArrayList<String>(cmdarray.length);
		for (String cmd : cmdarray){
			cmdList.add(cmd);
	    }
	    return execute(cmdList , "");
	}
	
	public static int execute(List<String> cmdList ,  String directory){
		StringBuffer cmdStr = new StringBuffer();
		for(String cmd: cmdList){
			cmdStr.append(cmd).append(" ");
		}
		logger.debug("执行命令:" + cmdStr);
		
		int rs = 0;
		Process process = null;
	 	ProcessBuilder builder = new ProcessBuilder(cmdList);
	 	if (!"".equals(directory)) {
	 		builder.directory(new File(directory));
		}
		builder.redirectErrorStream(true);
		try {
			process = builder.start();
			BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));  
			String output = null;  
			while (null != (output = br.readLine())){
				System.out.println(output);
			}
			rs = process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		return rs;
	}
	
	public static Process cmd(String command){
		StringTokenizer st = new StringTokenizer(command);
		String[] cmdarray = new String[st.countTokens()];
	 	for (int i = 0; st.hasMoreTokens(); i++){
		    cmdarray[i] = st.nextToken();
	 	}
	 	List<String> cmdList = new ArrayList<String>(cmdarray.length);
		for (String cmd : cmdarray){
			cmdList.add(cmd);
	    }
		return cmd(cmdList);
	}
	
	public static Process cmd(String[] cmdarray){
		List<String> cmdList = new ArrayList<String>(cmdarray.length);
		for (String cmd : cmdarray){
			cmdList.add(cmd);
	    }
	    return cmd(cmdList);
	}
	
	public static Process cmd(List<String> cmdList){
		StringBuffer cmdStr = new StringBuffer();
		for(String cmd: cmdList){
			cmdStr.append(cmd).append(" ");
		}
		System.out.println(cmdStr);
		ProcessBuilder builder = new ProcessBuilder(cmdList);
		builder.redirectErrorStream(true);
		Process process = null;
		try {
			process = builder.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return process;
	}
	
}
