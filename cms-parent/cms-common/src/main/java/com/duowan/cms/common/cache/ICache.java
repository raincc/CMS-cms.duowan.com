package com.duowan.cms.common.cache;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * Cache统一接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-11
 * <br>==========================
 * @param <K>
 * @param <V>
 */
public interface ICache<K, V> {
    /**
     * 保存数据
     * 
     * @param key
     * @param value
     * @return
     */
    public V put(K key, V value);

    /**
     * 保存有有效期的数据
     * 
     * @param key
     * @param value
     * @param ttl 数据超时的秒数
     * @return
     */
    public V put(K key, V value, int ttl);

    /**
     * 获取缓存数据
     * 
     * @param key
     * @return
     */
    public V get(K key);

    /**
     * 移出缓存数据
     * 
     * @param key
     * @return
     */
    public V remove(K key);

    /**
     * 删除所有缓存内的数据
     * 
     * @return
     */
    public void clear();
    
    /**
     * 是否包含该key对应的数据
     * 
     * @param key
     * @return
     */
    public boolean containsKey(K key);
    
    /**
     * 缓存数据的数量
     * @return
     */
    public int size();
    
    /**
     * 使得cache停用
     */
    public void destroy();
    

}