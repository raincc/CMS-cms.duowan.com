package com.duowan.cms.common.repository;
/**
 * 用于封装数据库表操作类型、映射到mapper的sql id、参数
 * @author yzq
 *
 */
public class SQLParameterPair {
    
    /**
     * 映射到mapper的sql id
     */
    private String sqlId;
    
    /**
     * 参数
     */
    private Object parameter;
    
    /**
     * 操作类型:insert/delete/update
     */
    private SQLOperateType operate;

    public String getSqlId() {
        return sqlId;
    }

    public void setSqlId(String sqlId) {
        this.sqlId = sqlId;
    }

    public Object getParameter() {
        return parameter;
    }

    public void setParameter(Object parameter) {
        this.parameter = parameter;
    }
    
     public SQLOperateType getOperate() {
        return operate;
    }

    public void setOperate(SQLOperateType operate) {
        this.operate = operate;
    }

    public SQLParameterPair(String sqlId, Object parameter, SQLOperateType operate) {
        this.sqlId = sqlId;
        this.parameter = parameter;
        this.operate = operate;
    }

    public enum SQLOperateType {
        INSERT,DELETE,UPDATE,SELECT_ONE,SELECT_LIST,UNKNOWN;
    }
}
