/**
 * 
 */
package com.duowan.cms.common.dto;

import java.io.Serializable;

/**
 * 数据传输对象标识接口
 * @author coolcooldee
 */
public interface DataTransferObject extends Serializable {

}
