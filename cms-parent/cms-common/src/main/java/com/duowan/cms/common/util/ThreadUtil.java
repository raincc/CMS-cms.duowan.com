package com.duowan.cms.common.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadUtil {

    //TODO 具体的参数有待调优
    private static ExecutorService threadPool = new ThreadPoolExecutor(10, 200, 5L, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(200),new ThreadPoolExecutor.CallerRunsPolicy());

    //private static ExecutorService threadPool = Executors.newCachedThreadPool();
    //private static ExecutorService threadPool = Executors.newFixedThreadPool(3); //还行
    //private static ExecutorService threadPool = Executors.newScheduledThreadPool(5);
    public static ExecutorService getThreadPool() {
        return threadPool;
    }
}
