/**
 * 
 */
package com.duowan.cms.common.repository;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;


/**
 * 封装的mybatis模板
 * 
 * @author coolcooldee
 * 
 */
public class MyBatisTemplate {
    
    protected SqlSessionFactory sqlSessionFactory;
    
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }
    
    public void executeWithNativeSession(MyBatisCallback callback) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            callback.invok(session); //执行具体操作
            session.commit();  // 提交事务
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback(); // 事务回滚
        } finally {
            if (null != session)
                session.close();
        }
    }
    
}
