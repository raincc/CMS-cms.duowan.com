package com.duowan.cms.common.webapp.init;

import javax.servlet.ServletContext;

/**
 * 
 * 接口或类的说明：WEB应用Servlet上下文初始Bean的接口，主要用来在ServletContext加载完成后初始化一些参数到ServletContext中<BR>
 * 使用方式：实现此类，将实现类配置成Spring的Bean对象即可，前置条件是应用中使用了ExtendedSpringContextLoaderListener类
 *
 * <br>==========================
 * <br> 公司：优视科技
 * <br> 开发：zhangxx2@ucweb.com
 * <br> 版本：1.0
 * <br> 创建时间：2011-5-25
 * <br>==========================
 */
public interface ServletContextInitializingBean {

    void initialized(ServletContext servletContext);

}
