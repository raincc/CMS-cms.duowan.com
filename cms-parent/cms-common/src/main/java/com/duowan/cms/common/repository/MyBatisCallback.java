/**
 * 
 */
package com.duowan.cms.common.repository;

import org.apache.ibatis.session.SqlSession;

/**
 * mybatis 回调函数
 * @author coolcooldee
 *
 */
public interface MyBatisCallback  {
    
    /**
     * 要执行的方法
     */
    public Object  invok(SqlSession session);
    
}
