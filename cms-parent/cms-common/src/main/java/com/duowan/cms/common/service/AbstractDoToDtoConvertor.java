package com.duowan.cms.common.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.dto.SearchCriteria;
import com.duowan.cms.common.repository.DomainObjectRepository;

/**
 * 抽象的DO转DTO转换器
 * @author coolcooldee
 */
public abstract class AbstractDoToDtoConvertor<O extends DomainObject, D extends DataTransferObject> implements DoToDtoConvertor<O, D> {

    protected Log logger = LogFactory.getLog(getClass());

    protected AbstractDoToDtoConvertor() {
        super();
    }

    public D doToDtoWithLazy(O obj) {
        return this.do2Dto(obj);
    }

    public List<D> dos2DtosWithLazy(List<O> os) {
        if (os == null || os.isEmpty()) {
            return new ArrayList<D>();
        }
        List<D> answerInfos = new ArrayList<D>(os.size());
        for (O o : os) {
            answerInfos.add(doToDtoWithLazy(o));
        }
        return answerInfos;
    }

    public List<D> dos2Dtos(List<O> os) {
        if (os == null || os.isEmpty()) {
            return new ArrayList<D>();
        }
        List<D> answerInfos = new ArrayList<D>(os.size());
        for (O o : os) {
            answerInfos.add(do2Dto(o));
        }
        return answerInfos;
    }

}
