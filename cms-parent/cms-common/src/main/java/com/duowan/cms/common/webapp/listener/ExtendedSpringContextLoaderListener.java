/**
 * 
 */
package com.duowan.cms.common.webapp.listener;

import javax.servlet.ServletContextEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.duowan.cms.common.webapp.content.SpringApplicationContextHolder;
import com.duowan.cms.common.webapp.init.InitializingProcessor;

/**
 * Spring上下文加载监听器的扩展类，启动Spring同时加载所有bean到自己的容器
 * 
 * @author coolcooldee
 * 
 */
public class ExtendedSpringContextLoaderListener extends ContextLoaderListener {
    
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        super.contextDestroyed(servletContextEvent);
        SpringApplicationContextHolder.destroy();
    }
    
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        long beginTime = System.currentTimeMillis();
        super.contextInitialized(servletContextEvent);
        Log logger = LogFactory.getLog(getClass());
        ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContextEvent.getServletContext());
        SpringApplicationContextHolder.init(ctx);
        logger.info("Spring上下文初始化完毕！所用时间为："+ (System.currentTimeMillis() - beginTime) + "毫秒");
        // ==================================================//
        beginTime = System.currentTimeMillis();
        InitializingProcessor.doInitInitializingBeans(ctx);
        InitializingProcessor.doInitInitializingBeans(ctx,servletContextEvent.getServletContext());
        logger.info("【初始化处理器】初始化所有需要初始化的业务BEAN所用时间为："+ (System.currentTimeMillis() - beginTime) + "毫秒");
    }
    
}
