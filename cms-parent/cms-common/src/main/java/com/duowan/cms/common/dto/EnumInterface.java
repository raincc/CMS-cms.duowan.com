package com.duowan.cms.common.dto;

/**
 * 枚举接口类，方便页面上显示相应枚举常量的名称
 * @author coolcooldee
 *
 */
public interface EnumInterface {

    /**
     * 得到枚举显示的名称
     * @return 
     */
    public abstract String getDisplay();
    /**
     * 得到枚举对应的数值(一般用于数据库对应)
     * @return 
     */
    public abstract String getValue();

}
