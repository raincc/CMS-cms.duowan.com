package com.duowan.cms.common.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 缓存持有者
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-11
 * <br>==========================
 */
public class CacheHolder {

    private final static Log logger = LogFactory.getLog(CacheHolder.class);

    /**
     * 缓存接口
     */
    private final ICache<String, Object> cache;

    public ICache<String, Object> getCache() {
        return cache;
    }

    public CacheHolder(ICache<String, Object> cache) {
        this.cache = cache;
    }

    /**
     * 从缓存中取指定KEY的对象
     * @param key
     * @return
     */
    public Object get(String key) {
        try {
            return this.cache.get(key);
        } catch (Throwable t) {
            try {
                return this.cache.get(key);
            } catch (Throwable t2) {
                printThrowableMessage(t2, "根据[KEY=" + key + "]从缓存中获取数据时出现异常");
            }
        }
        return null;
    }

    /**
     * 将对象以指定KEY放入到缓存中
     * @param key
     * @param value
     */
    public void put(String key, Object value) {
        try {
            this.cache.put(key, value);
        } catch (Throwable t) {
            try {
                this.cache.put(key, value);
            } catch (Throwable t2) {
                printThrowableMessage(t2, "将[KEY=" + key + "value=" + value + "]放入缓存中时出现异常");
            }
        }
    }

    /**
     * 将对象以指定KEY放入到缓存中
     * @param key
     * @param value
     * @param ttl 单位为秒数
     */
    public void put(String key, Object value, int ttl) {
        try {
            this.cache.put(key, value, ttl);
        } catch (Throwable t) {
            try {
                this.cache.put(key, value, ttl);
            } catch (Throwable t2) {
                printThrowableMessage(t2, "将[KEY=" + key + "value=" + value + "]放入缓存中时出现异常");
            }
        }
    }

    /**
     * 从缓存中移除掉指定KEY的对象
     * @param key
     */
    public Object remove(String key) {
        try {
            return this.cache.remove(key);
        } catch (Throwable t) {
            try {
                return this.cache.remove(key);
            } catch (Throwable t2) {
                printThrowableMessage(t2, "移除缓存中[KEY=" + key + "]的对象时出现异常");
            }
        }
        return null;
    }


    private void printThrowableMessage(Throwable t, String msg) {
        logger.warn(msg, t);
    }

}
