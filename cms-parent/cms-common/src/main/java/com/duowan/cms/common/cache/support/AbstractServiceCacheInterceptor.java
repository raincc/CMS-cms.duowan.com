/**
 * 
 */
package com.duowan.cms.common.cache.support;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.cache.CacheHolder;

/**
 * 服务层缓存抽象拦截器
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-12
 * <br>==========================
 */
public abstract class AbstractServiceCacheInterceptor {

    protected Log logger = LogFactory.getLog(getClass());
    /**
     * 缓存持有者
     */
    @Autowired
    protected CacheHolder cacheHolder;

    public void setCacheHolder(CacheHolder cacheHolder) {
        this.cacheHolder = cacheHolder;
    }

    /**
     * 通用的缓存方法，其逻辑是：先从缓存获取，不存在则调用目标方法，然后更新到缓存，最后返回结果
     * 
     * @param joinPoint
     * @param cacheKey
     * @return
     * @throws Throwable
     */
    protected Object cache(ProceedingJoinPoint joinPoint, String cacheKey, int seconds) throws Throwable {
        if (cacheHolder == null) {
            logger.debug("缓存持有者对象为空，直接调用相应服务.");
            return joinPoint.proceed();
        }
        Object result = cacheHolder.get(cacheKey);
        if (result == null) {
            if (logger.isDebugEnabled())
                logger.debug("缓存中没有KEY为[" + cacheKey + "]的对象，调用相应服务获取.");
            result = joinPoint.proceed();
            if (result != null)
                cacheHolder.put(cacheKey, result, seconds);
        } else {
            if (logger.isDebugEnabled())
                logger.debug("返回缓存中KEY为[" + cacheKey + "]的对象.");
        }
        return result;
    }
    
    protected void remove(String cacheKey) {
        cacheHolder.remove(cacheKey);
    }

}
