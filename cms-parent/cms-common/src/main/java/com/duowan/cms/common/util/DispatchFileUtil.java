package com.duowan.cms.common.util;

import java.io.IOException;

//分发文件工具类
public class DispatchFileUtil {

	public static void dispatch(String originalPath , String targetPath) throws IOException, InterruptedException{
		String command;
		if("linux".equals(ProcessUtil.getOS())){   //非windows系统  
			command = "mv -f "+ originalPath + "  " + targetPath ;
		}else{//windows系统:需要手动加上以RAR文件名为命名的文件夹   
			command = "move " + originalPath + "  " + targetPath ;
		}
		CommandLineTools.execCommand(command);
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		String originalPath = "E://zhibo.rar";
		String targetPath = "F://360Downloads";
		DispatchFileUtil.dispatch(originalPath, targetPath);
	}
	
}
