package com.duowan.cms.common.util;

public class ChineseCharactersUtil {

    // 字母Z使用了两个标签，这里有２７个值
    // i, u, v都不做声母, 跟随前面的字母 期
    private static char[] chartable = { '啊', '芭', '擦', '搭', '蛾', '发', '噶', '哈', '哈', '击', '喀', '垃', '妈', '拿', '哦', '啪', '期', '然', '撒', '塌', '塌', '塌', '挖', '昔', '压', '匝', '座' };

    private static char[] alphatable = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

    private static int[] table = new int[27];
    // 初始化
    static {
        for (int i = 0; i < 27; ++i) {
            table[i] = gbValue(chartable[i]);
        }
    }

    // 主函数,输入字符,得到他的声母,
    // 英文字母返回对应的大写字母
    // 其他非简体汉字返回 '0'
    public static char Char2Alpha(char ch) {
        // 未被识别的字

        if (ch >= '0' && ch <= '9')
            return ch;
        if (ch >= 'a' && ch <= 'z')
            return ch;
        if (ch >= 'A' && ch <= 'Z')
            return (char) (ch - 'A' + 'a');
        
        char tmp = tellByHand(ch);
        if('0' != tmp)
            return tmp;

        int gb = gbValue(ch);
        if (gb < table[0])
            return '0';

        int i;
        for (i = 0; i < 26; ++i) {
            if (match(i, gb))
                break;
        }
        if (i >= 26)
            return '0';
        else
            return alphatable[i];
    }

    // 根据一个包含汉字的字符串返回一个汉字拼音首字母的字符串
    public static String String2Alpha(String SourceStr) {
        String Result = "";
        int StrLength = SourceStr.length();
        int i;
        try {
            for (i = 0; i < StrLength; i++) {
                Result += Char2Alpha(SourceStr.charAt(i));
            }
        } catch (Exception e) {
            Result = "";
        }
        return Result;
    }

    private static boolean match(int i, int gb) {
        if (gb < table[i])
            return false;
        int j = i + 1;
        // 字母Z使用了两个标签
        while (j < 26 && (table[j] == table[i]))
            ++j;

        if (j == 26)
            return gb <= table[j];
        else
            return gb < table[j];

    }

    // 取出汉字的编码
    private static int gbValue(char ch) {
        String str = new String();
        str += ch;
        try {
            byte[] bytes = str.getBytes("GBK");
            if (bytes.length < 2)
                return 0;
            return (bytes[0] << 8 & 0xff00) + (bytes[1] & 0xff);
        } catch (Exception e) {
            return 0;
        }

    }

    private static char tellByHand(char c) {
        switch (c) {
        case '飚':
            return 'b';
        case '炫':
            return 'x';
        case '岐':
            return 'q';
        case '倩':
            return 'q';
        case '逍':
            return 'x';
        case '穹':
            return 'q';
        case '糗':
            return 'q';
        case '娅':
            return 'y';
        case '霆':
            return 't';
        case '浒':
            return 'h';
        case '圳':
            return 'z';
        case '叨':
            return 'd';
        case '蜓':
            return 't';
        case '筝':
            return 'z';
        case '蜻':
            return 'q';
        case '橘':
            return 'j';
        case '匕':
            return 'b';
        case '丐':
            return 'g';
        case '夭':
            return 'y';
        case '叽':
            return 'j';
        case '吆':
            return 'y';
        case '凫':
            return 'f';
        case '阱':
            return 'j';
        case '芙':
            return 'f';
        case '杈':
            return 'c';
        case '岖':
            return 'q';
        case '鸠':
            return 'j';
        case '沐':
            return 'm';
        case '姊':
            return 'z';
        case '卦':
            return 'g';
        case '拗':
            return 'a';
        case '茉':
            return 'm';
        case '昙':
            return 't';
        case '肴':
            return 'y';
        case '衩':
            return 'c';
        case '玷':
            return 'd';
        case '茴':
            return 'h';
        case '荞':
            return 'q';
        case '荠':
            return 'j';
        case '盹':
            return 'd';
        case '咧':
            return 'l';
        case '昵':
            return 'n';
        case '咪':
            return 'm';
        case '秕':
            return 'b';
        case '胧':
            return 'l';
        case '奕':
            return 'y';
        case '飒':
            return 's';
        case '祠':
            return 'c';
        case '荸':
            return 'b';
        case '莺':
            return 'y';
        case '桦':
            return 'h';
        case '唠':
            return 'l';
        case '蚣':
            return 's';
        case '蚪':
            return 'd';
        case '蚓':
            return 'y';
        case '唧':
            return 'j';
        case '秫':
            return 's';
        case '麸':
            return 'f';
        case '捺':
            return 'n';
        case '匾':
            return 'b';
        case '蚯':
            return 'q';
        case '蛉':
            return 'l';
        case '啰':
            return 'l';
        case '铐':
            return 'k';
        case '铛':
            return 'd';
        case '笙':
            return 's';
        case '笤':
            return 't';
        case '偎':
            return 'w';
        case '徙':
            return 'x';
        case '翎':
            return 'l';
        case '庵':
            return 'a';
        case '涮':
            return 's';
        case '悴':
            return 'c';
        case '裆':
            return 'd';
        case '谒':
            return 'y';
        case '雳':
            return 'l';
        case '跛':
            return 'b';
        case '锉':
            return 'c';
        case '掰':
            return 'b';
        case '牍':
            return 'd';
        case '腌':
            return 'y';
        case '猬':
            return 'w';
        case '愕':
            return 'e';
        case '鹉':
            return 'w';
        case '蒿':
            return 'h';
        case '榄':
            return 'l';
        case '楣':
            return 'm';
        case '嗦':
            return 's';
        case '跷':
            return 'q';
        case '蜈':
            return 'w';
        case '嗤':
            return 'c';
        case '馍':
            return 'm';
        case '禀':
            return 'b';
        case '缤':
            return 'b';
        case '榛':
            return 'z';
        case '榕':
            return 'r';
        case '嘁':
            return 'q';
        case '嘀':
            return 'd';
        case '幔':
            return 'm';
        case '箫':
            return 'x';
        case '漩':
            return 'x';
        case '橄':
            return 'g';
        case '嘹':
            return 'l';
        case '蝠':
            return 'f';
        case '蝌':
            return 'k';
        case '蝙':
            return 'b';
        case '鲫':
            return 'j';
        case '憔':
            return 'q';
        case '翩':
            return 'p';
        case '嬉':
            return 'x';
        case '缭':
            return 'l';
        case '薇':
            return 'w';
        case '噩':
            return 'e';
        case '蟥':
            return 'h';
        case '霎':
            return 's';
        case '踱':
            return 'd';
        case '蹂':
            return 'r';
        case '蟆':
            return 'm';
        case '螃':
            return 'p';
        case '鹦':
            return 'y';
        case '瘾':
            return 'y';
        case '缰':
            return 'j';
        case '檐':
            return 'y';
        case '檩':
            return 'l';
        case '蟋':
            return 'x';
        case '蟀':
            return 's';
        case '朦':
            return 'm';
        case '臊':
            return 's';
        case '鳄':
            return 'e';
        case '鳍':
            return 'q';
        case '癞':
            return 'l';
        case '璧':
            return 'b';
        case '簸':
            return 'b';
        case '鬓':
            return 'b';
        case '躏':
            return 'l';
        default:
            return '0';
        }
    }

    public static void main(String[] args) {
        System.out.println(String2Alpha("倩女幽魂"));
        System.out.println(String2Alpha("穹"));
        System.out.println(String2Alpha("岐山"));
        System.out.println(String2Alpha("逍遥剑"));
        System.out.println(String2Alpha("糗"));
    }
}
