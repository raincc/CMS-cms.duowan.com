package com.duowan.cms.common.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;



/**
 * 解压缩工具类：依赖于unrar,unrar的安装路径是D:/Program Files/Unrar
 * 
 * <p>作　　者：黄均杨
 * <p>完成日期：2013-3-28
 */
public class CompressUtil {
	private static Logger logger = Logger.getLogger(CompressUtil.class);
    /** 
     * 解压缩 
     */  
    public static void deCompress(String sourceFile,String destDir) throws Exception{  
        //保证文件夹路径最后是"/"或者"\"  
        char lastChar = destDir.charAt(destDir.length()-1);  
        if(lastChar!='/'&&lastChar!='\\'){  
            destDir += System.getProperty("file.separator");  
        }  
        File destDirFile = new File(destDir);
        if(!destDirFile.exists()){
        	destDirFile.mkdirs();
        }
        //根据类型，进行相应的解压缩  
        String type = sourceFile.substring(sourceFile.lastIndexOf(".")+1);  
        if(type.equals("zip")){  
        	logger.info("开始解压zip文件,源文件位置:" + sourceFile + ",解压后存放的位置 : " + destDir);
        	CompressUtil.unzip(sourceFile, destDir);  
        }else if(type.equals("rar")){  
        	logger.info("开始解压rar文件,源文件位置:" + sourceFile + ",解压后存放的位置 : " + destDir);
        	CompressUtil.unrar(sourceFile, destDir);  
        }else{  
            throw new Exception("只支持zip和rar格式的压缩包！");  
        }  
    } 
    /**
     * 
     * @param sourceRar
     * @param destDir
     * @throws Exception
     */
    private static void unrar(String sourceRar,String destDir) throws Exception{  
        if("linux".equals(ProcessUtil.getOS())){   //非windows系统  
        	String command = "unrar x -o- -y " +sourceRar + " " + destDir;
        	logger.info("调用系统命令:"+command);
        	ProcessUtil.execute(command);
        }else{//windows系统:需要手动加上以RAR文件名为命名的文件夹   

            File destDirFile = new File(destDir.replace("/" ,"\\"));
            if(!destDirFile.exists()){
            	destDirFile.mkdirs();
            }
    		String command = "WinRAR X " + sourceRar + " " + destDir ;
    		logger.info("调用系统命令:"+command);
        	ProcessUtil.execute(command);
        }
    }
    /**
     * 
     * @param sourceRar
     * @param destDir
     * @throws Exception
     */
    private static void unzip(String sourceRar,String destDir) throws Exception{  
        if("linux".equals(ProcessUtil.getOS())){   //非windows系统  
        	String command = "unzip -o  " +sourceRar + " -d  " + destDir;
        	logger.info("调用系统命令:"+command);
        	ProcessUtil.execute(command);
        }else{//windows系统:需要手动加上以RAR文件名为命名的文件夹   

            File destDirFile = new File(destDir.replace("/" ,"\\"));
            if(!destDirFile.exists()){
            	destDirFile.mkdirs();
            }
    		String command = "WinRAR X " + sourceRar + " " + destDir ;
    		logger.info("调用系统命令:"+command);
        	ProcessUtil.execute(command);
        }
    }
    

    
}
