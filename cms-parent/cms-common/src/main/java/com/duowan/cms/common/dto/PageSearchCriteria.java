/**
 * 
 */
package com.duowan.cms.common.dto;

/**
 * 分页查询条件
 * @author coolcooldee
 */
public abstract class PageSearchCriteria  implements SearchCriteria {
	
	/**
     * 
     */
    private static final long serialVersionUID = 5770836858231951632L;

    /**
	 * 分页查询，每页的数量,默认一页二十五条
	 */
	Integer pageSize = 25;
	
	/**
	 * 分页查询，当前页码
	 */
	Integer pageNo;
	
	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNo() {
		if(pageNo == null ){
			return 1;
		}
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

}
