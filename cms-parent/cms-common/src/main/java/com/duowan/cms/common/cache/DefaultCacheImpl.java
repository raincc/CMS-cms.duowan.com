/**
 * 
 */
package com.duowan.cms.common.cache;

import java.util.*;
import java.util.concurrent.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * 本地缓存的实现（与JVM同一个进程）
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-12
 * <br>==========================
 * @param <K>
 * @param <V>
 */
public class DefaultCacheImpl<K, V> implements ICache<K, V> {

    private static final Log Logger = LogFactory.getLog(DefaultCacheImpl.class.getName());

    /**
     * 存放缓存对象的  key， value 的容器
     */
    ConcurrentHashMap<K, V> caches[];
    /**
     * 存放缓存对象的过期时间  key， date
     */
    ConcurrentHashMap<K, Object> expiryCache;
    /**
     * 清除过期的缓存的定时器
     */
    private ScheduledExecutorService scheduleService;
    /**
     * 定时器的执行频率，默认10s
     */
    private int expiryInterval;
    /**
     * caches[]容器数组个数, 默认10个
     */
    private int moduleSize;

    public DefaultCacheImpl() {
        expiryInterval = 10;
        moduleSize = 10;
        init();
    }

    public DefaultCacheImpl(int expiryInterval, int moduleSize) {
        this.expiryInterval = 10;
        this.moduleSize = 10;
        this.expiryInterval = expiryInterval;
        this.moduleSize = moduleSize;
        init();
    }

    @Override
    public V put(K key, V value) {
        V result = getCache(key).put(key, value);
        expiryCache.put(key, Long.valueOf(-1L));
        return result;
    }

    public V put(K key, V value, Date expiry) {
        V result = getCache(key).put(key, value);
        expiryCache.put(key, Long.valueOf(expiry.getTime()));
        return result;
    }

    @Override
    public V put(K key, V value, int ttl) {
        V result = getCache(key).put(key, value);
        if (ttl == -1){
            expiryCache.put(key, -1L);
        }else {
            Calendar calendar = Calendar.getInstance();
            calendar.add(13, ttl);
            expiryCache.put(key, Long.valueOf(calendar.getTime().getTime()));
        }
        return result;
    }

    @Override
    public V get(K key) {
        checkValidate(key);
        return getCache(key).get(key);
    }

    @Override
    public V remove(K key) {
        V result = getCache(key).remove(key);
        expiryCache.remove(key);
        return result;
    }

    @Override
    public void clear() {
        if (caches != null) {
            ConcurrentHashMap<K, V> arr$[] = caches;
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; i$++) {
                ConcurrentHashMap<K, V> cache = arr$[i$];
                cache.clear();
            }

        }
        if (expiryCache != null) {
            expiryCache.clear();
        }
    }

    private void init() {
        caches = new ConcurrentHashMap[moduleSize];
        for (int i = 0; i < moduleSize; i++) {
            caches[i] = new ConcurrentHashMap<K, V>();
        }

        expiryCache = new ConcurrentHashMap<K, Object>();
        scheduleService = Executors.newScheduledThreadPool(1);
        scheduleService.scheduleAtFixedRate(new CheckOutOfDateSchedule(caches, expiryCache), 0L, expiryInterval * 60, TimeUnit.SECONDS);
        if (Logger.isInfoEnabled()) {
            Logger.info("DefaultCache CheckService is start!");
        }
    }

    private ConcurrentHashMap<K, V> getCache(Object key) {
        long hashCode = key.hashCode();
        if (hashCode < 0L) {
            hashCode = -hashCode;
        }
        int moudleNum = (int) hashCode % moduleSize;
        return caches[moudleNum];
    }

    private void checkValidate(Object key) {
        if (key != null && expiryCache.get(key) != null && ((Long) expiryCache.get(key)).longValue() != -1L
                && (new Date(((Long) expiryCache.get(key)).longValue())).before(new Date())) {
            getCache(key).remove(key);
            expiryCache.remove(key);
        }
    }

    private void checkAll() {
        String key;
        for (Iterator<K> iter = expiryCache.keySet().iterator(); iter.hasNext(); checkValidate(key)) {
            key = (String) iter.next();
        }
    }

    class CheckOutOfDateSchedule implements Runnable {

        ConcurrentHashMap<K, V> caches[];
        ConcurrentHashMap<K, Object> expiryCache;

        public void run() {
            check();
        }

        public void check() {
            try {
                ConcurrentHashMap<K, V> arr$[] = caches;
                int len$ = arr$.length;
                for (int i$ = 0; i$ < len$; i$++) {
                    ConcurrentHashMap<K, V> cache = arr$[i$];
                    Iterator<K> keys = cache.keySet().iterator();
                    do {
                        if (!keys.hasNext()) {
                            break;
                        }
                        K key = (K) keys.next();
                        if (expiryCache.get(key) != null) {
                            long date = ((Long) expiryCache.get(key)).longValue();
                            if (date > 0L && (new Date(date)).before(new Date())) {
                                expiryCache.remove(key);
                                cache.remove(key);
                            }
                        }
                    } while (true);
                }

            } catch (Exception ex) {
                DefaultCacheImpl.Logger.info("DefaultCache CheckService is start!");
            }
        }

        public CheckOutOfDateSchedule(ConcurrentHashMap<K, V> caches[], ConcurrentHashMap<K, Object> expiryCache) {
            super();
            this.caches = caches;
            this.expiryCache = expiryCache;
        }
    }

    @Override
    public boolean containsKey(K key) {
        checkValidate(key);
        return getCache(key).containsKey(key);
    }

    @Override
    public int size() {
        checkAll();
        return expiryCache.size();
    }

    @Override
    public void destroy() {
        try {
            clear();
            if (scheduleService != null) {
                scheduleService.shutdown();
            }
            scheduleService = null;
        } catch (Exception ex) {
            Logger.error(ex);
        }

    }

}
