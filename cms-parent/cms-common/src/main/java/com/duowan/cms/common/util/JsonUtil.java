package com.duowan.cms.common.util;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * JSON工具类
 * @author coolcooldee
 *
 */
public abstract class JsonUtil {

    protected final static Log logger = LogFactory.getLog(JsonUtil.class);

    /**
     * 将JSON对象字符串转成JavaBean对象
     * @param <T>
     * @param cls
     * @param jsonObjectString
     * @return
     */
    public static <T> T jsonString2JavaBean(Class<T> cls, String jsonObjectString) {
        JSONObject jsonObject2 = (JSONObject) JSONSerializer.toJSON(jsonObjectString.trim());
        @SuppressWarnings("unchecked")
        T jban = (T) JSONObject.toBean(jsonObject2, cls);
        jsonObject2 = null;
        return jban;
    }
   
    /**
     * 将JSON对象字符串转成JavaBean对象列表
     * @param <T>
     * @param cls
     * @param jsonObjectString
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> jsonString2JavaBeanList(Class<T> cls, String jsonObjectString) {
        List<T> ls = new ArrayList<T>();
        JSONArray jsonArray =JSONArray.fromObject(jsonObjectString);
       if(jsonArray==null || jsonArray.isEmpty()){
          return  ls;
       }else{
           for (int i=0; i<jsonArray.size(); i++) {
               JSONObject jsonObject = jsonArray.getJSONObject(i);
               T t =  (T)JSONObject.toBean(jsonObject, cls);
               ls.add(t);
        }
       }
        return ls;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> List<T> jsonArray2JavaBeanList(Class<T> cls, JSONArray jsonArray) {
        List<T> ls = new ArrayList<T>();
       if(jsonArray==null || jsonArray.isEmpty()){
          return  ls;
       }else{
           for (int i=0; i<jsonArray.size(); i++) {
               JSONObject jsonObject = jsonArray.getJSONObject(i);
               T t =  (T)JSONObject.toBean(jsonObject, cls);
               ls.add(t);
        }
       }
        return ls;
    }

    /**
     * 将JavaBean对象转成Json对象字符串
     * @param javaBean
     * @param ignoreFieldNames
     * @return
     */
    public static String javaBean2JsonString(Object javaBean, String... ignoreFieldNames) {
        return javaBean2JsonString(javaBean, null, ignoreFieldNames);
    }

    /**
     * 将JavaBean对象转成Json对象字符串
     * @param javaBean
     * @param jc
     * @param ignoreFieldNames
     * @return
     */
    public static String javaBean2JsonString(Object javaBean, JsonConfig jc, String... ignoreFieldNames) {
        if (javaBean == null) {
            logger.warn("javaBean is null!");
            return null;
        }
        if (jc == null)
            jc = JsonConfigUtil.getJsonConfig4CommonProperties();

        JSONObject jsonObject = JSONObject.fromObject(javaBean, jc);
        if (ignoreFieldNames != null) {
            for (String ignoreFieldName : ignoreFieldNames) {
                jsonObject.discard(ignoreFieldName);
                logger.debug("ignore field : " + ignoreFieldName);
            }
        }

        String jsonStr = jsonObject.toString();
        jsonObject = null;
        if (logger.isDebugEnabled())
            logger.debug("jsonStr: " + jsonStr);
        return jsonStr;
    }

    /**
     * 将JavaBean对象转成Json对象字符串
     * @param javaBean
     * @param ignoreFieldNames
     * @return
     */
    public static String javaBean2JsonString4AllProperties(Object javaBean, String... ignoreFieldNames) {
        return javaBean2JsonString(javaBean, JsonConfigUtil.getJsonConfig(), ignoreFieldNames);
    }

    public static void main(String[] args) {
        //String json="[{'id':'198','actName':'abc','gameName':'%E5%80%A9%E5%A5%B3%E5%B9%BD%E9%AD%82','hadbook':'2612233','url':'bbb','gameUrl':'ccc','imgUrl':'http:\/\/img1.yxtq.yy.com\/tq\/1335605819795.jpg','actState':'%E9%A2%86%E5%8F%96','actStateId':4,'gotcount':663338,'taoCount':0,'changeTime':'1346428865'}]";
//       String json = "[{id:\"19\",name:'abc'}, {id:'18',name:'abc'}]";
//        JSONArray jsonArray = JSONArray.fromObject(json);
//        Object[] os =  jsonArray.toArray();
//        JSONObject jsonObject =jsonArray.getJSONObject(0);
//        User user =  (User)JSONObject.toBean(jsonObject, User.class);
//        System.out.println(user.getId());
    }

    public static class User {
        String id;
        String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    
}
