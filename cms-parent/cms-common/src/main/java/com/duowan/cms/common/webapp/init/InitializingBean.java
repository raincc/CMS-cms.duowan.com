package com.duowan.cms.common.webapp.init;

/**
 * 接口或类的说明：WEB应用上下文初始Bean的接口，主要用来在应用上下文加载完成后执行初始化实现了此接口的Spring Bean对象<BR>
 * 使用方式：实现此类，将实现类配置成Spring的Bean对象即可，前置条件是应用中使用了{@link cn.uc.vip.common.webapp.listener.ExtendedSpringContextLoaderListener}类
 * @author coolcooldee
 *
 */
public interface InitializingBean {

    /**
     * 初始化方法
     */
    void init();

}
