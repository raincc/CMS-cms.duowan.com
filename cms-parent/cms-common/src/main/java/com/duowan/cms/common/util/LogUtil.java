package com.duowan.cms.common.util;
/**
 * 用于处理业务的日志的日志工具
 * <p>作　　者：黄均杨
 * <p>完成日期：2012-11-22
 */
public class LogUtil {

	/**
	 *  截取前十条日志
	 *  作用：防止日志过长，而导致存入数据库失败
	 * @return
	 */
	public static String getLogBeforeTen(String logs){
		String[] logArr = logs.split("<br>");
		if (logArr.length < 11) {
			return logs;
		}
		logs = logs.substring(0, logs.lastIndexOf(logArr[10]));
		return logs;
	}

}
