package com.duowan.cms.common.service;

import java.util.List;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.dto.DataTransferObject;

/**
 *  DO到DTO转换器
 * @author coolcooldee
 */
public interface DoToDtoConvertor<O extends DomainObject, D extends DataTransferObject> {

    /**
     * 把一个DO对象转成DTO对象
     * @param obj
     * @return
     */
    public D do2Dto(O obj);

    /**
     * 批量把List中的DO对象转成DTO对象
     * @param os
     * @return
     */
    public List<D> dos2Dtos(List<O> os);

    public D doToDtoWithLazy(O obj);

    public List<D> dos2DtosWithLazy(List<O> os);

}
