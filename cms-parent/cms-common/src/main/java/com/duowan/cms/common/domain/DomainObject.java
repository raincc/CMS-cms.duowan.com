package com.duowan.cms.common.domain;

import java.io.Serializable;

/**
 * 领域对象标识接口
 * @author coolcooldee
 */
public interface DomainObject extends Serializable {

}