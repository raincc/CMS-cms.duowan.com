/**
 * 
 */
package com.duowan.cms.common.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.regex.Pattern;

/**
 * 处理数字的工具
 * @author coolcooldee
 *
 */
public class NumberUtil {

    /**
     * 判断一个字符串是否是数字
     * @param numStr
     * @return
     */
    public static boolean isNum(String numStr) {
        if (numStr == null)
            return false;
        Pattern pattern = Pattern.compile("[0-9]+");
        return pattern.matcher(numStr).matches();
    }

    private NumberUtil() {

    }

}
