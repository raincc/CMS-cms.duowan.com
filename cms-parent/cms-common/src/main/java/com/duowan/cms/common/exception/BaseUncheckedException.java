/**
 * 
 */
package com.duowan.cms.common.exception;

/**
 * 不需要检查的异常基类
 * @author coolcooldee
 */
public class BaseUncheckedException extends RuntimeException {

	private static final long serialVersionUID = -6585490884345634143L;
	private String msgKey;

    public BaseUncheckedException(String msgKey, String message, Throwable throwable) {
        super(message, throwable);
        this.msgKey = msgKey;
    }

    public BaseUncheckedException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public BaseUncheckedException(String msgKey, String message) {
        super(message);
        this.msgKey = msgKey;
    }

    public BaseUncheckedException(String message) {
        super(message);
    }

    public BaseUncheckedException(Throwable throwable) {
        super(throwable);
    }

    public String getMsgKey() {
        return msgKey;
    }

    public void setMsgKey(String msgKey) {
        this.msgKey = msgKey;
    }

}
