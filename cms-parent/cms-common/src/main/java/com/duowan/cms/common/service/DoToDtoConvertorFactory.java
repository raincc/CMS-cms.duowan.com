/**
 * 
 */
package com.duowan.cms.common.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.exception.BaseUncheckedException;


/**
 * DoToDtoConvertor工厂
 * @author coolcooldee
 *
 */
public class DoToDtoConvertorFactory {
    protected final static Log LOG = LogFactory.getLog(DoToDtoConvertorFactory.class);

    @SuppressWarnings("rawtypes")
    private static Map map = new HashMap();
    private Set<String> convertorClassNames = new HashSet<String>();

    public static void register(Class<? extends DomainObject> domainObjectClass, DoToDtoConvertor<? extends DomainObject, ? extends DataTransferObject> convertor) {
        if (LOG.isInfoEnabled())
            LOG.info("注册转换器：【name=" + domainObjectClass + ";convertorClassName=" + convertor.getClass().getName() + "】");
        map.put(domainObjectClass, convertor);
    }

    @SuppressWarnings("rawtypes")
    public static DoToDtoConvertor getConvertor(Class<? extends DomainObject> domainObjectClass) {
        if (map.get(domainObjectClass) == null) {
            throw new BaseUncheckedException(DoToDtoConvertorFactory.class.getName().concat("unregisterDoToDtoConvertor"), "名称为\"" + domainObjectClass + "\"的转换器未注册，请先注册后再使用! ");
        }
        return (DoToDtoConvertor) map.get(domainObjectClass);
    }

    public void setConvertorClassNames(Set<String> convertorClassNames) {
        this.convertorClassNames = convertorClassNames;
    }

    public void init() throws ClassNotFoundException {
        for (String convertorClassName : convertorClassNames) {
            Class.forName(convertorClassName);
        }
        convertorClassNames.clear();
    }

}
