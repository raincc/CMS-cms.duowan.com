package com.duowan.cms.common.repository;

import java.util.List;

import com.duowan.cms.common.domain.DomainObject;
import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.dto.DataTransferObject;
import com.duowan.cms.common.dto.PageSearchCriteria;

/**
 * 领域对象仓库接口
 * @author coolcooldee
 * @version 1.0
 * @created 04-九月-2012 10:55:04
 */
public interface DomainObjectRepository<O extends DomainObject , D extends DataTransferObject, C extends PageSearchCriteria> {

	/**
	 * 保存领域对象并返回其ID
	 * 
	 * @param object
	 */
	public void save(O object);

	/**
	 * 更新领域对象
	 * 
	 * @param object    更新领域对象
	 */
	public void update(O object);

	/**
	 * 删除领域对象(慎用，直接是物理删除)
	 * 
	 * @param object
	 */
	public void delete(O object);

	/**
	 * 删除集合中的领域对象
	 * 
	 * @param objects
	 */
//	public void deleteAll(List<O> objects);

	/**
	 * 根据标识获取领域对象，当领域对象不存在时，不能返回null，需要抛出ObjectNotFoundException
	 * 
	 * @param id
	 */
//	public O getById(Serializable id);

	/**
	 * 根据ID的集合获取匹配的集合对象
	 * 
	 * @param  ids
	 */
//	public List<O> getByIds(List<Serializable>  ids);

	/**
	 * 根据ids删除持久化对象，返回删除的数量
	 * 
	 * @param  ids
	 */
//	public int deleteByIds(List<Serializable>  ids);

//	/**
//	 * 查询
//	 * 
//	 * @param searchCriteria    查询条件
//	 */
//	public List<D> search(C searchCriteria);

	/**
	 * 分页查询
	 * @param searchCriteria    查询条件
	 */
	public Page<D> pageSearch(C searchCriteria);
	
	/**
	 * 列表查询
	 * @param searchCriteria 查询条件
	 * @return
	 */
	public List<D> listSearch(C searchCriteria);

}