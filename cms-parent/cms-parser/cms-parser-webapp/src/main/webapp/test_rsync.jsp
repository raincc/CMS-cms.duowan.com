<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.IOException"%>
<%@page import="com.duowan.cms.common.util.StringUtil"%>
<%@page import="com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.duowan.cms.dto.article.ArticleInfo"%>
<%@page
	import="com.duowan.cms.core.rmi.client.article.ArticleRemoteService"%>
<%@page import="com.duowan.cms.dto.channel.ChannelInfo"%>
<%@page
	import="com.duowan.cms.core.rmi.client.channel.ChannelRemoteService"%>
<%@page
	import="com.duowan.cms.common.webapp.content.SpringApplicationContextHolder"%>
<%@page import="com.duowan.cms.parser.timetask.FlushTimeTask"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%! Logger logger = Logger.getLogger("test_rsync.jsp");  %>
<%
	String rsyncFileName = "ceshi/a/index.html";
	StringBuffer outputResult=new StringBuffer();
    try {
        String rsyncScript = PropertiesHolder.get("rsyncScriptFile");
        if (!StringUtil.isEmpty(rsyncScript)) {
            Runtime.getRuntime().exec(rsyncScript + " " + rsyncFileName);
                    //输入同步的结果日志
                    Process process = Runtime.getRuntime().exec(rsyncScript+" "+rsyncFileName);
                    BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));  
                    String output = null;  
                    while (null != (output = br.readLine())){
                        outputResult.append(output+"\n");
                    }
                    logger.info("文件"+rsyncFileName+"同步结果输出："+outputResult.toString());
            		logger.info("成功rsync分发同步文件:" + rsyncFileName);
        }
    } catch (IOException e1) {
        logger.error("rsync同步文件" + rsyncFileName + "失败，原因：" + e1.getMessage());
        e1.printStackTrace();
    }
%>
<%="文件"+rsyncFileName+"同步结果输出："+outputResult.toString() %>


