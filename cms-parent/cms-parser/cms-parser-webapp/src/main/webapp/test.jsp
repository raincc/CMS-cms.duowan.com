<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="com.duowan.cms.common.util.StringUtil"%>
<%@page import="com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder"%>
<%@page import="com.duowan.cms.common.util.ThreadUtil"%>
<%@page import="com.duowan.cms.core.rmi.client.tag.TagRemoteService"%>
<%@page import="com.duowan.cms.core.rmi.client.template.TemplateRemoteService"%>
<%@page import="com.duowan.cms.core.rmi.client.article.Article4TagListRemoteService"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.duowan.cms.common.exception.BaseCheckedException"%>
<%@page import="com.duowan.cms.dto.template.TemplateInfo"%>
<%@page import="com.duowan.cms.dto.template.TemplateSearchCriteria"%>
<%@page import="java.util.List"%>
<%@page import="com.duowan.cms.dto.channel.ChannelInfo"%>
<%@page import="com.duowan.cms.parser.timetask.FlushTimeTask"%>
<%@page import="com.duowan.cms.parser.TemplateBatchParserService"%>
<%@page import="com.duowan.cms.core.rmi.client.article.ArticleRemoteService"%>
<%@page import="com.duowan.cms.parser.TemplateParserService"%>
<%@page import="com.duowan.cms.common.webapp.content.SpringApplicationContextHolder"%>
<%@page import="com.duowan.cms.core.rmi.client.channel.ChannelRemoteService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%! Logger logger = Logger.getLogger("test.jsp");  %>
<%
//String channelId = "ceshi";
//Long articleId = 218124683304L;
//String tag = "测试";
//TemplateBatchParserService templateBatchParserService = SpringApplicationContextHolder.getBean("templateBatchParserService", TemplateBatchParserService.class);
FlushTimeTask flushTimeTask = SpringApplicationContextHolder.getBean("flushTimeTask", FlushTimeTask.class);
ChannelRemoteService channelRemoteService = SpringApplicationContextHolder.getBean("channelRemoteService", ChannelRemoteService.class);
//templateBatchParserService.flushOneTagInChannel(channelId, tag);
//flushTimeTask.flushTemplatesAndTagsByEffectTag();


		final int times = 20; // 15分钟以内变化的
        long startTime = System.currentTimeMillis();
        logger.info("开始【全频道刷新】任务.");
        List<ChannelInfo> channelInfoList = channelRemoteService.getAllChannel();
        for (final ChannelInfo channelInfo : channelInfoList) {
            ThreadUtil.getThreadPool().execute(new Runnable() {
                public void run() {
                    flushOnTimes(channelInfo, times);
                }
            });
           
        }
        long endTime = System.currentTimeMillis();
        logger.info("结束【全频道刷新】任务,共耗时" + (endTime - startTime) + "毫秒");
       


%>
OK~haha 


<%!
private void flushOnTimes(ChannelInfo channelInfo, int times) {
    Article4TagListRemoteService article4TagListRemoteService = SpringApplicationContextHolder.getBean("article4TagListRemoteService", Article4TagListRemoteService.class);
    TemplateRemoteService templateRemoteService = SpringApplicationContextHolder.getBean("templateRemoteService", TemplateRemoteService.class);
    TemplateParserService templateParserService = SpringApplicationContextHolder.getBean("templateParserRemoteService", TemplateParserService.class);
    TagRemoteService tagRemoteService = SpringApplicationContextHolder.getBean("tagRemoteService", TagRemoteService.class);
    
    List<String> effectTagList = article4TagListRemoteService.getRecentEffectTagsInArticle(channelInfo.getId(), times);
    if (effectTagList == null || effectTagList.isEmpty())
        return;
    logger.info("[" + channelInfo.getName() + "(" + channelInfo.getId() + ")" + "]频道" + times + "分钟内所影响的标签有：" + this.tagListToStr(effectTagList).replace(",", "   "));
    TemplateSearchCriteria searchCriteria = new TemplateSearchCriteria();
    searchCriteria.addRelateTags(this.tagListToStr(effectTagList));
    searchCriteria.setChannelId(channelInfo.getId());
    searchCriteria.setKeyword("data.");// 只有模板中包含了"data.XXXX"的方法的才需要刷新
    List<TemplateInfo> templateInfoList = templateRemoteService.listSearch(searchCriteria);
    logger.info("[" + channelInfo.getName() + "(" + channelInfo.getId() + ")" + "]频道" + times + "分钟内需要刷新需要刷新的模板数:" + templateInfoList.size());

    // 刷新模板静态页面
    for (TemplateInfo templateInfo : templateInfoList) {
        try {
            templateParserService.parseTemplateAndGenerateFile(channelInfo, templateInfo);
        } catch (BaseCheckedException e) {
            logger.error("刷新模板[channelId=" + channelInfo.getId() + ", templateId=" + templateInfo.getId() + "]的定时任务出错，错误原因：", e);
        }
    }

    // 刷新tag静态页面
    for (String tag : effectTagList) {
        try {
            templateParserService.parseTagAndGenerateFile(channelInfo, tagRemoteService.getByChannelIdAndName(channelInfo.getId(), tag));
        } catch (BaseCheckedException e) {
            logger.error("刷新标签[channelId=" + channelInfo.getId() + ", tag=" + tag + "]的定时任务出错，错误原因：", e);
        }
    }
    
    logger.info("频道【"+channelInfo.getId()+"】刷新完成.");
    
    
}


private String tagListToStr(List<String> effectTagList) {
    StringBuffer tags = new StringBuffer();
    for (String tag : effectTagList) {
        tags.append(",").append(tag);
    }
    return tags.toString();
}


%>