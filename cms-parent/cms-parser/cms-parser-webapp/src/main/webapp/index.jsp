<%@page import="com.duowan.cms.core.rmi.client.template.AutoFlushTagRemoteService"%>
<%@page import="com.duowan.cms.core.rmi.client.article.Article4TagListRemoteService"%>
<%@page import="com.duowan.cms.dto.template.AutoFlushTagInfo"%>
<%@page import="com.duowan.cms.common.util.StringUtil"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.duowan.cms.common.util.DateUtil"%>
<%@page import="java.util.Date"%>
<%@page import="com.duowan.cms.parser.TemplateBatchParserService"%>
<%@page import="com.duowan.cms.dto.article.ArticleInfo"%>
<%@page import="com.duowan.cms.core.rmi.client.article.ArticleRemoteService"%>
<%@page import="com.duowan.cms.dto.channel.ChannelInfo"%>
<%@page import="com.duowan.cms.core.rmi.client.channel.ChannelRemoteService"%>
<%@page import="com.duowan.cms.common.webapp.content.SpringApplicationContextHolder"%>
<%@page import="com.duowan.cms.parser.timetask.FlushTimeTask"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%! Logger logger = Logger.getLogger("index.jsp");  %>
<%

	String channelId = "zt2";
	String flag = request.getParameter("flag");
	Date date = DateUtil.parse("2013-04-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
	FlushTimeTask flushTimeTask = SpringApplicationContextHolder.getBean("flushTimeTask", FlushTimeTask.class);
	flushTimeTask.flushSpecifiedTermplate();
	
	//ChannelRemoteService channelRemoteService = SpringApplicationContextHolder.getBean("channelRemoteService", ChannelRemoteService.class);
	//ChannelInfo channelInfo = channelRemoteService.getById("wow");
	//ArticleRemoteService articleRemoteService = SpringApplicationContextHolder.getBean("articleRemoteService", ArticleRemoteService.class);
	//ArticleInfo articleInfo = articleRemoteService.getByChannelIdAndArticleId("wow", 218051918170L);
	//TemplateBatchParserService templateBatchParserService = SpringApplicationContextHolder.getBean("templateBatchParserRemoteService", TemplateBatchParserService.class);
	//templateBatchParserService.flushArticleAfterDateInChannelId(channelId, date);
%>

<%!

public void fetchTagsNeedFlush() {
  	ChannelRemoteService channelRemoteService = SpringApplicationContextHolder.getBean("channelRemoteService", ChannelRemoteService.class);
  	Article4TagListRemoteService article4TagListRemoteService = SpringApplicationContextHolder.getBean("article4TagListRemoteService", Article4TagListRemoteService.class);
  	AutoFlushTagRemoteService autoFlushTagRemoteService = SpringApplicationContextHolder.getBean("autoFlushTagRemoteService", AutoFlushTagRemoteService.class);
  	
    final int times = 30; // 6分钟以内变化的
    long startTime = System.currentTimeMillis();
    logger.info("开始【采集最近更新过的tag】任务====================");
    List<ChannelInfo> channelInfoList = channelRemoteService.getAllChannel();
    for (final ChannelInfo channelInfo : channelInfoList) {
        List<String> effectTagList = article4TagListRemoteService.getRecentEffectTagsInArticle(channelInfo.getId(), times);
        if (effectTagList == null || effectTagList.isEmpty())
            continue;
        for(String tagName : effectTagList){
            if(StringUtil.isEmpty(tagName)) 
                continue;
            AutoFlushTagInfo autoFlushTagInfo = new AutoFlushTagInfo(channelInfo.getId(), tagName);
            if(!autoFlushTagRemoteService.isExistUnFlushTag(channelInfo.getId(), tagName)){
                autoFlushTagRemoteService.save(autoFlushTagInfo);
                logger.info("在专区"+channelInfo.getId()+"内采集到需要刷新的tag："+tagName+"=======================");
            }else{
                logger.info("在专区"+channelInfo.getId()+"内的tag："+tagName+"已经存在刷新队列，无需重复采集====================");
            }
        }
    }
    long endTime = System.currentTimeMillis();
    logger.info("结束【采集最近更新过的tag】任务,共耗时" + (endTime - startTime) + "毫秒=================");
}


%>



