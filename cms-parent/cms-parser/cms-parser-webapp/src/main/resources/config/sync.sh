#!/usr/bin/env bash

nginx_servers="221.204.223.154
113.108.228.198
183.61.6.23
119.188.90.76
115.238.171.143
218.60.98.93
115.238.171.183
183.61.143.112
"

sync_subdomains="
qqxl.duowan.com
nba2k.duowan.com
dt2.duowan.com
d2.duowan.com
c9.duowan.com
rift.duowan.com
wot.duowan.com
pc.duowan.com
an.duowan.com
apple.duowan.com
mt.duowan.com
iphone.duowan.com
ipad.duowan.com
coc.duowan.com
nds.duowan.com
psp.duowan.com
newgame.duowan.com
aion.duowan.com
dnf.duowan.com	
df.duowan.com
wulin2.duowan.com
xunxian.duowan.com
jxsj.duowan.com
tx2.duowan.com 
tx3.duowan.com
dn.duowan.com
jf.duowan.com
xyq.duowan.com
xa.duowan.com
jx3.duowan.com
zx.duowan.com
zt2.duowan.com
bns.duowan.com
ssf4.duowan.com
tv.duowan.com
mhp3.duowan.com
mhp4.duowan.com
much.duowan.com
nds.duowan.com
3ds.duowan.com
psv.duowan.com
"

# code

# 保存上次函数的返回值。没有返回值的函数不应该修改这个对象。
last_result=''

local_sync_dir=/data1/www/cms.duowan.com/

# 从类似qqxl/index.html的文件名中得到对应url的Host。比如
# qqxl/index.html对应的Host是qqxl.duowan.com
function get_subdomain(){
	filename=$1
	last_result=${filename%%/*}.duowan.com
	#a=""
	#for s in $sync_subdomains
	#do
	#	if [ "$last_result" == "$s" ]
	#	then
	#		a=$last_result
	#	fi
	#done
	#last_result=$a
}

# 从类似qqxl/index.html的文件名中得到对应url的path。比如
# qqxl/index.html对应的path是/index.html
function get_path(){
	filename=$1
	left=${filename%%/*}
	num=${#left}
	path=${filename:num}
	last_result=$path
}

# Nginx机器检查失败后的处理函数
# 参数：$2 ip
#	$1 url (lol.duowan.com/index.html)
function nginx_fail_handler(){
	url=$1	
	ip=$2

        work_dir=`pwd`
        cd $local_sync_dir
	echo `pwd`
        command="rsync -vrz -R $url release@$ip::cms_release_code/ --password-file=/etc/rsyncd_users_tomcat"
	echo "`date`"
	echo "$command"
	$command
        cd $work_dir
}



# 检查文件的同步情况，通过md5sum判断文件是否一致。
# 参数：$1 filename 比如：qqxl/index.html
function check_file(){
	filename=$1
	get_subdomain $filename
	subdomain=$last_result

	#if [ -z "$subdomain" ]
	#then
		get_path $filename
		path=$last_result

		for ip in $nginx_servers
		do
			nginx_fail_handler "$subdomain$path" "$ip"
		done
	#fi
}



filename=$1
check_file $filename
