package com.duowan.cms.parser.rmi.client.template;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.tag.TagInfo;
import com.duowan.cms.dto.template.TemplateInfo;

/**
 * 接口或类的说明
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-8
 * <br>==========================
 */
public interface TemplateParserRemoteService extends  RemoteService{
    
    //异常码：文件生成失败
    public final static String FILE_CAN_NOT_BE_GENERATED = TemplateParserRemoteService.class.getName().concat("001");
    
    /**
     * 解析模板，并且生成文件
     */
    //public void parseTemplateAndGenerateFile(String channelId, Long templateId)  throws BaseCheckedException ;
    
    /**
     * 结合模板，解析文章，并且生成文件
     */
    //public void parseArticleAndGenerateFile(String channelId, Long articleId)  throws BaseCheckedException ;
    /**
     * 清除文章静态页面内容
     */
    public void cleanUpArticleFile(String channelId, Long articleId)  throws BaseCheckedException ;
    
    /**
     * 解析模板，并且生成文件
     */
    public void parseTemplateAndGenerateFile(ChannelInfo channelInfo, TemplateInfo templateInfo)  throws BaseCheckedException ;
    
    /**
     * 结合模板，解析文章，并且生成文件
     */
    public void parseArticleAndGenerateFile(ChannelInfo channelInfo, ArticleInfo articleInfo)  throws BaseCheckedException ;
    
    /**
     * 解析Tag模板，并且生成文件
     */
    public void parseTagAndGenerateFile(ChannelInfo channelInfo, TagInfo tagInfo) throws BaseCheckedException ;
    
    /**
     * 生成空链接页面
     * @param list
     * @throws BaseCheckedException
     */
    public void parseEmptyLinkAndGenerateFile(Article4TagListInfo a) throws BaseCheckedException;
    

}
