package com.duowan.cms.parser.rmi.client.dir;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder;

public interface DirRemoteService extends RemoteService {
    
    public static Logger logger = Logger.getLogger(DirRemoteService.class);
    
    //创建目录脚本
    public final static String MKDIR_FOR_CHANNEL = PropertiesHolder.get("mkdir4ChannelScriptFile");
    
    /**
     * 为开新专区创建目录
     * @param channelId
     */
    public void mkdir4NewChannel(String channelId) throws IOException, InterruptedException;

}
