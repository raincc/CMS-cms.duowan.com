/**
 * 
 */
package com.duowan.cms.dir;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.duowan.cms.common.util.SystemCommandUtil;
import com.duowan.cms.parser.rmi.client.dir.DirRemoteService;

/**
 * 模板机目录服务（主要用于开专区，创建目录）
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-26
 * <br>==========================
 */
@Service("dirRemoteService")
public class DirServiceImpl implements DirRemoteService{

    @Override
    public void mkdir4NewChannel(String channelId) throws IOException, InterruptedException {
        SystemCommandUtil.exec(MKDIR_FOR_CHANNEL +" "  + channelId);
        logger.info("开专区(channelId="+channelId+")，在模板机器建目录成功。");
    }
    
    

}
