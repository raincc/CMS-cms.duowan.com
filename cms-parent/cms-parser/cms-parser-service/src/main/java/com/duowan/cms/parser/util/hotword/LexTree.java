
package com.duowan.cms.parser.util.hotword;

import java.util.HashMap;
import java.util.Map;

public class LexTree<T> {

	private Map<Character, LexTree<T>> charMap = new HashMap<Character, LexTree<T>>();
	private T attachObj;

	public LexTree<T> getSubLexTree(Character c) {

		return charMap.get(c);
	}

	public void putSubChar(Character c) {

		LexTree<T> lex = new LexTree<T>();
		charMap.put(c, lex);
	}

	public T getAttachObj() {

		return attachObj;
	}

	public void setAttachObj(T attachObj) {

		this.attachObj = attachObj;
	}

}
