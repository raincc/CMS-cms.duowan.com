/**
 * 
 */
package com.duowan.cms.parser.timetask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.duowan.cms.common.exception.BaseCheckedException;

/**
 * 模板刷新的定时任务
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-10-30
 * <br>==========================
 */
public interface FlushTimeTask {
    
    public final Log logger = LogFactory.getLog(FlushTimeTask.class);
    
    /**
     * 刷新频率：0 0/15 * * * ?
     * 
     * 1.获取所有普通的频道的信息
     * 2.找到十五分钟内某频道更新的文章的Tag，或修改过的Tag，
     * 3.刷新所有指定标签关联的模板和指定标签对应的标签页
     */
    public void flushTemplatesAndTagsByEffectTag();
    
    /**
     * 刷新频率：0 0/15 * * * ?
     * 
     * 1.获取所有重点频道的信息
     * 2.找到十五分钟内某频道更新的文章的Tag，或修改过的Tag，
     * 3.刷新所有指定标签关联的模板和指定标签对应的标签页
     */
    public void flushImportantTemplatesAndTagsByEffectTag();
    
    
    /**
     * 刷新频率：0 0/15 * * * ?
     * 
     * 刷新最近更新的tag
     * 
     */
    public void flushTagByMinute();
    
    /**
     * 刷新频率：0 0/8 * * * ?
     * 
     * 1.获取首页频道的信息
     * 2.找到十五分钟内首页频道更新的文章的Tag，或修改过的Tag，
     * 3.刷新所有指定标签关联的模板和指定标签对应的标签页
     */
    public void flushWWWTemplatesAndTagsByEffectTag();
    
    
    /**
     * 刷新频率：0 0/15 * * * ? 
     * 处理预发布的文章（所以，预发布文章的发表时间会有误差，误差在15分钟以内）
     */
    public void publishPrepubArticle();
    
    /**
     * 刷新频率：0 0/15 * * * ? 
     * 处理预发布的空链接（所以，预发布空链接的发表时间会有误差，误差在15分钟以内）
     */
    public void publishPreEmptyLink();
    
    /**
     * 刷新频率：0 0/15 * * * ? 
     * 定时刷新后台指定的模板（重点频道和非重点频道）
     */
    public void flushSpecifiedTermplate();
    
    /**
     *  刷新频率：34,20 * * * * 
     *  刷新"合作频道"的指定其他模板(合作频道和指定模板从文件里读入)
     */
    public void flushCooperateTemplate();
    
    /**
     * 刷新频率：* / 11 * * * *
     * 刷新未过期的投票
     */
    public void flushVote();
    
    /**
     * 刷新频率：0 0/5 * * * ? 
     * 采集需要刷新的tags，并保存到自动刷新tag仓库，等待刷新
     */
   public void fetchTagsNeedFlush();
   
   /**
    * 刷新采集到自动刷新tag仓库的数据
    */
   public void flushTagsByFetched();
   
   /**
    * 刷新跨专区调用的模板
    */
   public void flushTagsCrossChannel();
    
}
