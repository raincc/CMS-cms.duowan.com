/**
 * 
 */
package com.duowan.cms.parser;

import java.util.Date;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.Service;

/**
 * 模板刷新服务
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-13
 * <br>==========================
 */
public interface TemplateBatchParserService extends Service {
    
    
    /**
     * 刷新本频道的指定Tag，使用“标签/标签图”模板，生成Tag静态页面
     * @param channelId
     */
    public void flushOneTagInChannel(String channelId, String tag)  ;

    /**
     * 刷新本频道的所有Tag，使用“标签/标签图”模板，生成Tag静态页面
     * @param channelId
     */
    public void flushAllTagInChannel(String channelId) throws BaseCheckedException ;
    
    /**
     * 刷新本频道的所有模板，生成模板静态页面
     * @param channelId
     */
    public void flushAllTemplateInChannnel(String channelId);
    
    /**
     * 刷新本频道的所有文章，生成文章静态页面
     * @param channelId
     */
    public void flushAllArticleInChannelId(String channelId);
    
    /**
     * 刷新本频道的指定日期之后的所有文章，生成文章静态页面
     * 注意：当data为null时候，即表示刷新所有的文章，等同于flushAllArticleInChannelId方法
     * @param channelId
     */
    public void flushArticleAfterDateInChannelId(String channelId, Date date);
    
    /**
     * 刷新指定tag下的所有文章
     * @param channelId
     * @param tag
     */
    public void flushArticleByTagInChannelId(String channelId, String tag);
    
    /**
     * 刷新本频道的按日期查询的文章，生成文章静态页面
     * @param channelId
     */
    public void flushArticleInChannelByDate(String channelId, Date beginTime, Date endTime);
    
    /**
     * 刷新本频道指定Tag下所有文章，生成文章静态页面
     * @param channelId
     */
    public void flushArticleInChannelByTag(String channelId);
    
}
