package com.duowan.cms.parser.util;

import java.io.IOException;

import com.duowan.cms.common.util.FileUtil;
import com.duowan.cms.common.util.StringUtil;

public class PagerGenerator {

    /**
     * 当在本页查看全文时，文件名称：channelId/YYMM/articleId_all.html
     */
    public static final String PAGER_NAME_SUFFIX_ALL = "all";
    
    /**
     * 是否显示首页
     */
    private boolean firstPage = false;
    
    /**
     * 是否显示尾页
     */
    private boolean lastPage = false;
    
    /**
     * 当前页码
     */
    private int curPage = 1;
    
    /**
     * 总页码
     */
    private int totalCount = 1;
    
    /**
     * 页码显示长度
     */
    private int showPageLength = 10;
    
    private boolean all = false;
    
    /**
     * 第一页的url=baseUrl+extension.第i页的url=baseUrl+separator+i+extension
     */
    private String baseUrl;
    
    /**
     * 分隔符，用以构造分页的url,默认是下划线"_"
     */
    private String separator = "_";
    
    /**
     * 后缀名，扩展名，用以构造分页的url,默认是".html"
     */
    private String extension = ".html";

    public boolean isFirstPage() {
        return firstPage;
    }

    public void setFirstPage(boolean firstPage) {
        this.firstPage = firstPage;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getShowPageLength() {
        return showPageLength;
    }

    public void setShowPageLength(int showPageLength) {
        this.showPageLength = showPageLength;
    }
    
    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
    
    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }
    
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public PagerGenerator(String baseUrl, int totalCount) {
        if(!StringUtil.isEmpty(baseUrl) && baseUrl.indexOf(".") != -1){
            int index = baseUrl.indexOf(".");
            this.baseUrl = baseUrl.substring(0, index);
            this.extension = baseUrl.substring(index);
        }else {
            this.baseUrl = baseUrl;
        }
        this.totalCount = totalCount;
    }
    
    /**
     * 
     * @param baseUrl
     * @param countPage
     * @param separator
     */
    public PagerGenerator(String baseUrl, int totalCount, String separator) {
        this(baseUrl, totalCount);
        this.separator = separator;
    }
    
    public PagerGenerator(String baseUrl, int totalCount, boolean all) {
        this(baseUrl, totalCount);
        this.all = all;
    }
    
    /**
     * 
     * @param baseUrl
     * @param countPage
     * @param separator
     * @param all 
     */
    public PagerGenerator(String baseUrl, int totalCount, String separator, boolean all) {
        this(baseUrl, totalCount, separator);
        this.all = all;
    }
    
    /**
     * 产生下标pageNum的页码的html链接代码，页码为当前页时链接为#
     * @param pageNum
     * @return
     */
    private String getPageHref(int pageNum) {
        StringBuffer sb = new StringBuffer();
        if(pageNum > 0 && pageNum <= totalCount) {
            if(pageNum == curPage){
                sb.append("<span id=\"pageNow\"><a target=\"_self\" href=\"#\">" ).append(pageNum).append( "</a></span>");
            }else if(pageNum == 1){
                sb.append("<span><a target=\"_self\" href=\"" ).append(baseUrl).append(extension).append( "\">" ).append(pageNum).append( "</a></span>");
            } else {
                sb.append("<span><a target=\"_self\" href=\"" ).append(baseUrl).append(separator).append(pageNum).append(extension).append( "\">" ).append(pageNum).append( "</a></span>");
            }
        }
        
        return sb.toString();
    }
    
    /**
     * 产生第curPage页的html链接代码
     * @return
     */
    public String getPageHtml(int curPage) {
        
        this.curPage = curPage;
        
        StringBuffer sb = new StringBuffer();
        sb.append("<div id=\"pageNum\">");
        
        //首页
        if(firstPage && curPage > 1) {
            sb.append("<span><a target=\"_self\" href=\"" ).append(baseUrl).append(extension).append( "\">首页</a></span>");
        }
        //上一页
        if(curPage > 1) {
            sb.append("<span><a target=\"_self\" href=\"" ).append(baseUrl);
            if(curPage > 2) {
                sb.append(separator).append(curPage - 1);
            }
            sb.append(extension).append( "\">上一页</a></span>");
        }
        
        int start = curPage - showPageLength / 2;
        
        if(start < 0) {
            //输出前n页
            for(int i = 0; i < showPageLength; i++) {
                sb.append(getPageHref(i + 1));
            }
        } else if(start + showPageLength > totalCount) {
            //输出后n页
            for(int i = showPageLength; i > 0; i--) {
                sb.append(getPageHref(totalCount - i + 1));
            }
        }else {
            for (; start <= curPage + showPageLength /2; start ++) {
                if(start > 0 && start <= totalCount) {
                    sb.append(getPageHref(start));
                }
            }
        }
        
        //下一页
        if(curPage < totalCount) {
            sb.append("<span><a target=\"_self\" href=\"" ).append(baseUrl).append(separator).append(curPage + 1).append(extension).append( "\">下一页</a></span>");
        }
        //尾页
        if(lastPage && curPage < totalCount) {
            sb.append("<span><a target=\"_self\" href=\"" ).append(baseUrl).append(separator).append(totalCount).append(extension).append( "\">尾页</a></span>");
        }
        
        //本页查看全文
        if(all){
            sb.append("<span><a target=\"_self\" href=\"").append(baseUrl).append(separator).append("all").append(extension).append("\">&nbsp;&nbsp;在本页查看全文</a></span>");
        }
        
        sb.append("</div>");
        return sb.toString();
    }
    
    public static void main(String[] args) throws IOException {
        
        String prefix = "<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />";
        int countPage = 4;
        PagerGenerator pb = new PagerGenerator("aa", countPage);
        pb.setFirstPage(false);
        pb.setLastPage(false);
        for(int i = 1; i <= countPage; i++) {
            if(i == 1) {
                FileUtil.write("D:/data/tmp_test/aa.html", prefix + pb.getPageHtml(i), false); 
            } else {
                FileUtil.write("D:/data/tmp_test/aa_" + i + ".html", prefix + pb.getPageHtml(i), false);
            }
        }
        
    }
}