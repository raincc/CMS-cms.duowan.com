
package com.duowan.cms.parser.util.hotword;

public class StringPart {

	private boolean regMatch;
	private String content;

	public StringPart(boolean regMatch, String content) {
		super();
		this.regMatch = regMatch;
		this.content = content;
	}

	public boolean isRegMatch() {

		return regMatch;
	}

	public void setRegMatch(boolean regMatch) {

		this.regMatch = regMatch;
	}

	public String getContent() {

		return content;
	}

	public void setContent(String content) {

		this.content = content;
	}

}
