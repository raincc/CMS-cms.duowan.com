package com.duowan.cms.parser.util;

/**
 * 
 *  HTML页面内容信息（用于模板解析后返回的的html内容）
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-10-23
 * <br>==========================
 */
public class HtmlPage {
    
    /**
     * 页面的HTML文本内容
     */
    private String content;
    
    /**
     * 所对应的页码
     */
    private int pageNo;
    
    /**
     * 所对应的模板ID
     */
    private Long templateId;
    
    /**
     * 页面别名
     */
    private String alias;
    
    /**
     * 文本内容的编码格式（UTF-8 / GBK）,保存文件使用
     */
    private String code;
    
    public HtmlPage(String content, int pageNo, Long templateId, String alias, String code) {
        super();
        this.content = content;
        this.pageNo = pageNo;
        this.templateId = templateId;
        this.alias = alias;
        this.code = code;
    }



    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }



    public HtmlPage() {
        
    }
    
   
}
