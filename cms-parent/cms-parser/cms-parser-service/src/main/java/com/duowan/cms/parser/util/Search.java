package com.duowan.cms.parser.util;

import java.net.URLEncoder;

import org.apache.log4j.Logger;
/**
 * 搜索
 * @author Administrator
 *
 */
public class Search {

	public static Logger logger = Logger.getLogger(Search.class);


	/**
	 * 返回关键词搜索链接
	 * 
	 * @param keyWord
	 * @param channelid
	 * @return
	 */
	public static String getSearchURL(String keyWord, String channelid) {
		if (0 == keyWord.indexOf("&&"))
			keyWord = keyWord.substring(2);

		// 得到每个单独的词
		String[] strs = keyWord.split(" ");
		String url = null;
		StringBuffer strBuf = new StringBuffer();
		String site = channelid + ".duowan.com";
		
		try {
			String param = URLEncoder.encode(keyWord+" site:"+site, "UTF-8");//+"%20site%3A"+site;
			for (int i = 0; i < strs.length; i++) {
				// 为每个词构造搜索链接
				url = "http://soso.duowan.com/s?wd=" + param ;
				strBuf.append("<a href=\"");
				strBuf.append(url);
				strBuf.append("\" target=\"_blank\">");
				strBuf.append(strs[i]);
				strBuf.append("</a> ");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return strBuf.toString();
	}
	
	//如果url前没有http://则添加
	public static String urlHandller(String src){
		if (src == null || src.startsWith("http")){
			return src;
		}
		return "http://"+src;
	}


}
