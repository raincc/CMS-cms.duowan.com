package com.duowan.cms.parser.util.hotword;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class HotWordReplaceUtil {

    public static LexTree<String> genLexTree(Map<String, String> map) {

        LexTree<String> root = new LexTree<String>();
        Set<String> keys = map.keySet();
        for(String key : keys) {
            updateLexTree(root, key, map.get(key));
        }
        return root;
    }

    private static void updateLexTree(LexTree<String> root, String key, String value) {

        char[] cs = key.toCharArray();
        LexTree<String> lt = root;

        for(char c : cs) {
            LexTree<String> subLt = lt.getSubLexTree(c);
            if(null == subLt) {
                lt.putSubChar(c);
                subLt = lt.getSubLexTree(c);
            }
            lt = subLt;
        }
        lt.setAttachObj(value);
    }
    
    public static String quickReplace(String content, LexTree<String> root) {
        StringBuilder sb = new StringBuilder(); 
        char[] cs = content.toCharArray();
        boolean start = false;
        boolean judge = false;
        for(int i = 0; i < cs.length; i++) {
            LexTree<String> lt = root;
            for(int j = 0; (i + j) < cs.length; j++) {
                
                int k = i+j;
                //add by zqm 
                if (cs[k] == '{'){
                    start=true;
                    if (judge){
                        sb.append(cs,i,j+1);
                        i += j;
                        judge = false;
                    }else{
                        sb.append(cs[k]);   
                    }
                    break;
                }
                
                if (start == true){
                    if (cs[k] == '}'){
                        start = false;
                        sb.append(cs[k]);
                        break;
                    }else{
                        sb.append(cs[k]);
                        break;
                    }
                }
                //end
                lt = lt.getSubLexTree(cs[k]);
                if(null == lt) {
                    //sb.append(cs,i,j+1);
                    sb.append(cs,i,1);
                    break;
                }else {
                    judge = true;
                    String value = lt.getAttachObj();
                    if(null != value){
                        sb.append(value);
                        i += j;
                        judge = false;
                        //热词只替换一次
                        lt.setAttachObj(null);
                        break;
                    }else if ((i+j) == (cs.length-1)){
                        sb.append(cs,i,1);
                        break;
                    }
                }
            }
        }
        return sb.toString();
    }
    
    /**
     * @param content
     * @param root
     * @param excludeRegex
     * @return
     */
    public static String quickReplace(String content, LexTree<String> root,String [] excludeRegex){
        if (excludeRegex == null)return content;
        if (excludeRegex.length == 1)return quickReplace(content, root, excludeRegex[0]);
        StringBuilder sb = new StringBuilder(); 
        List<StringPart> all = new LinkedList<StringPart> ();
        List<StringPart> first = StrSplit.splitByMatchAndUnmatch(content, excludeRegex[0]);
        for (StringPart part : first){
            if (part.isRegMatch()){
                all.add(part);
                continue;
            }
            for (int j=1; j<excludeRegex.length; j++){
                List<StringPart> subParts = StrSplit.splitByMatchAndUnmatch(part.getContent(), excludeRegex[j]);
                all.addAll(subParts);
            }
        }
        
        for(StringPart part:all){
            if(part.isRegMatch()){
                sb.append(part.getContent());
            }else{
                sb.append(quickReplace(part.getContent(),root));
            }
        }
        return sb.toString();
    }

//  public static String quickReplace(String content, LexTree<String> root) {
//      StringBuilder sb = new StringBuilder(); 
//      char[] cs = content.toCharArray();
//      for(int i = 0; i < cs.length; i++) {
//          LexTree<String> lt = root;
//          for(int j = 0; (i + j) < cs.length; j++) {
//              lt = lt.getSubLexTree(cs[i + j]);
//              if(null == lt) {
//                  //sb.append(cs,i,j+1);
//                  sb.append(cs,i,1);
//                  break;
//              }else {
//                  String value = lt.getAttachObj();
//                  if(null != value){
//                      sb.append(value);
//                      i += j;
//                      break;
//                  }
//              }
//          }
//      }
//      return sb.toString();
//  }
    
    /**
     * @param content
     * @param root
     * @param excludeRegex
     * @return
     */
    public static String quickReplace(String content, LexTree<String> root,String excludeRegex){
        StringBuilder sb = new StringBuilder(); 
        List<StringPart> list = StrSplit.splitByMatchAndUnmatch(content, excludeRegex);
        for(StringPart part:list){
            if(part.isRegMatch()){
                sb.append(part.getContent());
            }else{
                sb.append(quickReplace(part.getContent(),root));
            }
        }
        return sb.toString();
    }
}
