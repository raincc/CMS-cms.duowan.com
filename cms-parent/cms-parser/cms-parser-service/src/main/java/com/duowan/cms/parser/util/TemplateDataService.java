package com.duowan.cms.parser.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.comment.CommentInfo;
import com.duowan.cms.dto.gametest.GameTestInfo;
import com.duowan.cms.intf.fahao.FahaoInfo;

/**
 * 解析模板所需要的数据（提供给模板使用，对应模板语法中的 data.xxx 方法）
 * 
 * @author coolcooldee
 * 
 */
public interface TemplateDataService {

    final Log logger = LogFactory.getLog(TemplateDataService.class);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getListOther(String cname, String tag, int powerfrom, int powerto, int start, int len);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getList(String tag, int powerfrom, int powerto, int start, int len);

    // 输入频道id ，tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getList(String tag, String cid, int powerfrom, int powerto, int start, int len);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getListOther(String cname, String tag, int powerfrom, int powerto, int len);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getList(String tag, int powerfrom, int powerto, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getListOther(String cname, String tag, int start, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getList(String tag, int start, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getListOther(String cname, String tag, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getList(String tag, int len);

    // 输入开始时间和结束时间返回游戏测试信息列表
    public List<GameTestInfo> getGameTestList(String starttime, String endtime, int ctype);

    public List<GameTestInfo> getGameTestListRest(String starttime, String endtime, int ctype, int len);

    // 输入开始时间和结束时间返回游戏测试信息列表
    public List<GameTestInfo> getGameTestList(String starttime, String endtime, int ctype, int len);

    public List<GameTestInfo> getGameTestListAsc(String starttime, String endtime, int ctype, int len);

    public List<GameTestInfo> getGameTestListAsc2(String starttime, String endtime, int ctype, int len);

    // 按权重降序得到所有记录
    public List<GameTestInfo> getGameTestByPower(int powerfrom, int powerto, int ctype);

    // 得到主页游戏的测试记录
    public List<GameTestInfo> getGameTestByIndex(String starttime, String endtime);

    // 得到公测阶段的游戏记录
    public List<GameTestInfo> getGameTestByPub(String starttime, String endtime);

    // 取得非公测信息记录
    public List<GameTestInfo> getNoPubTest(String starttime, String endtime);

    // 取得免费游戏信息记录
    public List<GameTestInfo> getFreeGameInfo(String starttime, String endtime);

    // 取得已付费信息记录
    public List<GameTestInfo> getHasPayGameInfo(String starttime, String endtime);

    // 取得国外游戏信息记录
    public List<GameTestInfo> getAbroadGameInfo(String starttime, String endtime);

    // 根据测试类型得到记录
    public List<GameTestInfo> getGameTestByState(String state);

    // 获取文章当前支持票数
    public int getAgreeCount(String voteid, String channelid);

    // 获取文章当前反对票数
    public int getDisagreeCount(String voteid, String channelid);

    // 获取文章支持票数百分比
    public String getAgreeVotePrecent(String voteid, String channelid);

    // 获取文章反对票数百分比
    public String getDisagreeVotePrecent(String voteid, String channelid);

    // 获取文章投票后总数
    public int getNewVoteCount(String voteid, String channelid);

    // 查询指定时间段的投票排名
    public List getVoteRecordByTime(String channelid, String starttime, String endtime, int size);

    // 获取心情投票数量
    public int getMoodVoteNum(String channelid, String voteid, int index);

    // 获取心情投票总数
    public int getMoodVoteCount(String channelid, String voteid);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getPicList(String tag, int powerfrom, int powerto, int start, int len);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getPicList(String tag, int powerfrom, int powerto, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getPicList(String tag, int start, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getPicList(String tag, int len);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getPicListOther(String cname, String tag, int powerfrom, int powerto, int start, int len);

    // 输入tag和权重起始和数量返回文章列表
    public List<Article4TagListInfo> getPicListOther(String cname, String tag, int powerfrom, int powerto, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getPicListOther(String cname, String tag, int start, int len);

    // 输入tag和数量返回文章列表
    public List<Article4TagListInfo> getPicListOther(String cname, String tag, int len);

    // 输入tag列表和数量返回文章列表，按日期和权重顺序排列
    public List<Article4TagListInfo> getRelate(String tags, int powerfrom, int powerto, int len);

    // 获得新游戏热度榜列表
    public List newgameHotList(String type, int start, int len);

    // 获得新游戏热度榜指定游戏的信息
    // public Record newgameHot(String name) ;

    // 获得工会等级
    public String getGuildLv(int lv);

    // 获得工会等级数组
    public String[] getGuildCooperateLv();

    // 获得工会等级数组
    public int getGuildCount(int lv);

    // 获得工会合作记录
    public List getGuildCooperatelist(int lv, int start, int len);

    // 输入tag列表和数量返回文章列表，按时间顺序排列
    public List<Article4TagListInfo> getRelate(String tags, int len);

    // 输入文章id获得文章信息
    public ArticleInfo getArticleInfo(String articleId);

    public ArticleInfo getArticleInfo(String articleId, String channelId);

    // 用于创建表名为tablename的临时表
    // public boolean createListTmp(String tablename);

    // public boolean dropListTmp(String tablename);

    // 得到标签url static方法
    // public String getTagUrl(String channelid, long tagid, long templateid);

    // 得到标签url static方法
    // public String getTagUrl(String channelid, long tagid);

    // 得到标签url
    public String getTagUrl(String channelid, String tag);

    // 得到本频道标签url
    public String getTagUrl(String tag);

    // 得到合作相对url static方法
    // public String getPostCooperateUrl(String cname, String aid);

    // 得到标签相对url static方法
     public String getTagRelatedUrl(String channelid, String tag, String cooperate);

    // 得到模板相对url static方法
    // public String getTemplateRelatedUrl(String channelid, String
    // templatename, String cooperate);

    // 得到模板相对url static方法
    // public String getTemplateRelatedUrl(String channelid, String
    // templatename, String cooperate, String alias);

    // 得到本频道文章绝对url
    public String getUrl(String aa);

    // 得到本频道文章相对url
    public String getRelatedUrl(String aa);

    // 得到该频道文章绝对url
    public String getUrl(String cname, String aa);

    // 得到Web频道文章绝对url
//    public String getWebUrl(String aa);

    // 得到该频道文章相对url
    public String getRelatedUrl(String cname, String aa);

    // 得到某个tag的所有相关tag
    public String[] aboutTags(String tag);

    // 根据realtags字符串，获得所有tag超链接
    public String getTagsUrl(String realtags, String splits);

    // 得到第一个tag的链接
    public String getFirstTagUrl(String realtags);

    // 得到某个tag的父tag
    public String getParentTag(String tag);

    // 得到该频道某文章主帖的信息
    // public Record getThreadInfo(long threadid) ;

    // 得到该频道某文章排序后的前10个回贴(按support和ptime排序)
    public List getReplys(long threadid);

    // 得到该频道某文章排序后的从start开始的len个回贴(按support和ptime排序)
    public List getReplys(long threadid, int start, int len);

    // 得到该频道某文章排序后并在goodreplys中没有出现过的前10个回贴(按ptime排序)
    public List getReplysByPtime(long threadid);

    // 得到该频道某文章排序后从start前len个回贴(按ptime排序)
    public List getAllReplys(long threadid, int start, int len);

    // 得到该频道某文章没有被投诉的排序后从start前len个回贴(按ptime排序)
    public List getNotAccuseReplys(long threadid, int start, int len);

    // 获得数组长度
    public int getLength(Object list);

    // 减法
    public int sub(int t, int s);

    // 拆分字符串
    // public Record splits(String content, String regex) ;

    // 拆分字符串,返回数组
    public String[] splits_rs(String content, String regex);

    // 获得多个频道指定tag中权重在指定数以上的文章("彩虹岛,重点,120{%}彩虹岛,重点,120")
    public List getChannelsTag(String s, int num);

    /**
     * 获取发号数据
     * 
     * @param jsonParam
     * @return
     */
    public Map<String, List<FahaoInfo>> getFahaoRank(String jsonParam);

    /**
     * 获取投票排行榜数据
     * 
     * @param jsonParam
     * @return
     */
    public List getVoteRankData(String channelId, String tag, int status, int type, int start, int size, String startDate, String endDate, String queryType);

    /**
     * 获取投票排行榜数据
     * 
     * @param jsonParam
     * @return
     */
    public List<Article4TagListInfo> getVoteRankDataByArticle (String channelId, String cmsTag, int start, int size, String startDate, String endDate, String queryType) throws Exception;

    /**
     * 根据发布器中的tag和投票相关的选项获取投票排行榜数据的接口，
     * 
     * @param jsonParam
     * @return
     */
    public List<Article4TagListInfo> getVoteRankDataByTag(String channelId, String cmsTag, String voteTag, int status, int type, int start, int size, String startDate,
            String endDate, String queryType, boolean needDigest) throws Exception;

    /*
     * 获取文章评论数
     */
    public CommentInfo getVNumByAId(String channelId, String articleId);
    
    public List<Article4TagListInfo> getCommentRankDataByTag(String channelId, String cmsTags, int start, int size, String startDate, String endDate);

    public int getVNum(String articleId, String channelId, String title, String url);

    // 获取视频信息
    // public VideoBean getVideoInfo(String channelid, Long articleid);

    // 输入开始时间(相对现在的天数，支持负数)和结束时间(相对现在的天数,支持负数)，返回游戏测试信息列表 add by hc 2011-05-16
    public List getGameTestList(int startDay, int endDay, int ctype, boolean revers);

    // 获取游戏库信息
    // public DynaBean getGameCardInfo(String gameName);

    // 获取发号信息
    // public Fahao getFahaoInfo(String channelName);

    // 获取图库图集信息
    public List getTujiList(String jsonStr);

    // 获取游戏信息
    // public GameInfo getGameInfo(String name) ;

    public void setChannelInfo(ChannelInfo channelInfo);

}
