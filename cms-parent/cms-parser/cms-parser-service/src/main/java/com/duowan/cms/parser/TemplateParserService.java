package com.duowan.cms.parser;


import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.duowan.cms.parser.util.HtmlPage;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.tag.TagInfo;
import com.duowan.cms.dto.template.TemplateInfo;

/**
 * 
 * 模板解析服务
 * 
 * @author coolcooldee
 * @version 1.0
 * @created 08-10-2012 16:06:16
 */
public interface TemplateParserService {
    
    public final Log logger = LogFactory.getLog(TemplateParserService.class);
    
   //公会新闻频道
    public static String GH_CHANNEL_ID = "gh";
    public static String GH_TAG_NAME = "公会动态";
    public static String INFO_PROMOTION_TEMPLATE_NAME = "资讯推广公用模板";
    
    //异常码：文件生成失败
    public final static String TEMPLATE_CAN_NOT_BE_PARSER = TemplateParserService.class.getName().concat("001");
    public final static String FILE_CAN_NOT_BE_GENERATED = TemplateParserService.class.getName().concat("002");
    
   /**
   * 解析模板，返回解析后的HTML字符串组（分页的情况有多个结果返回，所以返回的是list）,目前只解析"资讯推广", "栏目", "专题", "公共" 类型的模板
    * @param channelid 频道id
    * @param templateid 模板id    
    * @return 
    * @throws BaseCheckedException
    */
    public  List<HtmlPage> parseTemplate(ChannelInfo channelInfo, TemplateInfo templateInfo) throws BaseCheckedException;
    
    /**
     * 根据某个文章的ID，结合文章对应的最终文章页模板，解析文章返回解析后的HTML字符串组
     * @param articleId
     * @return
     * @throws BaseCheckedException
     */
    public  List<HtmlPage> parseArticle(ChannelInfo channelInfo, ArticleInfo articleInfo) throws BaseCheckedException;

    
   /**
    *  利用tag模板，解析出tag静态页
    * @param channelId
    * @param tag
    * @return
    * @throws BaseCheckedException
    */
    public  List<HtmlPage> parseTag(String channelId, String tag) throws BaseCheckedException;
    
    /**
     * 使用关键字模板，解析出关键字静态页
     * @param param
     * @return
     * @throws BaseCheckedException
     */
    //public List<HtmlPage> parseKeyword(Map<String, Object> param) throws BaseCheckedException;
    
    /**
     * 解析投票模板
     * @param param
     * @return
     * @throws BaseCheckedException
     */
    public List<HtmlPage> parseVote(Map<String, Object> param) throws BaseCheckedException;
    
    /**
     * 解析投票结果模板
     * @param param
     * @return
     * @throws BaseCheckedException
     */
    public List<HtmlPage> parseVoteResult(Map<String, Object> param) throws BaseCheckedException;
    
    /**
     * 解析模板，并且生成文件，并且同步出去（使用Rsync！！！！）
     * @param htmlPage
     */
    public void parseTemplateAndGenerateFile(ChannelInfo channelInfo, TemplateInfo templateInfo)  throws BaseCheckedException ;
    
    /**
     * 结合模板，解析文章，并且生成文件，并且同步出去（使用Rsync！！！！）
     * @param htmlPage
     */
    public void parseArticleAndGenerateFile(ChannelInfo channelInfo, ArticleInfo articleInfo)  throws BaseCheckedException ;
    
    /**
     * 解析Tag模板，并且生成文件，并且同步出去（使用Rsync！！！！）
     * @param channelInfo
     * @param tagInfo
     * @throws BaseCheckedException
     */
    public void parseTagAndGenerateFile(ChannelInfo channelInfo, TagInfo tagInfo) throws BaseCheckedException ;
    
    /**
     * 解析空链接，并且生成文件，并且同步出去（使用Rsync！！！！）
     * @param list
     * @throws BaseCheckedException
     */
    public void parseEmptyLinkAndGenerateFile(Article4TagListInfo a) throws BaseCheckedException;
    
}
