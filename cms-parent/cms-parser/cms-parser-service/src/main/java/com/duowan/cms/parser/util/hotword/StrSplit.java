package com.duowan.cms.parser.util.hotword;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrSplit {
	/**
	 * @param content
	 * @param regex
	 * @return
	 */
	public static List<StringPart> splitByMatchAndUnmatch(String content,String regex)
	{
		List<StringPart> list = new LinkedList<StringPart>();
		
		Pattern p = Pattern.compile(regex,Pattern.CASE_INSENSITIVE|Pattern.MULTILINE);
		Matcher matcher = p.matcher(content);
		int nextBegin = 0;
		while(matcher.find()){
			if(matcher.start() > nextBegin){
				list.add(new StringPart(false,content.substring(nextBegin,matcher.start())));
			}
			list.add(new StringPart(true,content.substring(matcher.start(), matcher.end())));
			nextBegin = matcher.end();
		}
		if(nextBegin < content.length()){
			list.add(new StringPart(false,content.substring(nextBegin)));
		}
		return list;
	}
}
