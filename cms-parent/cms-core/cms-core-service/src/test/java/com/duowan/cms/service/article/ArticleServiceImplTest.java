package com.duowan.cms.service.article;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArticleServiceImplTest {
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void test() {
        String content = "<img src=\"http://a4.att.hudong.com/15/73/01300000251452123237735564074.jpg\">"
                +
                "dsafasf<img src=\"http://www.baidu.com/aa.jpg\">我是中国人dsafasf<img src=\"http://cms.duowan.com/cms_public/css/一.jpg\">";
        
        String channelId = "lol";
        long articleId = 212413056563L;

        
//        content = new ArticleServiceImpl().dealWithImgInContent(content,
//                channelId, articleId);
        System.out.println(content);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void test02() {

        new Thread(new Runnable() {
            
            @Override
            public void run() {
                
                int i = 10;
                while (i > 0) {

                    i--;
                }
            }
        }).start();
    }
    
    // 对正则中的特殊字符进行处理
    public String escapeRegex(String content) {
        Pattern pattern = Pattern.compile("([\\p{Punct}&&[^\\$\\\\]])");
        Matcher matcher = pattern.matcher(escapeDollarBackslash(content));
        StringBuffer buffer = new StringBuffer();
        while (matcher.find()) {
            String group = matcher.group();
            matcher.appendReplacement(buffer, "\\\\" + group);
            
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }
    
    private String escapeDollarBackslash(String content) {
        StringBuffer buffer = new StringBuffer(content.length());
        for (int i = 0; i < content.length(); i++) {
            char c = content.charAt(i);
            if (c == '\\' || c == '$') {
                buffer.append("\\").append(c);
            } else {
                buffer.append(c);
            }
        }
        return buffer.toString();
    }
    
}

class MyTmp {
    
    public static void sayHello(String name) {
        System.out.println("hello:" + name);
    }
}