package com.duowan.cms.service.cdn;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.NetUtil;
import com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder;

/**
 *   ChinaCache的CDN刷新类
 *
 */
public class ChinaCacheCdnHandler implements CdnHandler {

	public static String chinacacheUser = PropertiesHolder.get("cdn_chinacache_user");
	public static String chinacachePwd = PropertiesHolder.get("cdn_chinacache_pwd");
	public static String chinacacheUrl = PropertiesHolder.get("cdn_chinacache_url");
	private static Map<String, String> uParamsMap = new HashMap<String, String>();
	static{
		uParamsMap.put("user", chinacacheUser);
		uParamsMap.put("pswd", chinacachePwd);
		uParamsMap.put("ok", "ok");
	}
	
	@Override
	public boolean flushCdn(String url) throws BaseCheckedException {
		uParamsMap.put("urls", url);
		String flushResult = NetUtil.getHttpRequest(uParamsMap, chinacacheUrl, 3000);
		return flushResult.indexOf("<url>1</url>") > -1;
	}

	@Override
	public boolean flushCdnDir(String dir) throws BaseCheckedException {
		uParamsMap.put("dirs", dir);
		String flushResult = NetUtil.getHttpRequest(uParamsMap, chinacacheUrl, 3000);
		return flushResult.indexOf("<url>1</url>") > -1;
	}

}
