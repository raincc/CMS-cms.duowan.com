package com.duowan.cms.service.hotword;

import java.util.List;

import org.apache.log4j.Logger;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.domain.hotword.HotWord;
import com.duowan.cms.dto.hotword.HotWordInfo;
import com.duowan.cms.dto.hotword.HotWordSearchCriteria;

/**
 * 热词服务
 * @author yzq
 * @version 1.0
 * @created 04-九月-2012 17:43:12
 */
public interface HotWordService extends Service {

    public static Logger logger = Logger.getLogger(HotWordService.class);

    public List<HotWordInfo> getByChannelId(String channelId);

    public HotWord getByChannelIdAndName(String channelId, String name);
    
    /**
     * 分页查询热词，用于列表展示
     * 
     * @param pageSize    分页每页数量
     * @param pageNo    分页页码
     * @param searchCriteria    查询条件
     */
    public Page<HotWordInfo> pageSearch(HotWordSearchCriteria searchCriteria);
    
    /**
     * 添加一个热词
     * @param hotWordInfo
     */
    public void addHotWord(HotWordInfo hotWordInfo);
    
    /**
     * 更新
     * @param hotWordInfo
     */
    public  void updateHotWord(HotWordInfo hotWordInfo);
    
    /**
     * 删除
     * @param hotWordInfo
     */
    public void deleteHotWord(HotWordInfo hotWordInfo);
    
}