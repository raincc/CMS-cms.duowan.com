package com.duowan.cms.service.comment;

import org.apache.log4j.Logger;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.comment.CommentInfo;

/**
 * 评论服务
 * @author coolcooldee
 */
public interface CommentService extends Service {
    
    public static Logger logger = Logger.getLogger(CommentService.class);
    
    
    /**
     * 新增评论
     * @param commentInfo
     */
    public void add(CommentInfo commentInfo);
    
    /**
     * 根据输入的评论url，解析threadid
     * @param url
     * @return
     */
    public long getThreadIdFromCommentUrl(String url) ;
    
}
