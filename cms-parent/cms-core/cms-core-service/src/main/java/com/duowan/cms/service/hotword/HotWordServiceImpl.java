package com.duowan.cms.service.hotword;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.core.rmi.client.hotword.HotWordRemoteService;
import com.duowan.cms.domain.hotword.HotWord;
import com.duowan.cms.domain.hotword.rpst.HotWordRepository;
import com.duowan.cms.dto.hotword.HotWordInfo;
import com.duowan.cms.dto.hotword.HotWordSearchCriteria;

@Service("hotWordService")
public class HotWordServiceImpl implements HotWordService, HotWordRemoteService{

    @Autowired
    private HotWordRepository hotWordRepository;

    @Override
    public List<HotWordInfo> getByChannelId(String channelId) {
        return hotWordRepository.getByChannelId(channelId);
    }

    @Override
    public HotWord getByChannelIdAndName(String channelId, String name) {
        return hotWordRepository.getByChannelIdAndName(channelId, name);
    }

    @Override
    public Page<HotWordInfo> pageSearch(HotWordSearchCriteria searchCriteria) {
        return hotWordRepository.pageSearch(searchCriteria);
    }

    @Override
    public void addHotWord(HotWordInfo hotWordInfo) {
        HotWord hotWord = new HotWord(hotWordInfo);
        hotWordRepository.save(hotWord);
    }

    @Override
    public void updateHotWord(HotWordInfo hotWordInfo) {
        HotWord hw = hotWordRepository.getByChannelIdAndName(hotWordInfo.getChannelId(), hotWordInfo.getName());
        hw.update(hotWordInfo);
        hotWordRepository.update(hw);
    }

    @Override
    public void deleteHotWord(HotWordInfo hotWordInfo) {
        HotWord hw = hotWordRepository.getByChannelIdAndName(hotWordInfo.getChannelId(), hotWordInfo.getName());
        if (null != hw)
            hotWordRepository.delete(hw);
    }
}
