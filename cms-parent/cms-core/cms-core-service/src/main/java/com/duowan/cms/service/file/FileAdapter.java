package com.duowan.cms.service.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.FileUtil;
import com.duowan.cms.common.util.StringUtil;

/**
 * 文件适配器，用来“合并”页面文件目录和图片文件目录 （物理路径上是完全分开的两个目录）
 * 
 * @author coolcooldee
 * @version 1.0
 * @created 04-九月-2012 17:43:12
 */
public class FileAdapter {

    private static Logger logger = Logger.getLogger(FileAdapter.class);
    /**
     * 页面文件
     */
    private File htmlFile;

    /**
     * 图片文件
     */
    private File picFile;

    public FileAdapter(String htmlPath, String picPath) {
        // 两个路径不能同时为空
        if (StringUtil.isEmpty(htmlPath) && StringUtil.isEmpty(picPath)) {
            throw new NullPointerException();
        }
        if (!StringUtil.isEmpty(htmlPath)) {
            this.htmlFile = new File(htmlPath);
            if (!this.htmlFile.exists()) {
                this.htmlFile = null;
            }
        }
        if (!StringUtil.isEmpty(picPath)) {
            this.picFile = new File(picPath);
            if (!this.picFile.exists()) {
                this.picFile = null;
            }
        }
        if (this.htmlFile == null && this.picFile == null) { // 由于文件名字异常可能出现两个都为空的情况
            logger.error("路径" + htmlPath + "或者路径" + picPath + "下，由于文件名字异常不能读取");
            // return;
        }
    }

    /**
     * 获取最后的修改时间（合并之后）
     * @return
     */
    public String getLastModified() {
        long lastModified = 0;
        if (htmlFile != null)
            lastModified = htmlFile.lastModified();
        if (picFile != null && lastModified < picFile.lastModified())
            lastModified = picFile.lastModified();
        return DateUtil.getStringOfDate(new Date(lastModified), DateUtil.exportXlsDateCreateTimeFormat, null);
    }

    /**
     * 文件列表的字符串形式
     * @return
     */
    public String listFileStr() {
        FileAdapter[] fileInfoList = this.listFiles();
        StringBuilder strBuilder = new StringBuilder("");
        for (int i = 0; i < fileInfoList.length; i++) {
            strBuilder.append(fileInfoList[i].getName() + ",");
        }
        return strBuilder.toString();
    }

    /**
     * 获取文件列表（合并之后）
     * @return
     */
    public FileAdapter[] listFiles() {

        List<FileAdapter> flieAdapters = new ArrayList<FileAdapter>();

        if (htmlFile != null && picFile != null) { //如果两边目录都存在的情况，合并目录下的文件
            File[] htmlListFiles = htmlFile.listFiles();
            File[] picListFiles = picFile.listFiles();
            int length1 = htmlListFiles.length;
            int length2 = picListFiles.length;
            StringBuffer duplicateFileNames = new StringBuffer();  //计算两个目录下是否有同名的文件夹
            for (int i = 0; i < length1; i++) {
                String name1 = htmlListFiles[i].getName();
                int index = isFileInList(name1, picListFiles);
                String htmlPath = htmlListFiles[i].getPath();
                if (index > -1) {
                    duplicateFileNames.append(",").append(name1);
                    flieAdapters.add(new FileAdapter(htmlPath, picListFiles[index].getPath()));
                } else {
                    flieAdapters.add(new FileAdapter(htmlPath, ""));
                }
            }
            for (int i = 0; i < length2; i++) {
                String name2 = picListFiles[i].getName();
                if (duplicateFileNames.toString().indexOf("," + name2) > -1)
                    continue;
                flieAdapters.add(new FileAdapter("", picListFiles[i].getPath()));
            }
        } else if (picFile != null) {       //只有图片文件目录的情况
            File[] picListFiles = picFile.listFiles();
            for (File file : picListFiles) {
                if (new File(file.getPath()) != null)
                    flieAdapters.add(new FileAdapter("", file.getPath()));
            }
        } else if (htmlFile != null) {      //只有html文件目录的情况
            File[] htmlListFiles = htmlFile.listFiles();
            for (File file : htmlListFiles) {
                if (new File(file.getPath()) != null)
                    flieAdapters.add(new FileAdapter(file.getPath(), ""));
            }
        }

        FileAdapter[] fileInfoList = new FileAdapter[flieAdapters.size()];
        return flieAdapters.toArray(fileInfoList);

    }

    public boolean getIsDirectory() {

        if (getNotEmptyFile() == null) {
            return false;
        }
        return getNotEmptyFile().isDirectory();
    }

    public boolean isFile() {
        if (getNotEmptyFile() == null) {// 兼容部分文件在老发布器的文件目录，没有迁移到新发布器的情况
            return false;
        }
        return getNotEmptyFile().isFile();
    }

    public boolean isDirectory() {
        if (getNotEmptyFile() == null) {
            return false;
        }
        return getNotEmptyFile().isDirectory();
    }

    public String getAbsolutePath() {
        return getNotEmptyFile().getAbsolutePath().replace("\\", FileUtil.FILE_SEPARATOR);
    }

    /**
     * 文件名字
     * @return
     */
    public String getName() {
        if (getNotEmptyFile() == null) {    // 防止出现文件名字乱码读取不到的情况
            return "";
        }
        return getNotEmptyFile().getName();
    }

    /**
     * 文件大小
     * @return
     */
    public String getSize() {
        long size = 0;
        if (htmlFile != null)
            size = htmlFile.length();
        if (picFile != null)
            size += picFile.length();
        return addSeparator(String.valueOf(size));
    }

    /**
     * 是否是图片文件
     * @return
     */
    public boolean isPic() {
        File file = getNotEmptyFile();
        if(file==null)   return false;
        if(file.isFile()){
            return FileUtil.isPicFilename( file.getName());
        }
        return false;
    }

    private File getNotEmptyFile() {
        if (htmlFile != null)
            return htmlFile;
        else
            return picFile;
    }

    /**
     * 判断文件名字是否在文件的列表中，返回它在列表的索引位置
     * 不存在则返回 -1
     * @param name
     * @param list
     * @return
     */
    private int isFileInList(String name, File[] list) {
        if (list == null || StringUtil.isEmpty(name))
            return -1;
        for (int i = 0; i < list.length; i++) {
            if (name.equals(list[i].getName())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 给文件大小每三位放置一个分隔符.效果：addSeparator("8685") -->8,685
     * @param text
     * @return
     */
    private String addSeparator(String text) {
        String tempText = text;
        for (int i = text.length(); i > 1; i--) {
            int bitNum = text.length() - i + 1;
            if (bitNum % 3 == 0) {
                tempText = tempText.substring(0, i - 1) + "," + tempText.substring(i - 1, tempText.length());
            }
        }
        return tempText;
    }

}
