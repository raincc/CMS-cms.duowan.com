package com.duowan.cms.service.zone;

import java.util.List;

import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.zone.ZoneUserInfo;

public interface ZoneService extends Service {

	/**
	 * 获取某频道下的所有"有爱"用户
	 * @param channelId
	 * @return
	 */
	public List<ZoneUserInfo>  getZoneUserInfo(String channelId);
	/**
	 * 通过id获取用户信息
	 * @param id
	 * @return
	 */
	public ZoneUserInfo getById(String id);
	
}
