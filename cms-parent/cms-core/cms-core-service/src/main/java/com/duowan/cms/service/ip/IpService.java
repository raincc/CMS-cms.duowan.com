package com.duowan.cms.service.ip;

import org.apache.log4j.Logger;

import com.duowan.cms.common.service.Service;

/**
 * 评论服务
 * @author yzq
 */
public interface IpService extends Service {
    
    public static Logger logger = Logger.getLogger(IpService.class);
    
    public boolean isInnerIP(String ip);
}
