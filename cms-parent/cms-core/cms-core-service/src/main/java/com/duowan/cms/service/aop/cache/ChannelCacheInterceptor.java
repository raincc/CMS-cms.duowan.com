/**
 * 
 */
package com.duowan.cms.service.aop.cache;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.cache.support.AbstractServiceCacheInterceptor;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * 频道服务做缓存的拦截器
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-13
 * <br>==========================
 */
@Aspect
@Order(value = 0)
@Service("channelCacheInterceptor")
public class ChannelCacheInterceptor extends AbstractServiceCacheInterceptor {
    
    private static Logger logger = Logger.getLogger(ChannelCacheInterceptor.class);
    
    private static final int CACHE_TIME = 24 * 60 * 60;

    @Pointcut("execution(public * com.duowan.cms.service.channel.ChannelService.getById(..))")
    public void getByIdJsonPointcut() {}
    
    /**
     * 对 "根据频道ID获取频道的服务" 做缓存, 缓存不自动过期
     */
    @Around("getByIdJsonPointcut()")
    public Object cacheGetByIdJsonService(ProceedingJoinPoint joinPoint) throws Throwable {
       String channelId = (String) joinPoint.getArgs()[0];
        return cache(joinPoint, "CACHE_GET_BY_ID_"+channelId+"_KEY", CACHE_TIME);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.channel.ChannelService.getAllChannel(..))")
    public void getAllChannelPointcut() {}
    
    /**
     * 对 "获取所有频道信息的服务" 做缓存, 缓存不自动过期
     */
    @Around("getAllChannelPointcut()")
    public Object cacheGetAllChannelService(ProceedingJoinPoint joinPoint) throws Throwable {
        return cache(joinPoint, "CACHE_GET_ALL_CHANNEL_KEY", CACHE_TIME);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.channel.ChannelService.getByName(..))")
    public void getByNamePointcut() {}
    
    /**
     * 对 "根据频道名字获取频道信息的服务" 做缓存, 缓存不自动过期
     */
    @Around("getByNamePointcut()")
    public Object cacheGetByNameService(ProceedingJoinPoint joinPoint) throws Throwable {
        String channelName = (String) joinPoint.getArgs()[0];
        return cache(joinPoint, "CACHE_GET_BY_CHANNEL_NAME_"+channelName+"_KEY", CACHE_TIME);
    }
    
    
    @Pointcut("execution(public * com.duowan.cms.service.channel.ChannelService.add(..))")
    public void addPointcut() {}
    
    
    //========================以下是清除缓存的拦截==========================
    
    
    /**
     * 对 "新增频道信息的服务" 做拦截，清除旧的缓存, 
     */
    @Around("addPointcut()")
    public void cacheAddService(ProceedingJoinPoint joinPoint) throws Throwable {
        remove("CACHE_GET_ALL_CHANNEL_KEY");
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.channel.ChannelService.update(..))")
    public void updatePointcut() {}
    
    /**
     * 对 "新增频道信息的服务" 做拦截，清除旧的缓存, 
     */
    @Around("updatePointcut()")
    public void cacheUpdateService(ProceedingJoinPoint joinPoint) throws Throwable {
        ChannelInfo channelInfo = (ChannelInfo) joinPoint.getArgs()[0];
        if(channelInfo==null) return;
        remove("CACHE_GET_BY_ID_"+channelInfo.getId()+"KEY");
        remove("CACHE_GET_BY_CHANNEL_NAME_"+channelInfo.getName()+"_KEY");
        remove("CACHE_GET_ALL_CHANNEL_KEY");
    }
    
    
    @Pointcut("execution(public * com.duowan.cms.service.channel.ChannelService.delete(..))")
    public void deletePointcut() {}
    
    /**
     * 对 "新增频道信息的服务" 做拦截，清除旧的缓存, 
     */
    @Around("deletePointcut()")
    public void cacheDeleteService(ProceedingJoinPoint joinPoint) throws Throwable {
        ChannelInfo channelInfo = (ChannelInfo) joinPoint.getArgs()[0];
        if(channelInfo==null) return;
        remove("CACHE_GET_BY_ID_"+channelInfo.getId()+"KEY");
        remove("CACHE_GET_BY_CHANNEL_NAME_"+channelInfo.getName()+"_KEY");
        remove("CACHE_GET_ALL_CHANNEL_KEY");
    }
    
    
}
