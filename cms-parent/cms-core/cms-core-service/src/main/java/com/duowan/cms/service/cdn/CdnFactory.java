package com.duowan.cms.service.cdn;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.duowan.cms.common.util.CommandLineTools;

public class CdnFactory {
	private static Logger logger = Logger.getLogger(CdnFactory.class);
	public static CdnHandler getCdnHandler(String url) throws IOException, InterruptedException{
		url = url.replaceAll("http://", "");
		String domain = url.substring(0 , url.indexOf("/"));
		String result = CommandLineTools.execCommand("nslookup " +domain);
		logger.info("nslookup " +domain +"得到的结果：" + result );
		if (result.indexOf("hacdn.net") > -1 || result.indexOf("hadns.net") > -1   ) {
			logger.info("URL【"+url+"】使用快网的CDN");
			return new FastwebCdnHandler();
		}
		logger.info("URL【"+url+"】使用ChinaCache的CDN");
		return new ChinaCacheCdnHandler();
	}
	
}
