package com.duowan.cms.service.cdn;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.MessageDigestUtil;
import com.duowan.cms.common.util.NetUtil;
import com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder;

/**
 *  快网 CDN刷新类
 *
 */
public class FastwebCdnHandler implements CdnHandler {

	public static String fastWebCacheUrl = PropertiesHolder.get("cdn_fastweb_url");
	public static String fastWebCacheUser = PropertiesHolder.get("cdn_fastweb_user");
	public static String fastWebCachePwd = PropertiesHolder.get("cdn_fastweb_pwd");
	public static String fastWebKey = PropertiesHolder.get("cdn_fastweb_key");
	private static Map<String, String> uParamsMap = new HashMap<String, String>();
	static{
		uParamsMap.put("username", fastWebCacheUser);
	}
	
	@Override
	public boolean flushCdn(String url)throws BaseCheckedException {
		uParamsMap.put("url_list", url);
		uParamsMap.put("md5", MessageDigestUtil.getMD5Str(new SimpleDateFormat("yyyyMMdd").format(new Date()) + fastWebCacheUser + fastWebKey + fastWebCachePwd ));
		String flushResult = NetUtil.getHttpRequest(uParamsMap, fastWebCacheUrl, 3000);
		return flushResult.indexOf("<result>SUCCESS</result>") > -1;
	}

	@Override
	public boolean flushCdnDir(String dir)throws BaseCheckedException {
		uParamsMap.put("dir_list", dir);
		uParamsMap.put("md5", MessageDigestUtil.getMD5Str(new SimpleDateFormat("yyyyMMdd").format(new Date()) + fastWebCacheUser + fastWebKey + fastWebCachePwd ));
		String flushResult = NetUtil.getHttpRequest(uParamsMap, fastWebCacheUrl, 3000);
		return flushResult.indexOf("<result>SUCCESS</result>") > -1;
	}

}
