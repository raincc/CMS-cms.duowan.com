package com.duowan.cms.service.file;

import java.util.List;

import org.apache.commons.fileupload.FileItem;

/**
 * 
 * 文件管理器服务
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：haungjunyang@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-2
 * <br>==========================
 */
public interface FileService {

	/**
	 * 删除path路径的所有文件，如果path存在子目录，则迭代删除子目录的文件
	 * @param path
	 */
	public boolean deleteFile(String path);
	/**
	 * 把文件同步到外网
	 * @param destDir
	 */
	public void rsycFile(String destDir , String currentPath , String channelId);
	
	/**
	 * 分发destDir目录下的文件:把图片与非图片分开
	 * @param destDir
	 */
	public void dispatchFile(String destDir , String currentPath , String channelId);
	
	/**
	 * 写文件:使用随机文件名
	 * @param item
	 * @param basePath
	 * @throws Exception
	 */
	public void writeFile(FileItem item , String basePath  ,  String randomFileName) throws Exception ;
	/**
	 * 写文件:以上传的文件名为文件名
	 * @param item
	 * @param basePath
	 * @throws Exception
	 */
	public void writeFile(FileItem item , String basePath) throws Exception ;
	/**
	 * 创建目录
	 * @param path
	 */
	public void createFolder(String path);
	/**
	 * 在文件file内部，根据文件名查找文件
	 * @param searchPath
	 * @param name
	 * @return
	 */
	public List<FileAdapter> searchFileByName(FileAdapter searchFile, String name);
	/**
	 * 根据URL删除服务器上的文件
	 * @param url
	 */
	public void deleteFileByUrl(String channelId , String url);
	/**
	 * 对文件数组进行排序:
	 * 		排序规则：
	 * 		1.文件夹与文件分开来排序。先排文件夹，再排文件。
	 * 		2.具体的排序是对中英文进行排序,详见:ChineseCharactersUtil.String2Alpha
	 */
	public FileAdapter[] sortFile(FileAdapter []fileAdapterArr);
}
