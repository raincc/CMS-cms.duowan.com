package com.duowan.cms.service.cdn;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.Service;

public interface CdnService extends Service {

	/**
	 *  刷新某个URL的CDN
	 * @param url  url格式形如：http://www.chinacache.com/a.html
	 * @return
	 */
	
	public boolean flushCdn(String url ) throws BaseCheckedException;
	/**
	 * 刷新某个路径的CDN
	 * @param dir  dir的格式形如：http://www.chinacache.com/b/
	 * @return
	 */
	public boolean flushCdnDir(String dir) throws BaseCheckedException;
}
