/**
 * 
 */
package com.duowan.cms.service.template;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.template.AutoFlushTagInfo;
import com.duowan.cms.dto.template.AutoFlushTagSearchCriteria;

/**
 * 自动刷新标签服务
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
public interface AutoFlushTagService extends Service {
    
    /**
     * 判断是否存在未被刷新的tag
     * @param channelId
     * @param tag
     * @return
     */
    public boolean isExistUnFlushTag(String channelId, String tag);

    /**
     * 获取所有的未刷新的tag列表
     * @return
     */
    public List<AutoFlushTagInfo> listUnFlushTag();
    
    /**
     * 获取指定专区下未刷新的tag列表（即刷新时间flushTime为空）
     * @return
     */
    public List<AutoFlushTagInfo> listUnFlushTagBychannleId(String channelId);
    
    /**
     * 保存/更新自动刷新tag
     * @param autoFlushTagInfo
     */
    public void save(AutoFlushTagInfo autoFlushTagInfo);
    
    /**
     * 批量完成自动刷新任务（即更新其刷新时间）
     * @param autoFlushTagInfos
     */
    public void batcFinishedFlushTask(List<AutoFlushTagInfo> autoFlushTagInfos);
    
    /**
     * 按条件查找列表结果
     * @param criteria
     * @return
     */
    public List<AutoFlushTagInfo> list(AutoFlushTagSearchCriteria criteria);
    
}
