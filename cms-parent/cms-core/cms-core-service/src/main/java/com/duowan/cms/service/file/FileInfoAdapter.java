package com.duowan.cms.service.file;

import java.io.File;
import java.util.Date;

import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.FileUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;

//File类无getLastModified和getIsDirectory等方法，FileInfoAdapter起适配器作用
public class FileInfoAdapter  extends File {
	private  ChannelInfo channelInfo;
	private String dirType;
		public FileInfoAdapter(String pathname) {
			super(pathname);
			File file = new File(pathname);
			if (!file.exists()) {
				file.mkdirs();
			}
		}
		public FileInfoAdapter(String pathname ,ChannelInfo channelInfo , String dirType) {
			super(pathname);
			this.channelInfo = channelInfo;
			this.dirType = dirType;
			File file = new File(pathname);
			if (!file.exists()) {
				file.mkdirs();
			}
		}
		public String getLastModified(){
			return DateUtil.getStringOfDate(new Date(super.lastModified()), DateUtil.exportXlsDateCreateTimeFormat, null);
		} 
		public  FileInfoAdapter[] listFiles(){
			 File[] listFiles = super.listFiles();
			 if(listFiles==null){
				 listFiles = new File[0];
			 }
			     
			 FileInfoAdapter[] fileInfoList = new FileInfoAdapter[listFiles.length];
			 if ( channelInfo != null   && !StringUtil.isEmpty(dirType) ) {
				 for (int i = 0; i < listFiles.length; i++) {
					 fileInfoList[i] = new FileInfoAdapter(listFiles[i].getAbsolutePath() , channelInfo , dirType);
				}
			 }else{
				 for (int i = 0; i < listFiles.length; i++) {
					 fileInfoList[i] = new FileInfoAdapter(listFiles[i].getAbsolutePath());
				}
			 }

			 return fileInfoList;
		}
		public boolean getIsDirectory(){
			return super.isDirectory();
		} 
		public String getSize(){
			return addSeparator(String.valueOf(super.length()) );
		}
		public String getAbsolutePath(){
			return super.getAbsolutePath().replace("\\", FileUtil.FILE_SEPARATOR);
		}
		/**
		 * 若是网页类型的文件，文件的getAbsolutePath得到的路径形如D:/data2/www/cms.duowan.com/1000y/0708/m_52492685775.html
		 * 则返回的url形如:http://1000y.localcms.duowan.com/0708/m_52492685775.html
		 * 若是图片类型的文件，文件的getAbsolutePath得到的路径形如D:/data2/www/pic.duowan.com/1000y/1212/219056676530/搞笑1.png
		 * 则返回的url形如:http://localpic.duowan.com/1000y/1212/219056676530/搞笑1.png
		 * 
		 */
		public String getUrl(){
			//路径形如：D:/data2/www/cms.duowan.com/1000y/0708/m_52492685775.html或D:/data2/www/pic.duowan.com/1000y/1212/219056676530/搞笑1.png
			String absolutePath = this.getAbsolutePath();
			if ( channelInfo != null   && !StringUtil.isEmpty(dirType) && new File(absolutePath).isFile() ) {
			    if("htmldir".equals(dirType)){
			    	String basePath = channelInfo.getArticleFilePath().replace("\\", FileUtil.FILE_SEPARATOR);//路径形如：d:/data2/www/cms.duowan.com/1000y
			    	String relativeDomain = absolutePath.substring(absolutePath.indexOf(basePath) + basePath.length());
			    	return channelInfo.getDomain()+relativeDomain;
				}else if("picdir".equals(dirType)){
					String	basePath = channelInfo.getPicFilePath().replace("\\", FileUtil.FILE_SEPARATOR);//d:/data2/www/pic.duowan.com/1000y
					String relativeDomain = absolutePath.substring(absolutePath.indexOf(basePath) + basePath.length());
					return channelInfo.getPicDomain()+relativeDomain;
				}
			}
			return "";
		}
		/**
		 * 给文件大小每三位放置一个分隔符.效果：addSeparator("8685") -->8,685
		 * @param text
		 * @return
		 */
		private  String addSeparator(String text){
			String tempText = text;
			for(int i = text.length(); i >1 ; i--){
				int bitNum = text.length() - i +1 ;
				if(bitNum%3 == 0){
					tempText = tempText.substring(0, i-1)+","+tempText.substring(i-1, tempText.length() );
				}
			}
			return tempText;
		}
	}