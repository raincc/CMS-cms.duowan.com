package com.duowan.cms.service.template;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;
import com.duowan.cms.dto.template.AutoFlushTemplateSearchCriteria;

public interface AutoFlushTemplateService extends Service{

	/**
	 * 获取所有自动刷新模板对象
	 * @return
	 */
	public List<AutoFlushTemplateInfo> getAllAutoFlushTemplate();

	/**
	 * 按条件分页查询自动刷新模板对象
     * 
     * @param searchCriteria    查询条件
	 * @return
	 */
	public Page<AutoFlushTemplateInfo> pageSearch(AutoFlushTemplateSearchCriteria searchCriteria);
	
    /**
     * 创建自动刷新模板对象/修改自动刷新模板对象
     * @param templateInfo
     */
	public void save(AutoFlushTemplateInfo autoFlushTemplateInfo); 
	
    /**
     * 删除自动刷新模板对象
     * @param channelId
     * @param articleId
     */
    public void delete(String id);
	/**
	 * 判断自动刷新模板对象是否已经存在
	 * @param channelId
	 * @param templateId
	 * @return
	 */
	public boolean  isExist(AutoFlushTemplateInfo autoFlushTemplateInfo);
	/**
	 * 通过主键获取自动刷新模板对象
	 * @param id
	 * @return
	 */
	public AutoFlushTemplateInfo  getById(String id);
}
