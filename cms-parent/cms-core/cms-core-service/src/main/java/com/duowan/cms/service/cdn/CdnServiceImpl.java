package com.duowan.cms.service.cdn;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.duowan.cms.common.exception.BaseCheckedException;

@Service("cdnService")
public class CdnServiceImpl implements CdnService {

	@Override
	public boolean flushCdn(String url) throws BaseCheckedException {
		CdnHandler cdnHandler;
		try {
			cdnHandler = CdnFactory.getCdnHandler(url);
		} catch (Exception e) {
			throw new BaseCheckedException("", "获取cdn处理类异常");
		} 
		return cdnHandler.flushCdn(url);
	}

	@Override
	public boolean flushCdnDir(String dir)throws BaseCheckedException {
		CdnHandler cdnHandler;
		try {
			cdnHandler = CdnFactory.getCdnHandler(dir);
		}catch (Exception e) {
			throw new BaseCheckedException("", "获取cdn处理类异常");
		}
		return cdnHandler.flushCdnDir(dir);
	}


	

}
