/**
 * 
 */
package com.duowan.cms.service.template;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.core.rmi.client.template.AutoFlushTagRemoteService;
import com.duowan.cms.core.rmi.client.template.AutoFlushTemplateRemoteService;
import com.duowan.cms.domain.template.AutoFlushTag;
import com.duowan.cms.domain.template.rpst.AutoFlushTagRepository;
import com.duowan.cms.dto.template.AutoFlushTagInfo;
import com.duowan.cms.dto.template.AutoFlushTagSearchCriteria;

/**
 * 自动刷新标签服务
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-4-8
 * <br>==========================
 */
@Service("autoFlushTagService")
public class AutoFlushTagServiceImpl implements AutoFlushTagService, AutoFlushTagRemoteService  {

    @Autowired
    private AutoFlushTagRepository autoFlushTagRepository;
    
    @Override
    public List<AutoFlushTagInfo> listUnFlushTag() {
        return autoFlushTagRepository.listUnFlushTag();
    }

    @Override
    public void save(AutoFlushTagInfo autoFlushTagInfo) {
        if(autoFlushTagInfo.getId()==null){ //表示新增
            AutoFlushTag autoFlushTag = new AutoFlushTag(autoFlushTagInfo);
            autoFlushTagRepository.save(autoFlushTag);
        }else{
            AutoFlushTag autoFlushTag = autoFlushTagRepository.getById(autoFlushTagInfo.getId());
            if(autoFlushTag!=null){     //表示更新
                autoFlushTagRepository.update(autoFlushTag.update(autoFlushTagInfo));
            }
        }
    }

    @Override
    public List<AutoFlushTagInfo> listUnFlushTagBychannleId(String channelId) {
        AutoFlushTagSearchCriteria criteria = new AutoFlushTagSearchCriteria();
        criteria.setChannelId(channelId);
        criteria.setIsFlushed(Boolean.FALSE); //表示“未刷新”
        criteria.setPageSize(null); //表示不分页，取全部
        return autoFlushTagRepository.listSearch(criteria);
    }

    @Override
    public void batcFinishedFlushTask(List<AutoFlushTagInfo> autoFlushTagInfos) {
        if(autoFlushTagInfos!=null && !autoFlushTagInfos.isEmpty()){
            for(AutoFlushTagInfo autoFlushTagInfo : autoFlushTagInfos){
                if(autoFlushTagInfo.getFlushTime()==null)
                    autoFlushTagInfo.setFlushTime(new Date());
            }
            autoFlushTagRepository.batcUpdate(autoFlushTagInfos);
        }
    }

    @Override
    public List<AutoFlushTagInfo> list(AutoFlushTagSearchCriteria criteria) {
        return autoFlushTagRepository.listSearch(criteria);
    }

    @Override
    public boolean isExistUnFlushTag(String channelId, String tag) {
        AutoFlushTagSearchCriteria criteria = new AutoFlushTagSearchCriteria();
        criteria.setChannelId(channelId);
        criteria.setTagName(tag);
        criteria.setIsFlushed(Boolean.FALSE); //表示“未刷新”
        criteria.setPageSize(1); 
        List<AutoFlushTagInfo> autoFlushTagInfos = autoFlushTagRepository.listSearch(criteria);
        if(autoFlushTagInfos!=null && autoFlushTagInfos.size()>0)
            return true;
        return false;
    }

    @Override
    public void batcBeginFlushTask(List<AutoFlushTagInfo> autoFlushTagInfos) {
        if(autoFlushTagInfos!=null && !autoFlushTagInfos.isEmpty()){
            for(AutoFlushTagInfo autoFlushTagInfo : autoFlushTagInfos){
                if(autoFlushTagInfo.getFlushTime()==null)
                    autoFlushTagInfo.setFlushTime(new Date());
            }
            autoFlushTagRepository.batcUpdate(autoFlushTagInfos);
        }
    }

    
    
}
