package com.duowan.cms.service.worklog;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.worklog.WorkLogInfo;
import com.duowan.cms.dto.worklog.WorkLogSearchCriteria;
/**
 * 
 * <p>作　　者：黄均杨
 * <p>完成日期：2013-2-27
 */
public interface WorkLogService extends Service {

	/**
	 * 按条件分页查询文章
     * 
     * @param searchCriteria    查询条件
	 * @return
	 */
	public Page<WorkLogInfo> pageSearch(WorkLogSearchCriteria workLogSearchCriteria) throws BaseCheckedException;
	/**
	 * 获取某人在某个频道发了多少篇文章
	 */
	public int getMyWorkLog(String channelId, String userId);
	
	/**
	 * 频道流水统计
	 * @param workLogSearchCriteria
	 * @return
	 * @throws BaseCheckedException
	 */
	public Page<WorkLogInfo> channelStatistics(WorkLogSearchCriteria workLogSearchCriteria) throws BaseCheckedException;
	
	/**
	 * 用户流水统计
	 * @param workLogSearchCriteria
	 * @return
	 * @throws BaseCheckedException
	 */
	public Page<WorkLogInfo> userStatistics(WorkLogSearchCriteria workLogSearchCriteria) throws BaseCheckedException;
	
	/**
	 * 某个用户今天的工作统计
	 * @param usrInfo
	 * @return
	 */
	public List<WorkLogInfo> todayStatistics(UserInfo userInfo);
	
	/**
	 * 某个频道今天的工作统计
	 * @param channelId
	 * @return
	 */
	public List<WorkLogInfo> todayStatistics(String channelId);
}
