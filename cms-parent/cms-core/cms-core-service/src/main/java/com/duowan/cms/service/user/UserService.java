package com.duowan.cms.service.user;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserPowerInfo;
import com.duowan.cms.dto.user.UserPowerSearchCriteria;
import com.duowan.cms.dto.user.UserSearchCriteria;

/**
 * 用户服务
 * @author coolcooldee
 * @version 1.0
 * @created 04-九月-2012 17:43:12
 */
public interface UserService extends Service {
    
    /**
     * 用于验证用户的密码
     * @param userId
     * @param password
     * @return
     */
    public UserInfo getByUserIdAndPassword(String userId, String password);
    
    /**
     * 通过udbName获取用户信息
     * @param udbName
     * @return
     */
    public UserInfo getByUdbName(String udbName);
    
    /**
     * 
     * 冻结用户
     * @param userId
     */
    public void freeze(String userId);
    
    /**
     * 解冻用户
     * @param userId
     */
    public void unFreeze(String userId);
    
    /**
     * 修改用户邮箱地址
     * @param userId
     * @param email
     */
    public void updateEmail(String userId, String email);
    
    /**
     * 修改用户登录密码
     * @param userId
     * @param password
     */
    public void updatePassword(String userId, String password);
    
    /**
     * 修改用户登录用户名
     * @param oldName
     * @param newName
     */
    public void updateUserName(String oldName, String newName);
    
	/**
	 * 分页查询用户，用于列表展示
	 * 
	 * @param pageSize    分页每页数量
	 * @param pageNo    分页页码
	 * @param searchCriteria    查询条件
	 */
	public Page<UserInfo> pageSearch(UserSearchCriteria searchCriteria);
	
	
	public Page<UserPowerInfo> pageSearch(UserPowerSearchCriteria searchCriteria);
	
	/**
	 * 添加用户
	 * @param userInfo
	 */
	 public void addUser(UserInfo userInfo , UserPowerInfo userPowerInfo);
	 
	 /**
	  * 更新用户信息
	  * @param userInfo
	  */
	 public void updateUser(UserInfo userInfo, UserPowerInfo userPowerInfo);
	 
	 /**
	  * 更新用户信息
	  * @param userInfo
	  */
	 public void updateUser(UserInfo userInfo);
	 
	 /**
      * 新增用户信息
      * @param userInfo
      */
     public void addUserPower(UserPowerInfo userPowerInfo );
     
     /**
      * 更新用户信息
      * @param userInfo
      */
     public void updateUserPower(UserPowerInfo userPowerInfo );
	 
     
	/**
	 * 删除某个用户在某个频道的权限
	 * @param searchCriteria
	 */
	 public void deletePower(String userId , String channelId);
	
	/**
	 * 删除用户
	 * @param searchCriteria
	 */
	 public void deleteUser(String userId);
	 /**
	  * 通过userId获取User
	  * @param userId
	  * @return
	  */
	 public UserInfo getById(String userId);
	 
	 /**
	  * 通过用户邮箱获取User
	  * @param mail
	  * @return
	  */
	 public UserInfo getByMail(String mail);
	 
	 /**
	  * 通过userId获取User
	  * @param userId
	  * @return
	  */
	 public UserPowerInfo getByUserIdAndChannelId(String userId, String channelId);
	 
	 /**
	  * 通过用户id得到用户的所有权限
	  * @param userId
	  * @return
	  */
	 public List<UserPowerInfo> getAllPowerByUyserId(String userId) ;
	 
	 /**
	  * 在专区下，该用户是否拥有此权限
	  */
	 public boolean hasPower(String channelId, UserInfo userInfo, String powerId);
	 
	 /**
	  *判断用户是否已经存在，存在返回true,不存在返回false;
	  */
	 public boolean checkUserExist(String userId);
	 
	 /**
	  * 发送邮件信息到邮件
	  * @param mailTo
	  * @param title
	  * @param content
	  * @throws Exception
	  */
	 public void sendMessageToMail(String mailTo, String title, String content)throws Exception;
	 
}