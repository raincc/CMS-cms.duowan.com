package com.duowan.cms.service.template;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.common.util.IdManager;
import com.duowan.cms.core.rmi.client.template.AutoFlushTemplateRemoteService;
import com.duowan.cms.domain.template.AutoFlushTemplate;
import com.duowan.cms.domain.template.rpst.AutoFlushTemplateRepository;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;
import com.duowan.cms.dto.template.AutoFlushTemplateSearchCriteria;

@Service("autoFlushTemplateService")
public class AutoFlushTemplateServiceImpl implements AutoFlushTemplateService, AutoFlushTemplateRemoteService {
	
    @Autowired
    private AutoFlushTemplateRepository autoFlushTemplateRepository;
	
	@Override
	public List<AutoFlushTemplateInfo> getAllAutoFlushTemplate() {
		AutoFlushTemplateSearchCriteria searchCriteria = new AutoFlushTemplateSearchCriteria();
		searchCriteria.setPageSize(Integer.MAX_VALUE);
		return autoFlushTemplateRepository.pageSearch(searchCriteria).getData();
	}

	@Override
	public Page<AutoFlushTemplateInfo> pageSearch(AutoFlushTemplateSearchCriteria searchCriteria) {
		return autoFlushTemplateRepository.pageSearch(searchCriteria);
	}

	@Override
	public void save(AutoFlushTemplateInfo autoFlushTemplateInfo) {
		if (autoFlushTemplateInfo.getId() == null) {  //新增
			autoFlushTemplateInfo.setId(IdManager.generateId());
			autoFlushTemplateRepository.save(new AutoFlushTemplate(autoFlushTemplateInfo));
		}else{
			autoFlushTemplateRepository.update(new AutoFlushTemplate(autoFlushTemplateInfo));
		}
	}

	@Override
	public void delete(String id) {
		AutoFlushTemplate autoFlushTemplate = autoFlushTemplateRepository.getById(id);
		autoFlushTemplateRepository.delete(autoFlushTemplate);
	}

	@Override
	public AutoFlushTemplateInfo getById(String id) {
		AutoFlushTemplate autoFlushTemplate = autoFlushTemplateRepository.getById(id);
		return (AutoFlushTemplateInfo)DoToDtoConvertorFactory.getConvertor(AutoFlushTemplate.class).do2Dto(autoFlushTemplate);
	}

	@Override
	public boolean isExist(AutoFlushTemplateInfo autoFlushTemplateInfo) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
