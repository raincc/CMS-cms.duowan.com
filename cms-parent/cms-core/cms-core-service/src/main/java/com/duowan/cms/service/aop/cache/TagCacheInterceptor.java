/**
 * 
 */
package com.duowan.cms.service.aop.cache;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.cache.support.AbstractServiceCacheInterceptor;
import com.duowan.cms.dto.tag.TagInfo;

/**
 * 标签服务的缓存拦截器
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-12
 * <br>==========================
 */

@Aspect
@Order(value = 0)
@Service("tagCacheInterceptor")
public class TagCacheInterceptor extends AbstractServiceCacheInterceptor {
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.getTagsExcludeSpecialToJson(..))")
    public void getTagsExcludeSpecialToJsonPointcut() {}
    
    /**
     * 对 "获取所有非特殊标签信息的服务" 做缓存, 缓存不自动过期
     */
    @Around("getTagsExcludeSpecialToJsonPointcut()")
    public Object cacheGetTagsExcludeSpecialToJsonService(ProceedingJoinPoint joinPoint) throws Throwable {
        String channelId= (String) joinPoint.getArgs()[0];
        return cache(joinPoint, "GET_TAGS_EXCLUDE_SPECIAL_TO_JSON_"+channelId+"KEY", -1);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.getAllTagsInChannel(..))")
    public void getAllTagsInChannelPointcut() {}
    
    /**
     * 对 "获取某频道所有特殊标签信息的服务" 做缓存, 缓存不自动过期
     */
    @Around("getAllTagsInChannelPointcut()")
    public Object cacheGetAllTagsInChannelService(ProceedingJoinPoint joinPoint) throws Throwable {
        String channelId= (String) joinPoint.getArgs()[0];
        return cache(joinPoint, "GET_ALL_TAGS_IN_CHANNEL_"+channelId+"KEY", -1);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.addChild(..))")
    public void addChildPointcut() {}
    /**
     * 对"增加标签的服务"做拦截,清除缓存
     */
    @Around("addChildPointcut()")
    public void cacheAddChileService(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedAndCleanByTagInfo(joinPoint);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.updateTag(..))")
    public void updateTagPointcut() {}
    
    /**
     * 对 "更新标签信息的服务" 做拦截, 清除缓存
     */
    @Around("updateTagPointcut()")
    public void cacheUpdateTagService(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedAndCleanByTagInfo(joinPoint);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.updateTagName(..))")
    public void updateTagNamePointcut() {}
    
    /**
     * 对 "修改标签名字的服务" 做拦截，清除缓存
     */
    @Around("updateTagNamePointcut()")
    public void cacheUpdateTagNameService(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedAndCleanByChannelId(joinPoint);
    }
    
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.deleteTag(..))")
    public void deleteTagNamePointcut() {}
    
    /**
     * 对 "删除标签的服务" 做拦截，清除缓存
     */
    @Around("deleteTagNamePointcut()")
    public void cacheDeleteTagNameService(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedAndCleanByTagInfo(joinPoint);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.removeTagAllMsg(..))")
    public void removeTagAllMsgPointcut() {}
    
    /**
     * 对 "删除标签的服务" 做拦截，清除缓存
     */
    @Around("removeTagAllMsgPointcut()")
    public void cacheRemoveTagAllMsgService(ProceedingJoinPoint joinPoint) throws Throwable {
        TagInfo tagInfo= (TagInfo) joinPoint.getArgs()[0];
        logger.info("删除tag"+tagInfo.getName()+"被拦截到。");
        proceedAndCleanByTagInfo(joinPoint);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.pasteTag(..))")
    public void pasteTagPointcut() {}
    
    /**
     * 对 "黏贴标签" 做拦截，清除缓存
     */
    @Around("pasteTagPointcut()")
    public void cachePasteTagService(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedAndCleanByChannelId(joinPoint);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.addAlias(..))")
    public void addAliasPointcut() {}
    
    /**
     * 对 "增加标签别名" 做拦截，清除缓存
     */
    @Around("addAliasPointcut()")
    public void cacheAddAliasService(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedAndCleanByChannelId(joinPoint);
    }
    
    @Pointcut("execution(public * com.duowan.cms.service.tag.TagService.exchangeTagImage(..))")
    public void exchangeTagImagePointcut() {}
    
    /**
     * 清除缓存
     */
    @Around("exchangeTagImagePointcut()")
    public void cacheexchangeTagImageService(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedAndCleanByChannelId(joinPoint);
    }
    
    private void proceedAndCleanByTagInfo(ProceedingJoinPoint joinPoint) throws Throwable {
        joinPoint.proceed();
        
        TagInfo tagInfo= (TagInfo) joinPoint.getArgs()[0];
        String channelId = tagInfo.getChannelId();
        this.clean(channelId);
    }
    
    private void proceedAndCleanByChannelId(ProceedingJoinPoint joinPoint) throws Throwable {
        joinPoint.proceed();
        String channelId= (String) joinPoint.getArgs()[0];
        this.clean(channelId);
    }
    
    
    /*private void clean(TagInfo tagInfo){
        String channelId = tagInfo.getChannelId();
        remove("GET_ALL_TAGS_IN_CHANNEL_"+channelId+"KEY");
        remove("GET_TAGS_EXCLUDE_SPECIAL_TO_JSON_"+channelId+"KEY");
    }*/
    
    /**
     * 清除所有的缓存
     * @param tagInfo
     */
    private void clean(String channelId){
        remove("GET_ALL_TAGS_IN_CHANNEL_"+channelId+"KEY");
        remove("GET_TAGS_EXCLUDE_SPECIAL_TO_JSON_"+channelId+"KEY");
    }
    
    
    
    
}
