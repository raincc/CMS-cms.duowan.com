package com.duowan.cms.service.worklog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.domain.article.ArticleCount;
import com.duowan.cms.domain.article.rpst.ArticleCountRepository;
import com.duowan.cms.domain.article.rpst.ArticleRepository;
import com.duowan.cms.dto.article.ArticleCountInfo;
import com.duowan.cms.dto.article.ArticleCountSearchCriteria;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserPowerInfo;
import com.duowan.cms.dto.user.UserRole;
import com.duowan.cms.dto.worklog.WorkLogInfo;
import com.duowan.cms.dto.worklog.WorkLogSearchCriteria;
import com.duowan.cms.service.channel.ChannelService;

/**
 * 
 * <p>作　　者：黄均杨
 * <p>完成日期：2013-2-27
 */
@Service("workLogService")
public class WorkLogServiceImpl implements WorkLogService {

    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private ArticleCountRepository articleCountRepository;
    @Autowired
    private ChannelService channelService;

    /**
     * 按条件分页查询文章
     * 
     * @param searchCriteria    查询条件
     * @return
     * @throws BaseCheckedException 
     */
    public Page<WorkLogInfo> pageSearch(WorkLogSearchCriteria workLogSearchCriteria) throws BaseCheckedException {
        ArticleCountSearchCriteria articleCountSearchCriteria = new ArticleCountSearchCriteria().updateByWorkLogSearchCriteria(workLogSearchCriteria);
        ChannelInfo channelInfo = channelService.getByName(workLogSearchCriteria.getChannelName());
        if (channelInfo != null) {
            articleCountSearchCriteria.setChannelId(channelInfo.getId());
        }
        Page<ArticleCountInfo> articleCountInfoPage = articleCountRepository.pageSearch(articleCountSearchCriteria);
        List<ArticleCountInfo> articleCountInfoList = articleCountInfoPage.getData();
        List<WorkLogInfo> workLogInfoList = new ArrayList<WorkLogInfo>();
        for (ArticleCountInfo articleCountInfo : articleCountInfoList) {
            WorkLogInfo workLogInfo = new WorkLogInfo().updateByArticleCountInfo(articleCountInfo);
            workLogInfo.setChannelInfo(channelService.getById(workLogInfo.getChannelId()));
            workLogInfoList.add(workLogInfo);
        }

        return new Page<WorkLogInfo>(articleCountInfoPage.getStart(), articleCountInfoPage.getPageSize(), articleCountInfoPage.getTotalCount(), workLogInfoList);
    }

    @Override
    public int getMyWorkLog(String channelId, String userId) {
        Integer articleCount = articleRepository.getUserCountToday(channelId, userId);
        if (articleCount == null) {
            return 0;
        }
        return articleCount;
    }

    @Override
    public Page<WorkLogInfo> channelStatistics(WorkLogSearchCriteria workLogSearchCriteria) throws BaseCheckedException {
        ArticleCountSearchCriteria articleCountSearchCriteria = new ArticleCountSearchCriteria().updateByWorkLogSearchCriteria(workLogSearchCriteria);
        ChannelInfo channelInfo = channelService.getByName(workLogSearchCriteria.getChannelName());
        if (channelInfo != null) {
            articleCountSearchCriteria.setChannelId(channelInfo.getId());
        }
        Page<ArticleCountInfo> articleCountInfoPage = articleCountRepository.channelStatistics(articleCountSearchCriteria);
        List<ArticleCountInfo> articleCountInfoList = articleCountInfoPage.getData();
        List<WorkLogInfo> workLogInfoList = new ArrayList<WorkLogInfo>();
        for (ArticleCountInfo articleCountInfo : articleCountInfoList) {
            WorkLogInfo workLogInfo = new WorkLogInfo().updateByArticleCountInfo(articleCountInfo);
            workLogInfo.setChannelInfo(channelService.getById(workLogInfo.getChannelId()));
            workLogInfoList.add(workLogInfo);
        }

        return new Page<WorkLogInfo>(articleCountInfoPage.getStart(), articleCountInfoPage.getPageSize(), articleCountInfoPage.getTotalCount(), workLogInfoList);
    }

    @Override
    public Page<WorkLogInfo> userStatistics(WorkLogSearchCriteria workLogSearchCriteria) throws BaseCheckedException {
        ArticleCountSearchCriteria articleCountSearchCriteria = new ArticleCountSearchCriteria().updateByWorkLogSearchCriteria(workLogSearchCriteria);
        Page<ArticleCountInfo> articleCountInfoPage = articleCountRepository.userStatistics(articleCountSearchCriteria);
        List<ArticleCountInfo> articleCountInfoList = articleCountInfoPage.getData();
        List<WorkLogInfo> workLogInfoList = new ArrayList<WorkLogInfo>();
        for (ArticleCountInfo articleCountInfo : articleCountInfoList) {
            WorkLogInfo workLogInfo = new WorkLogInfo().updateByArticleCountInfo(articleCountInfo);
            workLogInfoList.add(workLogInfo);
        }

        return new Page<WorkLogInfo>(articleCountInfoPage.getStart(), articleCountInfoPage.getPageSize(), articleCountInfoPage.getTotalCount(), workLogInfoList);
    }

    @Override
    public List<WorkLogInfo> todayStatistics(UserInfo userInfo) {
        List<WorkLogInfo> resultList = new ArrayList<WorkLogInfo>();
        List<UserPowerInfo> list = userInfo.getAllChannelPowerInfo();
        String userId = userInfo.getUserId();
        Date date = new Date();
        if(null != list){
            for(UserPowerInfo userPowerInfo : list){
                if(UserRole.ADMINISTRATOR != userPowerInfo.getUserRole()){
                    String channelId = userPowerInfo.getChannelId();
                    int count = articleRepository.getUserCountToday(channelId, userId);
                    if(count > 0){
                        WorkLogInfo workLogInfo = new WorkLogInfo();
                        workLogInfo.setChannelId(channelId);
                        workLogInfo.setChannelInfo(userPowerInfo.getChannelInfo());
                        workLogInfo.setUserId(userId);
                        workLogInfo.setDate(date);
                        workLogInfo.setCount(count);
                        resultList.add(workLogInfo);
                    }
                }
            }
        }
        return resultList;
    }

    @Override
    public List<WorkLogInfo> todayStatistics(String channelId) {
        List<ArticleCount> list = articleRepository.getAllUserCount(channelId, 0);
        ChannelInfo channelInfo = channelService.getById(channelId);
        List<WorkLogInfo> workLogInfoList = new ArrayList<WorkLogInfo>();
        Date date = new Date();
        for (ArticleCount articleCount : list) {
            WorkLogInfo workLogInfo = new WorkLogInfo();
            workLogInfo.setChannelId(channelId);
            workLogInfo.setUserId(articleCount.getUserId());
            workLogInfo.setCount(articleCount.getCount());
            workLogInfo.setDate(date);
            workLogInfo.setChannelInfo(channelInfo);
            workLogInfoList.add(workLogInfo);
        }
        return workLogInfoList;
    }

}
