/**
 * 
 */
package com.duowan.cms.service.channel;

import java.util.List;

import org.apache.log4j.Logger;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.channel.ChannelSearchCriteria;
import com.duowan.cms.dto.user.UserInfo;

/**
 * @author coolcooldee
 *
 */
public interface ChannelService extends Service {
    
    public static Logger logger = Logger.getLogger(ChannelService.class);
    
    //异常码：该专区不存在
    public final static String CHANNEL_IS_NOT_EXITS = ChannelService.class.getName().concat("001");
    
    /**
     * 新增一个频道(开专区)
     * @param channelInfo
     */
    public void openNewChannel(ChannelInfo channelInfo, String logoPath) throws Exception;
    
    /**
     * 专区上线(切外网)
     * @param channelInfo
     * @throws Exception
     */
    public void upline(ChannelInfo channelInfo) throws Exception;
    
    /**
     * 专区下线
     * @param channleInfo
     * @throws Exception
     */
    public void downline(ChannelInfo channelInfo) throws Exception;
    
    /**
     * 以主编权限将userId(userId必须存在于系统)加入新频道
     * @param userId
     * @param newChannelInfo
     * @return
     */
    public void addMainEditorToNewChannel(List<UserInfo> mainEditors, ChannelInfo newchannelInfo, String adminId) throws BaseCheckedException;
    
    /**
     * 根据ID获取频道信息
     * @param id
     * @return
     */
    public ChannelInfo getById(String id);
    
    /**
     * 获取所有的频道信息（按照频道的首字母排序）
     * @return
     */
    public List<ChannelInfo> getAllChannel();
    
    /**
     * 根据专区名字获取专区
     * @param cname
     * @return
     */
    public ChannelInfo getByName(String cname) ;
    
    /**
     * 分页查询
     * @param searchCriteria
     * @return
     */
    public Page<ChannelInfo> pageSearch(ChannelSearchCriteria searchCriteria);
    
    /**
     * 使某频道成为重点频道
     * @param channelId
     */
    public void important(String channelId);
    
    /**
     * 使某频道成为非重点频道
     * @param channelId
     */
    public void unimportant(String channelId);
    
    /**
     * 更新频道信息
     * @param channelInfo
     */
    public void updateChannelInfo(ChannelInfo channelInfo);
}
