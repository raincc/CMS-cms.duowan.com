/**
 * 
 */
package com.duowan.cms.service.gametest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duowan.cms.core.rmi.client.gametest.GameTestRemoteService;
import com.duowan.cms.domain.gametest.rpst.GameTestRepository;
import com.duowan.cms.dto.gametest.GameTestInfo;
import com.duowan.cms.dto.gametest.GameTestSearchCriteria;

@Service("gameTestService")
public class gameTestServiceImpl implements GameTestService, GameTestRemoteService {

    @Autowired
    private GameTestRepository gameTestRepository;

    @Override
    public List<GameTestInfo> listSearch(GameTestSearchCriteria searchCriteria) {
        
        return gameTestRepository.listSearch(searchCriteria);
    }

    

}
