/**
 * 
 */
package com.duowan.cms.service.timetask;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 模板刷新的定时任务
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-10-30
 * <br>==========================
 */
public interface CoreTimeTask {
    
    public final Log logger = LogFactory.getLog(CoreTimeTask.class);
    
    /**
     * 统计某频道某人发表了多少篇文章 的定时任务
     */
    public void countArticle();
    
    
    public void fixCountArticle(String channelId, Date date);
    
    /**
     * 统计文章的回复数（从评论系统抓取）
     */
    public void countReply();
    
    /**
     * 刷新频率："0 0 2 ? * MON"
     * 定期更新用户的密码，每周一的凌晨两点钟
     */
    public void updateUserPassword();
    
    
    
}
