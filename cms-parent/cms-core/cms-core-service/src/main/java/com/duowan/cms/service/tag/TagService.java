/**
 * 
 */
package com.duowan.cms.service.tag;

import java.util.List;

import org.apache.log4j.Logger;

import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.tag.TagInfo;

/**
 * @author coolcooldee
 *
 */
public interface TagService extends Service{
    
    public static Logger logger = Logger.getLogger(TagService.class);
    
    /**
     * 在某个频道下，根据tag名字获取Tag
     * @param channelId
     * @param name
     * @return
     */
    public TagInfo getByChannelIdAndName(String channelId, String name);
    
    /**
     * 获取某个tag下一级的子tag（只是一级的，超过一级的不返回），如果获取最高级，则parent参数置为“”
     * @param channelId
     * @param parent
     * @return
     */
    public List<TagInfo> getChildren(String channelId, String parent);
    
    /**
     * 在某个专区下的某个父tag下添加新的子tag，如果是新增一级的tag，那么父tag值置为“空字符”
     */
    public void addChild(TagInfo tagInfo);
    
    
    /**
     * 获取某频道下整体tag树，包括所有子孙tag（需要按照“tree”属性排序，方便页面展示）
     * @return
     */
    public List<TagInfo> getAllTagsInChannel(String channelId);
    
    
    /**
     * 获取非特殊tag的json数据(用于页面显示tag的下拉列表)，需要做缓存
     * 形如：[{"多玩日常":["假日活动","日常活动","每周活动","特殊活动"]},{"心情故事":["玩家文集"]},{"新闻资讯":["倩女动态","更新公告"]},{"游戏介绍":["新手上路"]},{"游戏图片":["游戏截图","精美壁纸","装备秀场"]},{"游戏视频":["对战视频","攻略视频","爆笑恶搞","精彩CG"]},{"游戏资料":["PK心得","升级赚钱","攻略技巧","装备生产"]},{"玩法活动":["休闲活动","周末活动","常规活动"]},{"系统介绍":[{"装备系统":["白色装备","紫色装备","红色装备","绿色装备","蓝色装备","鬼器"]},{"道具介绍":["包裹","物品"]},"交流系统","任务系统","其他系统","合成系统","坐骑系统","天气系统","帮会系统","战斗相关","技能装备","收集系统","生活系统","组队系统","经济系统","职业技能"]},{"职业攻略":["侠客","刀客","医师","射手","异人","方士","甲士","职业心得","魅者"]},"专题资料","回复系统","帮会战报"]
     * @return
     */
    public String getTagsExcludeSpecialToJson(String channelId);
    
    /**
     * 获取特殊tag的列表，需要做缓存
     * @return
     */
    public List<TagInfo> getSpecialTags(String channelId);
    
  
    /**
     * 发表文章时，根据tags，获取tagTree，包括所有的
     * @param tags  多个用","或者";"分割
     * @return
     */
    public String  getTagTreeByPostTags(String channelId, String tags) ;
    
    /**
     * 发表文章时，根据tags，获取tagTree，不包括特殊的
     * @param tags  多个用","或者";"分割
     * @return
     */
    public String  getTagTreeByPostTagsExcludeSpecial(String channelId, String tags) ;
    
    /**
     * 在原来的tags上增加一个tag，用于字符串的方式拼接tag
     * @param tags
     * @return
     */
    public String addTagStr(String tags, String tag);
    /**
     * 更新标签
     * @param tagInfo
     */
    public void updateTag(TagInfo tagInfo);
    /**
     * 更新标签名称
     */
    public void updateTagName(String channelId , String originalTagName , String newTagName);
    
    /**
     * 删除标签,仅删除标签对应的数据库记录
     * @param tagInfo
     */
    public void deleteTag(TagInfo tagInfo);
    /**
     * 删除标签及标签相关的所有信息
     * @param tagInfo
     */
    public void removeTagAllMsg(TagInfo tagInfo);
    /**
     * 把 originalChannelId频道的originalTagName标签 粘贴到 channelId频道的parentName标签
     * @param channelId 被剪切的标签的频道
     * @param parentName 被剪切的标签的标签名
     * @param originalChannelId 
     * @param originalTagName
     */
    public void pasteTag(String channelId , String  parentName  , String originalChannelId , String originalTagName);
    /**
     * 添加别名，别名为模板名
     * @param channelId
     * @param tagName
     * @param alias          别名
     */
    public void addAlias(String channelId ,String tagName , String alias );
    /**
     * 切换标签"图"与"非图"
     * @param channelId
     * @param tagName
     * @param isTagImage true为"图",false为"非图"
     */
    public void exchangeTagImage(String channelId ,String tagName, boolean isTagImage);
    /**
     * 判断标签在channelId频道下是否已经存在
     */
    public boolean isExist(String channelId,String tagName);
    /**
     * 获取子孙标签深度最深子标签的深度
     */
    public int getMaxDepthOfSonTag(String channelId,String tagName);
    
}
