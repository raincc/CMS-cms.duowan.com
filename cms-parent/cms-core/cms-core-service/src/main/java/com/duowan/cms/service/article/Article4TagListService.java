package com.duowan.cms.service.article;

import org.apache.log4j.Logger;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.Article4TagListSearchCriteria;
import com.duowan.cms.dto.article.ArticleInfo;

/**
 * 文章tag列表服务
 * @author yzq
 * @version 1.0
 * @created 04-九月-2012 17:43:12
 */
public interface Article4TagListService extends Service {

    public static Logger logger = Logger.getLogger(Article4TagListService.class);

    /**
     * 根据文章ID获取某个频道某tag列表下的文章信息
     * 
     * @param channelId 频道ID
     * @param articleId 文章ID
     * @return
     */
    public Article4TagListInfo getByPrimaryKey(String channelId, String tag, Long articleId);

    /**
     * 发布空链接
     * 
     * @param articleInfo
     */
    @Deprecated
    public void publishEmptyLink(Article4TagListInfo article4TagListInfo) throws BaseCheckedException;
    /**
     * 保存某篇文章下的所有"列表文章"数据
     * @param articleInfo
     */
    public void saveArticle4TagListInfo(ArticleInfo articleInfo);
    /**
     * 更新某篇文章下的某tag下的"列表文章"
     * @param article4TagListInfo
     */
    public void updateArticle4TagListInfo(Article4TagListInfo article4TagListInfo);

    /**
     * 更新某篇文章下的所有"列表文章"数据
     * @param articleInfo
     */
    public void  updateArticle4TagListInfo(ArticleInfo articleInfo);
    /**
     * 调整某tag列表下文章的权重
     * 
     * @param articleInfo
     * @param num
     */
    public void updatePower(String channelId, String tag, Long articleId, int power);

    /**
     * 调整某tag列表文章的优先级，顶或者降
     * 
     * @param articleId 文章ID
     * @param num 上浮或者下降的天数，正数时为上浮，负数时为下降
     */
    public void updateDayNum(String channelId, String tag, Long articleId, int num);

    /**
     * 还原某tag列表文章的优先级
     * @param articleId
     */
    public void resetDayNum(String channelId, String tag, Long articleId);

    /**
     * 按条件分页查询文章
     * 
     * @param searchCriteria    查询条件
     * @return
     */
    public Page<Article4TagListInfo> pageSearch(Article4TagListSearchCriteria searchCriteria);
    
    /**
     * 	删除某文章列表数据
     * @param channelId
     * @param tag
     * @param articleId
     */
    public void deleteArticle4TagList(String channelId, String tag, Long articleId);
    /**
     * 根据频道id和文章id批量删除文章列表数据
     */
    public void deleteByChannelIdAndArticleId(String channelId, Long articleId);
	/**
	 * 检查题目是否已经存在
	 * @param channelId
	 * @param title
	 * @return
	 */
	public boolean checkTitleIsExisted(String channelId, Long articleId ,String title ,String tag);
}