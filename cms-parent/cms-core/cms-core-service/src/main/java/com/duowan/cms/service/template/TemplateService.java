/**
 * 
 */
package com.duowan.cms.service.template;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.template.SimpleTemplateInfo;
import com.duowan.cms.dto.template.TemplateInfo;
import com.duowan.cms.dto.template.TemplateSearchCriteria;
import com.duowan.cms.dto.user.UserInfo;

/**
 * @author coolcooldee
 * @version 1.0
 * @created 08-10-2012 17:40:15
 */
public interface TemplateService  extends Service {
    
    //异常码：该模板在该专区下不存在
    public final static String TEMPLATE_IS_NOT_EXIST_IN_CHANNEL = TemplateService.class.getName()+"001";
    
    /**
     * 创建新模板/修改模板
     * @param templateInfo
     */
    public void publish(TemplateInfo templateInfo) throws BaseCheckedException;
    /**
     *  创建新模板/修改模板  并预览
     *  @return 返回预览路径
     */ 
    public String publishAndPreview(TemplateInfo templateInfo) throws BaseCheckedException;
    
    /**
     * 删除模板
     * @param channelId
     * @param articleId
     */
    public void delete(String channelId, Long templateId);
    
    /**
     * 按条件分页查询模板
     * 
     * @param searchCriteria    查询条件
     * @return
     */
    public Page<TemplateInfo> pageSearch(TemplateSearchCriteria searchCriteria);
    /**
     * 分页查询，用于显示
     * @param searchCriteria
     * @return
     */
    public Page<SimpleTemplateInfo> pageSearch4Show(TemplateSearchCriteria searchCriteria);
    
    /**
     * 获取所有的模板类别，JSON格式返回，用于页面下拉框
     * @return
     */
    public String getAllCategoryJson();

    /**
     * 是否还有文章在使用该模板: 有文章使用返回true , 无文章使用返回false;
     */
    public boolean hasArticleUse(String channelId, Long templateId);
    
    /**
     * 从session中之前复制的模板的id串，逐个新增到数据库（与copy操作配合使用）
     */
    public void paste(String channelId , String templatecopy , TemplateInfo templateInfo);
    
    
    /**
     * 保session中指定频道的所有模板，保存到新的频道
     * @param channelId
     * @param newTemplateInfo 提供channelId , userIp , proxyIp , createUserId ,lastUpdateUserId等信息
     */
    public String pasteAll(String destChannelId , TemplateInfo newTemplateInfo) throws BaseCheckedException ;
    
    /**
     * 通过模版名字和频道id获取频道信息
     * @param channelId
     * @param templateName
     * @return
     */
    public TemplateInfo getByChannelIdAndTemplateName(String channelId, String templateName);
    
    /**
     * 通过模版id和频道id获取频道信息
     * @param channelId
     * @param templateId
     * @return
     */
    public TemplateInfo getByChannelIdAndTemplateId(String channelId, Long templateId);
    
    /**
     *  根据查询条件获取模板列表(慎用，结果集过大的时候，为了不影响性能，请使用pageSearch分页查询)
     * @param searchCriteria
     * @return
     */
    public List<TemplateInfo> listSearch(TemplateSearchCriteria searchCriteria) ;

    /**
     * 检查分类是否已经存在
     */
    public boolean isTemplateCategoryExist(String channelId , String templateCategory);
    
    /**
     * 获取“需要自动刷新的模板”的列表
     * @return
     */
    public List<TemplateInfo> listAutoFlushTemplates();
    /**
     * 判断是否拥有复制最终页模板的权限
     */
    public boolean hasPermissionCopyFinalTemplate(String channelId , UserInfo userInfo  , Long templateId);
    /**
     * 是否合法的别名
     * @param name
     * @return
     */
    public boolean isValidAlias(String channelId, Long templateId, String name);
    
}
