/**
 * 
 */
package com.duowan.cms.service.user;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.domain.user.Power;
import com.duowan.cms.dto.user.PowerInfo;

/**
 * @author coolcooldee
 * 
 */
@Service("powerService")
public class PowerServiceImpl implements PowerService {
    
    private static List<PowerInfo> allPowerInfoList =null;
    
    @Override
    public List<PowerInfo> getAll() {
        if(allPowerInfoList!=null)
            return allPowerInfoList;
        allPowerInfoList = new ArrayList<PowerInfo>();
        Map<String, String> map = Power.getAllPowerMap();
        Set<Map.Entry<String, String>> set = map.entrySet();
        for (Iterator<Map.Entry<String, String>> it = set.iterator(); it
                .hasNext();) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>)it
                    .next();
            if(!StringUtil.isEmpty(entry.getValue()))
                allPowerInfoList.add(new PowerInfo(entry.getKey(),  entry.getValue()));
        }
        return allPowerInfoList;
    }

    @Override
    public PowerInfo getById(String id) {
        String powerName = Power.getAllPowerMap().get(id);
        if(StringUtil.isEmpty(powerName))
            return null;
        return new PowerInfo(id, powerName);
    }
    
}
