/**
 * 
 */
package com.duowan.cms.service.user;

import java.util.List;

import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.user.PowerInfo;

/**
 * 权限服务
 * @author coolcooldee
 *
 */
public interface PowerService extends Service {
 
    /**
     * 获取所有的权限
     * @return
     */
    public List<PowerInfo> getAll();
    
    /**
     * 根据ID获取权限
     * @return
     */
    public PowerInfo getById(String id);
    
}
