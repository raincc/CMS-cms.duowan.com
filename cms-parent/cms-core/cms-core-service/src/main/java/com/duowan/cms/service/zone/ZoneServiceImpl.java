package com.duowan.cms.service.zone;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.service.DoToDtoConvertorFactory;
import com.duowan.cms.domain.tag.Tag;
import com.duowan.cms.domain.zone.ZoneUser;
import com.duowan.cms.domain.zone.rpst.ZoneUserRepository;
import com.duowan.cms.dto.tag.TagInfo;
import com.duowan.cms.dto.zone.ZoneUserInfo;


@Service("zoneService")
public class ZoneServiceImpl implements ZoneService {
	
	@Autowired
	private ZoneUserRepository zoneUserRepository;

	@Override
	public List<ZoneUserInfo> getZoneUserInfo(String channelId) {
		return zoneUserRepository.getByChannelId(channelId);
	}

	@Override
	public ZoneUserInfo getById(String id) {
		ZoneUser zoneUser =	zoneUserRepository.getById(Long.parseLong(id));
		return (ZoneUserInfo)DoToDtoConvertorFactory.getConvertor(ZoneUser.class).do2Dto(zoneUser);
	}

	
}
