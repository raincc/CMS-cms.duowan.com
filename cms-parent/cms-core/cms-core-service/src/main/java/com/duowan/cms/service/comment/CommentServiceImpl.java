/**
 * 
 */
package com.duowan.cms.service.comment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.comment.CommentInfo;

/**
 * @author coolcooldee
 *
 */
@Service("commentService")
public class CommentServiceImpl implements CommentService {
    
    /**
     * 根据输入的评论url，解析threadid
     * 
     * @param url
     * @return
     */
    public long getThreadIdFromCommentUrl(String url) {
        
        if (StringUtil.isEmpty(url))
            return 0L;
        
        long threadid = 0L;
        String[] results = new String[2];
        Pattern pattern = Pattern.compile("/(([\\w])_)?([^\\./]+).html$",Pattern.CASE_INSENSITIVE);
        
        Matcher m = pattern.matcher(url);
        // 2是类型，3是id
        if (m.find()) {
            if (!StringUtil.isEmpty(m.group(2)))
                results[0] = m.group(2);
            else
                results[0] = "";
            results[1] = m.group(3);
        }
        if (StringUtil.isEmpty(results[0])) { // 说明是文章url
            try {
                threadid = Long.parseLong(results[1]);
            } catch (Exception e) {
                logger.error("评论URL=" + url + "转换出错。");
            }
        }
        
        return threadid;
    }

    @Override
    public void add(CommentInfo commentInfo) {
        
    }
    
}
