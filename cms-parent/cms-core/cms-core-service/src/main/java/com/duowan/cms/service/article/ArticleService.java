package com.duowan.cms.service.article;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.Service;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;
import com.duowan.cms.dto.article.SimpleArticleInfo;
import com.duowan.cms.dto.user.UserInfo;

/**
 * 文章服务
 * @author coolcooldee
 * @version 1.0
 * @created 04-九月-2012 17:43:12
 */
public interface ArticleService extends Service {
    
    public static Logger logger = Logger.getLogger(ArticleService.class);
    
    //异常码：该专区下文章不存在
    public final static String ARTICLE_IS_NOT_EXITS = ArticleService.class.getName().concat("001");
    //异常码：文章不是普通类型
    public final static String ARTICLE_IS_NOT_NORNAL = ArticleService.class.getName().concat("002");

    /**
     * 根据文章ID获取某个频道下的文章信息
     * 
     * @param channelId 频道ID
     * @param articleId 文章ID
     * @return
     */
    public ArticleInfo getByChannelIdAndArticleId(String channelId, Long articleId);
    
	/**
	 * 发布文章
	 * 
	 * @param articleInfo
	 */
	public void publish(ArticleInfo articleInfo)  throws BaseCheckedException ;
	
	/**
	 * 发布空链接
	 * 
	 */
	public void publishEmptyLink(String channelId , List<Article4TagListInfo> list)  throws BaseCheckedException ;
	
	
	/**
	 *  删除文章 article表逻辑删除 list表物理删除 更新相关tag的updatetime 文章的html文件，内容替换为空。重新写文件
	 * @param channelId
	 * @param articleId
	 */
	public void delete(UserInfo userInfo , String channelId, Long articleId)throws BaseCheckedException;
	
	/**
     * 隐藏文章，保留article表，删除所有list和文章静态页面
     * @param channelId
     * @param articleId
     */
    public void hide(UserInfo userInfo ,String channelId, Long articleId) throws ParseException, BaseCheckedException;
    
    /**
     * 更新文章的发布时间
     * @param channelId
     * @param articleId
     * @param newPublishTime
     * @throws BaseCheckedException
     */
    public void updatePublishTime(String channelId, Long articleId, Date newPublishTime) throws BaseCheckedException;
	
	/**
	 * 调整文章的权重
	 * 
	 * @param articleInfo
	 * @param num
	 */
	public void updatePower(String channelId, Long articleId,  int power);
	
	/**
	 * 调整文章的优先级，顶或者降
	 * 
	 * @param articleId 文章ID
	 * @param num 上浮或者下降的天数，正数时为上浮，负数时为下降
	 */
	public void updateDayNum(String channelId, Long articleId, int num );
	
	/**
	 * 还原文章的优先级
	 * @param articleId
	 */
	public void resetDayNum(String channelId, Long articleId);
	

	/**
	 * 按条件分页查询文章
     * 
     * @param searchCriteria    查询条件
	 * @return
	 */
	public Page<ArticleInfo> pageSearch(ArticleSearchCriteria searchCriteria);
	/**
     * 分页查询，用于显示
     * @param searchCriteria
     * @return
     */
    public Page<SimpleArticleInfo> pageSearch4Show(ArticleSearchCriteria searchCriteria);
	/**
	 * 检查题目是否已经存在
	  * @param channelId
      * @param articleId 为空时，表示是新增文章时的比较
      * @param title
	 * @return
	 */
	public boolean checkTitleIsExisted(String channelId, String articleId ,String title);
}