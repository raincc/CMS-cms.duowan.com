
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.net.HttpURLConnection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<%=doGet("http://www.ip138.com/", "utf-8") %>


<%!
private static final String DEFAULT_CHARSET = "utf-8";
private static final String METHOD_POST = "POST";
public static final String METHOD_GET = "GET";
public static final String USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.29 Safari/525.13";
public static final String CTYPE = "application/x-www-form-urlencoded;charset=" + DEFAULT_CHARSET;

public static String doGet(String url, String charset) throws IOException {
	HttpURLConnection conn = null;
	String rsp = null;
	try {
		try {
			conn = getConnection(new URL(url), METHOD_GET, CTYPE);
			rsp = getResponseAsString(conn);
		} catch (IOException e) {
			throw e;
		}
	} finally {
		if (conn != null) {
			conn.disconnect();
		}
	}
	return rsp;
}

public static String getResponseAsString(HttpURLConnection conn) throws IOException {
	String charset = getResponseCharset(conn.getContentType());
	InputStream es = conn.getErrorStream();
	if (es == null) {
		return getStreamAsString(conn.getInputStream(), charset);
	}
	return null;
}

public static HttpURLConnection getConnection(URL url, String action, String ctype) throws IOException {

	//setLocalProxy("59.34.57.88","8080");

	HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	conn.setRequestMethod(action);
	conn.setDoInput(true);
	conn.setDoOutput(true);
	conn.setRequestProperty("Accept", "text/xml,text/javascript,text/html");
	conn.setRequestProperty("User-Agent", USER_AGENT);
	conn.setRequestProperty("Content-Type", ctype);
	
	//System.setProperty("http.proxyHost", "222.165.130.82");  
	//System.setProperty("http.proxyPort", "80");  
	
	
	return conn;
}

private static String getResponseCharset(String ctype) {
	String charset = DEFAULT_CHARSET;
	if (ctype != null) {
		String[] params = ctype.split(";");
		for (String param : params) {
			param = param.trim();
			if (param.startsWith("charset")) {
				String[] pair = param.split("=", 2);
				if (pair.length == 2) {
					if (pair[1] != null) {
						charset = pair[1].trim();
					}
				}
				break;
			}
		}
	}
	return charset;
}

private static String getStreamAsString(InputStream stream, String charset) throws IOException {
	try {
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset));
		StringWriter writer = new StringWriter();
		char[] chars = new char[256];
		int count = 0;
		while ((count = reader.read(chars)) > 0) {
			writer.write(chars, 0, count);
		}
		return writer.toString();
	} finally {
		if (stream != null) {
			stream.close();
		}
	}
}

%>>
