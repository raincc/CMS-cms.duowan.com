var sideLength = 6;
function getAllColorArray() {
	var allColorArray = [];
	allColorArray = new Array(sideLength * 3 + 2);
	// 计算第一列的数组
	var mixColorArray = [];
	var blackWhiteGradientArray = gradientColor(new RGB(0, 0, 0), new RGB(255,
			255, 255));
	for ( var i = 0; i < blackWhiteGradientArray.length; i++) {
		mixColorArray[i] = blackWhiteGradientArray[i];
	}
	var baseArray = [ new RGB(255, 0, 0), new RGB(0, 255, 0),
			new RGB(0, 0, 255), new RGB(255, 255, 0), new RGB(0, 255, 255),
			new RGB(255, 0, 255), new RGB(204, 255, 0), new RGB(153, 0, 255),
			new RGB(102, 255, 255), new RGB(51, 0, 0) ];
	mixColorArray = mixColorArray.concat(baseArray.slice(0, sideLength));
	allColorArray[0] = mixColorArray;

	// 计算第二列的数组
	var blackArray = new Array(sideLength * 2);
	for ( var i = 0; i < blackArray.length; i++) {
		blackArray[i] = new RGB(0, 0, 0);
	}
	allColorArray[1] = blackArray;

	// 计算六个区域的数组
	var cornerColorArray = new Array();// 六个元素，每个元素放6个区域的四角颜色二维数组
	cornerColorArray.push(generateBlockcornerColor(0),
			generateBlockcornerColor(51), generateBlockcornerColor(102),
			generateBlockcornerColor(153), generateBlockcornerColor(204),
			generateBlockcornerColor(255));
	var count = 0;
	var halfOfAllArray1 = [];
	var halfOfAllArray2 = [];
	for ( var i = 0; i < cornerColorArray.length; i++) {
		var startArray = gradientColor(cornerColorArray[i][0][0],
				cornerColorArray[i][0][1]);
		var endArray = gradientColor(cornerColorArray[i][1][0],
				cornerColorArray[i][1][1]);
		for ( var j = 0; j < sideLength; j++) {
			if (i < 3) {
				halfOfAllArray1[count] = gradientColor(startArray[j],
						endArray[j]);
			} else {
				halfOfAllArray2[count - sideLength * 3] = gradientColor(
						startArray[j], endArray[j]);

			}
			count++;
		}
	}
	for ( var i = 0; i < halfOfAllArray1.length; i++) {
		allColorArray[i + 2] = halfOfAllArray1[i].concat(halfOfAllArray2[i]);
	}

	// 将数组里所有的RGB颜色转换成Hex形式
	for ( var i = 0; i < allColorArray.length; i++) {
		for ( var j = 0; j < allColorArray[i].length; j++) {
			allColorArray[i][j] = RGBToHex(allColorArray[i][j]);
		}
	}
	return allColorArray;
}

// 创建小区域的四个角的颜色二维数组
function generateBlockcornerColor(r) {
	var a = new Array(2);
	a[0] = [ new RGB(r, 0, 0), new RGB(r, 255, 0) ];
	a[1] = [ new RGB(r, 0, 255), new RGB(r, 255, 255) ];
	return a;
}

// 两个颜色的渐变颜色数组
function gradientColor(startColor, endColor) {
	var gradientArray = [];
	gradientArray[0] = startColor;
	gradientArray[sideLength - 1] = endColor;
	var averageR = Math.round((endColor.r - startColor.r) / sideLength);
	var averageG = Math.round((endColor.g - startColor.g) / sideLength);
	var averageB = Math.round((endColor.b - startColor.b) / sideLength);
	for ( var i = 1; i < sideLength - 1; i++) {
		gradientArray[i] = new RGB(startColor.r + i * averageR, startColor.g
				+ i * averageG, startColor.b + i * averageB);
	}
	return gradientArray;
}
function RGBToHex(rgb) {
	var hex = "#";
	for (c in rgb) {
		var h = rgb[c].toString(16).toUpperCase();
		if (h.length == 1) {
			hex += "0" + h;
		} else {
			hex += h;
		}
	}
	return hex;
}
// RGB对象函数，用于创建一个RGB颜色对象
function RGB(r, g, b) {
	this.r = Math.max(Math.min(r, 255), 0);
	this.g = Math.max(Math.min(g, 255), 0);
	this.b = Math.max(Math.min(b, 255), 0);
}