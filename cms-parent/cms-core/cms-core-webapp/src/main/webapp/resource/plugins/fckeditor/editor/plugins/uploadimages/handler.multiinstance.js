


/* **********************
   Event Handlers
   These are my custom event handlers to make my
   web application behave the way I went when SWFUpload
   completes different tasks.  These aren't part of the SWFUpload
   package.  They are part of my application.  Without these none
   of the actions SWFUpload makes will show up in my application.
   ********************** */
function fileDialogStart() {
		if($('#isWatermark').attr('checked') == 'checked' || $('#isWatermark').attr('checked')){
			upload1.addPostParam('isWatermark' , $('#isWatermark').val() );
		}else{
			upload1.addPostParam('isWatermark' , '' );
		}
		upload1.addPostParam('watermarkPic' , $('#watermarkPic').val() );
		upload1.addPostParam('maskPosition' , $('#maskPosition').val() );
	/* I don't need to do anything here */
}

//在文件选择窗关闭后，文件排队发生错误时触发
function fileQueueError(file, errorCode, message) {
	try {
		if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
			alert("You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
			return;
		}

		switch (errorCode) {
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
			this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
			this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
			alert("You have selected too many files.  " +  (message > 1 ? "You may only add " +  message + " more files" : "You cannot add any more files."));
			break;
		default:
			if (file !== null) {
			}
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}
//在文件选择窗关闭后，文件排队上传的过程中触发
function fileDialogComplete(numFilesSelected, numFilesQueued) {
	try {
		/* I want auto start and I can do that here */
		this.startUpload();
	} catch (ex)  {
        this.debug(ex);
	}
}


//文件上传成功后触发的事件
function uploadSuccess(file, serverData) {
	//serverData: PicUrlOnCms + PicUrlOnLine() + FileName
	if(serverData.indexOf('[error]:') > -1){
		alert(serverData);
		return ;
	}
	try {
		var serverDataArr = serverData.split(',');
		var fileHtml = buildTable(serverDataArr , true);
		$('#showPic').append(fileHtml);
	} catch (ex) {
		this.debug(ex);
	}
}

function buildTable(serverDataArr , isInsert){
	var linkId = new Date().getMilliseconds();
	var fileHtml =  '<tr  align="center">'
		   + 	'<td style="width: 25px;" >';
	if(isInsert){
		fileHtml +=  '<input type="checkbox"  name="imgCheckbox" checked = "checked" value="'+serverDataArr[0]+'" />';
	}else{
		fileHtml +=  '<input type="checkbox"  name="imgCheckbox"  value="'+serverDataArr[0]+'" />';
	}
	fileHtml += '</td>'
		   + 	'<td style="width: 55px;">'
		   +	'<img class="showLittleImg" src="'+serverDataArr[0]
		   +   '" onmouseout="imgMouseout(this)" onmouseover="imgMouseover(this)" />'
		   + 	'</td>'
		   + 	'<td style="width: 150px;">'
		   +		'<textarea rows="2" cols="85" name="remark" >'+serverDataArr[1]+'</textarea>'
		   + 	'</td>'
		   + 	'<td style="width: 50px;">'
		   +		'<a name="copy" id  = "'+ linkId +'" href="" onmouseover= "javascript:copyLink(\''+serverDataArr[1]+'\'  , \''+linkId+'\')" >复制链接</a>'
		   + 	'</td>'		
		   + 	'<td style="width: 50px;">'
		   +		'<a name="delete" href="javascript:deleteFile(\''+articleId+'\' , \''+serverDataArr[2]+'\')">删除</a>'
		   + 	'</td>'	
        + '</tr>';
	return fileHtml;
}

function deleteFile(articleId , fileName){
	if(confirm('确定要删除此文件？只删除本地，不会删除外网的文件')){
		$.get(root+"/article/deletePic.do", {"articleId" : articleId , "fileName" : fileName }, function(data){
				location.reload();
		});
		
	}
}

var clip = null;  
function copyLink(link , linkId){
  clip = new ZeroClipboard.Client();
  clip.setHandCursor(true);
  clip.setText(link);
  clip.addEventListener('complete', function (client) {
	  $('#' + linkId).html('已复制');
  });
  clip.glue(linkId);
}

function imgMouseover(obj){
    var w = $(obj).width();
    var h = $(obj).height();
    var x = $(obj).position().left+w;
    var y = $(obj).position().top+h;
    $('#showBigImg > img').attr('src',obj.src);
    $("#showBigImg").css({'position':'absolute','left':x,'top':y});
    $('#showBigImg').show();
}

function imgMouseout(obj){
    var w = $(obj).width();
    var h = $(obj).height();
    var x = $(obj).position().left+w;
    var y = $(obj).position().top+h;
    
    $('#showBigImg > img').attr('src',this.src);
    $("#showBigImg").css({'position':'absolute','left':x,'top':y});				
	 $('#showBigImg').hide();
}


//文件上传队列全部上传成功触发的事件
function uploadComplete(file) {
	try {
		/*  I want the next upload to continue automatically so I'll call startUpload here */
		if (this.getStats().files_queued === 0) {
		} else {	
			this.startUpload();
		}
	} catch (ex) {
		this.debug(ex);
	}

}
//文件上传的过程中发生了错误触发的事件
function uploadError(file, errorCode, message) {
	try {
		switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
			this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.MISSING_UPLOAD_URL:
			this.debug("Error Code: No backend file, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
			this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.IO_ERROR:
			this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
			this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND:
			this.debug("Error Code: The file was not found, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
			this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			if (this.getStats().files_queued === 0) {
			}
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			break;
		default:
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}