<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>      
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
<title>文章-图片上传</title>
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all" />
<link rel="stylesheet" type="text/css" href="${ROOT}/resource/plugins/swfupload/default.css" media="all"  />
<!-- 引用剪切板依赖的JS -->	
<script type="text/javascript" src="${ROOT}/resource/plugins/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript" src="${ROOT}/resource/plugins/swfupload/swfupload.js"></script>
<script type="text/javascript" src="${ROOT}/resource/plugins/swfupload/swfupload.queue.js"></script>
<script type="text/javascript" src="${ROOT}/resource/plugins/swfupload/fileprogress.js"></script>
<script type="text/javascript" src="${ROOT}/resource/plugins/kind_editor/plugins/uploadimages/handler.multiinstance.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>   
<script type="text/javascript"  src="${ROOT}/resource/js/cookie.js"></script>   
<style type="text/css">
body { 
	font: normal 11px auto "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif; 
	color: #4f6b72; 
	background: #E6EAE9; 
} 

.showLittleImg {
	width: 50px;
	height: 50px;
	padding: 1px;
	border: 0px solid #dfdfdf;
}
.showBigImg {
	width: 200px;
	height: 150px;
	border: 0px solid #dfdfdf;
}
#showPic { 
	width: 750px; 
	padding: 0; 
	margin: 0; 
	
} 

th { 
	font: bold 11px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif; 
	color: #4f6b72; 
	border-right: 1px solid #C1DAD7; 
	border-bottom: 1px solid #C1DAD7; 
	border-top: 1px solid #C1DAD7; 
	letter-spacing: 2px; 
	text-transform: uppercase; 
	text-align: center; 
	padding: 6px 6px 6px 12px; 
	background: #CAE8EA  no-repeat; 
} 

td { 
	border-right: 1px solid #C1DAD7; 
	border-bottom: 1px solid #C1DAD7; 
	background: #fff; 
	font-size:11px; 
	padding: 6px 6px 6px 12px; 
	color: #4f6b72; 
} 
</style>

<script type="text/javascript">
//和clipboard_in_all_brower.html不在同一目录需设置setMoviePath
ZeroClipboard.setMoviePath('${ROOT}/resource/plugins/zeroclipboard/ZeroClipboard.swf');

var upload1;
var  channelId=parent.parent.channelId;
var  articleId=parent.parent.articleId;
var  root=parent.parent.root;
var imgStr = '';
var waterMaskPicInfoArr = [];
window.onload = function() {
	upload1 = new SWFUpload({
		// Backend Settings
		upload_url: root+"/article/uploadArticlePic.do",
		post_params: {  //附带的表单参数
			"channelId" : channelId,
			"articleId" : articleId,
			"JSESSIONID" : "<%=request.getSession().getId()%>"
		},
		// File Upload Settings
		file_size_limit : "102400",	// 100MB
		file_types : "*.*",
		file_types_description : "All Files",
		file_upload_limit : "100",
		file_queue_limit : "0",

		// Event Handler Settings (all my handlers are in the Handler.js file)
		file_dialog_start_handler : fileDialogStart,
		file_queue_error_handler : fileQueueError,//在文件选择窗关闭后，文件排队发生错误时触发
		file_dialog_complete_handler : fileDialogComplete,//在文件选择窗关闭后，文件排队上传的过程中触发
		upload_error_handler : uploadError,//文件上传的过程中发生了错误触发的事件
		upload_success_handler : uploadSuccess,//文件上传成功后触发的事件
		upload_complete_handler : uploadComplete,//文件上传队列全部上传成功触发的事件


		// Button settings
		button_image_url: root+"/resource/plugins/swfupload/images/button.png",
		button_width: "65",
		button_height: "29",
		button_placeholder_id: "spanButtonPlaceholder",
		button_text: '<span class="theFont">上传图片</span>',
		button_text_style: ".theFont { font-size: 13; }",
		button_text_left_padding: 3,
		button_text_top_padding: 6,
		
		// Flash Settings
		flash_url : root+"/resource/plugins/swfupload/swfupload.swf",

		custom_settings : {
		},
		// Debug Settings
		debug: false
	});
	
	$.get(root+"/article/getWaterMaskList.do", function(data){
		if(data != ''){
			$('#watermarkArea').show();
		}else{
			$('#noWatermarkTip').html('没有水印图片，请点击管理水印进行配置');
			$('#isWatermark').removeAttr('checked');
			return;
		}
		var waterMaskPicArr = data.toString().split(';waterMaskSeperator;');// data.toString()是为了兼容firefox
		for(var i = 0 ; i < waterMaskPicArr.length ; i++ ){
			if(waterMaskPicArr[i] == ''){
				continue;
			}
			var waterMaskPicInfo = waterMaskPicArr[i].split(';infoSeperator;');
			$('#watermarkPic').append('<option value="'+waterMaskPicInfo[0]+'">'+waterMaskPicInfo[0]+'</option>');
			waterMaskPicInfoArr.push({"name" : waterMaskPicInfo[0]  ,"width" : waterMaskPicInfo[1] , "height" : waterMaskPicInfo[2]});
			if(i == 0){
				$('#watermarkInfoTips').html('宽:' + waterMaskPicInfoArr[i].width + ',高:' + waterMaskPicInfoArr[i].height);
			}
		}
	});
	
	$.get(root+"/article/getUploadPicList.do", {"channelId" : channelId,"articleId" : articleId}, function(data){
		var uploadPicArr = data.toString().split(';fileSeperator;');
		for(var i = 0 ; i < uploadPicArr.length ; i++ ){
			if( uploadPicArr[i] == ''){
				continue;
			}
			var serverDataArr = uploadPicArr[i] .split(',');
			var fileHtml = buildTable(serverDataArr ,false);
			$('#showPic').append(fileHtml);
		}
	});
	
	var isWatermark = cookieUtil.getCookie(channelId + '_isWatermark');
	if(!isWatermark || isWatermark==''){
		$('#isWatermark').removeAttr('checked');
	}else{
		$('#isWatermark').attr('checked' ,'checked');
	}
}

function changeWatermarkPic(){
	var watermarkPicSelected = $('#watermarkPic').val();
	for(var i = 0 ; i < waterMaskPicInfoArr.length ; i++ ){
		if(waterMaskPicInfoArr[i].name == watermarkPicSelected){
			$('#watermarkInfoTips').html('宽:' + waterMaskPicInfoArr[i].width + ',高:' + waterMaskPicInfoArr[i].height);
		}
	}
	
}
//确定
function Ok(){

	//用于插入编辑器的代码
	$('input[type=checkbox][name=imgCheckbox]').each(function (){
		if($(this).attr('checked') =='checked'){
			imgStr += '<p style="text-align: center"><img src="' + $(this).val()
			+'"   alt="" /></p>';
		}
	});
	
	return imgStr;
}
//使所有checkbox处于选中状态
function selectAllCheckbox(obj){
	if($(obj).attr('checked') == 'checked' || $(obj).attr('checked')){
		$('input[type=checkbox][name=imgCheckbox]').attr('checked' , 'checked' );
	}else{
		$('input[type=checkbox][name=imgCheckbox]').removeAttr('checked' );
	}
}

//改变是否加水印的状态
function changeIsWatermark(){
	if($('#isWatermark').attr('checked') == 'checked' || $('#isWatermark').attr('checked')){
		cookieUtil.setCookie( channelId + '_isWatermark' , 'checked' );
	}else{
		cookieUtil.setCookie( channelId + '_isWatermark' , '' );
	}
}
</script>
</head>
<body>

		<!--文件管理 start-->
		<div class="doc-manage">
			<div class="action-bar" >
				<form id="form1" action="" method="post" enctype="multipart/form-data">
					<span class="search-bar">
						<span id="spanButtonPlaceholder"></span>
						&nbsp;	&nbsp;	&nbsp;
						<span id="noWatermarkTip"></span>
						<span id="watermarkArea"  style="display: none;">
							<input id="isWatermark"  name="isWatermark"  type="checkbox"  onclick="changeIsWatermark()"    />	加水印 
							<select id="watermarkPic" name="watermarkPic"  onchange="changeWatermarkPic()" >
							</select>
							<span id="watermarkInfoTips" ></span>
						&nbsp;	&nbsp;	&nbsp;
											水印位置：
							<select name="maskPosition"  id="maskPosition">
								<option value="1">顶部左</option>
								<option value="2">顶部中</option>
								<option value="3">顶部右</option>
								<option value="4">中部左</option>
								<option value="5">正中央</option>
								<option value="6">中部右</option>
								<option value="7">底部左</option>
								<option value="8">底部中</option>
								<option value="9" selected="selected">底部右</option>
							</select>
					</span>
							&nbsp;	&nbsp;	&nbsp;
						<a href="/file/fileManage.do?toDirPath=/mask/" target="_blank">管理水印图</a>						
					</span>
					</form>	
				</div>
		</div>
		<ul class="file-list"  id="fileList">

		</ul>

		<table id="showPic" border="1"  >
			<tr > 
				<th align="center"><input type="checkbox" id="selectAll" onclick="selectAllCheckbox(this)" /></th> 
				<th >预览</th> 
				<th >外网地址</th> 
				<th nowrap="nowrap">操作1</th> 
				<th nowrap="nowrap">操作2</th> 
			</tr> 
		</table>		
	</div>	
	<!-- 显示大图的Div -->
	<div id="showBigImg" style="padding:0;margin:0;display: none;">
		<img class="showBigImg" src="" />
	</div>	


</body>
</html>