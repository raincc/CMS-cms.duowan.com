 var postJspUrl = '/upload/img/post_article_img.jsp'; //图片处理后台url
 var ajaxUrl = '/upload/img/articleImgAjax.jsp';//ajax处理url
 
  //初始化
  function init(){
  	
  	var obj= document.getElementById("add_flag")
    obj.checked =getCookie("defaultmask")==null?false:true;
    uploadJsHelper.show_pic();
    $('#showBigImg').hide();//隐藏大图预览层
    
  }

function imgMouseover(obj){
         
	     var w = $(obj).width();
	     var h = $(obj).height();
	     var x = $(obj).position().left+w;
	     var y = $(obj).position().top+h;
	     //alert("imgMouseover"+x+","+y+","+h);
	     $('#showBigImg > img').attr('src',obj.src);
	     $("#showBigImg").css({'position':'absolute','left':x,'top':y});
	     $('#showBigImg').show();
}

function imgMouseout(obj){
	     var w = $(obj).width();
	     var h = $(obj).height();
	     var x = $(obj).position().left+w;
	     var y = $(obj).position().top+h;
	     
	     $('#showBigImg > img').attr('src',this.src);
	     $("#showBigImg").css({'position':'absolute','left':x,'top':y});				
		 $('#showBigImg').hide();
}
function showImgSize(obj){

	var state_text=obj.parentNode;
	var _img=new Image();
	_img.src=obj.src;
	state_text.innerHTML="<img class='showLittleImg' src='"+obj.src+"' onmouseout='imgMouseout(this)' onmouseover='imgMouseover(this)'/><br/>"+_img.width+" x "+_img.height;
	
}

//确定
function Ok(){
	var showPic = $('#showPic');
	var _imgArr = showPic.find('img');
	var isBuildTuxiu = $('#buildTuxiu').attr('checked');//是否生成图秀 
	
	if(_imgArr && _imgArr.length>0){
		var str = '';
		$(_imgArr).each(function(){
			var imgObj = $(this);
			var checkObj = imgObj.parent().parent().find('input[type=checkbox][name=_img_id]');
			var remarkObj = imgObj.parent().parent().find('textarea[name=remark]');
			
			var isCheck = checkObj.attr('checked');//是否勾选
			var remark = remarkObj.val();//图片描述
			
			if(isCheck){
				var imgUrl = imgObj.attr('src');
				if(isBuildTuxiu){
					str  +=  '<li><span>'+imgUrl+'</span><em>'+remark+'</em></li>';
				}else{
					str+="<p style=\"text-align:center;\"><img src=\""+imgUrl+"\" alt=\"\" name=\"_img\" /></p>";
				}
			}
		});
		
		if(''!=str){
			oEditor.FCKUndo.SaveUndoStep() ;
			oEditor.FCK.InsertHtml(str);			
		}
	}
	
	/*
	var picInfoForm = $('#pic_info_form');
	//alert(picInfoForm.serialize())
	var numberOfPic = picInfoForm.find("input[type=hidden][name=img_id]").length;//图片数量
	if(numberOfPic>0){
		var info = 'error';
		var dataStr = "&optType=postPicInfo" +"&channelid="+channelid +"&articleid="+articleid +'&'+picInfoForm.serialize();
		//alert(dataStr)
		$.ajax({
			type: "POST",
		    url: ajaxUrl,
			data: dataStr,
		    async: false,
		    cache: false,
		    dataType: "text",			
		    success: function(data){
		    		data = $.trim(data);
					if('error'!=data) {
						info = data;
					}
		    }
		});
		if('error'==info){
			alert('保存失败');
		}
		
	}
	* */
	
	return true;
}

//上传
function imgSubmit(){
	if(document.form1.img_file.value=="") 
    {
	    alert("压缩文件不能为空");
	    return;
    }

	var _str=document.form1.img_file.value;
	_str=_str.substr(_str.length-3).toLowerCase();
	if(_str=="zip" || _str=="rar"){//判断文件后缀

     var flag = $$("add_flag")
        
     var radios = document.getElementsByName("operator_flag");
     var oValue;
        
        for(var i=0;i<radios.length;i++){
             if(radios[i].checked)
             {
                oValue = radios[i].value; 
             }
        }

        if(oValue==2){
        	if(isNullValue("upload_mask")){
                alert("水印图不能为空");
                return;   
              }
         }
        
		$$("upload_btn").disabled=true;
        
		document.form1.action = postJspUrl+ "?channelid="+channelid+"&articleid="+articleid;
		document.form1.submit();
	}else{
	  alert("只上传压缩包，格式限制为：ZIP或RAR。");
	}
}
	
	//只能输入整数
	var oldSort;
	function onlyNum(obj) { 
		if(!(event.keyCode==46)&&!(event.keyCode==8)&&!(event.keyCode==37)&&!(event.keyCode==39)  && !((event.keyCode>=48&&event.keyCode<=57)||(event.keyCode>=96&&event.keyCode<=105)) ) {
			event.returnValue=false; 
		}else{
			oldSort = obj.value;
		}
	}	
	
	//检查图片备注是否超长
	function isLengthOk(obj,MAX){
		obj = $(obj);
		
		var text = obj.val();
		var textLen = getStrLen(text);
		
		
		return textLen <= MAX;
	}
	
	/**
	 * 获取字符长度，英文为1，中文为2
	 * @param str,字符
	 */	
	function getStrLen(str) {
	    var len = 0;   
	    for (var i=0; i<str.length; i++) {   
	        if (str.charCodeAt(i)>127 || str.charCodeAt(i)==94) {   
	            len += 2;   
	        } else {   
	            len ++;   
	        }   
	    }   
	    return len;   
	}	

	//图片上传帮助类 开始
	var uploadJsHelper = {
		/**
		 * 展示图片
		 * @param picUrlStr,图片url串
		 */
		show_pic : function (picUrlStr){
			var table = $('#showPic');
			table.html('');
			var picJsonStr = this.getArticlePicInfo();//获取当前的图片信息，|号隔开
			
			if(''!=picJsonStr){
				picJsonStr = $.trim(picJsonStr);
				eval("var picJsonArr = "+picJsonStr);
				if(picJsonArr){
					var len  = picJsonArr.length;
					for(var i=0;i<len;i++){
						var picJson = picJsonArr[i];
						var html = this.bulidPicShowTable(picJson);//生成图片表格
						table.append(html);
					}
					//this.changeLittleImgEvent();//修改小图的响应事件
				}
				
			}
			
		},
		
		/**
		 * 修改小图的响应事件
		 */
		changeLittleImgEvent : function(){
			$('.showLittleImg').each(function(){
				var img = $(this);
				img.mouseover(function (){
				     var x = $(this).position().left;
				     var h = $(this).height();
				     var y = $(this).position().top+h;
				    
				     $('#showBigImg > img').attr('src',this.src);
				     $("#showBigImg").css({'position':'absolute','left':x,'top':y});
				     $('#showBigImg').show();
				});
				img.mouseout(function (){
				     var x = $(this).position().left;
				     var h = $(this).height();
				     var y = $(this).position().top+h;
				     
				     $('#showBigImg > img').attr('src',this.src);
				     $("#showBigImg").css({'position':'absolute','left':x,'top':y});				
					 $('#showBigImg').hide();
				});			
			});
		},	
		
		/**
		 * 生成图片表格
		 * @param url,图片url
		 */
		bulidPicShowTable : function bulidPicShowTable(picJson){
			var html  = '';
			
			var img_id = picJson.img_id;//图片id
			var imgUrl = picJson.imgUrl;//文章url
			var sort = picJson.sort;//排序
			var remark = picJson.remark;//备注
			var imgUrl = picJson.imgUrl;//文章url	
			
			
			//var fileName = this.getPicFileName(imgUrl);
			//+'" onload="showImgSize(this)"/>'
			html +=  '<tr>'
			
					   + 	'<td style="width: 10px;">'
					   +    	'<input type="checkbox" name="_img_id" value="'+img_id+'"/>'
					   +    	'<input type="hidden" name="img_id" value="'+img_id+'"/>'
					   + 	'</td>'
					   
					   + 	'<td style="width: 55px;">'
					   +		'<img class="showLittleImg" src="'+imgUrl+'" onload="showImgSize(this)"/>'
					   + 	'</td>'
					   
					   + 	'<td style="width: 150px;">'
					   +		'<textarea rows="2" cols="85" name="remark" onchange="uploadJsHelper.updateRemark(this)" >'+remark+'</textarea>'
					   + 	'</td>'
					   
					   //+ 	'<td style="width: 15px;">'
					  // +		'<input type="text" name="sort" value="'+(sort)+'" size="2" onkeydown="onlyNum(this);" onblur="uploadJsHelper.updateSort(this)" style="ime-mode:Disabled" />'
					  // + 	'</td>'				   
					   
					   + 	'<td style="width: 30px;">'
					   +		'<a name="delete" href="javascript:void(0)" onclick="uploadJsHelper.deleteImg(this)">删除</a>'
					   + 	'</td>'	
					   
					   + 	'<td style="width: 50px;">'
					   +		'<a name="copy" href="javascript:void(0)" onclick="uploadJsHelper.copy(this)" >复制链接</a>'
					   + 	'</td>'					   		   
					   	   			   			   
			           + '</tr>';
			return html;        
		},
		
		/**
		 * 根据图片url，获取图片名
		 */
		getPicFileName :function (url){
			var fileName = '';
			var lastIndexOfSlash = url.lastIndexOf('/');//最后一个斜杠
			//var lastIndexOfFullStop = url.lastIndexOf('.');//最后一个句号
			fileName = url.substring(lastIndexOfSlash+1,url.length);
			return fileName
		},
		
		//获取当前的图片信息，|号隔开
		getArticlePicInfo : function (){
			var info = '';
			$.ajax({
			   type: "GET",
			   url: ajaxUrl,
			   data : "optType=articleImgAjax" +"&channelid="+channelid +"&articleid="+articleid,
			   async: false,
			   cache: false,
			   dataType: "text",
			   success: function(data){
			   			data = $.trim(data);
			   			
						if(-1 != data.indexOf("error")) {
							alert('连接成功,但后台出错,请联系系统管理员');
						}else if(data.length>0){
							info = data;
						}
			   }
			});		
			return info;				
		},
		
		//拷贝图片url
		copyPicUrl : function(text){
			clipboardData.setData('Text',text);
			//alert('复制图片链接成功!');
		},
		
		//复制链接
		copy : function(obj){
			obj = $(obj);
			var img = obj.parent().parent().find(".showLittleImg");
			var imgUrl = img.attr('src');
			uploadJsHelper.copyPicUrl(imgUrl);
		},
		
		//删除图片
		deleteImg : function(obj){
			var isTrue=confirm("确定删除该图片？");	
			if(!isTrue){
				return;
			}	
			
			var info = 'error';
			
			obj = $(obj);
			var img_id = obj.parent().parent().find("input[type=hidden][name=img_id]").val();
			
			$.ajax({
			   type: "GET",
			   url: ajaxUrl,
			   data : "optType=deleteImg" +"&img_id="+img_id  +"&channelid="+channelid +"&articleid="+articleid,
			   async: false,
			   cache: false,
			   dataType: "text",
			   success: function(data){
			   			data = $.trim(data);
						if('ok'==data) {
							info = '';
							
							obj.parent().parent().remove();
						}
			   }
			});	
			if('error'==info){
				alert('删除失败');
			}else{
				this.resort();//重新编号
				alert('删除成功');
			}
		},
		
		//更新图片备注
		updateRemark : function(obj){
			obj = $(obj);
			
			var MAX = 250;
			var isLengOk = isLengthOk(obj,MAX);//长度是否合法
			if(!isLengOk){
				alert('您输入内容过多，字节数不能超过：'+MAX);
				obj.focus();
				return ;			
			}
			
			var remark = obj.val();
			
			var img_id = obj.parent().parent().find("input[type=hidden][name=img_id]").val();
			
			var dataStr = "&remark="+remark +"&img_id="+img_id;
			
			$.ajax({
			   type: "POST",
			   url: ajaxUrl,
			   data : "optType=updateRemark" +"&channelid="+channelid +"&articleid="+articleid +dataStr,
			   dataType: "text",
			    error : function(){
			    	alert('更新图片备注出错')
			    }
			});	
		},
		
		//更新序号
		updateSort : function(obj){
			obj = $(obj);
			var img_id = obj.parent().parent().find("input[type=hidden][name=img_id]").val(); 
			var sort = obj.val();
			//alert(oldSort+'|'+sort)
			
			if(''==sort){
				obj.val('1');
				sort = obj.val();
			}
			
			var showPicObj = $('#showPic');//图片区
			var trArr = showPicObj.find('tr');
			var trLen = trArr.length;
			
			for(var i=0;i<trLen;i++){
				var trObj = $(trArr[i]);
				
				var sortObj = trObj.find('input[type=text][name=sort]');
				var img_idTemp = sortObj.parent().parent().find("input[type=hidden][name=img_id]").val();
				var sortObjV = sortObj.val();
				
				//alert(img_id+'|'+img_idTemp);
				//alert(oldSort+'|'+sortObjV);
				
				if(img_id!=img_idTemp && sortObjV == sort){
					sortObj.val(oldSort);
				}
			}
			
			this.resort();//重新编号
			
		},
		
		//重编序号
		resort : function(){
			var showPicObj = $('#showPic');//图片区
			var trArr = showPicObj.find('tr');
			var trLen = trArr.length;			
			
			//排序
			trArr.sort(function compare(a,b){
				var a_tr = $(a);
				var b_tr = $(b);
				
				var a_sort = a_tr.find('input[type=text][name=sort]');
				var b_sort = b_tr.find('input[type=text][name=sort]');
				
				var a_sortV = parseInt(a_sort.val());
				var b_sortV = parseInt(b_sort.val());
				
 				return a_sortV -  b_sortV;
			});
			
			
			showPicObj.html('');
			
			var updateData = '';
			for(var i=0;i<trLen;i++){
				var trObj = $(trArr[i]);
				var sortObj = trObj.find('input[type=text][name=sort]');
				var img_idTemp = sortObj.parent().parent().find("input[type=hidden][name=img_id]").val();
				
				sortObj.val(i+1);
				showPicObj.append(trObj);
				
				updateData += '&img_id='+img_idTemp +"&sort="+sortObj.val();
			}
			
			var dataStr = "&optType=updateSort" +"&channelid="+channelid +"&articleid="+articleid  +updateData;
			//alert(dataStr)
			$.ajax({
				type: "POST",
			    url: ajaxUrl,
				data: dataStr,
			    dataType: "text",
			    error : function(){
			    	alert('更新图片序号出错')
			    }
			});		
		},
		
		//全选
		checkAll : function(obj){
			obj = $(obj);
			var isChecked = obj.attr('checked');
			var imgIdArr = $('#showPic  input[type=checkbox][name=_img_id]');
			$(imgIdArr).each(function(){
				$(this).attr('checked',isChecked);
			});
		}
		
	}
	//图片上传帮助类 结束

//
uploadImagCheck = function(){
	 this.AllowZipExt=".zip,.rar";
	 this.AllowExt=".jpg,.jpeg,.gif,.png";
	 this.ImgObj=new Image(); 
	 this.FileExt=""; 
	 this.ErrMsg="";
}

  uploadImagCheck.prototype.CheckExt = function(obj){
	   this.ErrMsg=""; 
	   this.ImgObj.src=obj.value;
		if(obj.value=="") 
		{ 
		   this.ErrMsg="\n请选择一个文件";    
		} 
		else 
		{   
		  this.FileExt=obj.value.substr(obj.value.lastIndexOf(".")).toLowerCase(); 
		  if(this.AllowExt!='' && this.AllowExt.indexOf(this.FileExt)==-1)//判断文件类型是否允许上传 
		  { 
		   obj.value = ""; 
		   this.ErrMsg="\n文件类型非法,请上传 "+this.AllowExt+" 类型的文件,当前文件类型为"+this.FileExt;   
		  } 
		 }
		 if(this.ErrMsg!="") 
		 { 
		  this.ShowMsg(this.ErrMsg);
		    $$("upload_btn").disabled=true;
		  return false; 
		 }
		   $$("upload_btn").disabled=false;
		 return true;
	}
	
	
	uploadImagCheck.prototype.CheckZipExt = function(obj){
	   this.ErrMsg=""; 
	   var ext=obj.value;
		 if(ext=="") 
		 { 
		   this.ErrMsg="\n请选择一个文件";    
		 } 
		else 
		{   
		  this.FileExt=obj.value.substr(obj.value.lastIndexOf(".")).toLowerCase(); 
		  if(this.AllowZipExt!='' && this.AllowZipExt.indexOf(this.FileExt)==-1)//判断文件类型是否允许上传 
		  { 
		   obj.value = ""; 
		   this.ErrMsg="\n文件类型非法,请上传 "+this.AllowZipExt+" 类型的文件,当前文件类型为"+this.FileExt;
		  } 
		 }
		 if(this.ErrMsg!="") 
		 { 
		  this.ShowMsg(this.ErrMsg);
		    $$("upload_btn").disabled=true;
		  return false; 
		 }
		   $$("upload_btn").disabled=false;
		 return true;
	}
	
	uploadImagCheck.prototype.ShowMsg = function(message){
	   alert(message);  
	}

	function $$(obj){
	    return document.getElementById(obj);
	 }
	
	function checkImag(obj) 
	{ 
	 var image=new uploadImagCheck(); 
	 image.CheckExt(obj); 
	}
	
	function checkZip(obj){
	  	 var image=new uploadImagCheck(); 
	     image.CheckZipExt(obj);  	
	}
	 

    //检查
	 function checkChoose(obj){
       if(obj.checked){
       	   addCookie();
    	    $$("mask_bar").style.display="block";
    	    $$("mask_location").style.display="block";
       }else{
       	  delCookie('defaultmask');
    	   $$("mask_bar").style.display="none";
    	   $$("mask_location").style.display="none";
       }
	 }

	function showMask(obj){
	  if(obj.checked){
		   $$("mask_src").style.display="block";;
		   $$("mask_location").style.display="block";
		   $$("upload_btn").disabled=false;
	  }
	}
	
	function hiddenMask(obj){
		 if(obj.checked){
	   	   $$("mask_src").style.display="none";;
	    }
	 }

	function check_mask(state){
		$$("message").innerText=state;
    }

	function isNullValue(obj){
	      return $$(obj).value==null||$$(obj).value==""?true:false;
	    }
	    
	    
  //设置cookie   
  function setCookie(name,value) {   
    var Days = 30; //此 cookie 将被保存 30 天   
    var exp  = new Date();  //new Date("December 31, 9998");   
    exp.setTime(exp.getTime() + Days*24*60*60*1000);   
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();   
  }  
	 
  //取cookies函数    
  function getCookie(name){   
    var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));   
    if(arr != null) return unescape(arr[2]);   
        
    return null;   
  }   
  
  //删除cookie   
  function delCookie(name){   
    var exp = new Date();   
    exp.setTime(exp.getTime() - 1);   
    var cval=getCookie(name);   
    if(cval!=null) document.cookie= name + "="+cval+";expires="+exp.toGMTString();   
  }
  
  //添加cookie
  function addCookie(){
  	setCookie("defaultmask","true");
  }  
  
