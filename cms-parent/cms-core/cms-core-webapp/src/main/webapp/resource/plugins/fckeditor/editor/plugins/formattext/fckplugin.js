function $myformat(){
	//var re=/<ol.*?>|<\/ol>|<ul.*?>|<\/ul>|　|<embed.*?<\/embed>|<center.*?>|<\/center>|&nbsp;|<!--.*?-->|<input[\s\S]*?>|<textarea[\s\S]*?<\/textarea>|<style[\s\S]*?<\/style>|<font[\s\S]*?>|<\/font>|<table[\s\S]*?>|<\/table>|<tr[\s\S]*?>|<\/tr>|<tbody[\s\S]*?>|<\/tbody>|<object[\s\S]*?<\/object>|<form[\s\S]*?<\/form>|<iframe[\s\S]*?>[\s\S]*?<\/iframe>|<script[\s\S]*?<\/script>/img;
	var re=/<ol.*?>|<\/ol>|<ul.*?>|<\/ul>|　|<center.*?>|<\/center>|&nbsp;|<!--.*?-->|<input[\s\S]*?>|<textarea[\s\S]*?<\/textarea>|<style[\s\S]*?<\/style>|<font[\s\S]*?>|<\/font>|<table[\s\S]*?>|<\/table>|<tr[\s\S]*?>|<\/tr>|<tbody[\s\S]*?>|<\/tbody>|<object[\s\S]*?<\/object>|<form[\s\S]*?<\/form>|<iframe[\s\S]*?>[\s\S]*?<\/iframe>|<script[\s\S]*?<\/script>/img;
	var _obj=FCK.EditorDocument.body;
	$doc=_obj.innerHTML;
	$doc=$doc.replace(re,"");
	$doc=$doc.replace(/<td.*?>|<th.*?>|<div.*?>|<h[1-7].*?>|<li.*?>|<dt.*?>|<dd.*?>/ig,"<p>");
	$doc=$doc.replace(/<br[\s\r\n]*?[\/]?>|<\/td>|<\/th>|<\/div>|<\/h[1-7]>|<\/li>|<\/dt>|<\/dd>/ig,"</p>");
	_obj.innerHTML=$doc;
	$doc=FCK.GetXHTML(true);
	re=/\s(on\w+)[\s\r\n]*=[\s\r\n]*?('|")([\s\S]*?)\2/img;
	$doc=$doc.replace(re,"");
	re=/\s(class|vspace|id|align|style|border|hspace)[\s\r\n]*=[\s\r\n]*?('|")([\s\S]*?)\2/img;
	$doc=$doc.replace(re,"");
	$doc=$doc.replace(/<a.*?>(<img.*?>)<\/a>/ig,"$1");
	$doc=$doc.replace(/<(\w+)( \w+)*?>(\s|&nbsp;)*?<\/\1>/ig,"");
	_obj.innerHTML=$doc;
	_p=_obj.getElementsByTagName("p");
	re=/<img.*?>/i;
	for(var i=0;i<_p.length;i++){
		if(re.test(_p[i].innerHTML)){
			_p[i].style.textAlign="center";
		}else{
			_p[i].innerHTML="　　"+_p[i].innerHTML;
		}
	}
}


var $formattext=function(){};
//$formattext.prototype.Execute=function(){};
$formattext.GetState=function(){return FCK_TRISTATE_OFF;};
$formattext.Execute=function(){
	$myformat();
};
FCKCommands.RegisterCommand('FormatText',$formattext);
var $btn=new FCKToolbarButton('FormatText', '格式化文档');
$btn.IconPath=FCKConfig.PluginsPath + 'formattext/1.gif';




FCKToolbarItems.RegisterItem("FormatText",$btn);