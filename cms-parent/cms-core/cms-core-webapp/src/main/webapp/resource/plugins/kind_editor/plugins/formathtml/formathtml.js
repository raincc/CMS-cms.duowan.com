KindEditor.plugin('formathtml', function(K) {
        var editor = this, name = 'formathtml';
        // 点击图标时执行
        editor.clickToolbar(name, function() {
        	var editorContent = editor.html();
        	var re=/<ol.*?>|<\/ol>|<ul.*?>|<\/ul>|　|<center.*?>|<\/center>|&nbsp;|<!--.*?-->|<input[\s\S]*?>|<textarea[\s\S]*?<\/textarea>|<style[\s\S]*?<\/style>|<font[\s\S]*?>|<\/font>|<table[\s\S]*?>|<\/table>|<tr[\s\S]*?>|<\/tr>|<tbody[\s\S]*?>|<\/tbody>|<object[\s\S]*?<\/object>|<form[\s\S]*?<\/form>|<iframe[\s\S]*?>[\s\S]*?<\/iframe>|<script[\s\S]*?<\/script>/img;
        	editorContent=editorContent.replace(re,"");
        	editorContent=editorContent.replace(/<td.*?>|<th.*?>|<div.*?>|<h[1-7].*?>|<li.*?>|<dt.*?>|<dd.*?>|<span.*?>/ig,"<p>");
        	editorContent=editorContent.replace(/<\/td>|<\/th>|<\/div>|<\/h[1-7]>|<\/li>|<\/dt>|<\/dd>|<\/span>/ig,"</p>");
        	editorContent=editorContent.replace(/<br[\s\r\n]*?[\/]?>/ig,"</p><p>");
        	
        	//re = /<\/p>/img;
        	//var resultArr = re.exec(editorContent)
        	var resultArr = editorContent.split('</p>');
        	
        	for(var i = 0 ; i < resultArr.length;i++ ){
        		if(resultArr[i].replace(/(^\s*)|(\s*$)/g, '') != '' && resultArr[i].indexOf('<p>') == -1){
        			editorContent = editorContent.replace( resultArr[i] , '<p>' + resultArr[i] );
        		}
        	}
        	re=/\s(on\w+)[\s\r\n]*=[\s\r\n]*?('|")([\s\S]*?)\2/img;
        	editorContent=editorContent.replace(re,"");
        	re=/\s(class|vspace|id|align|style|border|hspace)[\s\r\n]*=[\s\r\n]*?('|")([\s\S]*?)\2/img;
        	editorContent=editorContent.replace(re,"");
        	editorContent=editorContent.replace(/<a.*?>(<img.*?>)<\/a>/ig,"$1");
        	editorContent=editorContent.replace(/<(\w+)( \w+)*?>(\s|&nbsp;)*?<\/\1>/ig,"");
        	re=/<img.*?>/i;
        	var oDiv = document.createElement('DIV'); 
        	oDiv.innerHTML=editorContent;
        	var _p = oDiv.getElementsByTagName("p");
        	for(var i=0;i<_p.length;i++){
        		_p[i].innerHTML = _p[i].innerHTML.replace(/\r/ig , '');
        		if(re.test(_p[i].innerHTML)){
        			_p[i].style.textAlign="center";
        			_p[i].innerHTML = _p[i].innerHTML.replace(/(<img.*)(width=\"\d*\")(.*?>)/g,'$1width=\"\"$3');//图片的宽度属性格式化之后自动清空
        		}else{
        			//_p[i].style.textIndent="2em";
        		//	_p[i].innerHTML="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+_p[i].innerHTML;
        			_p[i].innerHTML="　　　　"+_p[i].innerHTML;
        		}
        	}
        	re=/(<p>)[\s\r\n]*(&nbsp;)*[\s\r\n]*(<b><\/b>)*(<\/p>)/img;//去掉<p>&nbsp;</p>形式的空行
        	editorContent=oDiv.innerHTML.replace(re, "");
        	editorContent=editorContent.replace(/(<\/p>)/img , '</p>\n');
        	editorContent=editorContent.replace(/(<p>)([\s\r\n])*(\u3000)*((<b>)*([\s\r\n])*(<\/b>)*)*(<\/p>)/img , '');//去掉<p><b></b></p>形式的空行
        	editor.html(editorContent);
        	
        });
});