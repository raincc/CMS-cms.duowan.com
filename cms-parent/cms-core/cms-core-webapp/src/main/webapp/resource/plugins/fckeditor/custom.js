FCKConfig.ToolbarSets["duowan"] = [
	['Source','-','Templates'],
	['Find','Replace'],
	['Bold','Italic','Underline'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
	['Link','Unlink','Anchor'],
	//['Image','Flash','PageBreak','-','RemoveFormat','FormatText','-','InsertImages','InsertImg'],
	['Image','Flash','PageBreak','-','RemoveFormat','FormatText','-','uploadimages'],//modify by tiger ,'uploadimages'
	'/',
	['FontName','FontSize'],
	['TextColor','BGColor'],
	['Table','-','TableInsertRowAfter','TableDeleteRows','TableInsertColumnAfter','TableDeleteColumns','TableInsertCellAfter','TableDeleteCells','TableMergeCells','TableHorizontalSplitCell','TableCellProp'],
	['FitWindow','ShowBlocks']		// No comma for the last row.
] ;

var sOtherPluginPath = FCKConfig.BasePath.substr(0, FCKConfig.BasePath.length - 7) + 'editor/plugins/' ;
FCKConfig.Plugins.Add('tablecommands',null,sOtherPluginPath);
FCKConfig.Plugins.Add('formattext',null,sOtherPluginPath);
FCKConfig.Plugins.Add('insertimages',null,sOtherPluginPath);
FCKConfig.Plugins.Add('insertimg',null,sOtherPluginPath);
FCKConfig.Plugins.Add('uploadimages',null,sOtherPluginPath);//add by tiger