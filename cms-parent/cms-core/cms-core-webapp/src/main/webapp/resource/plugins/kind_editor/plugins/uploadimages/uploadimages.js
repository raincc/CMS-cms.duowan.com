KindEditor.plugin('uploadimages', function(K) {
        var editor = this, name = 'uploadimages';
        // 点击图标时执行
        editor.clickToolbar(name, function() {
        	//
    		var dialog = editor.createDialog({
    			name : name,
    			width : 800,
    			height:600,
    			title : '批量上传',
    			body : '<iframe class="ke-textarea" id = "uploadImagesIframe"  frameborder="0" src="'+
    						root+'/resource/plugins/kind_editor/plugins/uploadimages/article_picture_upload.jsp'
    						+'" style="width:800px;height:600px;background-color:#FFF;"></iframe>',
    			yesBtn : {
    				name : '确定',
    				click : function(e) {
    					var imgStr = document.getElementById("uploadImagesIframe").contentWindow.Ok();
    					editor.insertHtml(imgStr).hideDialog().focus();
    				}
    			}
    		});
        });
});