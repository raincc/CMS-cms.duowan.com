//标题字数统计：中文算1，数字与英文算0.5，最多能提交50个。（即是最多能提交50个中文字符或100个英文字符）
//修改该方案需要改三个地方:count(),checkTitle(),validateFormCommonPart()
function count(that) {
	var txt = that.value;//.replace(/\s*/g, '')
	txt = txt.replace(/([\u0391-\uFFE5])/ig,'11');//中文字符算两个字节
	var count = txt.length;
	document.getElementById('title_check').innerHTML = '';
	document.getElementById(that.name+'_count').innerHTML = '<b>' + count/2 + ' /50</b>';
}
//进行统计
count(document.getElementById('title'));


/**
 * 检查标题
 */
function checkTitle(){
	isFocusTitleFlag = false;//将"标题焦点标记"设为false;只有触发id=title的onfocus事件才设为true;	
	var title=document.getElementById('title').value;
	if(containIllegalCharacter(title)){
		document.getElementById('title_check').innerHTML="标题不允许出现 , . * \" ; : ' _和空格";
		return ;
	}
    
    if(title==''){
      document.getElementById('title_check').innerHTML= '请输入标题';
      return;
    }
    var title2 = title;
    title2 = title2.replace(/([\u0391-\uFFE5])/ig,'11');//中文字符算两个字节
    if(title2.length/2 > 50){
    	document.getElementById('title_count').innerHTML = '<b>' +title.length/2 + ' /50</b>';
    	return ;
    }
    
	$.ajax({
	   type: 'GET',
	   url: '/article/checkTitle.do?title=' + title+'&channelId=' + channelId+'&articleId='+articleId,
	   async: false,
	   cache: false,
	   dataType: 'text',
	   success: function(data){
	         if(-1 != data.indexOf('exist')){
	            document.getElementById('title_check').innerHTML='标题重复';
	            isLegalTitle = false;
	         }else{
	        	 isLegalTitle = true;
	         }
	   }
	});
}

//校验表单
function validateFormCommonPart(){
	var title = $('#title').val();//.replace(/\s*/g, '');
	if(title == ''){
		alert('标题为空');
		return false;
	}
	if(containIllegalCharacter(title)){
		alert("标题不允许出现 , . * \" ; : ' _和空格");
		return false;
	}
	//title = title.replace(/\s*/g, '');
	title = title.replace(/([\u0391-\uFFE5])/ig,'11');//中文字符算两个字节
    if(title.length/2 > 50){
    	alert('标题字数超过上限');
    	return false;
    }
    if(!isLegalTitle){
    	alert('标题重复');
    	return false;
    }
	var subtitle = $('#subtitle').val();
    if(subtitle.length  > 100){
    	alert('副标题字数超过上限100');
    	return false;
    }
	var source = $('#source').val();
    if(source &&source.length  > 100){
    	alert('来源字数超过上限100');
    	return false;
    }
	var author = $('#author').val();
    if(author && author.length  > 30){
    	alert('作者字数超过上限30');
    	return false;
    }
	var digest = $('#digest').val();
    if( digest && digest.length  > 250 && channelId != 'newgame'){
    	alert('摘要字数超过上限250');
    	return false;
    }
	var power = $('#power').val();
	if(power){
		try {
			power = parseInt(power);
		    if(isNaN(power) || power < 0 ||  power > 255){
		    	alert('请输入0-255之间的数字');
		    	return false;
		    }
		} catch (e) {
			alert('请输入0-255之间的数字');
		}
	}
	return true;
}

function containIllegalCharacter(title){
	//title = title.trim();没有trim()方法
//	if(title.indexOf(',') != -1 || title.indexOf('*') != -1 ||  title.indexOf('"') != -1 
//			|| title.indexOf(';') != -1 || title.indexOf('\'') != -1  || title.indexOf('_') != -1){
//		return true;
//	}
	return false;
	
}