(function( window, undefined ) {
	window.cookieUtil = {
			getCookie : function (cookieName) {
				var cookies=decodeURI(document.cookie).split(";");//一个分号加一个空格
				if (!cookies.length) {return "";}
				for (var i=0;i< cookies.length;i++) {
					var pair = cookies[i].split("=");//以赋值号分隔,第一位是Cookie名,第二位是Cookie值
					if (pair[0] && pair[0].replace(/(^\s*)|(\s*$)/g,'')==cookieName) {
						return pair[1];
					}
				}
				return '';
			},
			setCookie : function (name,value) {
				var expiresDate  = new Date();   
				expiresDate.setTime(expiresDate.getTime() + 30*24*60*60*1000);//30天过期时间
				var str = name+"="+encodeURI(value)  + "; expires="+expiresDate.toGMTString();
				document.cookie = str;
			}
	};

})(window);