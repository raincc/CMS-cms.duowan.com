//<!-- UEditor编辑器的配置 -->
var uEditor;
LoadUtil.load([
    [root+'/resource/plugins/ueditor/editor_config.js'],//UEditor编辑器依赖的两个JS
    [root+'/resource/plugins/ueditor/editor_all.js']
], function() {// 全局回调
	 uEditor = new UE.ui.Editor();//所有关于Ueditor的配置放在editor_config.js
	uEditor.render('editor');
	uEditor.ready(function(){
	    //需要ready后执行，否则可能报错
		uEditor.setContent($('#editorHidden').val());
	})
	
	//默认UEditor不会让“标题”输入框失去焦点，需要手动设置
	uEditor.addListener( 'click', function () {
		if(isFocusTitleFlag){//上一焦点在"标题"输入框(input[id=title])才触发
			$('#title').blur();
		}
	} );
});


