var $_sisy;
//提示定时器
var move_tip_timer;
//5秒前提示
var tip_seconds = 20;
var seconds = tip_seconds;
var save_tip_text = '<font color="blue">保存成功...... &nbsp;&nbsp;</font>';
var tmp = 0;
function moveTipTimerWork(){
	if(seconds > 0){
		$('#autoSaveTips').html('<font color="red">'+seconds+'</font><font color="blue">秒后自动保存&nbsp;&nbsp;</font>').fadeIn();
		seconds -= 5;
		move_tip_timer = window.setTimeout(moveTipTimerWork,5000);
	}else {
		$_sisy.saveAllData();
		seconds = tip_seconds;
		move_tip_timer = window.setTimeout(moveTipTimerWork,1000);
	}
	
}
$(document).ready(function(){
	//save data to client --by yzq
	$_sisy = $('#editorForm').sisyphus({
		//timeout: auto_save_time,
		autoRelease: false,
		autoRestore: false,
		autoSave: false,
		onSave: function() {
			$('#autoSaveTips').html(save_tip_text).fadeIn().delay(500).fadeOut();
		},
		onStart: function(){
			if("false" !=  cookieUtil.getCookie("autoSave")){
				//move_tip_timer = window.setInterval("moveTipTimerWork()", 1000);//循环定时器
				move_tip_timer = window.setTimeout(moveTipTimerWork,1000);
			}else{//关闭自动保存
				$('#closeAutoSave').hide();
				$('#openAutoSave').show();
			}
		}
	});
	$('#editorForm').bind("submit", function(){
		if("false" !=  cookieUtil.getCookie("autoSave")){
			//清除定时器
			clearTimeout(move_tip_timer);
			//保存
			$_sisy.saveAllData();
		}
		
		//保存指定域editor
		//$_sisy.saveFieldDataById("editor");
	});
	//--end
});

//手动临时保存数据
function saveData(){
	$_sisy.saveAllData();
}
//恢复
function restoreData(){
	if(confirm('确定要恢复数据吗?')){
		$_sisy.restoreAllData();
	}
}
//清空
function releaseData(){
	if(confirm('确定要清空数据吗?')){
		$_sisy.manuallyReleaseData();
		location.href=window.location.href;
	}
}
//关闭自动保存
function closeAutoSave(){
	if(confirm('确定要关闭自动保存吗?')){
		
		//设置cookie,记住用户行为
		cookieUtil.setCookie("autoSave", "false");
		
		$('#closeAutoSave').hide();
		$('#openAutoSave').show();
		//clearInterval(move_tip_timer);//清除循环定时器
		clearTimeout(move_tip_timer);
		$('#autoSaveTips').html('').fadeIn();
		//$_sisy.manuallyReleaseData();//清除数据
	}
}
//开启自动保存
function openAutoSave(){
	if(confirm('确定要开启自动保存吗?')){
		
		//设置cookie,记住用户行为
		cookieUtil.setCookie("autoSave", "true");
		
		$('#openAutoSave').hide();
		$('#closeAutoSave').show();
		seconds = tip_seconds;
		$_sisy.restart();
	}
}