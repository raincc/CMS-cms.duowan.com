var LoadUtil = {
    /**
     * 全局js列表
     */
    scripts: {},
    head: document.head || document.getElementsByTagName("head")[0] || document.documentElement,
 
    /**
     * 异步加载js文件 
     */
    load: function(queue, callback) {
        var self = this, queued = queue.length;
 
        for (var i = 0, l = queue.length; i < l; i++) {
            var elem;
            elem = document.createElement("script");
            elem.setAttribute("type", "text/javascript");
            elem.setAttribute("async", "async");
            elem.setAttribute("src", queue[i][0]);
            // 文件还没有处理过
            if (typeof this.scripts[elem.src] === "undefined") {
                // 使onload取得正确elem和index
                (function(elem, index) {
                    elem.onload = elem.onreadystatechange = function() {
                        if (! elem.readyState || /loaded|complete/.test(elem.readyState)) {
                            queued--;
                            // 解决IE内存泄露
                            elem.onload = elem.onreadystatechange = null;
                            // 释放引用
                            elem = null;
                            // 调用callback
                            queue[index][1] && queue[index][1]();
                            // 队列全部加载，调用最终callback
                            if (queued === 0) {
                                callback && callback();
                            }
                        }
                    };
                })(elem, i);
            }
            // 已处理，调用callback 
            else {
                queued--;
                queue[i][1] && queue[i][1]();
            }
            this.head.appendChild(elem);
 
            // 队列全部加载，调用最终callback
            if (queued === 0) {
                callback && callback();
            }
        }
    }
};