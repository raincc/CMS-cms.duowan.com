document.writeln('<script type="text/javascript"  src="'+root+'/resource/js/load_util.js"></script>');
//<!-- 文章里所有操作UEditor -->
//document.writeln('<script type="text/javascript"  src="'+root+'/resource/js/article/article_ueditor.js"></script>');
document.writeln('<script type="text/javascript"  src="'+root+'/resource/js/article/article_common.js"></script>');
//<!-- 客户端保存 -->
document.write("<script language='javascript' src='"+root+"/resource/js/saveClient/json2.js'><\/script>");
document.write("<script language='javascript' src='"+root+"/resource/js/saveClient/jstorage.js'><\/script>");
document.write("<script language='javascript' src='"+root+"/resource/js/saveClient/sisyphus.js'><\/script>");
document.writeln('<script type="text/javascript"  src="'+root+'/resource/js/article/article_save_client.js"></script>');

$(function (){
	$('#checkLogButton').toggle(function (){
		$('#logArea').fadeIn();
	},function (){
		$('#logArea').fadeOut();
	});
	
	//jquery的toggle事件与checkbox的"选中与否"发生冲突
	$('#isSyncZone').click(function (){
		if($('#isSyncZone').attr('checked') == 'checked'){
			$('#zoneUserArea').fadeIn();
		}else{
			$('#zoneUserArea').fadeOut();
		}
	});
});

function validateForm(){
	var content = FCKeditorAPI.GetInstance("editor").GetXHTML(true);
	content = content.replace(/([\u0391-\uFFE5])/ig,'111');
	if(content.length > 60000){
		alert('内容超过上限!');
		return false;
	}
    content = content.replace(/\s*/g, '');
	if(content == ''){
		alert('内容为空');
		return false;
	}
	return validateFormCommonPart();
}

//<!-- 表单的普通触发事件 -->
function submit(){
	if(!validateForm()){
		return ;
	}
	var content = FCKeditorAPI.GetInstance("editor").GetXHTML(true);
	$('#content').val(dealUeditorContent(content));
	article_tag.setUseMostTag( $('#tags').val() );
	$('#pictureUrl').val(document.getElementById("uploadHeadPicIframe").contentWindow.getHeadPicFile());
	$('#zoneUserId').val($('#zoneUser').val());
	$('#editorForm').submit();
}
//对Ueditor的内容进行处理
function dealUeditorContent(content){
	return content.replace(new RegExp(picUrlOnCms , 'g') , picUrlOnline);
}

//适配iframe的高度
function adaptIframeHeight(iframeId){
	var uploadIframe = document.getElementById(iframeId);
	uploadIframe.height = '30px';
}

//删除文章
function deleteArticle(channelId , articleId){
	if(confirm('是否删除文章?')){
		location.href = '/article/deleteArticle.do?channelId=' + channelId + '&articleId=' + articleId;
	}
}

//隐藏文章
function hideArticle(){
	if(confirm('是否隐藏文章?')){
		$.get("/article/hideArticle.do", { 'channelId': channelId, 'articleId': articleId },
				  function(data){
				    alert('隐藏成功,可以到预发布文章找到隐藏的文章');
				    location.href = '/article/toEditArticlePage.do?channelId=' + channelId + '&userId='+ userId ;
				  });
	}
}

//
function focusTitleInput(){
	 isFocusTitleFlag = true;
}

function updatePublishTime(){
	var val = $('#newPublishTimeStr').val();
	if('' != val){
		$('#updatePublishTimeForm').submit();
	}else{
		alert('请检查修改时间');
	}
}