document.writeln('<script type="text/javascript"  src="'+root+'/resource/js/template/template_validate.js"></script>');

//获取编辑器内容
function getEditorValue(){
	//editor.getValue();
	return $('#content_current').val();
}

function setEditorValue(value){
	//editor.setValue(value)
	$('#content_current').val(value);
}


// 上一版本
function preVision() {
	if (template_version == 1) {
		content1 = getEditorValue();// 保存当前内容到内存
		template_version++;
		setEditorValue(content2);
		$('#templateInfoTips').html('上一次更新的模板' + updateTime2Str);
	} else if (template_version == 2) {
		content2 = getEditorValue();// 保存当前内容到内存
		template_version++;
		setEditorValue(content3);
		$('#templateInfoTips').html('最后更新的模板' + updateTime3Str);
	}
}
// 下一版本
function nextVision() {
	if (template_version == 2) {
		content2 = getEditorValue();// 保存当前内容到内存
		template_version--;
		setEditorValue(content1);
		$('#templateInfoTips').html('正在使用的模板' + updateTimeStr);
	} else if (template_version == 3) {
		content3 = getEditorValue();// 保存当前内容到内存
		template_version--;
		setEditorValue(content2);
		$('#templateInfoTips').html('上一次更新的模板'+updateTime2Str);
	}
}



// 设置提交内容/
function setSubmitContent(submitType) {
	$('#content').val(getEditorValue());
	$('#submitType').val(submitType);
	var templateCategory = $('#templateCategorySelector').val();
	if (templateCategory == '最终文章') {
		$('#editorForm')
				.attr('action', '/template/publishFinalPageTemplate.do');
	} else {
		$('#editorForm').attr('action', '/template/publishTemplate.do');
	}
}

// 提交
function submit(submitType) {
	if (!validate()) {
		return;
	}
	setSubmitContent(submitType);
	var checkLinkResult =  checkLink(getEditorValue());
	var checkImageResult = checkImage(getEditorValue());
	var checkPreViewResult = checkPreView(getEditorValue());
	var s = '';
	if(checkLinkResult != '' || checkImageResult  != '' || checkPreViewResult != ''){
		s+= checkLinkResult + checkImageResult + checkPreViewResult;
    	s+='<table id="chktable" width="80%" border="1" cellpadding="0" cellspacing="0" bgcolor="white" bordercolordark="white" bordercolorlight="cccccc" align="center">';
    	s+='<tr align="center"><td>';
    	s+='	<input type="button" value="返 回" onclick="delDiv()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    	s+='	<input id="btn_ignore_href" type="button" value="确 定" onclick="javascript:$(\'#editorForm\').submit();" /></td></tr>';
        s+="</table>";
        addDiv(s);//添加链接提示div
        document.getElementById('btn_ignore_href').focus();//弹出链接警告div时，把焦点移到div的确定按钮，方便回车
	}else{
		$('#editorForm').submit();
	}
	
}
//检查模板里的链接
function checkLink(content) {
	var re = /<\s*a\s+.*?href\s*=\s*\'?\"?http:\/\/([^\s>\"\']+)\'?\"?[^>]*>(.*?)<\s*\/a\s*>/ig;
	var arr = null;
	var hcount = 0;
	var s = '';
	while (arr = re.exec(content)) {//用正则表达式匹配模板内容的链接，
		if (arr[1].indexOf("duowan.com") == -1 && arr[1].indexOf("dwstatic.com") == -1) {//若发现不是"多玩网"的链接作如下处理
			if (hcount == 0) {//加入Table的表头
				s += '<table id="chktable" width="80%" border="1" cellpadding="0" cellspacing="0" bgcolor="white" bordercolordark="white" bordercolorlight="cccccc" align="center">';
				s += '{超链接发现总数}';
				s += '<tr bgcolor="#F4FEFF" align="center"><td><font color=blue>超链地址</font></td><td><font color=blue>超链名</font></td></tr>';
			}
			s += '<tr align="center"><td>' + arr[1] + "</td><td>" + arr[2].replace('\\s', '')
					+ "</td></tr>";
			hcount++;
		}
	}
    if(hcount!=0){
        s=s.replace('{超链接发现总数}','<tr bgcolor="#F4FEFF"><td style="color:red" colspan="2">发现'+hcount+'个非多玩链接</td></tr>');
        s+="</table>";
     }
    return s;
}

//检查模板里的图片
function checkImage(content){
	var re=/<\s*img\s+.*?src\s*=\s*\'?\"?http:\/\/([^\s>\"\']+)\'?\"?[^>]*>/ig;
	var arr = null;
	var mcount = 0;
	var s = '';
    while(arr=re.exec(content)){//用正则表达式匹配模板内容的图片信息，
    	if(arr[1].indexOf("duowan.com")==-1&& arr[1].indexOf("dwstatic.com") == -1){//若发现不是"多玩网"的图片作如下处理
    	  if(mcount==0){
    	    s+='<table id="chktable" width="80%" border="1" cellpadding="0" cellspacing="0" bgcolor="white" bordercolordark="white" bordercolorlight="cccccc" align="center">';
    	    s+='{图片发现总数}';
    	    s+='<tr bgcolor="#F4FEFF" align="center"><td><font color=blue>图片地址</font></td></tr>';
    	  }
    	  s+='<tr align="center"><td>'+arr[1]+"</td></tr>";
    	  mcount++;
    	}
    }
    
    if(mcount!=0){
      s=s.replace('{图片发现总数}','<tr bgcolor="#F4FEFF"><td style="color:red">发现'+mcount+'个非多玩图片</td></tr>');
      s+="</table>";
    }
    return s ;
}

function checkPreView(content){
	return checkPreViewLink(content) + checkPreViewImage(content);
}

function checkPreViewImage(content){
	var re=/<\s*img\s+.*?src\s*=\s*\'?\"?http:\/\/([^\s>\"\']+)\'?\"?[^>]*>/ig;
	var arr = null;
	var mcount = 0;
	var s = '';
    while(arr=re.exec(content)){//用正则表达式匹配模板内容的图片信息，
    	if(arr[1].indexOf("cms.duowan.com")!=-1){//若发现不是"多玩网"的图片作如下处理
    	  if(mcount==0){
    	    s+='<table id="chktable" width="80%" border="1" cellpadding="0" cellspacing="0" bgcolor="white" bordercolordark="white" bordercolorlight="cccccc" align="center">';
    	    s+='{预览图片总数}';
    	    s+='<tr bgcolor="#F4FEFF" align="center"><td><font color=blue>图片地址</font></td></tr>';
    	  }
    	  s+='<tr align="center"><td>'+arr[1]+"</td></tr>";
    	  mcount++;
    	}
    }
    
    if(mcount!=0){
      s=s.replace('{预览图片总数}','<tr bgcolor="#F4FEFF"><td style="color:red">发现'+mcount+'个用于预览图片,请替换成dwstatic域下的图片</td></tr>');
      s+="</table>";
    }
    return s ;
}
function checkPreViewLink(content){
	var re = /<\s*a\s+.*?href\s*=\s*\'?\"?http:\/\/([^\s>\"\']+)\'?\"?[^>]*>(.*?)<\s*\/a\s*>/ig;
	var arr = null;
	var hcount = 0;
	var s = '';
	while (arr = re.exec(content)) {//用正则表达式匹配模板内容的链接，
		if (arr[1].indexOf("cms.duowan.com") != -1) {//若发现不是"多玩网"的链接作如下处理
			if (hcount == 0) {//加入Table的表头
				s += '<table id="chktable" width="80%" border="1" cellpadding="0" cellspacing="0" bgcolor="white" bordercolordark="white" bordercolorlight="cccccc" align="center">';
				s += '{预览超链接总数}';
				s += '<tr bgcolor="#F4FEFF" align="center"><td><font color=blue>超链地址</font></td><td><font color=blue>超链名</font></td></tr>';
			}
			s += '<tr align="center"><td>' + arr[1] + "</td><td>" + arr[2].replace('\\s', '')
					+ "</td></tr>";
			hcount++;
		}
	}
    if(hcount!=0){
        s=s.replace('{预览超链接总数}','<tr bgcolor="#F4FEFF"><td style="color:red" colspan="2">发现'+hcount+'个为预览链接,请替换成外网地址</td></tr>');
        s+="</table>";
     }
    return s;
}

/**
 * 添加链接提示div
 */
function addDiv(content){
	 selectShow("hidden");
   Overlay=document.createElement("DIV");	       
	 document.body.appendChild(Overlay);
   Overlay.style.backgroundColor="#F4FEFF";               
   Overlay.style.width="700px";
   Overlay.style.height="350px";
   Overlay.style.filter="Alpha(Opacity=100,Style=0)"; 
   Overlay.style.position="absolute";
   Overlay.style.left="100px";
   Overlay.style.top="50px";
   Overlay.innerHTML=content;
   var tabs=document.getElementsByName("chktable");
   var h=0;
   for(var i=0;i<tabs.length;i++){
     h+=tabs[i].clientHeight; 	
   }
   Overlay.style.height=h+"px";
}

function selectShow(show){
	   var selectTags=document.getElementsByTagName("select");
	   for(var i=0;i<selectTags.length;i++){
	     selectTags[i].style.visibility=show;
	   }
}

/**
 * 删除链接提示div
 */          
function delDiv(){
   document.body.removeChild(Overlay);
   selectShow("visible");
}

