function validate() {
	// 最终文章页不允许引用data语法
	var templateCategory = $('#templateCategorySelector').val();
	var content = getEditorValue();// 从编辑器中得到数据,getEditorValue方法写在template.js
	if ($('#name').val() == '') {
		alert('请输入模板名!');
		return false;
	}
	if (isTemplateNameExist) {
		alert('模板已存在，请更改模板名');
		return false;
	}
	if (content == '') {
		alert('请输入模板内容');
		return false;
	}

	var reg = /\${data.*/g;// "${"假如写在JSP里，会被解析；此段代码一定要写在独立的JS文件
	if (templateCategory == '最终文章') {
		content = content.replace(/\s*/g, '');
		if (reg.test(content)) {// 如果存在${data，则提示错误
			alert('最终文章页不允许引用data语法');
			return false;
		}
	}
	if (!isLegalTemplateCategory) {
		alert('分类为标签或标签图，一个频道只有一个');
		return false;
	}
	if (!isLegalAlias) {
		alert('别名不合法，请输入合法的别名或者重复');
		return false;
	}

	return true;
}

var isLegalAlias = true ; 
function checkAlias(){
	var alias = $('#alias').val();
	if (alias == '') {
		isLegalAlias =true;
		return;
	}
	$.ajax({
		   type: 'GET',
		   url: '/template/checkAlias.do?alias=' +alias+'&channelId=' + channelId+'&templateId='+templateId,
		   async: false,
		   cache: false,
		   dataType: 'text',
		   success: function(data){
			   if(data.indexOf('error:') > -1){
				   document.getElementById('template_alias_check').innerHTML = '别名不合法';
				   isLegalAlias =false;
		   		}else{
		   			document.getElementById('template_alias_check').innerHTML = '别名可以使用.';
		   			isLegalAlias =true;
		   		}
		   }
	});
}

var isTemplateNameExist = false;// 默认不存在
//检查标题是否已经存在
function checkTemplateName() {
	if ($('#name').val() == '') {
		return;
	}
	$.ajax({
				type : 'GET',
				url : '/template/checkName.do?name=' + $('#name').val()+ '&channelId=' + channelId,
				async : false,
				cache : false,
				dataType : 'text',
				success : function(data) {
					if (-1 != data.indexOf('exist')) {
						document.getElementById('template_name_check').innerHTML = '标题已存在,请更换标题.';
						document.getElementById('name').focus();
						isTemplateNameExist = true;
					} else {
						document.getElementById('template_name_check').innerHTML = '该标题可以使用.';
						isTemplateNameExist = false;
					}
				}
			});
}

var isLegalTemplateCategory = true;
//分类为标签或标签图，一个频道只有一个的限制。
function checkTemplateCategory(){
	var templateCategory = $('#templateCategorySelector').val();
	if(!isAddTemplate){//只针对增加时，作“分类为标签或标签图，一个频道只有一个的限制。”修改时，不作限制。
		return isLegalTemplateCategory;
	}
	if( templateCategory != '标签' &&  templateCategory !='标签图' ){
		return true;
	}
	
	$.ajax({
		   type: 'GET',
		   url: '/template/checkTemplateCategory.do?templateCategory=' +templateCategory+'&channelId=' + channelId,
		   async: false,
		   cache: false,
		   dataType: 'text',
		   success: function(data){
			   if(data.indexOf('error:') > -1){
				   isLegalTemplateCategory =false;
		   		}else{
		   			isLegalTemplateCategory =true;
		   		}
		   }
	});
	return 	 isLegalTemplateCategory ; 	
}