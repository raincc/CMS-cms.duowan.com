(function( window, undefined ) {
	var TagUtil = function (){};
	window.TagUtil = TagUtil ;
	//仅显示所有一级标签,即根标签
	TagUtil.showRootTag = function (){
		$('tbody > tr').each(function (){//仅显示一级标签
			var tagValue = parseInt($(this).attr("rel"));
			if(tagValue == 1){
				$(this).fadeIn();
			}
		});
	}
	
	//显示所有标签
	TagUtil.showAllSonTag = function (){
		$('tbody > tr').each(function (){
			var tagValue = parseInt($(this).attr("rel"));
			if(tagValue != 1){
				$(this).show();
			}
		});
	}
	//只显示根标签（一级标签）
	TagUtil.hideAllSonTag = function (){
		$('tbody > tr').each(function (){
			var tagValue = parseInt($(this).attr("rel"));
			if(tagValue != 1){
				$(this).hide();
			}
		});
	}
	//修改标签图标为打开
	TagUtil.changeTagActiveOpen = function (){
		$('tbody > tr').each(function (){
			if($(this).find('i').hasClass('tag-manage-active')){
				$(this).find('i').removeClass('tag-manage-active').addClass('tag-manage-actived');
			}
		});
	}
	//修改标签图标为关闭
	TagUtil.changeTagActiveClose = function (){
		$('tbody > tr').each(function (){
			if($(this).find('i').hasClass("tag-manage-actived")){
				$(this).find('i').removeClass('tag-manage-actived').addClass('tag-manage-active');
			}
		});
	}
	//隐藏子标签
	TagUtil.showSonTag = function (currentTag){
		$(currentTag).removeClass('tag-manage-active').addClass('tag-manage-actived');
		var tt = $(currentTag).parent().parent();
		var parentTagValue = parseInt(tt.attr('rel'));
		var tagValue;
		tt.nextAll('tr').each(function(event) {
			tagValue = parseInt($(this).attr("rel"));
			if(tagValue-1 == parentTagValue){//子目录，比父目录少一级的展开
				if($(this).find('i').hasClass("tag-manage-actived")){//若子标签的展开图标存在，将变为缩回图标
					$(this).find('i').removeClass('tag-manage-actived').addClass('tag-manage-active');
				}
				$(this).fadeIn();
			}
			if(tagValue == parentTagValue){//遇到同级目录，表示循环寻找子目录结束
				return false;
			}
		});
	}
	
	TagUtil.hideSonTag = function (currentTag){
		$(currentTag).removeClass('tag-manage-actived').addClass('tag-manage-active');
		var tt = $(currentTag).parent().parent();
		var parentTagValue = parseInt(tt.attr('rel'));
		var tagValue;
		tt.nextAll('tr').each(function(event) {
			tagValue = parseInt($(this).attr("rel"));
			if(tagValue > parentTagValue){
				$(this).fadeOut();
			}
			if(tagValue == parentTagValue){//遇到同级目录，表示循环寻找子目录结束
				return false;
			}
		});
	}
	
	
})(window);