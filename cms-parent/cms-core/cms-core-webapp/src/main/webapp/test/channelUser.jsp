<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="com.duowan.cms.common.webapp.content.SpringApplicationContextHolder"%>
<%@page import="com.duowan.cms.service.user.UserService"%>
<%@page import="com.duowan.cms.dto.user.UserPowerSearchCriteria"%>
<%@page import="com.duowan.cms.common.util.StringUtil"%>
<%@page import="com.duowan.cms.service.channel.ChannelService"%>
<%@page import="com.duowan.cms.common.domain.Page"%>
<%@page import="com.duowan.cms.dto.user.UserPowerInfo"%>
<%@page import="com.duowan.cms.dto.user.UserInfo"%>
<%@page import="com.duowan.cms.dto.user.UserRole"%>
<%@page import="com.duowan.cms.common.util.MailUtil"%>
<%@page import="com.duowan.cms.dto.user.UserSearchCriteria"%><%@page import="com.duowan.cms.dto.user.UserStatus"%>



<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	request.setAttribute("ROOT", "http://"+request.getHeader("host"));

	List<UserInfo> userList = new ArrayList<UserInfo>();
	StringBuffer sb = new StringBuffer();
	if(request.getMethod().equalsIgnoreCase("post")){
	    
	    String mode = request.getParameter("mode");
	    UserService userService = SpringApplicationContextHolder.getBean("userService", UserService.class);
	    if("search_tag".equals(mode)){
	        String channelIds = request.getParameter("searchkey");
		    Set<String> set = new HashSet<String>();
		    if(!StringUtil.isEmpty(channelIds)){
		        
		        ChannelService channelService = SpringApplicationContextHolder.getBean("channelService", ChannelService.class);
			    for(String channelId : channelIds.split(",")){
			        if(null != channelService.getById(channelId)){
			            UserPowerSearchCriteria searchCriteria = new UserPowerSearchCriteria();
					    searchCriteria.setPageSize(500);
			            searchCriteria.setChannelId(channelId);
			            Page<UserPowerInfo> pagedata = userService.pageSearch(searchCriteria);
			            for(UserPowerInfo userPowerInfo : pagedata.getData()){
			                //if("杨壮秋".equals(userPowerInfo.getUserId())){
		                    //    System.out.println(userPowerInfo.getUserRole().getDisplay());
		                    //}
			                if(UserRole.ADMINISTRATOR != userPowerInfo.getUserRole()){
			                    set.add(userPowerInfo.getUserId());
			                }
			            }
			        }
			    }
			    
			    for(String userId : set){
			        UserInfo userInfo = userService.getById(userId);
			        if(null != userInfo){
			            userList.add(userInfo);
			            if(!StringUtil.isEmpty(userInfo.getMail())){
			                sb.append(userInfo.getMail()).append(";");
			            }
			        }
			    }
		    }
	    }else if("send_mail".equals(mode)){
	        
	        String mailsto = request.getParameter("allMailStr");
	        String mailTopic = request.getParameter("mailTopic");
	        String mailContent = request.getParameter("mailContent");
	        
	        StringBuffer failName = new StringBuffer(); 
	        int successCount = 0;
	        if(!StringUtil.hasOneEmpty(mailsto, mailTopic, mailContent)){
	            if("All".equalsIgnoreCase(mailsto)){//发送cms全部用户
		            UserSearchCriteria searchCriteria = new UserSearchCriteria();
		        	searchCriteria.setPageSize(1000);
		            Page<UserInfo> _page = userService.pageSearch(searchCriteria);
		            for(UserInfo _user : _page.getData()){
		                if(UserStatus.NORMAL == _user.getUserStatus()){
		                    String mailto = _user.getMail();
		                    if(!StringUtil.isEmpty(mailto) && mailto.indexOf("@") != -1){
			                    try{
				                    MailUtil.sendHtmlEmail(mailto, mailTopic, mailContent);
				                    successCount ++;
				                }catch(Exception e){
				                    failName.append(mailto).append(";");
				                }
			                }
		                }
		            }
		            
		        }else {
		            String[] mailtoArray = mailsto.split(";");
		            for(String mailto : mailtoArray){
		                if(!StringUtil.isEmpty(mailto) && mailto.indexOf("@") != -1){
		                    try{
			                    MailUtil.sendHtmlEmail(mailto, mailTopic, mailContent);
			                    successCount ++;
			                }catch(Exception e){
			                    failName.append(mailto).append(";");
			                }
		                }
		            }
		        }
	        }
	        
	        if(!StringUtil.isEmpty(failName.toString())){
	            request.setAttribute("failSendMail", failName.toString());
	        }else{
	            request.setAttribute("successSendMail", successCount);
	        }
	        
	    }
	}
	request.setAttribute("userList", userList);
	request.setAttribute("allMailStr", sb.toString());
%>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<title>指定频道下的用户列表</title>
	<script type="text/javascript">
	<c:if test="${not empty failSendMail}">
		alert("发送失败：${failSendMail}");
	</c:if>
	<c:if test="${not empty successSendMail}">
		alert("发送成功:${successSendMail}");
	</c:if>
	</script>
	
</head>
<body>
	<div class="main-content">
		<div class="blank10"></div>
		<!--所有用户列表 start-->
		<div class="user-list">
			<div class="action-bar">
			<form name="search_tag" action="" method="post" >
				<label for="">频道id(多个id用“,”分隔):</label>
						<input type="text" name="searchkey" value="${param.searchkey}" class="style-input fixwidth-l mlm"/>
						<input type="hidden" name="mode" value="search_tag" />
						<input type="submit" class="go-search-bt c-green-bt mlm" value="搜索" />
				</form>
				<span class="mlm">共有<span class="c-red-font"><%=userList.size() %></span>用户</span>
			</div>
			<div class="blank10"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>用户名</th>
						<th>邮箱</th>
						<th>状态</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="tmpUserInfo" items="${userList}" varStatus="idx" begin="0">
					 <c:if test="${idx.index%2==0}">
				<tr  class="odd">
					 </c:if>
					<c:if test="${idx.index%2 !=0}">
				<tr >
					 </c:if>
						<td><a href="">${tmpUserInfo.userId}</a></td>
						<td>${tmpUserInfo.mail}</td>
						<td>
						 <c:if test="${tmpUserInfo.userStatus.value == 1}">
								<span class="c-black-font">
									${tmpUserInfo.userStatus.display}
								</span>
							</c:if>	
								<c:if test="${tmpUserInfo.userStatus.value  == 0}">
									<span class="c-red-font">
										${tmpUserInfo.userStatus.display}
									</span>
								</c:if>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			
		</div>
		<!--所有用户列表 end-->
		<div class="blank10"></div>
			<div>
				<div class="action-bar">
				<form name="send_mail" action="" method="post" >
					<input type="hidden" name="mode" value="send_mail" />
					<label for="">所有用户邮箱拼接：<font color="red">(填"All"-->发布器所有用户)</font></label><br />
					<textarea rows="5" cols="250" name="allMailStr" id="allMailStr">${allMailStr}</textarea><br />
					<label for="">邮件主题：</label><input type="text" name="mailTopic" id="mailTopic"/><br />
					<label for="">邮件内容：</label><br />
					<textarea rows="5" cols="250" name="mailContent" id="mailContent"></textarea><br />
							<input type="submit" class="go-search-bt c-green-bt mlm" value="发送邮件" />
					</form>
				</div>
			</div>
		
		
		
	</div>
</body>
</html>