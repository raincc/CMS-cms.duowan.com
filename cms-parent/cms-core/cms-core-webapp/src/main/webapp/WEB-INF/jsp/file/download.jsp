<%@page import="com.duowan.cms.common.util.FileUtil"%>
<%@page language="java" contentType="application/x-msdownload"    pageEncoding="gb2312"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.io.*" %>
<% 
	String filePath = String.valueOf(request.getAttribute("filePath")) ;
    String fileName = filePath.substring(filePath.lastIndexOf(FileUtil.FILE_SEPARATOR) + 1);	
    if (filePath != null) {            
    	System.out.println("Download: " + filePath);
	  	java.io.BufferedInputStream bis=null;
	  	java.io.BufferedOutputStream  bos=null;
		try{
	 		response.setContentType("application/x-download;charset=UTF-8");
	 		String downloadFileName = URLEncoder.encode(fileName,"UTF-8");
	 		response.setHeader("Content-disposition","attachment; filename=" +downloadFileName );
	 		bis =new java.io.BufferedInputStream(new java.io.FileInputStream(filePath));
	 		bos=new java.io.BufferedOutputStream(response.getOutputStream()); 
	 		byte[] buff = new byte[2048];
	 		int bytesRead;
	 		while(-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
	  			bos.write(buff,0,bytesRead);
	 		}
		}
		catch(Exception e){
	 		e.printStackTrace();
		}
		finally {
	 		if (bis != null)bis.close();
	 		if (bos != null)bos.close(); 
	 		out.clear();  
	 		out=pageContext.pushBody();  
		}
    }
%>