<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>        
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>文章的图片管理</title>
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
			<table class="normal-table">
				<thead>
					<tr>
						<th>名称</th>
						<th>大小</th>
						<th>最后上传时间</th>
						<th>目录/文件</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
<c:forEach var="file" items="${listFiles}" varStatus="idx" begin="0">
					<c:if test="${ idx.index%10==0 ||  idx.index == 0 }">
							<tr  class="odd">
					</c:if>
					<c:if test="${ (idx.index%5 ==0 && idx.index%10 !=0) }">
							<tr >
					</c:if>
						<td>
							<c:if test="${file.isDirectory}">
									<a href="/file/fileManage.do?toDirPath=${file.absolutePath}&basePath=${basePath}" style="text-decoration: underline; ">${file.name}</a>
							</c:if>
							<c:if test="${!file.isDirectory}">
									${file.name}
							</c:if>
						</td>
						<td>
							<!-- 不显示文件夹大小 -->
							<c:if test="${!file.isDirectory}">
								${file.size}b
							</c:if>
						</td>
						<td>${file.lastModified}</td>
						<td>${file.absolutePath}</td>
						<td>
							<a class="mhs" href="javascript:deleteFile('${file.absolutePath}')">删除</a>
							<c:if test="${!file.isDirectory}">
								|<a class="mhs" href="/file/download.do?path=${file.absolutePath}"  target="_blank">下载</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
		</tbody>
			</table>

</body>
</html>