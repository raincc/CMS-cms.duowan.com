<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>频道操作主界面</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
	<div class="main-content">
	<jsp:include page="/head.do?channelId=${channelInfo.id}"></jsp:include>
	<div class="blank10"></div>
		<!--频道开始界面 start-->
		<div class="channal-begin">
			<div class="search-bar">
			<form action="/channel/quickEdit.do" method="post"  id="editForm" onsubmit="javascript:submitEdit();return false;">
				<label for="">快速编辑：</label><input type="text" name="url"  id="url"  class="style-input fixwidth-l">
				<a href="javascript:submitEdit()" class=" mlm c-blue-bt edit-bt">编辑</a>
			</form>	
			</div>
			<div class="blank10"></div>
		   <div class="channal-set">
					<div class="line">
						<a href="/article/toEditArticlePage.do?channelId=${channelInfo.id}&userId=${userInfo.userId}" class="set-bt">发表文章</a>
						<a href="/article/toArticleKindEditor.do?channelId=${channelInfo.id}&userId=${userInfo.userId}" class="set-bt">发表文章(新版编辑器)</a>
						<span class="notice mlm">标题不允许出现,.*";:'_</span>
					</div>
					<div class="line">
						<a href="/article/articleList.do?channelId=${channelInfo.id}" class="set-bt">文章列表</a>
						<p class="sublist">
							<a href="/article/articleRoll.do?channelId=${channelInfo.id}" class="set-bt">滚动新闻</a>
							<a href="/article/articleRoll.do?channelId=${channelInfo.id}&status=1" class="set-bt">预定发布文章</a>
							<a href="/article/articleRoll.do?channelId=${channelInfo.id}&status=2" class="set-bt">标签为空文章</a>
						</p>
					</div>
					<div class="line">
						<a href="/tag/tagManage.do?channelId=${channelInfo.id}" class="set-bt">标签管理</a>
					</div>
					<div class="line">
						<a href="/template/templateList.do?channelId=${channelInfo.id}" class="set-bt">模版管理</a>
					</div>
					<div class="line">
						<a href="/file/fileManage.do?channelId=${channelInfo.id}" class="set-bt">文件管理</a>
					</div>
					<div class="line">
						<a href="/user/userManage.do?channelId=${channelInfo.id}" class="set-bt">用户管理</a>
					</div>
				</div>
				<div class="blank10"></div>
				<div class="search-bar">
					<label for="">今日发文章<font color="red">${workCount}</font>篇</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="/template/toGrammarPage.do?display=true"><font color="blue">语法说明</font></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="/channel/resource.do?src=shouce_v1.0.0.pdf"><font color="blue">操作手册</font></a>
			</div>
		</div>
		<!--频道开始界面 end-->

	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery.form.js"></script>	
<script type="text/javascript">

function submitEdit(){
	//$('#editForm').submit();
	
	var options = { 
        url:'/channel/quickEdit.do', //提交给哪个执行
        type:'POST', 
        dataType: 'text',
        success: function(data){
        		if(data.indexOf('success') != -1){
        			var newUrl = data.substr('success:'.length);
        			location.href=newUrl;
        		}else if(data.indexOf('fail') != -1) {
        			alert(data.substr('fail:'.length))
        		}else {
        			alert(data);
        		}
        }, //显示操作提示
        error: function(){
        	alert('服务器连接超时，请稍后重试');
        },   
        timeout:5000 
	};
	$('#editForm').ajaxSubmit(options);
	
}

</script>	
</body>
</html>