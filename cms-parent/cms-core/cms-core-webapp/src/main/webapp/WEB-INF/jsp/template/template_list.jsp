<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>            
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>模板管理</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
<style type="text/css">

table.normal-table tr.warn td{background-color:red;}

</style>	
</head>
<body >
	<div class="main-content">
		<jsp:include page="/head.do"></jsp:include>

		<!--创建模版 start-->
		<div class="creat-mod">
			<div class="action-bar">
				<a href="/template/toAddTemplatePage.do?channelId=${channelInfo.id }" class="set-mod-bt">创建模版</a>
				<form name="search_template" id="search_template" action="/template/templateList.do" method="get" >
						<input type="hidden"  name="channelId" value="${channelInfo.id}" />
						<select name="type" id="type" class="style-select fixwidth-m mlm"  onchange="changeType()" >
								<OPTION  value="name">按名称</OPTION>
								<OPTION  value="alias">按别名</OPTION>
								<OPTION  value="createUserId">按创建者</OPTION>
								<OPTION  value="lastUpdateUserId">按最后修改者</OPTION>
								<OPTION  value="templateId">按模板id</OPTION>
						</select>
						<input type="text"  name="searchkey"  value="${searchkey}" class="style-input fixwidth-l mlm">

						<select class="style-select fixwidth-m mlm" name="templateCategory" 
							 id="templateCategory" onchange="exchangeTemplateCategory(this.value)">
								<option value="">--请选择--</option>
								<option value="栏目">栏目</option>
								<option value="专题">专题</option>
								<option value="标签">标签</option>
								<option value="标签图">标签图</option>
								<option value="最终文章">最终文章</option>
								<option value="投票">投票</option>
								<option value="投票结果">投票结果</option>
								<option value="公共">公共</option>
								<option value="投票结果(新版)">投票结果(新版)</option>
								<option value="配置文件">配置文件</option>
								<option value="资讯推广">资讯推广</option>
							</select>
					
				<span class="fr copy-bar">
					<a class="c-blue-bt copy" href="javascript:pasteTemplate('${channelInfo.id}')">粘贴</a>
					<a class="c-blue-bt stick" href="javascript:copyAll('${channelInfo.id}')">全复制</a>
					<a class="c-blue-bt allcopy" href="javascript:pasteAll('${channelInfo.id}')">全粘贴</a>
				</span>
				<a href="javascript:document.search_template.submit();" class="go-search-bt c-green-bt mlm">搜索</a>
				</form>
			</div>
			
			<div class="blank20"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>分类</th>
						<th>模板名称</th>
						<th>创建者</th>
						<th>说明</th>
						<th>别名</th>
						<th>最后修改时间</th>
						<th class="action">操作</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="templateInfo" items="${templateInfoPage.data}" varStatus="idx" begin="0">
					<c:if test="${templateInfo.templateStatus.display == '解析有误的模板'}">
						<tr class="error-mud-notice"  title="解析有误的模板">
					</c:if>
					<c:if test="${templateInfo.templateStatus.display != '解析有误的模板' && idx.index%2==0}">
						<tr  class="odd">
					</c:if>
					<c:if test="${templateInfo.templateStatus.display != '解析有误的模板'  && idx.index%2 !=0}">
					    <tr >
					</c:if>
						<td>${templateInfo.templateCategory.display }</td>
						<td  class="name">
							<c:if test="${templateInfo.hasUrlOnLine }">
									<a href="${templateInfo.urlOnLine}" target="_blank">★</a>
							</c:if>
									<a href="/template/toEditTemplatePage.do?templateId=${templateInfo.id }&channelId=${channelInfo.id}">${templateInfo.name }</a>
							<c:if test="${templateInfo.hasUrlOnCms}">
									<a href="${templateInfo.urlOnCms}" target="_blank">☆</a>
							</c:if>
						</td>
						<td>${templateInfo.createUserId }</td>
						<td><span >${templateInfo.digest }</span></td><!-- class="c-red-font" -->
						<td>${templateInfo.alias }</td>
						<td>${templateInfo.updateTimeStr }</td>
						<td class="action">
							<a href="javascript:copyTemplate('${templateInfo.id }','${channelInfo.id}'  , '${templateInfo.name }')"  ><font id="${templateInfo.id }_copy">复制</font></a>
							<a href="javascript:deleteTemplate('${templateInfo.id }','${channelInfo.id}')">删除</a>
							<a href=""  id="${templateInfo.id }" onmouseover="javascript:copyGrammar( '${templateInfo.id }','${templateInfo.name }')">语法复制</a>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${templateInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${templateInfoPage.pageSize}"/>
						<jsp:param name="total" value="${templateInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/template/templateList.do?channelId=${channelInfo.id}&pageNo=:pageNo${queryStr}"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/template/templateList.do?channelId=${channelInfo.id}&pageNo=1${queryStr}"/>
						<jsp:param name="totalPageCount" value="${templateInfoPage.totalPageCount}"/>
			</jsp:include>
		</div>
		<input type="hidden" id="templateNameStore"    />
		<!--创建模版 end-->
	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>    
<!-- 引用剪切板依赖的JS -->	
<script type="text/javascript" src="${ROOT}/resource/plugins/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript">
var clip = null;  
//和clipboard_in_all_brower.html不在同一目录需设置setMoviePath
ZeroClipboard.setMoviePath('${ROOT}/resource/plugins/zeroclipboard/ZeroClipboard.swf');
function copyGrammar(templateId , templateName){
	clip = new ZeroClipboard.Client();
    clip.setHandCursor(true);
    clip.setText('{[模板=' + templateName + ']}');
    clip.addEventListener('complete', function (client) {
            alert("复制成功!");
    });
    clip.glue(templateId);
}


function exchangeTemplateCategory(templateCategory){
	document.search_template.submit();
}
function changeType(){
	if ($('#type').val() == 'posttime'){
		$('#searchkey').click(function (){
			WdatePicker({dateFmt:'yyyy-MM-dd'});
		});
	}else{
		$('#searchkey').unbind('click' );
	}
}
function copyAll(channelId){
	if(!confirm('是否确定复制本频道所有模板？')){
		return;
	}
	$.get('/template/copyAllTemplate.do?channelId=' + channelId
			, function (data){},'text');
}
function pasteAll(channelId){
	if(!confirm('是否把其他频道的所有模板粘贴到本频道？')){
		return;
	}
	
	//进行忙碌提示
	var bg = busyTip();
	
	$.get('/template/pasteAllTemplate.do?channelId=' + channelId
			, function (data){
				if(data.indexOf('[error]') > -1){
					alert(data.replace(/\s*/g, ''));
					return;
				}else{
					document.body.removeChild(bg);
					alert(data);
					location.href ='/template/templateList.do?channelId=' + channelId;
				}
			},'text'
	);
}
function busyTip(){
	var iWidth = document.documentElement.clientWidth;
	var iHeight = document.documentElement.clientHeight;
	var bgObj = document.createElement("div");
	bgObj.id = "bgObj";
	bgObj.style.cssText = "position:absolute;left:0px;top:0px;width:"+iWidth+"px;height:"+Math.max(document.body.clientHeight, iHeight)+"px;filter:Alpha(Opacity=10);opacity:0.5;background-color:#000000;z-index:101;";
	document.body.appendChild(bgObj);
	$("#bgObj").html('<center><img src="${ROOT}/resource/images/loading.gif" /></center>');
	return bgObj;
}
function deleteTemplate(templateId , channelId){
	if(!confirm('是否确定删除模板？')){
		return;
	}
	$.get('/template/deleteTemplate.do?templateId='+templateId + '&channelId=' + channelId
			, function (data){
			if(data.indexOf('[error]') > -1){
				alert(data.replace(/\s*/g, ''));
			}else{
				location.href = data;
			}
		},'text');
}

//复制模板
function copyTemplate(templateId , channelId ,templateName ){
	$('a font').each(function (){
		$(this).html('复制');
	});
	$.get('/template/copyTemplate.do?templateId='+templateId+'&channelId='+channelId+'&templateName='+templateName
			, function (data){
					if(data.indexOf('[error]') > -1){
						alert(data.replace(/\s*/g, ''));
					}else{
						$('#'+templateId+'_copy').html('已复制');
					}
			},'text');
}
//粘贴模板
function pasteTemplate(channelId){
	$.get('/template/pasteTemplate.do?channelId='+channelId + '&userId=${userInfo.userId}'
			, function (data){
				if(data.indexOf('[error]') > -1){
					alert(data.replace(/\s*/g, ''));
				}else{
					location.href = data;
				}
			},'text');
}

</script>
<script type="text/javascript">
var type = '${type}';
var preViewUrl = '${preViewUrl}';
var templateCategory = '${templateCategory}';
$(function (){
	if(type != ''){
		$('#type option[value='+type+']').attr('selected', true); 
	}
	if(templateCategory != ''){
		$('#templateCategory option[value="'+templateCategory+'"]').attr('selected', true); 
	}
	if(preViewUrl != ''){//预览页面不是空
		window.open(preViewUrl);
	}
});
</script>	
</body>
</html>