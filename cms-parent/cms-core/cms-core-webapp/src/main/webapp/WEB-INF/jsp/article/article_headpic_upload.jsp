<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>          
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<script type="text/javascript"  src="${ROOT}/resource/js/file_manage.js"></script>   
	<script type="text/javascript" src="${ROOT}/resource/plugins/swfupload/swfupload.js"></script>
	<script type="text/javascript" src="${ROOT}/resource/plugins/swfupload/swfupload.queue.js"></script>
	<script type="text/javascript" src="${ROOT}/resource/plugins/swfupload/fileprogress.js"></script>
<script type="text/javascript">
var root = '${ROOT}';//发布器项目根目录
window.onload = function() {
	upload1 = new SWFUpload({
		// Backend Settings
		upload_url: root+"/article/uploadHeadPic.do",
		post_params: {  //附带的表单参数
			"articleId" : "${articleId}",
			"channelId" : "${channelId}",
			"JSESSIONID" : "<%=session.getId()%>"
		},
		button_window_mode : "transparent",
		// File Upload Settings
		file_size_limit : "102400",	// 100MB
		file_types : "*.*",
		file_types_description : "All Files",
		file_upload_limit : "1",
		file_queue_limit : "0",
		
		file_queue_error_handler : fileQueueError,//在文件选择窗关闭后，文件排队发生错误时触发
		file_dialog_complete_handler : fileDialogCompleteHandler,//在文件选择窗关闭后，文件排队上传的过程中触发
		upload_error_handler : uploadError,//文件上传的过程中发生了错误触发的事件
		upload_success_handler : uploadSuccessHandler,//文件上传成功后触发的事件

		// Button settings
		button_image_url: root+"/resource/plugins/swfupload/images/button.png",
		button_width: "65",
		button_height: "29",
		button_placeholder_id: "spanButtonPlaceholder",
		button_text: '<span class="theFont" >上传文件</span>',
		button_text_style: ".theFont { font-size: 13; }",
		button_text_left_padding: 5,
		button_text_top_padding: 8,
		
		// Flash Settings
		flash_url : root+"/resource/plugins/swfupload/swfupload.swf",

		custom_settings : {
		},
		// Debug Settings
		debug: false
	});
}

function uploadSuccessHandler(file, serverData) {
	location.href = serverData.toString();
}

</script>	
</head>
<body style="overflow-y:hidden ;">  

<form action="/article/uploadHeadPic.do" id="uploadHeadPicForm" enctype="multipart/form-data"  method="post">
    	<input type="hidden"  name="articleId" id="articleId"  value="${articleId}" />
		<input type="hidden"  name="channelId" id="channelId"  value="${channelId }" />
		<span class="form-item upload-headimg mlm">
    	头图 ：
    	  <c:if test="${headPicOnLineUrl != ''}">
    			<input type="text" id="headPicInput" value="${headPicOnLineUrl}" class="style-input fixwidth-s" placeholder="网络图片地址"  onblur="changeHeadPicInput()" />
    		</c:if>
    		<c:if test="${preHeadPic != '' && headPicOnLineUrl == ''}">
    				<input type="text" id="headPicInput" value="${preHeadPic}" class="style-input fixwidth-s" placeholder="网络图片地址"  onblur="changeHeadPicInput()" />    		
    		</c:if>
    		<c:if test="${preHeadPic == '' && headPicOnLineUrl == ''}">
    			<input type="text" id="headPicInput" value="" class="style-input fixwidth-s" placeholder="网络图片地址"  onblur="changeHeadPicInput()" />
    		</c:if>	
    	</span>
    		&nbsp;&nbsp;
    		<div style="display:inline;float:left;top:12px;">
				<span id="spanButtonPlaceholder" ></span>
			</div>
    		 <c:if test="${preHeadPic != ''}">
    		 	<span class="short-img">
	    			<img src="${preHeadPic}"  height="30" width="50"  />
	    			<a class="ui-link-bt" href="javascript:clearHeadPic()">清除</a>
	    		</span>	
    		</c:if>
    	
</form>

<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>    
<script type="text/javascript">

function changeHeadPicInput(){
	location.href = '/article/toUploadHeadPicIframe.do?channelId=${channelId }&articleId=${articleId}&preHeadPic=' +encodeURIComponent($('#headPicInput').val() );
}

//上传头图
function uploadHeadPic(){
	if($('#headPicFile').val() == ''){
		alert('请选择文件后再点击上传');
		return;
	}
	$('#uploadHeadPicForm').submit();
}
//
function getHeadPicFile(){
	if($('#headPicInput').val() != ''){
		return $('#headPicInput').val();
	}
	return '${preHeadPic}';
}

function clearHeadPic(){
	var preHeadPic =  '${preHeadPic}';
	if(confirm('确定要清除头图?')){
			$.get("/article/clearHeadPic.do", { 'channelId': '${channelId }', 'picUrl': preHeadPic,'articleId':'${articleId}' },
					  function(data){
						window.parent.document.getElementById('pictureUrl').value = '';
						if(window.parent.adaptIframeHeight){//如果父窗口存在adaptIframeHeight方法，则执行adaptIframeHeight方法
							window.parent.adaptIframeHeight('uploadHeadPicIframe');
						}
						location.href = data.toString();//兼容firefox
					  });
	}
}


</script>
</body>
</html>