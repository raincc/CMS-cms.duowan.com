<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div  id="grammarIntro"  <c:if test="${param.display ne 'true'}">style="display:none"</c:if>>
<a id="firstLineTemplateGrammar"  > </a>
<table width="100%" border="1">
	<tr>
		<td bgcolor="#C4ECC9">
			<div align="center" class="STYLE1">
				可直接使用的变量说明
			</div>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			最终文章类模板
		</td>
	</tr>
	<tr>
		<td>
			$articleid &nbsp;文章id
			<br />
			$title &nbsp;文章标题
			<br />
			$titlecolor &nbsp;文章标题颜色
			<br />
			$digest &nbsp;摘要
			<br />
			$picurl &nbsp;头图
			<br />
			$source &nbsp;来源
			<br />
			$author &nbsp;作者
			<br />
			$posttime &nbsp;发布时间
			<br />
			$tags &nbsp;标签列表用,分隔
			<br />
			$userid &nbsp;编辑
			<br />
			$content &nbsp;正文
			<br />
			$power &nbsp;权重
			<br />
			$PageCount &nbsp;分页总数
			<br />
			$channelname &nbsp;频道中文名
			<br />
			$curpage &nbsp;当前页
			<br />
			$split_line &nbsp;分页符
			<br />
			$userid &nbsp;发布者
			<br />
			$diy1~5 &nbsp;自定义1~5
			<br />
			$comment &nbsp;得到评论js语法
			<br />
			$realtags &nbsp;文章相关tag链接
			<br />
			$subtitle &nbsp;副标题
			<br />
			$channelid &nbsp;频道id
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			栏目、专题、公共类、标签列表类模板
		</td>
	</tr>
	<tr>
		<td>
			<p>
				$channelname &nbsp;频道中文名
				<br />
				$alias &nbsp;别名
			</p>
		</td>
	</tr>
	
	<!-- tr>
		<td align="center" class="style5">
			关键字类模板
		</td>
	</tr>
	<tr>
		<td height="70">
			$content &nbsp;关键字正文
			<br />
			$channelname &nbsp;频道中文名
			<br />
			$channelid &nbsp;频道id
			<br />
			$PageCount &nbsp;分页总数
			<br />
			$curpage &nbsp;当前页
		</td>
	</tr -->
	<tr>
		<td align="center" class="style5">
			所有类型模板均可
		</td>
	</tr>
	<tr>
		<td>
			<p>
				$split_line &nbsp;分页行
				<br />
				$channelname &nbsp;频道中文名
				<br />
				$kuGameName &nbsp;频道对应的游戏名称
			</p>
		</td>
	</tr>
</table>
<table width="100%" border="1">
	<tr>
		<td bgcolor="#C4ECC9">
			<div align="center" class="STYLE1">
				$data方法说明
			</div>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取游戏测试列表
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6">获得游戏测试信息列表:</span>
				<br />
				$data.getGameTestListRest(开始时间（格式 YYYY-MM-dd）, 结束时间（格式 YYYY-MM-dd）, 类型, 长度)
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($list=$data.getGameTestListRest("2011-01-01","2012-01-01",1,10))
				<br />
				#foreach($one in $list)
				<br />
				$one
				<br />
				#end
			</p>
			
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取发号数据
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6">获得获取发号数据信息列表:</span>
				<br />
				$data.getFahaoRank("{类型,游戏名称}")
				<br />
				<span class="style7">用例:</span>
				<br /><font color="blue">
				#set ($rankData=$data.getFahaoRank("{action:'newGameTest',gameName:'剑灵'}"))
				<br />
				#set ($list=$rankData.get("剑灵"))</font>
				<br />
				#foreach($one in $list)
				<br />
				$one
				<br />
				#end
			</p>
			
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			<font color="red">获取图集</font>
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6" ><font color="red">获取图集:</font></span>
				<br />
				$data.getTujiList("{'alias':专题名称,'tag':图集标签,'num':每页记录数,'page':第几页,'sort':排序}")
				<br />
				注：1)alias:若为空，则所有专题;2)tag:若为空，则所有标签;3)num:默认50;4)page:默认第一页;5)排序：0-->最新，1-->热门(按评论数),默认0-->最新
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($list=$data.getTujiList("{'alias':'bns','num':'1'}"))
				<br />
				#foreach($one in $list)
				<br />
				$one
				<br />
				#end
			</p>
			
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取投票排行榜数据
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6">获取投票排行榜数据信息列表:</span>
				<br />
				$data.getVoteRankDataByArticle(频道id,tag,偏移量,获取数据数量,开始时间（格式 YYYY-MM-dd）,结束时间（格式 YYYY-MM-dd）,统计类型)
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($list=$data.getVoteRankDataByArticle("aion","职业视频",0,8,"2011-01-01","2012-01-01","month_statistic_article"))
				<br />
				#foreach($one in $list)
				<br />
				$one
				<br />
				#end
			</p>
			
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取浏览数数据
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6">获取浏览数数据信息:</span>
				<br />
				getVNum(文章id, 频道id, 标题, 文章url)
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($vnum=$data.getVNum("190725976972","news","末日前的大狂欢！春节前必玩的八大新游盘点","http://news.duowan.com/1201/190725976972.html"))
				<br />
				$vnum
			</p>
			
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取标签相关信息
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6">获得某tag的所有相关tag:</span>
				<br />
				$data.aboutTags(标签名)
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($list=$data.aboutTags(&quot;t2&quot;))
				<br />
				#foreach($one in $list)
				<br />
				$one
				<br />
				#end
			</p>
			<p>
				<span class="style6">获得某tag的父tag:</span>
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($ptag=$data.getParentTag(&quot;a2&quot;))
				<br />
				&lt;a
				href=&quot;$data.getTagUrl($channelid,$ptag)&quot;&gt;$ptag&lt;/a&gt;
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取文章列表
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6">输入tag和权重起始和数量返回本频道文章列表</span>
				<br />
				$data.getList(tag,powerfrom,powerto,start,len)或
				<br />
				$data.getList(tag,powerfrom,powerto,len)或
				<br />
				$data.getList(tag,$pagecount*len,len)或
				<br />
				$data.getList(tag,len)
			</p>
			<span class="style7">用例:</span>
			<br />
			#set($list=$data.getList(&quot;test2&quot;,0,5)) ##获取tag名为test2的前5条记录
			<br />
			#foreach($one in $list) 循环得到所有文章列表信息
			<br />
			$one.getString(&quot;tag&quot;)) 文章tag
			<br />
			$one.getLong(&quot;articleid&quot;)) 文章id
			<br />
			$one.getString(&quot;title&quot;)) 标题
			<br />
			$one.getString(&quot;titlecolor &quot;)) 标题颜色
			<br />
			$one.getString(&quot;subtitle&quot;)) 副标题
			<br />
			$one.getString(&quot;digest&quot;)) 摘要
			<br />
			$one.getString(&quot;url&quot;)) 空文章地址
			<br />
			$one.getString(&quot;picurl&quot;)) 图片地址
			<br />
			$one.getString(&quot;source&quot;)) 来源
			<br />
			$one.getString(&quot;author&quot;)) 作者
			<br />
			$one.getString(&quot;userid&quot;)) 发布者
			<br />
			$one.geLong(&quot;daynum&quot;)) 天数
			<br />
			$one.getString(&quot;power&quot;)) 权重
			<br />
			$one.getString(&quot;posttime&quot;)) 发布时间
			<br />
			$one.getString(&quot;updatetime&quot;)) 更新时间
			<br />
			$one.getString(&quot;ispic&quot;)) 是否为图片
			<br />
			$one.getString(&quot;sontag&quot;)) 子标签
			<br />
			$one.getString(&quot;sontagid&quot;)) 子标签id
			<br />

			#end
			</p>
			</p>
			<p>
				<span class="style6">输入频道中文名,tag和权重起始和数量返回文章列表,以下方法的使用方法及参数获取同example1,区别为多&quot;频道中文名&quot;参数,便于获取其他频道的文章列表.</span>
				<br />
				$data.getListOther(频道中文名,tag,powerfrom,powerto,start,len)或
				<br />
				$data.getListOther(频道中文名,tag,powerfrom,powerto,len)或
				<br />
				$data.getListOther(频道中文名,tag,start,len)或
				<br />
				$data.getListOther(频道中文名,tag,len)
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			只获取有头图的文章列表
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<br />
				<span class="style6">
					输入频道中文名,tag和权重起始和数量返回本频道图片文章列表,以下方法的使用方法及参数获取同example1,区别为获取的是该频道有头图的文章列表.</span>
				<br />
				$data.getPicListOther(频道中文名,tag,powerfrom,powerto,start,len)或
				<br />
				$data.getPicListOther(频道中文名,tag,powerfrom,powerto,len)或
				<br />
				$data.getPicListOther(频道中文名,tag,start,len)或
				<br />
				$data.getPicListOther(频道中文名,tag,len)
			</p>
			<p>
				<span class="style6">输入tag和权重起始和数量返回本频道图片文章列表,以下方法的使用方法及参数获取同上,区别为获取的是&quot;本频道&quot;文章列表.</span>
				<br />
				$data.getPicList(tag,powerfrom,powerto,start,len)或
				<br />
				$data.getPicList(tag,powerfrom,powerto,len)或
				<br />
				$data.getPicList(tag,start,len)或
				<br />
				$data.getPicList(tag,len)
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取多标签文章列表
		</td>
	</tr>
	<tr>
		<td>
			<span class="style6">相关文章或多tag文章列表(多tag用,分开)</span>
			<br />
			$list=$data.getRelate(&quot;$tags&quot;,显示数量)或
			<br />
			$list=$data.getRelate(&quot;$tags&quot;,权重开始,权重结束,显示数量)
			<br />
			<span class="style7">用例:</span>
			<br />
			&lt;ul&gt;
			<br />
			#set ($list=$data.getRelate($tags,8))
			<br />
			#foreach ($one in $list)
			<br />
			#set ($taglink =
			$data.getRelatedUrl($one.getString(&quot;articleid&quot;)))
			<br />
			&lt;li&gt;&lt;a
			href=&quot;$taglink&quot;&gt;$one.getString(&quot;title&quot;)&lt;/a&gt;&lt;/li&gt;
			<br />
			#end
			<br />
			&lt;/ul&gt;
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			获取文章信息
		</td>
	</tr>

	<tr>
		<td>
			<p>
				$data.getArticleInfo($articleid)
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($one=$data.getArticleInfo($articleid))
				<br />
				$one.getString(&quot;title&quot;)&nbsp;文章标题
				<br />
				$one.getString(&quot;subtitle&quot;)&nbsp; 文章副标题
				<br />
				$one.getString(&quot;digest&quot;)&nbsp;摘要
				<br />
				$one.getString(&quot;picurl&quot;)&nbsp;头图
				<br />
				$one.getString(&quot;source&quot;)&nbsp;来源
				<br />
				$one.getString(&quot;author&quot;)&nbsp;作者
				<br />
				$one.getString(&quot;posttime&quot;)&nbsp;发布时间
				<br />
				$one.getString(&quot;ordertime&quot;)&nbsp;预定发布时间
				<br />
				$one.getString(&quot;updatetime&quot;)&nbsp;最后修改时间
				<br />
				$one.getString(&quot;tags&quot;)&nbsp;标签列表用,分隔
				<br />
				$one.getString(&quot;userid&quot;)&nbsp;发布者
				<br />
				$one.getLong(&quot;articleid&quot;)&nbsp;文章id
				<br />
				$one.getString(&quot;updateuserid&quot;)&nbsp;最后修改者
				<br />
				$one.getString(&quot;content&quot;)&nbsp;正文
				<br />
				$one.getString(&quot;proxyip&quot;)&nbsp;代理ip
				<br />
				$one.getInt(&quot;power&quot;)&nbsp;权重
				<br />
				$one.getLong(&quot;threadid&quot;)&nbsp;主贴id
				<br />
				$one.getLong(&quot;templateid&quot;)&nbsp;模板id
				<br />
				$one.getString(&quot;logs&quot;)&nbsp;修改日志
				<br />
				$one.getString(&quot;catchurl&quot;)&nbsp;抓取url
				<br />
				$one.getString(&quot;logs&quot;)&nbsp;修改日志
				<br />
				$one.getInt(&quot;status&quot;)&nbsp;状态(0普通，1预定 2未加tag,3抓取 99删除)
				<br />
				$one.getString(&quot;ispic&quot;)&nbsp;是否为图片
				<br />
				$one.getLong(&quot;daynum&quot;)&nbsp;天数(用于排序)
				<br />

				$one.getString(&quot;diy1&quot;)&nbsp;自定义1
				<br />
				$one.getString(&quot;diy2&quot;)&nbsp;自定义2
				<br />
				$one.getString(&quot;diy3&quot;)&nbsp;自定义3
				<br />

				#end
			</p>
			<p>
				<span class="style6">获得多个频道指定tag中权重在指定数以上的文章(&quot;彩虹岛,重点,120{%}彩虹岛,重点,120&quot;)</span>
				<br />
				$data.getChannelsTag(String s,int num)
			</p>
		</td>
	</tr>
	<tr>
		<td align="center">
			<span class="style5">获取地址</span>
		</td>
	</tr>
	<tr>
		<td>
			<span class="style6">得到本频道文章绝对url</span>
			<br />
			$data.getUrl(文章id)
			</p>
			<p>
				<span class="style6">得到本频道文章相对url</span>
				<br />
				$data.getRelatedUrl(文章id)
			</p>
			<p>
				<span class="style6"> 得到该频道文章绝对url</span>
				<br />
				$data.getUrl(频道中文名,文章id)
			</p>
			<p>
				<span class="style6">得到该频道文章相对url</span>
				<br />
				$data.getRelatedUrl(频道中文名,文章id)
			</p>
			<p>
				<span class="style6">得到文章url(无域名)</span>
				<br />
				$data.getUrl($articleid)或$data.getUrl($articleid,$guest)
				<br />
				<br />
				<br />
				<br />
			</p>
		</td>
	</tr>
	<tr>
		<td>
			<span class="style6">获得标签url:</span>
			<br />
			$data.getTagUrl(频道id,标签名);
			<Br />
			<span class="style7">用例:</span>
			<br />
			#set ($list=$data.getList(&quot;test2&quot;,$start,$len))
			<br />
			#foreach($one in $list)
			<br />
			$one.getString(&quot;title&quot;);
			<br />
			&lt;a
			href=&quot;$data.getTagUrl(&quot;ceshi&quot;,$one.getString(&quot;tag&quot;))&quot;&gt;$one.getString(&quot;tag&quot;)&lt;/a&gt;
			<br />
			#end
			</p>
			<p>
				<span class="style6">获得标签相对url:</span>
				<br />
				$cooperate 可获得合作名
				<br />
				$data.getTagRelatedUrl(频道id,标签名,$cooperate)
			</p>
			<p>
				<span class="style6">根据realtags字符串，获得所有tag超链接</span>
				<br />
				$realtags &nbsp;文章相关tag链接
				<br />
				$data.getTagsUrl($realtags,String splits) #splits 分隔符
				<br />
				<br />
			</p>
		</td>
	</tr>
	<tr>
		<td>
			<br />
			<span class="style6">获得模板相对url:</span>
			<br />
			$alias 可获得别名
			<br />
			$cooperate 可获得合作名
			<br />
			$data.getTemplateRelatedUrl(频道id,模板名,$cooperate)
			<br />
			$data.getTemplateRelatedUrl(频道id,模板名,$cooperate,$alias)
			</p>
			<p>
				合作路径 $guest
			</p>
		</td>
	</tr>
	<tr>

		<td align="center" class="style5">
			模板引用
		</td>
	</tr>
	<tr>
		<td>
			<p>
				<span class="style6">标注是合作模板</span>
				<br />
				{[合作页面=合法方]}
			</p>
			<p>
				<span class="style6">标签群模板和栏目的分页</span>
				<br />
				{[分页数=5,len]}
				<br />
				len 表示每页记录数
				<br />
				$pagecut 模板里面的分页值
				<br />
				<span class="style7">用例:</span>
				<br />
				{[分页数=5,30]}
				<br />
				page $pagecut
				<br />
				#set ($start=$pagecut*$len)
				<br />
				#set ($list=$data.getList(&quot;test2&quot;,$start,$len))
				<br />
				#foreach($one in $list)
				<br />
				$one.getString(&quot;title&quot;);
				<br />
				#end
				<br />
				$split_line
			</p>
			<p>
				<span class="style6"> 注释不显示代码</span>
				<br />
				<br />
				{[注释任意=任意]}
				<br />
				<br />
				<span class="style6">包含制定静态页中的代码</span>
				<br />
				{[包含=/aa.html]}
				<br />
				<br />
				<span class="style6">模板引用</span>
				<br />
				{[模板=模板中文名]} &nbsp;
				<span class="style7">用例:</span>{[模板=新游戏]}
				<br />
				<br />
				<span class="style6">模板绝对引用</span>
				<br />
				{[绝对模板=模板中文名]}
			</p>
			<p>
				<span class="style6">公共模板引用</span>
				<br />
				{[公共模板=频道中文名:模板中文名]} &nbsp;
				<span class="style7">用例:</span>{[公共模板=测试:测试]}
				<br />
				<br />
				<br />
				
				<span class="style6">动态公共模板</span>
				<br />
				普通频道模板引用动态公共模板用例：<br />
				<span class="style7">公共频道模板名称：</span>公共模板名
				<br />
				<span class="style7">普通频道模板</span>名称：模板test	&nbsp;&nbsp; 内容：{[动态公共模板=公共模板名]}
				<br />				
				<br />
				<br />				
				
				<span class="style6">js模板引用</span>
				<br />
				{[js模板=模板中文名]} &nbsp;
				<span class="style7">用例:</span>{[模板=新游戏新闻]}
				<br />
				<br />
				<span class="style6">关键字url</span>
				<br />
				{[关键字=关键字]}或{[关键字=频道中文名:关键字]}
				<br />
				<br />
				<span class="style6">回复引用</span>
				<br />
				{[回复=文章url]}
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			字符串处理
		</td>
	</tr>
	<tr>
		<td height="268">
			<p>
				<span class="style6">拆分字符串</span>
			</p>

			<p>
				方法一:
				<br />
				$data.splits(String content,String regex) &nbsp;## content
				为拆分内容,regex 为用于拆分的字符串.
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($list=$data.getList(&quot;test&quot;,60,255,4))
				<br />
				#foreach ($one in $list)
				<br />
				#set
				($artical=$data.getArticleInfo($one.getString(&quot;articleid&quot;)))
				<br />
				#set ($cons=false)
				<br />
				#set
				($cons=$data.splits($artical.getNonNull(&quot;diy1&quot;),&quot;{%}&quot;))
				<br />
				#if($cons)
				<br />
				$cons.getNonNull(&quot;0&quot;)拆分后第一个数据
				$cons.getNonNull(&quot;1&quot;)拆分后第二个数据
				$cons.getNonNull(&quot;2&quot;)拆分后第三个数据
				<br />
				#end
				<br />
				#end
				<br />
				<br>
				方法二:
				<br />
				data.splits_rs(String content,String regex)&nbsp; ##参数说明同上
				<br />
				<span class="style7">用例:</span>
				<br />
				#set ($list=$data.splits_rs(字符串,&quot;,&quot;))
				<br />
				#foreach($one in $list)
				<br />
				$one
				<br />
				#end
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			数组处理
		</td>
	</tr>
	<tr>
		<td>
			<span class="style6">获得数组长度:</span>
			<br />
			$data.getLength(数组)
			<br />
			<span class="style7">用例:</span>
			<br />
			#set($list=$data.splits_rs(字符串,&quot;,&quot;))
			<br />
			$data.getLength($list)
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			公会相关
		</td>
	</tr>
	<tr>
		<td align="center" class="style5"></td>
	</tr>
	<tr>
		<td>
			<span class="style6">获得公会等级</span> $data.getGuildLv(int lv)
			<br />
			<span class="style6">获得公会等级数组</span> $data.getGuildCooperateLv()
			<br />
			<span class="style6">获得公会总数</span> $data.getGuildCount(int lv)
			<br />
			<span class="style6">获得公会合作记录</span> $data.getGuildCooperatelist(int
			lv,int start,int len)
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			评论相关
		</td>
	</tr>
	<tr>
		<td>
			<span class="style6">得到该频道某文章主帖的信息</span>
			<br />
			$data.getThreadInfo(long threadid)
			<br />
			<br />
			<span class="style6">得到该频道某文章排序后的前10个回贴(按support和ptime排序)</span>
			<br />
			$data.getReplys(long threadid)
			<br />
			<br />
			<span class="style6">得到该频道某文章排序后并在goodreplys中没有出现过的前10个回贴(按ptime排序)</span>
			<br />
			$data.getReplysByPtime(long threadid)
			<br />
			<br />
			<span class="style6">获取评论排行<font color="red">(新)</font></span>
			<br />
			<font color="red">$data.getCommentRankDataByTag(频道id,tags,偏移量,获取数据数量,开始时间(格式 YYYY-MM-dd),结束时间(格式 YYYY-MM-dd))</font><br />
			用例：<br />
			#foreach($attr in $data.getCommentRankDataByTag("lol","游戏视频,重点",0,10,"",""))<br />
			&lt;li&gt;&lt;a href="$attr.url"&gt;$tools.substring($attr.title,0,30)&lt;/a&gt;    作者：$attr.author    评论数：$attr.commentCount&lt;/li&gt;<br />
			#end <br />
			<font color="red">说明：1.此处支持多个tag,以“,(英文)”分隔;2.日期如未写,则默认最近三个月,3.调用此语法先确保文章页模板使用$comment语法</font>
		</td>
	</tr>
</table>
<table width="100%" border="1">
	<tr>
		<td bgcolor="#C4ECC9">
			<div align="center" class="STYLE1">
				$tools方法说明
			</div>
		</td>
	</tr>

	<tr>
		<td align="center" class="style5">
			字符串处理的相关方法
		</td>
	</tr>
	<tr>
		<td>
			<p>
				$tools.replaceAll(字符串1,字符串2,字符串3)
				&nbsp;把字&quot;符串1&quot;中的所有&quot;字符串2&quot;替换成&quot;字符串3&quot;
				<br />
				$tools.parseTime(时间字符串,字符串)
				&nbsp;用&quot;时间字符串&quot;对&quot;字符串&quot;进行替换操作
				<br />
				<span class="style7">用例:</span>
				<br />
				用$tools.parseTime(&quot;2007年-1月-29日 11:30:30&quot;,&quot;今天是Y日期MD
				h点m分s秒&quot;)替换后变为&quot;今天是2007年日期1月29日 11点30分30秒&quot;
				其中年月日间一定要用&quot;-&quot;号分隔 年月日和时间用&quot;空格&quot;分隔(可以只写年月不写时间)
				<br />
				Y M D h m s
				<br />
				$tools.cutImg(字符串) &nbsp;删除&quot;字符串&quot;中所有&lt;img&gt;标签
				<br />
				<span class="style7">用例:</span>
				<br />
				$tools.cutTags(&quot;&lt;div&gt;&lt;p&gt;欢迎光临多玩官方网站&lt;/p&gt;&lt;/div&gt;&quot;)
				得到:&quot;欢迎光临多玩官方网站&quot;
				<br />
				<br />
				$tools.IndexOf(字符串1,字符串2)
				&nbsp;得到&quot;字符串2&quot;在&quot;字符串1&quot;中第一次出现的位置(注意位置是从0开始计算)
				<br />
				<br />
				$tools.substring(字符串,开始截取位置) &nbsp;得到从字符串某位开始截取的字符串
				<br />
				<span class="style7">用例:</span>
				<br />
				$tools.substring(&quot;今天是2007年日期1月29日&quot;,3)(注意位置是从0开始算)
				&nbsp;得到&quot;2007年日期1月29日&quot;
				<br />
				$tools.substring(字符串,开始截取位置,停止截取位置) &nbsp;得到字符串中的部分字符串
				<br />
				<span class="style7">用例:</span>
				<br />
				$tools.substring(&quot;今天是2007年日期1月29日&quot;,3,8)(注意位置是从0开始算,停止截取位会自动-1)
				得到&quot;2007年&quot;
				<br />
				$tools.substring(字符串1,字符串2)
				得到&quot;字符串1&quot;中到&quot;字符串2&quot;第一次出现的字符串
				<br />
				<span class="style7">用例:</span>
				<br />
				$tools.substring(&quot;2007年日期1月29日&quot;,&quot;日&quot;)
				得到&quot;2007年&quot;
				<br />
				<br />
				$tools.split(字符串1,字符串2)
				&nbsp;得到&quot;字符串1&quot;按&quot;字符串2&quot;进行拆分后的数组
				<br />
				<span class="style7">用例:</span>
				<br />
				$tools.split(&quot;test1,test2,test3&quot;,&quot;,&quot;)
				&nbsp;按&quot;,&quot;对&quot;test1,test2,test3&quot;进行拆分
				<br />
				$tools.ruship(ip地址) 得到&quot;ip地址&quot;的前3位
				<br />
				<span class="style7">用例:</span>
				<br />
				$tools.ruship(&quot;192.168.1.1&quot;)
				&nbsp;得到&quot;192.168.1.*&quot;
				<br />
			</p>
			<p>
				$tools.join(字符串1,字符串2) 得到&quot;字符串1&quot;和&quot;字符串2&quot;合并后的字符串
				<br />
				$tools.join(字符串1,字符串2,字符串3) 同上
				<br />
				$tools.join(字符串1,字符串2,字符串3,字符串4) 同上
				<br />
				$tools.join(字符串1,字符串2,字符串3,字符串4,字符串5) 同上
				<br />
				$tools.join(字符串1,字符串2,字符串3,字符串4,字符串5,字符串6) 同上
				<br />
				<span class="style7">用例:</span>
				<br />
				$url=$tools.join(&quot;www.duowan.&quot;,&quot;com&quot;)
				<br />
				$url ## 结果为 www.duowan.com
			</p>
			<p>
				$tools.insert(字符串1,字符串2,int point) 得到在字符串1的第point个字节后插入字符串2后的新字符串
				<br />
				<span class="style7">用例:</span>
				<br />
				$newstr=$tools.insert("zhongren","guo",4) $newstr 的值为"zhongguoren"
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			加法运算
		</td>
	</tr>
	<tr>
		<td>
			<p>
				$tools.plus(数字1,数字2) &nbsp;得到&quot;数字1&quot;加&quot;数字2&quot;的结果
				<br />
				$tools.plus(字符串1,字符串2)
				&nbsp;得到&quot;字符串1代表的数字&quot;加&quot;字符串2代表的数字&quot;的结果
				<br />
				<span class="style7">用例:</span>
				<br />
				$count=$tools.plus(3.4)
				<br />
				$count &nbsp;## 结果为7
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			对字符串中的特殊字符进行编码、解码
		</td>
	</tr>
	<tr>
		<td>
			<p>
				$tools.encodes(字符串)
				&nbsp;对&quot;字符串&quot;中的特殊字符进行&quot;UTF-8&quot;编码
				<br />
				$tools.decodes(字符串)
				&nbsp;对&quot;字符串&quot;中的特殊字符进行&quot;UTF-8&quot;解码码
				<br />
				<span class="style7">用例:</span>
				<br />
				$url1=$tools.encodes(url)
				<br />
				$url2=$tools.decodes(url1)
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" class="style5">
			专区公共配置
		</td>
	</tr>
	<tr>
		<td>
			<p>
				1，配置文件一定要按照指定格式进行配置，否则将影响本专区的所有模板。
				<br />
				2，配置文件的变量只能在类别为"栏目"，"专题"，"公共"的模板使用。
				<br />
				<span class="style7">格式如下:</span>
				<br />
				模板名：配置文件<br/>
				类别：配置文件<br/>
				内容设置:<br/>
				#set($变量名="变量值")
				<br />
			</p>
		</td>
	</tr>
</table>
</div> 