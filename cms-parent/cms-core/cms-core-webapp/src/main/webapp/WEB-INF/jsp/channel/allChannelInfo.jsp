<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>全频道管理</title>
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
<style type="text/css">
.mod-tab-hd{overflow:hidden;padding:0 10px;height:32px;line-height:32px;background:#61b0e9;}
.mod-tab-hd .tab-nav{float:left;}
.mod-tab-hd li{float:left;cursor:pointer;text-align:center;width:80px;height:32px;background:none;color:#FFF;}
.mod-tab-hd li.selected{font-weight:700;background:#D9D9D9;color:#474A4B;}
.mod-tab-hd li.hover{background:#EFEFEF;color:#474A4B;}
</style>

</head>
<body>

<div class="mod-tab-hd">
	<ul class="tab-nav" key="head_info">
        <li key="open_channel">开专区</li>
		<li key="channel_info" class="selected">全频道信息</li>
	</ul>
</div>
<div class="main-content">
	
	<!--所有频道列表 start-->
		<div class="user-list">
			<div class="action-bar">
				<form name="search_tag" action="/channel/allChannelInfo.do" method="get" >
					<select class="style-select" name="type"   id="type" >
						<option value="byName" <c:if test="${type eq 'byName'}">selected="selected"</c:if>>频道名</option>
						<option value="byId" <c:if test="${type eq 'byId'}">selected="selected"</c:if>>频道id</option>
					</select>
					<input type="text" name="searchkey" value="${searchkey}" class="style-input fixwidth-l mlm"/>
					
					<label for="">频道分类：</label>
					<select class="style-select" name="category" id="category" >
						<option value=""></option>
			    		<c:forEach var="category" items="${categoryList}">
			    			<option value="${category}" <c:if test="${param.category eq category}">selected="selected"</c:if>>${category}</option>
			    		</c:forEach>
					</select>
					<input type="submit" class="go-search-bt c-green-bt mlm" value="搜索" />
				</form>
			</div>
			<div class="blank20"></div>
			<table class="normal-table" >
				<thead>
					<tr>
						<th>频道id</th>
						<th>频道名称</th>
						<th>频道域名</th>
						<th>分类信息</th>
						<th>是否重点</th>
						<th>是否上线</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="channelInfo" items="${channelInfoPage.data}" varStatus="idx" begin="0">
					 <c:if test="${idx.index%2==0}">
				<tr  class="odd">
					 </c:if>
					<c:if test="${idx.index%2 !=0}">
				<tr >
					 </c:if>
						<td>${channelInfo.id}</td>
						<td><div key="name_${channelInfo.id}" val="${channelInfo.name}">${channelInfo.name}</div></td>
						<td><a href="${channelInfo.domain}" target="_blank">${channelInfo.domain}</a></td>
						<td><div key="category_${channelInfo.id}" val="${channelInfo.category}">${channelInfo.category}</div></td>
						<td>
						 <c:if test="${channelInfo.important}">
								<span >
									<span style="color:green;">
										√
									</span>
								</span>
							</c:if>	
								<c:if test="${!channelInfo.important}">
									<span style="color:red;">
										×
									</span>
								</c:if>
						</td>
						<td>
						 <c:if test="${channelInfo.upline}">
								<span >
									<span style="color:green;">
										√
									</span>
								</span>
							</c:if>	
								<c:if test="${!channelInfo.upline}">
									<span style="color:red;">
										×
									</span>
								</c:if>
						</td>
						<td>
							<c:if test="${userInfo.admin}">
								<a class="mhs"  href="javascript:confirmMessage('确定删除该频道？' ,  '/channel/deleteChannel.do?channelId=${channelInfo.id}')">删除</a>
								|
							    <c:if test="${channelInfo.important}">
									<a href="javascript:confirmMessage('确定将该频道定成非重点？' ,  '/channel/unimportant.do?channelId=${channelInfo.id}')"   >非重点</a>
								</c:if>
								<c:if test="${!channelInfo.important}">
									<a href="javascript:confirmMessage('确定将该频道定成重点？' ,  '/channel/important.do?channelId=${channelInfo.id}') "  >重点</a>
								</c:if>
							    |
							    <c:if test="${channelInfo.upline}">
									<a href="javascript:ajaxMessage('确定将该频道下线？' ,  '/channel/downlineChannel.do', '${channelInfo.id}')"   >下线</a>
								</c:if>
								<c:if test="${!channelInfo.upline}">
									<a href="javascript:ajaxMessage('确定将该频道上线？' ,  '/channel/uplineChannel.do', '${channelInfo.id}')"  >上线</a>
								</c:if>
								|
								<span key="tip_${channelInfo.id}" val="${channelInfo.id}"><a href="javascript:changeInfo('${channelInfo.id}')" >修</a></span>
							</c:if>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${channelInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${channelInfoPage.pageSize}"/>
						<jsp:param name="total" value="${channelInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/channel/allChannelInfo.do?pageNo=:pageNo${queryStr}"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/channel/allChannelInfo.do?pageNo=1${queryStr}"/>
						<jsp:param name="totalPageCount" value="${channelInfoPage.totalPageCount}"/>
			</jsp:include>
			
		</div>
		<!--所有频道列表 end-->
	
</div>	
	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery.form.js"></script>
<script type="text/javascript">
var root = '${ROOT}';
$(function(){
	$("[key=head_info] > [key]").click(function(){
		if($(this).attr("class") != "selected"){
			location.href="/channel/channelManage.do"
		}
	});
	
})

function confirmMessage(message , url){
	if(confirm(message)){
		location.href =url ;
	}
}

function ajaxMessage(message , ajaxurl, channelId){
	
	var msg = "下线";
	if(ajaxurl.indexOf('upline') != -1){
		msg = "上线"
	}
	
	if(confirm(message)){
		$.ajax({
		   type: 'POST',
		   url: ajaxurl,
		   async: false,
		   cache: false,
		   dataType: 'text',
		   data:{"channelId":channelId},
		   success: function(respdata){
			   		if('success' == respdata){
			   			alert("频道["+channelId+"]" + msg + "成功");
			   			location.href=window.location.href;
			   		} else {
			   			alert("频道["+channelId+"]" + msg + "失败：" + respdata);
			   		}
		   }
		});
	}
}

function changeInfo(channelId){
	//alert(channelId);
	//确定|取消
	restoreTip();
	restoreCategory();
	
	var tip='(<a href="javascript:sureChange(\''+channelId+'\')">确定</a>|<a href="javascript:cancelChange(\''+channelId+'\')">取消</a>)';
	$("span[key='tip_"+channelId+"']").html(tip).attr("change", "true");
	
	var srcValue = $("div[key='category_"+channelId+"']").attr("val");
					
	var selectHtml = '<select class="style-select" id="category_'+channelId+'">';
	$("#category > option").each(function(){
		var tmpValue = $(this).attr("value");
		if("" != tmpValue){
			if(srcValue == tmpValue){
				selectHtml += '<option value="'+tmpValue+'" selected="selected">'+tmpValue+'</option>';
			}else{
				selectHtml += '<option value="'+tmpValue+'" >'+tmpValue+'</option>';
			}
		}
	});
	selectHtml += "</select>";
	$("div[key='category_"+channelId+"']").html(selectHtml).attr("change", "true");
	
}
function sureChange(channelId){
	//alert(channelId);
	restoreTip();
	var postValue = $("div[key='category_"+channelId+"']").find("option:selected").attr("value");
	
	if(confirm("确定修改？")){
		$.ajax({
		   type: 'POST',
		   url: '/channel/updateChannelInfo.do',
		   async: false,
		   cache: false,
		   dataType: 'text',
		   data:{"_channelId":channelId, "_category":postValue},
		   success: function(respdata){
			   		if('success' == respdata){
			   			alert("频道["+channelId+"]信息修改成功");
			   			location.href=window.location.href;
			   		} else {
			   			alert("频道["+channelId+"]" + msg + "失败：" + respdata);
			   			restoreCategory();
			   		}
		   }
		});
	}else{
		restoreCategory();
	}
	
}
function cancelChange(channelId){
	//alert(channelId);
	restoreTip();
	restoreCategory();
}

function restoreTip(){
	$("span[key^='tip_']").each(function(){
		if($(this).attr("change")=="true"){
			var storeHtml = '<a href="javascript:changeInfo(\''+$(this).attr("val")+'\')">修</a>';
			$(this).html(storeHtml).attr("change", "false");
		}
	});
}

function restoreCategory(){
	$("div[key^='category_']").each(function(){
		if($(this).attr("change")=="true"){
			$(this).html($(this).attr("val")).attr("change", "false");
		}
	});
}

</script>
</body>
</html>