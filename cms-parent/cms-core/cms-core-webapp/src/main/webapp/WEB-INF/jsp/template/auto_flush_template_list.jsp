<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>            
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>自动刷新模板对象管理</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/plugins/jquery_ui/jquery-ui-1.9.2.custom.min.css"/>
<style type="text/css">

table.normal-table tr.warn td{background-color:red;}

</style>	
</head>
<body >
	<div class="main-content">
		<jsp:include page="/head.do"></jsp:include>
		<div class="blank20"></div>
		<!--创建模版 start-->
		<div class="creat-mod">
			<div class="action-bar">
				<a href="javascript:publishAutoFlushTemplate()" class="set-mod-bt">添加自动刷新模板</a>
				<form name="search_template" id="search_template"  action="/template/autoFlushTemplateList.do" method="get" >
						模板id：<input type="text" id="templateId"  name="templateId"  value="${param.templateId}" class="style-input fixwidth-l mlm">
				<a href="javascript:document.search_template.submit();" class="go-search-bt c-green-bt mlm">搜索</a>
				</form>
			</div>
			
			<div class="blank20"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>频道id</th>
						<th>频道名称</th>
						<th>模板id</th>
						<th> 刷新间隔时间（单位秒）</th>
						<th> 上一次的成功刷新时间</th>
						<th>说明</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="autoFlushTemplateInfo" items="${autoFlushTemplateInfoPage.data}" varStatus="idx" begin="0">
					<c:if test="${ idx.index%2==0}">
						<tr  class="odd"  ondblclick="updateAutoFlushTemplate('${autoFlushTemplateInfo.id}')">
					</c:if>
					<c:if test="${ idx.index%2 !=0}">
					    <tr  ondblclick="updateAutoFlushTemplate('${autoFlushTemplateInfo.id}')">
					</c:if>
						<td><a href="${ROOT }/channel/toChannelMainPage.do?channelId=${autoFlushTemplateInfo.channelInfo.id }">${autoFlushTemplateInfo.channelInfo.id }</a></td>
						<td>${autoFlushTemplateInfo.channelInfo.name }</td>
						<td><a href="${ROOT }/template/toEditTemplatePage.do?templateId=${autoFlushTemplateInfo.templateId }&channelId=${autoFlushTemplateInfo.channelInfo.id }">${autoFlushTemplateInfo.templateId }</a></td>
						
						<td>${autoFlushTemplateInfo.interval }</td>
						<td>${autoFlushTemplateInfo.lastFlushTimeStr }</td>
						<td>${autoFlushTemplateInfo.description }</td>
						<td class="action">
							<a href="javascript:confirmMessage('确定从自动刷新模板中除去?' ,  '/template/deleteAutoFlushTemplate.do?id=${autoFlushTemplateInfo.id}')">删除</a>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${autoFlushTemplateInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${autoFlushTemplateInfoPage.pageSize}"/>
						<jsp:param name="total" value="${autoFlushTemplateInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/template/autoFlushTemplateList.do?channelId=${channelInfo.id}&pageNo=:pageNo"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/template/autoFlushTemplateList.do?channelId=${channelInfo.id}&pageNo=1"/>
						<jsp:param name="totalPageCount" value="${autoFlushTemplateInfoPage.totalPageCount}"/>
			</jsp:include>
		</div>
		<!--创建模版 end-->
	</div>
	
	<div id="publish-dialog" title="发表自动刷新模板对象">
			<iframe height="30px;" src=""  id="publishDialogIframe"
						frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="yes"
						style="width:100%;overflow:hidden;scroll:no;display:block; "
						onload="this.height=(this.contentWindow.document.documentElement.scrollHeight)*0.9" >
				</iframe>
</div>	
	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>    
<script type="text/javascript"  src="${ROOT}/resource/plugins/jquery_ui/jquery-ui-1.9.2.custom.min.js"></script>   
<script type="text/javascript">

$(function (){
	$('#publish-dialog').dialog({
	       autoOpen: false,
	       height: 320,
	       width: 400,
	       modal: true,
	       show: "clip",
    	   hide: "explode",
	       buttons: {
	    	   "提交" : function (){
	    		   document.getElementById("publishDialogIframe").contentWindow.submit();//子窗口的iframe方法
	    		   $('#publish-dialog').dialog( "close" );
	    		   window.setTimeout(function() { 
	    				 location.reload();
	    			  }, 500);
	    	   },
	    	   "取消" : function (){
	    		   $('#publish-dialog').dialog( "close" );
	    	   }
	       }
	});
});

//发表自动刷新对象
function publishAutoFlushTemplate(){
	$('#publishDialogIframe').attr('src' , '/template/toPublishAutoFlushTemplatePage.do');
	$('#publish-dialog').dialog('open');
}

//
function updateAutoFlushTemplate(id){
	$('#publishDialogIframe').attr('src' , '/template/toPublishAutoFlushTemplatePage.do?id=' + id);
	$('#publish-dialog').dialog('open');
}
function confirmMessage(message , url){
	if(confirm(message)){
		location.href =url ;
	}
}
</script>	
</body>
</html>