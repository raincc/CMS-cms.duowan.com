<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>        
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title></title>
	<link rel="stylesheet" href="http://assets.dwstatic.com/b=lego/1.0.0/css&f=mod.pop.css">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/plugins/bigcolorpicker/css/jquery.bigcolorpicker.css" media="all">
	<script src="http://www.duowan.com/public/assets/sys/js/jquery.js?02131514.js"></script>
	<script src="http://assets.dwstatic.com/b=lego/1.0.0/js&f=lego.popupbox.js"></script>
</head>
<body>
<!-- 发表空连接 start-->
	<form action="/article/publishEmptyLink.do" name="editorForm"  id="editorForm"  method="post">
			<input type="hidden"  name="lastUpdateUserId" id="lastUpdateUserId"  value="${userId}" />
			<input type="hidden"  name="userId" id="userId"  value="${userId}" />
			<input type="hidden"  name="articleId" id="articleId"  value="${articleId}" />
			<input type="hidden"  name="id" id="id"  value="${articleId}" />
			<input type="hidden"  name="channelId" id="channelId"  value="${channelId }" />
			<input type="hidden"  name="pictureUrl" id="pictureUrl"  value="${article4TagListInfo.pictureUrl }" />
		<div class="post-link edit-artical">
			<div class="action-bar">
				<p class="form-line">
					<label for="">空链接：</label>
					<input type="text" name="emptyLink" onchange="checkPreViewUrl()" id="emptyLink" value="${article4TagListInfo.emptyLink}" class="style-input fixwidth-400">
				</p>
				<div class="blank10"></div>
				<p class="form-line">
					<label for="">标&nbsp;&nbsp;&nbsp;题：</label>
						<input type="text"  name="title"  id="title" value="${article4TagListInfo.title}"
							  class="style-input fixwidth-l" onchange=""  onkeyup="count(this)"  />
					<em class="note-num mlm"><font id="title_count">0</font></em>
					<em class="c-red-font mlm"> <font  id="title_check" ></font></em>
					<span class="form-item mlm">
						标题颜色：
						<input type="text" id="titleColor" name="titleColor"  value="${article4TagListInfo.titleColor}"  class="style-input fixwidth-s"></span>
				</p>
				<div class="blank10"></div>
				<p class="form-line">
					<label for="">副标题：</label>
					<input type="text" id="subtitle" name="subtitle"  value="${article4TagListInfo.subtitle}" class="style-input fixwidth-s">
					<span class="form-item mlm">
						来源：<input type="text" id="source" name="source" value="${article4TagListInfo.source}" class="style-input fixwidth-s">
					</span>
					<span class="form-item mlm">作者：<input type="text" id="author" name="author"  value="${article4TagListInfo.author}"  class="style-input fixwidth-s"></span>
				</p>
				<div class="blank10"></div>
				<p >
					<iframe  src=""  frameborder="0" height="40" width=""  style="width: 860px;" id="uploadHeadPicIframe"  
							scrolling="no" allowtransparency="yes" onload="this.height=this.contentWindow.document.documentElement.scrollHeight" >
					</iframe>
				</p>
				<div class="blank10"></div>
				<p class="form-line">
					<span class="form-item mlm">
					权重：<input type="text" id="power" name="power" value="${article4TagListInfo.power}" class="style-input fixwidth-s">
					</span>
					<span class="form-item mlm">
						<input type="text" id="digest" name="digest" value="${article4TagListInfo.digest}" placeholder="摘要：" class="style-input fixwidth-l">
					</span>
				</p>
			</div>
			<div class="blank10"></div>
				<jsp:include page="/article/toArticleTagInclude.do?width=400&currentTag=${currentTag }"></jsp:include>
			<div class="blank10"></div>
			<label for="">发布时间：</label>
			<input type="text" name="publishTimeStr"  id="publishTimeStr" 
								 class="style-input fixwidth-m"  autocomplete="off" 
									onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									value="${article4TagListInfo.publishTimeStr}" />
			<div class="blank10"></div>
			<div class="center">
				<span class="ui-button"><a href="javascript:parent.submitEmptyLink()">发表空连接</a></span>
			</div>
		</div>
		</form>
		<!--发表空连接 end-->
  		
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>    
<script type="text/javascript"  src="${ROOT}/resource/plugins/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/plugins/bigcolorpicker/js/jquery.bigcolorpicker.js"></script>
<script type="text/javascript">
var channelId = '${channelId}';
var userId = '${userId}';
var articleId = '${articleId}';
var tagJsonData = '${tagJsonData}';//"发布器普通标签"的下拉框内容的json数据
var root = '${ROOT}';//发布器项目根目录
var specialTagArr = [];//存放特殊标签的数组
var isLegalTitle = true;
$(function (){
	<c:forEach var="specialTag" items="${specialTagList}" varStatus="idx" begin="0">
		specialTagArr.push('${specialTag.name}');
	</c:forEach>
	$('#uploadHeadPicIframe').attr('src' , '/article/toUploadHeadPicIframe.do?channelId=${channelId }&articleId=${articleId}&preHeadPic='+encodeURIComponent('${article4TagListInfo.pictureUrl}') );
	$("#titleColor").bigColorpicker("titleColor","L",10);
});


function validateForm(){
	if($('#emptyLink').val().replace(/\s*/g, '') == ''){
		alert('空链接不能为空');
		return false ;
	}
	if($('#tags').val().replace(/\s*/g, '') == ''){
		alert('编辑发空链接 ， 空链接标签不能为空');
		return false ;
	}
	return validateFormCommonPart();
}
function submit(){
	if(!validateForm()){
		return 'false';
	}
	$('#pictureUrl').val(document.getElementById("uploadHeadPicIframe").contentWindow.getHeadPicFile());
	$('#editorForm').submit();
}

function checkTitleInTag(){
    document.getElementById('title_check').innerHTML='正在检查标题是否可用....';
    var title=document.getElementById('title').value;
    if(title==''){
    	document.getElementById('title_check').innerHTML= '标题不能为空!';
      return;
    }
    if(title.length/2 > 100){
    	document.getElementById('title_check').innerHTML= '字数超过上限100';
    	return ;
    }
	$.ajax({
	   type: 'GET',
	   url: '/article4TagList/checkTitle.do?title=' + title+'&channelId=' + channelId+'&articleId='+articleId,
	   async: false,
	   cache: false,
	   dataType: 'text',
	   success: function(data){
                 if(-1 != data.indexOf('exist')){
                    document.getElementById('title_check').innerHTML='标题已存在,可以使用';
                    document.getElementById('title').focus();
                 }else{
                    document.getElementById('title_check').innerHTML='该标题可以使用.';
                 }
	   }
	});
}

function checkPreViewUrl(){
	var title=document.getElementById('emptyLink').value;
	if(title.indexOf('.cms.duowan.com') != -1){
		alert("空链接为预览地址，请注意修改!");
		$("#emptyLink").select().focus();
	}
}
</script>
<!-- 业务JS代码必须放在末尾，否则报错导致应用不能运行 -->
<script type="text/javascript"  src="${ROOT}/resource/js/article/article_common.js"></script>
</body>
</html>