<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>全频道管理</title>
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
<style type="text/css">
.mod-tab-hd{overflow:hidden;padding:0 10px;height:32px;line-height:32px;background:#61b0e9;}
.mod-tab-hd .tab-nav{float:left;}
.mod-tab-hd li{float:left;cursor:pointer;text-align:center;width:80px;height:32px;background:none;color:#FFF;}
.mod-tab-hd li.selected{font-weight:700;background:#D9D9D9;color:#474A4B;}
.mod-tab-hd li.hover{background:#EFEFEF;color:#474A4B;}
</style>

</head>
<body>

<div class="mod-tab-hd">
	<ul class="tab-nav" key="head_info">
        <li class="selected" key="open_channel">开专区</li>
		<li key="channel_info">全频道信息</li>
	</ul>
</div>
<div class="main-content">
	
	<div key="open_channel_div">
		<form action="/channel/openNewChannel.do"  method="post"  id="openNewChannelForm" >
			<table align="center" width="60%" border="0">
				  <thead>
				  	<tr align="center">
				  		<td colspan="2">
				  			新专区信息登记
				  			<div class="blank15"></div>
				  		</td>
				  	</tr>
				  </thead>
				  <tbody>
				  	<tr>
				    <td align="right">
				    	<label for="">是否重点频道：</label>
				    </td>
				    <td >
				    	<input type="checkbox" name="important" id="important" value="false">
				    	<div class="blank15"></div>
					</td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">是否立即上线：</label>
				    </td>
				    <td >
				    	<input type="checkbox" name="upline" id="upline" value="false">
				    	<div class="blank15"></div>
					</td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">频道id：</label>
				    </td>
				    <td>
				    	<input type="text" name="id" id="id" size="35" class="style-input"/>
						<em class="note-num c-red-font"  >
					 		<font id="channelId_check" ></font>
						</em>
						<div class="blank15"></div>
					</td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">频道名称：</label>
			    	</td>
				    <td>
				    	<input type="text"  name="name"  id="name" onblur="checkChannelName()" size="35" class="style-input" />
				    	<em class="note-num c-red-font"  >
					 		<font id="channelName_check" ></font>
						</em>
						<div class="blank15"></div>
				    </td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">频道分类：</label>
			    	</td>
				    <td>
				    	<select class="style-select" name="category"   id="category" >
				    		<c:forEach var="category" items="${categoryList}">
				    			<option value="${category}" >${category}</option>
				    		</c:forEach>
						</select>
				    	<div class="blank15"></div>
				    </td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">频道域名：</label>
			    	</td>
				    <td>
				    	<input type="text"  name="domain"  id="domain" readonly="readonly"  size="35" class="style-input"/>
				    	<div class="blank15"></div>
				    </td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">频道文章的存放地址：</label>
				    </td>
				    <td>
				    	<input type="text"  name="articleFilePath"  id="articleFilePath" readonly="readonly" size="35" class="style-input"/>
				    	<div class="blank15"></div>
				    </td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">图片域名：</label>
				    </td>
				    <td>
				    	<input type="text"  name="picDomain"  id="picDomain" readonly="readonly" size="35" class="style-input"/>
				    	<div class="blank15"></div>
				    </td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">频道图片的存放地址 ：</label>
				    </td>
				    <td>
						<input type="text"  name="picFilePath"  id="picFilePath" readonly="readonly" size="35" class="style-input"/>
						<div class="blank15"></div>						    
			    	</td>
				  </tr>
				  <tr>
				    <td align="right">
				    	<label for="">主编权限 ：</label>
				    </td>
				    <td>
						<input type="text"  name="mainEditor"  id="mainEditor" size="35" class="style-input"/>
						<em class="note-num c-red-font"  >
					 		<font id="mainEditor" >多个主编请用"，"或"、"分隔</font>
						</em>
						<div class="blank15"></div>						    
			    	</td>
				  </tr>
				  <tr>
				  	<td colspan="2" align="center">
				  		<span class="ui-button"><a href="javascript:submitNewChannelInfo()">确定</a></span>
				  	</td>
				  </tr>
			  </tbody>
			</table>
			
		</form>
	</div>
	
</div>	
	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery.form.js"></script>
<script type="text/javascript">
var root = '${ROOT}';
$(function(){
	$("[key=head_info] > [key]").click(function(){
		if($(this).attr("class") != "selected"){
			location.href="/channel/allChannelInfo.do"
		}
	});
	
	$("#id").bind("blur",function(){
		checkNewChannelId();
		generateRelateInfo();
	});
	
	$("#id").bind("keyup",function(){
		generateRelateInfo();
	})
})

function submitNewChannelInfo(){
	if($("#channelId_check").html() != "该频道id可以使用."){
		alert("请检查频道id!");
		$("#id").css({"border-color":"red"}).select();
		return ;
	}
	
	if($("#channelName_check").html() != "该频道名称可以使用."){
		alert("请检查频道名称!");
		$("#name").css({"border-color":"red"}).select();
		return ;
	}
	$("#important").val(document.getElementById("important").checked);
	$("#upline").val(document.getElementById("upline").checked);
	var options = { 
        url:'/channel/openNewChannel.do', //提交给哪个执行
        type:'POST', 
        dataType: 'text',
        success: function(data){
        		if(data.indexOf('success') != -1){
        			alert('新专区(频道):'+$("#id").val()+'开通成功!');
        			location.href=root;
        		}else if(data.indexOf('fail') != -1){
        			$("#channelId_check").html("");
        			$("#channelName_check").html("");
        			alert('新专区(频道)'+$("#id").val()+'开通失败!!' + data);
        		}else{
        			alert('新专区(频道):'+$("#id").val()+'开通成功!但主编加入失败：' + data);
        			location.href=root;
        		}
        }, //显示操作提示
        error: function(){
        	alert('服务器连接超时，请稍后重试');
        	$("#channelId_check").html("");
        	$("#channelName_check").html("");
        },   
        timeout:10000 
	};
	$('#openNewChannelForm').ajaxSubmit(options); 
	
}

function generateRelateInfo(){
	
	var domain = "http://?.duowan.com";
	var articleFilePath = "/data2/www/cms.duowan.com/?";
	var picDomain = "http://img.dwstatic.com/?";
	var picFilePath = "/data2/www/pic.duowan.com/?";
	
	var channelId = $("#id").val().trim();
	if("" != channelId){
		$("#domain").val(domain.replace("?",channelId));
		$("#articleFilePath").val(articleFilePath.replace("?",channelId));
		$("#picDomain").val(picDomain.replace("?",channelId));
		$("#picFilePath").val(picFilePath.replace("?",channelId));
	}
}

function checkNewChannelId(){
    var newChannelId = $("#id").val().trim();
    var reg = new RegExp("^[0-9a-zA-Z]{1,18}$");
	if(!reg.test(newChannelId)){
		$("#id").css({"border-color":"red"}).select();
		$("#channelId_check").html("频道id只允许1-18个字母或数字!");
		return ;
	}
    $("#channelId_check").html("正在检测频道id是否可用....");
	
    $.ajax({
	   type: 'GET',
	   url: '/channel/checkNewChannelId.do?newChannelId=' + newChannelId,
	   async: false,
	   cache: false,
	   dataType: 'text',
	   success: function(data){
		   		if('success' == data){
		   			$("#id").css({"border-color":""})
		   			$("#channelId_check").html("该频道id可以使用.");
		   		} else {
		   			$("#id").css({"border-color":"red"}).select();
		   			$("#channelId_check").html("该频道id已存在,请更换.");
		   		}
	   }
	});
}

function checkChannelName(){
	
	var newChannelName = $("#name").val().trim();
	if(newChannelName =="" || newChannelName.length > 32){
		$("#name").css({"border-color":"red"}).select();
		$("#channelName_check").html("频道名称不能为空且字数必须小于32个!");
		return ;
	}
    $("#channelName_check").html("正在检测频道名称是否可用....");
	
    $.ajax({
	   type: 'GET',
	   url: '/channel/checkNewChannelName.do?newChannelName=' + newChannelName,
	   async: false,
	   cache: false,
	   dataType: 'text',
	   success: function(data){
		   		if('success' == data){
		   			$("#name").css({"border-color":""})
		   			$("#channelName_check").html("该频道名称可以使用.");
		   		} else {
		   			$("#name").css({"border-color":"red"}).select().focus();
		   			$("#channelName_check").html("该频道名称已存在,请更换.");
		   		}
	   }
	});
	
}

</script>
</body>
</html>