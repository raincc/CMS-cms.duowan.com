<%@page import="com.duowan.cms.util.Constant"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>默认异常页面</title>
</head>
<body>
未知错误！请到"双版运行"群反馈，群号:3535005
<br />
<%
	 Logger logger = Logger.getLogger("默认异常页面");
	//StackTraceElement[]elementArr  =((Throwable)request.getAttribute("javax.servlet.error.exception")).getStackTrace();
	logger.error("应用程序出错:", ((Throwable)request.getAttribute("javax.servlet.error.exception")));
	String errorMessage = ((Throwable)request.getAttribute("javax.servlet.error.exception")).getMessage() ;
%>
<%=errorMessage %>
</body>
</html>