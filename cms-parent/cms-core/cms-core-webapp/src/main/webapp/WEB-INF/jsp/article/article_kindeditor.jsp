<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>编辑文章页</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<link rel="stylesheet" href="http://assets.dwstatic.com/b=lego/1.0.0/css&f=mod.pop.css">
	 <link rel="stylesheet" type="text/css" href="${ROOT }/resource/plugins/jquery_ui/jquery-ui-1.9.2.custom.min.css"/>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/plugins/bigcolorpicker/css/jquery.bigcolorpicker.css" media="all">
	<link rel="stylesheet" href="${ROOT}/resource/plugins/kind_editor/themes/default/default.css" />
	<script src="http://www.duowan.com/public/assets/sys/js/jquery.js?02131514.js"></script>
	<script src="http://assets.dwstatic.com/common/js/jquery.placeholder.min.js"></script>
	<script src="http://assets.dwstatic.com/b=lego/1.0.0/js&f=lego.popupbox.js"></script>
<script type="text/javascript">
var channelId = '${channelId}';
var tagJsonData = '${tagJsonData}';//"发布器普通标签"的下拉框内容的json数据
var root = '${ROOT}';//发布器项目根目录
var picUrlOnCms = '${picUrlOnCms}';// 获取图片在发布器服务器的URL
var picUrlOnline = '${picUrlOnline}';// 获取图片在外网的URL
var articleId = '${articleId}';
var userId = '${userId}';
var specialTagArr = [];//存放特殊标签的数组
var isLegalTitle = true;
//标题焦点标记：用于标记上一焦点是否在"标题"输入框:id=title的onfocus事件设为true,onblur事件设为false;
//目的是使只有上一焦点是id=title的输入框，才触发UEditor的click事件
var isFocusTitleFlag = false;
var templateId = '${articleInfo.templateId}';
var countUploadTime = 0;

</script>	
<style type="text/css">
.fixwidth-800{width:800px;}
.ke-icon-formathtml {
	background-position: 0px -1056px;
	width: 16px;
	height: 16px;
}
.ke-icon-uploadimages {
	background-position: 0px -1232px;
	width: 16px;
	height: 16px;
}
</style>
</head>
<body>
	<div class="main-content">
	<jsp:include page="/head.do"></jsp:include>
	<div class="blank10"></div>
		<!--修改文章 start-->
		<form action="/article/publish.do"  name="editorForm"  id="editorForm"  method="post">
			<input type="hidden"  name="zoneUserId" id="zoneUserId" />
			<input type="hidden"  name="content" id="content" />
			<input type="hidden"  name="lastUpdateUserId" id="lastUpdateUserId"  value="${userId}" />
			<input type="hidden"  name="userId" id="userId"  value="${userId}" />
			<input type="hidden"  name="id" id="articleId"  value="${articleId}" />
			<input type="hidden"  name="channelId" id="channelId"  value="${channelId }" />
			<input type="hidden"  name="isAddArticle" id="isAddArticle"  value="${isAddArticle }" />
			<input type="hidden"  name="pictureUrl" id="pictureUrl"  value="${articleInfo.pictureUrl }" />
			<input type="hidden"  name="logs" id="logs"  value="${articleInfo.logs }" />
			<input type="hidden"  name="tagPictureUrl" id="tagPictureUrl"  value="${articleInfo.tagPictureUrl }" />

		<div class="edit-artical">
			<div class="action-bar">
				<p class="form-line">
					<label for="title">标题：</label>
					    <input type="text" value="${articleInfo.title}" name="title" id="title"  class="style-input fixwidth-360"  onfocus="focusTitleInput()"
							 onblur="checkTitle()"  onkeyup="count(this)"   />
					<em class="note-num mlm"><font id="title_count"></font></em>
					<em class="c-red-font mlm"><font  id="title_check" ></font></em>
					<span class="form-item mlm"></span>
					<span class="form-item mlm"></span>
					<c:if test="${!isAddArticle}">
								<a class="ui-link-bt"  href="${articleInfo.urlOnLine }"  target="_blank">本篇</a> 
					</c:if>
					<span class="form-item mlm">
							<c:if test="${!isAddArticle}">
								<input type="checkbox"  name="isChangeAllTags"  id="isChangeAllTags"  >修改所有tag
							</c:if>
					</span>
					<span class="form-item mlm">
						<c:if test="${!isAddArticle}">
							编辑:${articleInfo.userId}
						 </c:if>
					</span>
					<span class="form-item mlm">
							<label for="title">标题颜色：</label>
							<input type="text" value="${articleInfo.titleColor}" name="titleColor" id="titleColor"  class="style-input fixwidth-s"   />
					</span>
				</p>
				<div class="blank10"></div>
				<p class="form-line">
					<label for="subtitle">副标题：</label>
					<input type="text" value="${articleInfo.subtitle}"  name="subtitle" id="subtitle" class="style-input fixwidth-s"  />
		             <span class="form-item mlm">
							来源：<input type="text" value="${articleInfo.source}" name="source" id="source" class="style-input fixwidth-s" />
					</span>
					<span class="form-item mlm">
						作者：<input type="text" value="${articleInfo.author}" name="author"  id="author" class="style-input fixwidth-s">
					</span>
					<span class="form-item mlm">
							<input type="checkbox"  name="needComment"  id="needComment"  <c:if test="${articleInfo.threadId != 0}">checked="checked"</c:if>>评论
					</span>
					<span class="form-item mlm">
							<input class="mrs" type="checkbox" name="isShowAllPage" id="isShowAllPage" 
									<c:if test="${articleInfo.isShowAllPage == 'on'}"> checked="checked" </c:if> />下拉分页
					</span>
					<span class="form-item mlm">模板：
							<select class="style-select" name="templateId" id="templateId">
								<c:forEach var="templateInfo" items="${templateInfoPage.data}" varStatus="idx" begin="0">
									<option value="${templateInfo.id }">${templateInfo.name }</option>
								</c:forEach>	
							</select>
					</span>

				</p>
			</div>
	
			<div class="blank10"></div>
			<div class="edit-artical-ctn">
					<textarea cols="100" rows="20" id="editor" name="editor">${articleInfo.content}</textarea>
					<div style="position: absolute;bottom:0px; width:100%; right: 0px;">
						<span style="position: absolute;right: 0px;">
						    <span id="wordCountSpan" >您当前输入了 <span class="word_count">0</span> 个文字</span>
							<span id="autoSaveTips"></span>
							<span id="autoSave">
								<span id="openAutoSave" style="display: none">
									<a href="javascript:openAutoSave()">开启自动保存</a>&nbsp; | &nbsp;
								</span>
								<span id="closeAutoSave">
									<a href="javascript:closeAutoSave()">关闭自动保存</a> &nbsp; | &nbsp;
								</span> 
								<a href="javascript:saveData()">保存</a> &nbsp; | &nbsp;
								<a href="javascript:restoreData()">恢复</a> &nbsp; | &nbsp; 
								<a href="javascript:releaseData()">清空</a> &nbsp; | &nbsp;
							</span>
						</span>
					</div>
			</div>

			
			
			<jsp:include page="/article/toArticleTagInclude.do?width=800"></jsp:include>
	
			<div class="set-mod">
				<div class="set-box-ctn clearfix">
						<div class="set-box">
							<textarea name="digest" id="digest" placeholder="摘要">${articleInfo.digest}</textarea>
						</div>
							<div class="set-box">
								<textarea name="diy1" id="diy1" placeholder="自定义1:">${articleInfo.diy1}</textarea>
						</div>
						
						<div class="set-box">
							<textarea name="diy2" id="diy2"  placeholder="自定义2:">${articleInfo.diy2}</textarea>
						</div>
						<div class="set-box">
							<textarea name="diy3" id="diy3" placeholder="自定义3:">${articleInfo.diy3}</textarea>
						</div>
						<div class="set-box"  id="diy4Area" >
								<textarea name="diy4" id="diy4"  placeholder="自定义4:">${articleInfo.diy4}</textarea>
						</div>
						<div class="set-box" id="diy5Area"  >
								<textarea name="diy5" id="diy5"  placeholder="自定义5:">${articleInfo.diy5}</textarea>
						</div>
					</div>
			</div>
	
			<div class="blank10"></div>
			<div class="extend-set">
					<p class="form-line">
					<c:if test="${articleInfo.isEmptyStatus || articleInfo.status.display == '预定发布的文章'}">
						<label for="">发布时间：</label>
						<input type="text" name="prePublishTimeStr"  id="prePublishTimeStr" 
								 class="style-input fixwidth-m"  autocomplete="off" 
									onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									value="<c:if test="${articleInfo.hasPrePublishTime}">${articleInfo.prePublishTimeStr}</c:if>" />
						</c:if>
	
						<span class="form-item mlm">
							权重：
								<input class="style-input fixwidth-s" name="power" id="power"  value="${articleInfo.power}">
							</span>
						<span class="form-item mlm">
							相关游戏：
								<input type="text"  id="relateGame" name="relateGame" value="${articleInfo.relateGame}"  class="style-input fixwidth-s">
						</span>
				
					<iframe src=""   frameborder="0" height="40"    id="uploadHeadPicIframe" 
					scrolling="no" allowtransparency="yes" onload="this.height=this.contentWindow.document.documentElement.scrollHeight" >
					   </iframe>
				
					</p>
				</div>
			
			<div class="blank10"></div>
			<div class="center">
				<span class="ui-button"><a href="javascript:submit()">发表文章</a></span>
			</div>
			
			<div class="log-list">
				<c:if test="${!isAddArticle}"> 
					<div class="action">
						<span class="ui-button"><a href="javascript:hideArticle()">隐藏</a></span>
						<span class="ui-button mlm">
							<a href="javascript:deleteArticle('${channelId }',  '${articleId}' )">彻底删除</a>
						</span>
						<span class="ui-button mlm">
							<a href="javascript:showLog()"  id="checkLogButton">查看日志</a>
						</span>
					</div>
				 </c:if>
				<div class="blank10"></div>
				<ul class="list" style="display:none;" id="logArea">
					<c:forEach var="log" items="${logArr}" varStatus="idx" begin="0">
						<li>${log}</li>
					</c:forEach>	
				</ul>
			</div>

		</div>
		</form>
		<c:if test="${!isAddArticle}"> 
			<div style="border:1px solid #4f748e;padding:10px;margin-top:10px;background-color:#f0f0f0;">
			    <form action="/article/updatePublishTime.do" method="post" id="updatePublishTimeForm" name="updatePublishTimeForm">
			  	     修改文章日期:&nbsp;&nbsp;<input type="text" name="newPublishTimeStr"  id="newPublishTimeStr" 
						 class="style-input fixwidth-m"  autocomplete="off" 
							onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							 />
					<span class="ui-button mlm">
						<a href="javascript:updatePublishTime();"  id="updatePublishTime">提交</a>
					</span>
					<input type="hidden" id="articleId" name="articleId" value="${articleId}" />
			        <input type="hidden" id="channelId" name="channelId" value="${channelId}" />
				</form>
			</div>
			<div class="blank10"></div>
		</c:if>
		<!--修改文章 end-->
	</div>

	<!--发表空连接弹出框 end-->	
<script type="text/javascript"  src="${ROOT}/resource/plugins/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/plugins/bigcolorpicker/js/jquery.bigcolorpicker.js"></script>
<!-- kindEditor -->
<script  type="text/javascript" src="${ROOT}/resource/plugins/kind_editor/kindeditor-all.js"></script>
<script  type="text/javascript" src="${ROOT}/resource/plugins/kind_editor/lang/zh_CN.js"></script>
<script  type="text/javascript" src="${ROOT}/resource/plugins/kind_editor/getAllColor.js"></script>


<!-- 设置全局使用的变量 -->
<script type="text/javascript">

$(function (){
	<c:forEach var="specialTag" items="${specialTagList}" varStatus="idx" begin="0">
		specialTagArr.push('${specialTag.name}');
	</c:forEach>
	if(templateId != ''){
		$('#templateId option[value='+templateId+']').attr('selected', true); 
	}
	$('#uploadHeadPicIframe').attr('src' , '/article/toUploadHeadPicIframe.do?channelId=${channelId }&articleId=${articleId}&preHeadPic='+encodeURIComponent('${articleInfo.pictureUrl}') );
	$("#titleColor").bigColorpicker("titleColor","L",10);

});

KindEditor.lang({
	formathtml : '格式化文档',
	uploadimages : '批量传图'
});

var editor;
KindEditor.ready(function(K) {
	editor = K.create('#editor', {
		allowFileManager : true , 
		resizeType : 0,//2或1或0，2时可以拖动改变宽度和高度，1时只能改变高度，0时不能拖动。
		width: '98%' ,
		height:'300px',
		//wellFormatMode : false,
		//pagebreakHtml:'<div style="page-break-after: always;"><span style="display:none">&nbsp;</span></div>' ,//使用kind_editor默认的换行符
		items : ["source","|","undo","redo","|","preview","code","cut","copy","paste",
		         "plainpaste","wordpaste","|","justifyleft","justifycenter","justifyright","justifyfull",
		         "insertorderedlist","insertunorderedlist","indent","outdent","subscript","superscript",
		         "clearhtml","formathtml","uploadimages","selectall","|","fullscreen","/","formatblock",
		         "fontname","fontsize","|","forecolor","hilitecolor","bold","italic","underline",
		         "strikethrough","lineheight","|","flash","media",
		         "table","hr","pagebreak","anchor","link","unlink"],
		colorTable : getAllColorArray(),        
		 htmlTags : {
		    		font : ['id', 'class', 'color', 'size', 'face', '.background-color'],
		    		span : [
		    			'id', 'class', '.color', '.background-color', '.font-size', '.font-family', '.background','.display',
		    			'.font-weight', '.font-style', '.text-decoration', '.vertical-align', '.line-height'
		    		],
		    		div : [
		    			'id', 'class', 'align', '.border', '.margin', '.padding', '.text-align', '.color',
		    			'.background-color', '.font-size', '.font-family', '.font-weight', '.background',
		    			'.font-style', '.text-decoration', '.vertical-align', '.margin-left','.page-break-after'
		    		],
		    		table: [
		    			'id', 'class', 'border', 'cellspacing', 'cellpadding', 'width', 'height', 'align', 'bordercolor',
		    			'.padding', '.margin', '.border', 'bgcolor', '.text-align', '.color', '.background-color',
		    			'.font-size', '.font-family', '.font-weight', '.font-style', '.text-decoration', '.background',
		    			'.width', '.height', '.border-collapse'
		    		],
		    		'td,th': [
		    			'id', 'class', 'align', 'valign', 'width', 'height', 'colspan', 'rowspan', 'bgcolor',
		    			'.text-align', '.color', '.background-color', '.font-size', '.font-family', '.font-weight',
		    			'.font-style', '.text-decoration', '.vertical-align', '.background', '.border'
		    		],
		    		a : ['id', 'class', 'href', 'target', 'name'],
		    		embed : ['id', 'class', 'src', 'width', 'height', 'type', 'loop', 'autostart', 'quality','pluginspage',
		    		         '.width', '.height', 'align', 'allowscriptaccess' , 'wmode' , 'allowFullscreen' , 'play' , 'menu' ],
		    		img : ['id', 'class', 'src', 'width', 'height', 'border', 'alt', 'title', 'align', '.width', '.height', '.border'],
		    		'p,ol,ul,li,blockquote,h1,h2,h3,h4,h5,h6' : [
		    			'id', 'class', 'align', '.text-align', '.color', '.background-color', '.font-size', '.font-family', '.background',
		    			'.font-weight', '.font-style', '.text-decoration', '.vertical-align', '.text-indent', '.margin-left'
		    		],
		    		pre : ['id', 'class'],
		    		hr : ['id', 'class', '.page-break-after'],
		    		'br,tbody,tr,strong,b,sub,sup,em,i,u,strike,s,del' : ['id', 'class'],
		    		iframe : ['id', 'class', 'src', 'frameborder', 'width', 'height', '.width', '.height']
		    	},
	    	afterChange : function() {
	    		var count = this.count();
	    		K('.word_count').html(this.count());
	    		if(count > 60000){//
	    			$('#wordCountSpan').css('color','red');
	    		}else{
	    			$('#wordCountSpan').css('color','');
	    		}
				
			} 	
	  });
});
</script>


<!-- 业务JS代码必须放在末尾，否则报错导致应用不能运行 -->
<script type="text/javascript"  src="${ROOT}/resource/js/article/article_edit_kindeditor.js"></script>
</body>
</html>