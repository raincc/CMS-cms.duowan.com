<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@page import="com.duowan.cms.dto.template.TemplateInfo"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>     
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>编辑模板</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/plugins/code_mirror/codemirror.css" media="all">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/plugins/code_mirror/eclipse.css" media="all">
</head>
<body>
	<div class="main-content">
		<jsp:include page="/head.do"></jsp:include>
		<div class="blank10"></div>
<form action=""  name="editorForm"  id="editorForm"  method="post">
	<input type="hidden"  name="channelId" id="channelId"  value="${channelInfo.id }" />
	<input type="hidden"  name="templateId" id="templateId"  value="${templateInfo.id }" />
	<input type="hidden"  name="id" id="id"  value="${templateInfo.id }" />
	<input type="hidden"  name="content" id="content" />
	<input type="hidden"  name="submitType" id="submitType" />
	<input type="hidden"  name="userId" id="userId"  value="${userInfo.userId}" />
	<input type="hidden"  name="logs" id="logs"  value="${templateInfo.logs }" />
	<input type="hidden"  name="lastUpdateUserId" id="lastUpdateUserId"  value="${userInfo.userId}" />

	  <c:if test="${!isAddTemplate}">
		<input type="hidden"  name="createUserId" id="createUserId"  value="${templateInfo.createUserId }" />
	</c:if>
	  <c:if test="${isAddTemplate}">
	  	<input type="hidden"  name="createUserId" id="createUserId"  value="${userInfo.userId}" />
	  </c:if>
	  
	
 <div class="edit-mod">
			<table class="layout-table">
				<tbody>
					<tr>
						<td><label for="">模版名称：</label></td>
						<td>
							<c:if test="${!isAddTemplate}">
								<input  class="style-input fixwidth-l style-input-disable"  type="text"  id="name"  name="name" value="${templateInfo.name}"  disabled="true">
							</c:if>
							<c:if test="${isAddTemplate}">	
								<input class="style-input  fixwidth-l"  type="text"  id="name"  name="name"  onchange="checkTemplateName()">
							</c:if>
							<em class="note-num c-red-font"  ><font  id="template_name_check" ></font></em>
				     </td>
						<td><label for="">分类：</label></td>
						<td>
							<input class="style-input fixwidth-s" type="">
							<select class="style-select" name="templateCategorySelector"   id="templateCategorySelector"  onchange="changeTemplateCategory(this.value)">
										<option value="栏目" <c:if test="${powerValue > 5}">selected="selected"</c:if>>栏目</option>
										<option value="专题" <c:if test="${powerValue == 5}">selected="selected"</c:if>>专题</option>
										<option value="标签">标签</option>
										<option value="标签图">标签图</option>
										<option value="最终文章">最终文章</option>
										<option value="投票">投票</option>
										<option value="投票结果">投票结果</option>
										<option value="公共">公共</option>
										<option value="投票结果(新版)">投票结果(新版)</option>
										<option value="配置文件">配置文件</option>
										<option value="资讯推广">资讯推广</option>
										
							</select>
							<label class="mlm" for="">编码：</label>
							<select class="style-select" name="code" id="code">
											<option value="UTF-8">UTF-8</option>
											<option value="GBK">GBK</option>
							</select>
						</td>
						<td>
							<label for="">别名：</label>
							<input class="style-input fixwidth-m" type="text" id="alias" name="alias" value="${templateInfo.alias}" onblur="checkAlias()"  />
							<em class="note-num c-red-font"  ><font  id="template_alias_check" ></font></em>
						</td>
					</tr>
					<tr>
						<td><label for="">说明：</label></td>
						<td>
							<input class="style-input fixwidth-l" type="text" id="digest" name="digest"  value="${fn:escapeXml(templateInfo.digest)}" />
						</td>
						<td><label for=""><c:if test="${!isAddTemplate}">涉及tag：</c:if></label></td>
						<td>
							<c:if test="${!isAddTemplate}">
								<input class="style-input style-input-disable fixwidth-l" disabled="true" type="text"   value="${templateInfo.relatedtag}">
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="blank10"></div>
			<div class="content-input"  style="height: 530px;">
				<textarea name="content_current" id="content_current"  style="height: 100%;">${content}</textarea>
				<div class="content-input"  id="template_pre1" style="display:none">
					<textarea name="content2" id="content2">${content2}</textarea>
					<textarea name="content3" id="content3">${content3}</textarea>
				</div>
			</div>
			<div class="action-bar clearfix">
				<p class="info"  id="templateInfoTips">
					模板信息：正在使用的模板 ${templateInfo.updateTimeStr}
				</p>
				<div class="fl">
					
					 <c:if test="${!isAddTemplate}">
						<input type="button" class="u-inputBtn" value="上一版本" onclick="preVision()" >
						<input type="button" class="u-inputBtn" value="下一版本"  onclick="nextVision()" >
						<input type="button" class="u-inputBtn" value="查看日志"  id="toggleLog">
					 </c:if>
					<input type="button" class="u-inputBtn" value="语法说明" id="toggleGrammar">
				</div>
				<div class="fr">
						<span class="ui-button mrm"><a href="javascript:submit('onlySubmit')">提交</a></span>
						<span class="ui-button"><a href="javascript:submit('submitAndPreview')">提交并预览</a></span>
				</div>
			</div>
			<div class="blank10"></div>
			<jsp:include page="/template/toGrammarPage.do"></jsp:include>
			
			<div class="log-list" id="logList" style="display:none">
				<div class="box">
				    <div class="box-hd">
				        <h3 class="title center">日志</h3>
				    </div>
				    <div class="box-bd">
				    	<ul>
				    		<li><a id="firstLineLog"  > </a></li>
							<c:forEach var="log" items="${logArr}" varStatus="idx" begin="0">
								<li>${fn:substring(log,0,220)}</li>
							</c:forEach>	
				    	</ul>
				    </div>
				</div>
			</div>
		
		</div>
		
		
		</form>

	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>    	
<script type="text/javascript"  src="${ROOT}/resource/plugins/code_mirror/codemirror.js"></script>    	
<script type="text/javascript"  src="${ROOT}/resource/plugins/code_mirror/velocity.js"></script>    	
<script type="text/javascript"  src="${ROOT}/resource/plugins/code_mirror/htmlembedded.js"></script>    	
<script type="text/javascript"  src="${ROOT}/resource/plugins/code_mirror/htmlmixed.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/plugins/code_mirror/css.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/plugins/code_mirror/javascript.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/plugins/code_mirror/xml.js"></script>	
<script type="text/javascript">
$(function (){
	$('#toggleLog').click(function (){
		if($('#logList').css('display') == 'none'){
			$('#grammarIntro').fadeOut();
			$('#logList').fadeIn();
			location.href = '#firstLineLog';
		}else{
			$('#logList').fadeOut();
		}
	});
	
	//切换语法说明
	//id = 'grammarIntro' 的div在template_grammar.jsp
	$('#toggleGrammar').click(function (){
		if($('#grammarIntro').css('display') == 'none'){
			$('#grammarIntro').fadeIn();
			$('#logList').fadeOut();
			location.href = '#firstLineTemplateGrammar';
		}else{
			$('#grammarIntro').fadeOut();
		}
	});
});
</script>
<script type="text/javascript">
$(function (){
	//codeMirror('content_current');
});
var editor;
 function codeMirror(areaId){
	 editor   = CodeMirror.fromTextArea(document.getElementById(areaId), {
		     tabMode: "indent",
		     matchBrackets: true,
		     theme: "eclipse",
		     lineNumbers: true,
		     lineWrapping : true, 
		     indentUnit: 4,
		     mode: "text/html"
		   });
 }
 </script>
<script type="text/javascript">
var template_version = 1;//1表示当前模板,2表示上一次更新的模板，3最后更新的模板
var content1 = $('#content_current').val();
var content2 = $('#content2').val();
var content3 = $('#content3').val();
var updateTimeStr = '${templateInfo.updateTimeStr}';
var updateTime2Str = '${templateInfo.updateTime2Str}';
var updateTime3Str = '${templateInfo.updateTime3Str}';
var isAddTemplate = ${isAddTemplate}; 
var templateId = '${templateInfo.id }';
var root = '${ROOT}';//发布器项目根目录

//新建/修改模板功能：“别名的输入项”会根据选择不同的模板类别判断是否隐藏；
//栏目——有 专题——有 标签——无 标签图——无 最终文章——无 投票——无 投票结果——无 公共——有 投票结果（新版）——无 配置文件——无 资讯推广——有
function changeTemplateCategory(templateCategory){
	if( templateCategory == '栏目' || templateCategory == '专题' || templateCategory == '资讯推广' || templateCategory == '公共'){
		$('#aliasArea').show();
	}else{
		$('#aliasArea').hide();
	}
}

var templateCategory = '${templateInfo.templateCategory.value}';
var channelId = '${channelInfo.id }';
var code = '${templateInfo.code}';
$(function (){
	if(templateCategory != ''){
		changeTemplateCategory(templateCategory);
		$('#templateCategorySelector option[value='+templateCategory+']').attr('selected', true); 
	}
	if(code != ''){
		$('#code option[value='+code+']').attr('selected', true); 
	}
});


</script>
<script type="text/javascript"  src="${ROOT}/resource/js/template/template.js"></script>  
</body>
</html>