<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>频道流水统计</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
	<div class="main-content">
		<!--头部动作条 start-->
		<jsp:include page="/head.do"></jsp:include>
		<!--头部动作条 end-->

		<div class="blank10"></div>
		<!--工作日志 start-->
		<div class="work-log">
			<form id="searchLogListForm" action="/worklog/channelStatistics.do"  method="get">
			<div class="action-bar">
				<input type="radio" name="mode" value="currentChannel" checked>当前频道：${channelInfo.name} &nbsp;&nbsp;&nbsp;
				<c:if test="${userInfo.admin}">
					<input type="radio" name="mode" value="allChannel" <c:if test="${param.mode == 'allChannel'}">checked</c:if>>全频道 &nbsp;&nbsp;&nbsp;
				</c:if>
				<input type="radio" name="mode" value="givenChannel" <c:if test="${param.mode == 'givenChannel'}">checked</c:if>>选定频道：&nbsp;
				<select name="channelName" id="" class="style-select">
					<c:forEach var="userPower" items="${userChannelList}" >
						<option value="${userPower.channelInfo.name}" <c:if test="${param.channelName eq userPower.channelInfo.name}">selected</c:if>>${userPower.channelInfo.name}</option>
					</c:forEach>
				</select>
				
				<label for="" class="mll">选择日期</label>
				<input type="text" name="startDateStr"  id="startDateStr" 
							 class="style-input" size="20" autocomplete="off" 
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"
								value="${workLogSearchCriteria.startDateStr }" />
				 --
				<input type="text" name="endDateStr"  id="endDateStr" 
							 class="style-input" size="20" autocomplete="off" 
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"
								value="${workLogSearchCriteria.endDateStr }" />				
				<a href="javascript:search()" class="go-search-bt c-green-bt mlm">搜索</a>
			</div>
			</form>
			<div class="blank20"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>日期</th>
						<th>频道</th>
						<th>文章数</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="workLogInfo" items="${workLogInfoPage.data}" varStatus="idx" begin="0">
					 <c:if test="${ idx.index%2==0  }">
							<tr  class="odd">
						 </c:if>
					<c:if test="${ idx.index%2 !=0  }">
							<tr >
					</c:if>
						<td>${workLogInfo.dateStr }</td>
						<td>${workLogInfo.channelInfo.name }</td>
						<td>${workLogInfo.count }</td>
					</tr>
					
					</c:forEach>
				</tbody>
			</table>
			<div class="blank20"></div>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${workLogInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${workLogInfoPage.pageSize}"/>
						<jsp:param name="total" value="${workLogInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/worklog/channelStatistics.do?pageNo=:pageNo${queryStr }"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/worklog/channelStatistics.do?pageNo=1${queryStr }"/>
						<jsp:param name="totalPageCount" value="${workLogInfoPage.totalPageCount}"/>
			</jsp:include>
		    <div class="blank20"></div>
		</div>
		<!--工作日志 end-->

	</div>

<script type="text/javascript"  src="${ROOT}/resource/plugins/My97DatePicker/WdatePicker.js"></script>	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>   
<script type="text/javascript">
function search(){
	$('#searchLogListForm').submit();
}
</script>
</body>
</html>