<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>当天工作统计</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
	<div class="main-content">
		<!--头部动作条 start-->
		<jsp:include page="/head.do"></jsp:include>
		<!--头部动作条 end-->

		<div class="blank10"></div>
		
		<div class="work-log">
			<span class="mlm">我今天的工作：共发了<span class="c-red-font" id="selfAll">0</span>篇文章</span>
			<table class="normal-table">
				<thead>
					<tr>
						<th>日期</th>
						<th>频道名</th>
						<th>用户名</th>
						<th>文章数</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="workLogInfoSelf" items="${listSelf}" varStatus="idx" begin="0">
					 <c:if test="${ idx.index%2==0  }">
							<tr  class="odd">
						 </c:if>
					<c:if test="${ idx.index%2 !=0  }">
							<tr >
					</c:if>
						<td>${workLogInfoSelf.dateStr }</td>
						<td>${workLogInfoSelf.channelInfo.name }</td>
						<td>${workLogInfoSelf.userId }</td>
						<td key="selfCount">${workLogInfoSelf.count }</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="blank20"></div>
		<div class="work-log">
			<span class="mlm">频道今天发表文章总数:<span class="c-red-font" id="channelAll">0</span></span>
			<table class="normal-table">
				<thead>
					<tr>
						<th>日期</th>
						<th>频道名</th>
						<th>用户名</th>
						<th>文章数</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="workLogInfoChannel" items="${listChannel}" varStatus="indx" begin="0">
					 <c:if test="${ indx.index%2==0  }">
							<tr  class="odd">
						 </c:if>
					<c:if test="${ indx.index%2 !=0  }">
							<tr >
					</c:if>
						<td>${workLogInfoChannel.dateStr }</td>
						<td>${workLogInfoChannel.channelInfo.name }</td>
						<td>${workLogInfoChannel.userId }</td>
						<td key="channelCount">${workLogInfoChannel.count }</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
	</div>	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
	$(function(){
		
		var selfAll = 0;
		var channelAll = 0;
		$("td[key=selfCount]").each(function(){
			var selfCount = $(this).html();
			selfAll += parseInt(selfCount);
		});
		
		$("td[key=channelCount]").each(function(){
			var channelCount = $(this).html();
			channelAll += parseInt(channelCount);
		});
		
		$("#selfAll").html(selfAll);
		$("#channelAll").html(channelAll)
		
	})
</script>		
</body>
</html>