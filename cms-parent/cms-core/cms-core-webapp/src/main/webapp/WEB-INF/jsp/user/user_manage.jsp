<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<title>用户管理</title>
</head>
<body>
	<div class="main-content">
	<jsp:include page="/head.do"></jsp:include>
	
	<!--所有用户列表 start-->
		<div class="blank10"></div>
		<div class="user-manage">
			<div class="action-bar">
				<form name="search_tag" action="/user/userManage.do" method="get" >
				<span class="chanal">频道名：${channelId}</span>
				<label class="mll" for="userId">用户名：</label>
						<input type="text" name="userId"  value="${userId}" class="style-input fixwidth-l mlm"/>
						<input type="hidden" name="channelId" value="${channelId}" class="style-input fixwidth-l mlm"/>
						<input type="submit" class="go-search-bt c-green-bt mlm" value="搜索" />
				<input class="u-inputBtn mll" type="button" onclick="javascript:location.href='/user/toEditUserPage.do?channelId=${channelId}&type=add'" value="添加新用户">
				</form>
			</div>
			<div class="blank10"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>用户名</th>
						<th>权限</th>
						<th>最后登录时间</th>
						<th>管理</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="singleUserPower" items="${userPowerInfoPage.data}" varStatus="idx" begin="0">
					 <c:if test="${idx.index%2==0}">
						<tr  class="odd">
					 </c:if>
					<c:if test="${idx.index%2 !=0}">
						<tr >
					 </c:if>
							<td>${singleUserPower.userId}</td>
							<td class="right-name">
								<span class="c-black-font">${singleUserPower.userRole.display}</span>
								 <c:if test="${singleUserPower.addedPowerSize > 0}">
									<span class="c-green-font">&nbsp; + &nbsp; ${singleUserPower.addedPowerStr }</span>
								 </c:if>
								 <c:if test="${singleUserPower.removedPowerSize > 0}">
								 	<span class="c-red-font">&nbsp; - &nbsp; ${singleUserPower.removedPowerStr}</span>
								 </c:if>
							</td>
							<td>${singleUserPower.lastModifyTimeStr}</td>
							<td>
								<c:if test="${singleUserPower.userRole.powerValue < curPowerInfo.userRole.powerValue}">
									<a class="mhs" href="/user/toEditUserPage.do?userId=${singleUserPower.userId}&channelId=${channelId}">修改</a>
									|
									<a class="mhs" href="javascript:deleteUser('${singleUserPower.userId}', '${channelId}')">删除</a>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${userPowerInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${userPowerInfoPage.pageSize}"/>
						<jsp:param name="total" value="${userPowerInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/user/userManage.do?channelId=${channelId}&pageNo=:pageNo"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/user/userManage.do?channelId=${channelId}&pageNo=1"/>
						<jsp:param name="totalPageCount" value="${userPowerInfoPage.totalPageCount}"/>
			</jsp:include>
		</div>
		<!--所有用户列表 end-->
	</div>
<script type="text/javascript">
function deleteUser(userId, channelId){
	if(confirm('确定删除该用户？')){
		location.href = '/user/deletePower.do?userId=' + userId + '&channelId=' + channelId;
	}
}

</script>
</body>
</html>