<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../include/common_taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>热词管理</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
	<div class="main-content">
		<jsp:include page="/head.do"></jsp:include>

		<!--热词管理 start-->
		<div class="hotword-manage">
			<h2></h2>
			<div class="action-bar">
				<form name="add_hotword" id="add_hotword" action="/hotword/addHotword.do" method="post" >
				
					<label for="hotword">添加热词：</label><input type="text" id="hotword" name="name" onchange="javascript:checkHotword()" placeholder="热词" class="style-input fixwidth-l mlm">
					<label class="mlm" for="url">对应地址：</label><input type="text" id="url" name="href" placeholder="链接" class="style-input fixwidth-l mlm">
					<input type="hidden" id="channelId" name="channelId" value="${channelInfo.id}">
					<input type="hidden" id="checkHotwordOK" value="false" />
					<a href="javascript:addHotword()" class="c-blue-bt edit-bt mlm">提交</a>
					<div class="blank15"></div>
						<em class="note-num c-red-font"  >
							 <font id="hotword_check" ></font>
						</em>
				</form>
			</div>
			<div class="action-bar">
				<form name="search_hotword" action="/hotword/hotwordManage.do" method="get">
					<label for="hotword">&nbsp;&nbsp;&nbsp;关键字：</label><input type="text" value="${searchkey}" name="searchkey" id="searchkey" placeholder="热词 / 链接" class="style-input fixwidth-l  mlm">
					<input type="hidden" name="channelId" value="${channelInfo.id}">
					<a href="javascript:document.search_hotword.submit();" class="go-search-bt c-green-bt mlm">搜索</a>
				</form>
			</div>
			<div class="blank20"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>热词</th>
						<th>链接</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="hotWordInfo" items="${hotWordInfoPage.data}" varStatus="idx" begin="0">
						<c:if test="${idx.index%2==0}">
							<tr  class="odd">
						 </c:if>
						<c:if test="${idx.index%2 !=0}">
							<tr >
						 </c:if>
							<td>${hotWordInfo.name}</td>
							<td>
								<div key="${hotWordInfo.name}"><a href="${hotWordInfo.href}" target="_blank">${hotWordInfo.href}</a></div>
							</td>
							<td>
								<a href="javascript:deleteHotword('${hotWordInfo.name}')">删除</a>&nbsp;|&nbsp;
								<a href="javascript:editHotword('${hotWordInfo.name}', '${hotWordInfo.href}')">修改</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${hotWordInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${hotWordInfoPage.pageSize}"/>
						<jsp:param name="total" value="${hotWordInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/hotword/hotwordManage.do?channelId=${channelInfo.id}&pageNo=:pageNo${queryStr}"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/hotword/hotwordManage.do?channelId=${channelInfo.id}&pageNo=1${queryStr}"/>
						<jsp:param name="totalPageCount" value="${hotWordInfoPage.totalPageCount}"/>
			</jsp:include>
		</div>
		<!--热词管理 end-->
		<form action="" onsubmit="ajaxEditHotWord(hotword, url);return false"></form>

	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>	
<script type="text/javascript">
function checkHotword(){
	var hotword = $('#hotword').val().trim();
	var channelId = $('#channelId').val().trim();
	$.ajax({
	   type: 'GET',
	   url: '/hotword/isExistHotword.do?channelId=' + channelId + '&name=' + hotword,
	   async: false,
	   cache: false,
	   dataType: 'text',
	   success: function(data){
		   		if('false' == data){
		   			$("#hotword").css({"border-color":""});
		   			$("#checkHotwordOK").val("true");
		   		} else {
		   			$("#checkHotwordOK").val("false");
		   			if('true' == data){
		   				alert("该热词已存在本频道！请重新输入.");
		   			}else{
		   				alert("请检查热词.");
		   			}
		   			$("#hotword").css({"border-color":"red"}).select().focus();
		   		}
	   }
	});
}

function addHotword(){
	
	var flag = $("#checkHotwordOK").val();
	if(flag != "true"){
		checkHotword();
		if($("#checkHotwordOK").val() != "true")
			return;
	}
	
	var url = $('#url').val().trim();
	if(url.indexOf("://") == -1){
		alert("请检查热词对应地址后才点击提交");
		return ;
	}
	$('#add_hotword').submit();
}	

function deleteHotword(hotword){
	if(confirm('确定删除热词?')){
		location.href="${ROOT }/hotword/deleteHotword.do?channelId=${channelInfo.id}&name="+hotword;
	}
}

function editHotword(hotword, url){
	var hotword_val = $("#editForm_url").val();
	$("#editForm").parent().html('<a href="'+hotword_val+'" target="_blank">'+hotword_val+'</a>');
	
	var editForm = '<form id="editForm" action="" onsubmit="ajaxEditHotWord();return false">'+
					'<input type="hidden" value="'+hotword+'" id="editForm_hotword">'+
					'<input type="hidden" value="'+url+'" id="editForm_url_old">'+
					'<input type="text" value="'+url+'" id="editForm_url" class="style-input fixwidth-400" /></form>'
	$('div[key="'+hotword+'"]').html(editForm);
	$("#editForm_url").select();
}

function ajaxEditHotWord(){
	
	var channelId = $("#channelId").val();
	var editForm_hotword = $("#editForm_hotword").val();
	var editForm_url = $("#editForm_url").val();
	var editForm_url_old = $("#editForm_url_old").val();
	
	//发送请求
	$.ajax({
	   type: 'POST',
	   url: '/hotword/updateHotWord.do',
	   async: false,
	   cache: false,
	   dataType: 'text',
	   data:{"name":editForm_hotword,"href":editForm_url,"channelId":channelId},
	   success: function(data){
		   		if('success' == data){
		   			$("#editForm").parent().html('<a href="'+editForm_url+'" target="_blank">'+editForm_url+'</a>');
		   		} else {
		   			alert('修改失败!请稍后再试');
		   			$("#editForm").parent().html('<a href="'+editForm_url_old+'" target="_blank">'+editForm_url_old+'</a>');
		   		}
	   }
	});
	
	
}
</script>
</body>
</html>