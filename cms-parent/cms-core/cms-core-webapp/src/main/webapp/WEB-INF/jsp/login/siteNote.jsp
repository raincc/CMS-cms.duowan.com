<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<script src="http://www.duowan.com/public/assets/sys/js/jquery.js?02131514.js"></script>
</head>
<body>
	<div class="site-note">
		<h2>更新公告<span class="c-gray-font">红色更新表示需要重启发布器，请注意保存手头工作</span></h2>
		<div class="updata-item">
		<p>
			<p>V4.4.5 - 2013.06.19 12:30</p>
			<p>修复“开通新专区，缺少logo”的问题<p/>
			<p>增加发表文章时，预览地址提醒<p/>
			<p>增加排行榜加缓存，防止空白页面<p/>
			<hr/>
			<p>V4.4.3 - 2013.06.06 13:00</p>
			<p>频道管理：频道增加分类信息<p/>
			<p>修复“生成模板文件有空行”的问题<p/>
			<p>修复“文章字数统计不准确”的问题<p/>
			<p>修复“预定发布的文章排序不正确”的问题<p/>
			<p>增加使用说明手册<p/>
			<hr/>
			<p>V4.4.2 - 2013.05.15 13:00</p>
			<p>修复“文章列表没有按发布时间排序”<p/>
			<p>增加“刷新本频道使用指定最终页模板的所有文章静态页”<p/>
			<p>修复模板有别名时分页连接出错<p/>
			<p>预发布的文章也生成文件，以便预览。但不出现在列表中，即无外链进入该文件。<p/>
			<p>修复首页模板1212-新游测试表语法支持:getString("gstatus")<p/>
			<p>修复“在文章列表某个tag下,点发表文章,打开的发表文章界面,需要默认打上这个tag”<p/>
			<p>修复bug:gameTest调用的排序更正<p/>
			<p>上传.zip自动解压并且根据是否图片自动分发<p/>
			<hr/>
		</div>
	</div>
</body>
</html>