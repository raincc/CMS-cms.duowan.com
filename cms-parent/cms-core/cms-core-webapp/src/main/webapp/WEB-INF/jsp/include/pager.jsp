<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="pageUrl" value="${fn:replace(param.pageUrl,':26','&')}"></c:set>
<c:set var="firstPageUrl" value="${empty param.firstPageUrl ? pageUrl : fn:replace(param.firstPageUrl,':26','&')}"></c:set>
<fmt:parseNumber type="number" var="pageNo" value="${empty param.pageNo ? 1 : param.pageNo}"/>
<fmt:parseNumber type="number" var="pageSize" value="${empty param.pageSize?20:param.pageSize }"/>
<fmt:parseNumber type="number" var="total" value="${empty param.total?0:param.total }"/>
<fmt:parseNumber type="number" var="pageTotal" value="${empty param.totalPageCount ?0:param.totalPageCount}" />
<c:set var="HTML">
<div class="blank20"></div>
<div class="mod-page main-page center">
	<c:if test="${pageTotal > 1}">
		<c:if test="${pageNo == 2}">
			<a title="上一页" class="c-blue-bt" rel="prev" href="${fn:replace(firstPageUrl,':pageNo',pageNo-1) }">上一页</a>
		</c:if>
		<c:if test="${pageNo > 2}">
			<a title="上一页" class="c-blue-bt" rel="prev" href="${fn:replace(pageUrl,':pageNo',pageNo-1) }">上一页</a>
		</c:if>
		<c:if test="${pageNo == 4}">
			<a title="首页" class="c-blue-bt"  href="${fn:replace(firstPageUrl,':pageNo',1) }">1</a>
		</c:if>
		<c:if test="${pageNo > 4}">
			<a title="首页" class="c-blue-bt"  href="${fn:replace(firstPageUrl,':pageNo',1) }">1...</a>
		</c:if>
		
		<c:forEach var="n" begin="${pageNo > 2 ? pageNo-2 : 1}" end="${pageNo < pageTotal ? (pageTotal - pageNo > 6 ? pageNo + 6 : pageTotal) : pageNo}">
			<c:if test="${n != pageNo}">
				<c:choose>
					<c:when test="${n == 1}">
						<a class="c-blue-bt"  href="${fn:replace(firstPageUrl,':pageNo',n) }">${n}</a>
					</c:when>
					<c:otherwise>
						<a class="c-blue-bt"  href="${fn:replace(pageUrl,':pageNo',n) }">${n}</a>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${n == pageNo}">
				 <a class="c-blue-bt current" href="#" title="已经是当前页" class="current">${n}</a>
			</c:if>
		</c:forEach>
		<c:if test="${pageNo < pageTotal - 6}">
			<a title="末页" class="c-blue-bt"  href="${fn:replace(pageUrl,':pageNo',pageTotal) }">...${pageTotal}</a>
		</c:if>
		<c:if test="${pageNo < pageTotal}">
			<a title="下一页"  class="c-blue-bt" rel="next" href="${fn:replace(pageUrl,':pageNo',pageNo+1) }">下一页</a>
		</c:if>
	</c:if>
	<div class="blank20"></div>
</div>
</c:set>
<%
String html = pageContext.findAttribute("HTML").toString();
html = html.replaceAll(">\\s*",">").replaceAll("\\s*<","<");
out.clearBuffer();
out.println(html);
%>