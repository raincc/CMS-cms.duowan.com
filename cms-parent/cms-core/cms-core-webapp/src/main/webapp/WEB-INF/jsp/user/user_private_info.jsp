<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑用户个人信息</title>
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
<div class="main-content">
	<div class="top-toolbar clearfix">
		<div class="mod-crumbs fl"><a class="back-to-index">编辑用户个人信息</a></div>
	</div>
	<form action="/user/editUserPrivateInfo.do"  method="post"  id="updateUserForm" >
		<div class="edit-mod">
			<p class="form-line">
				<label for="">用户名：</label>
				<input type="text"  name="userId"  id="userId"  value="${userInfo.userId}" onchange="checkUserId()"  class="style-input fixwidth-l">
							<em class="note-num c-red-font"  >
								 <font  id="userId_check" ></font>
							</em>
				<div class="blank15"></div>
				<label for="">&nbsp;&nbsp;&nbsp;邮箱：</label>
				<input type="text"  name="mail"  id="mail" value="${userInfo.mail}"  class="style-input fixwidth-l">
				<div class="blank15"></div>
				<label for="">&nbsp;&nbsp;&nbsp;密码：</label>
				<input type="password" name="password"  id="password"  class="style-input fixwidth-l">
				<div class="blank15"></div>
			</p>
		</div>
	<span class="ui-button"><a href="javascript:submit()">确定</a></span>
	</form>
</div>	
	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery.form.js"></script>
<script type="text/javascript">
function submit(){
	var options = { 
        url:'/user/editUserPrivateInfo.do', //提交给哪个执行
        type:'POST', 
        dataType: 'text',
        success: function(data){
        		if(data.indexOf('success') != -1){
        			alert('保存成功！');
        		}else {
        			alert('保存失败，请稍后重试');
        		}
        }, //显示操作提示
        error: function(){
        	alert('保存失败，请稍后重试');
        },   
        timeout:5000 
	};
	$('#updateUserForm').ajaxSubmit(options); 
}

function checkUserId(){
	document.getElementById('userId_check').innerHTML='正在用户名是否可用....';
    var userId=document.getElementById('userId').value;
    if(userId==''){
      alert('用户名不能为空!');
      return;
    }
    
	$.ajax({
	   type: 'GET',
	   url: '/user/checkUserId.do?userId=' + userId,
	   async: false,
	   cache: false,
	   dataType: 'text',
	   success: function(data){
		   		if('addUser' == data){
		   			document.getElementById('userId_check').innerHTML='该用户名可以使用.';
		   		} else {
		   			document.getElementById('userId_check').innerHTML='用户名已存在,请更换.';
                    document.getElementById('userId').focus();
		   		}
	   }
	});
}

</script>
</body>
</html>