<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>        
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>同一篇文章，不同标签，显示的文章信息不一样</title>
	<link rel="stylesheet" href="http://assets.dwstatic.com/b=lego/1.0.0/css&f=mod.pop.css">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/plugins/bigcolorpicker/css/jquery.bigcolorpicker.css" media="all">
	<script src="http://www.duowan.com/public/assets/sys/js/jquery.js?02131514.js"></script>
	<script src="http://assets.dwstatic.com/b=lego/1.0.0/js&f=lego.popupbox.js"></script>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
<form action="/article/editArticle4TagList.do"  name="editorForm"  id="editorForm"  method="post">
	<input type="hidden"  name="pictureUrl" id="pictureUrl"  value="${articleInfo.pictureUrl }" />
	<input type="hidden"  name="channelId" id="channelId"  value="${channelId}" />
	<input type="hidden"  name="tag" id="tag"  value="${tag}" />
	<input type="hidden"  name="id" id="id"  value="${articleId}" />
	<input type="hidden"  name="power" id="power"  value="${articleInfo.power}" />
	
<div class="edit-artical">
	<div class="action-bar">
			<c:if test="${articleInfo.isEmpltyLink}">
				<p class="form-line">
					<label for="subtitle">空链接：</label>
					<input type="text" name="emptyLink"  id="emptyLink" value="${articleInfo.emptyLink}" class="style-input fixwidth-400">
				</p>
				<div class="blank10"></div>
		    </c:if>
		    
		    <p class="form-line">
				<label for="title">标&nbsp;&nbsp;&nbsp;题：</label>
				<input type="text" value="${articleInfo.title}" name="title" id="title" 
					 class="style-input fixwidth-l"  onkeyup="count(this)" />
				<em class="note-num mlm" >
					 <font id="title_count">0</font>
				</em>
				<em class="c-red-font mlm">
					<font  id="title_check" ></font>
				</em>
				<label for="title">修改所有tag：</label>
				<input type="checkbox"  name="isChangeAllTags"  id="isChangeAllTags"  >
				<label for="title">标题颜色：</label>
				<input type="text" value="${articleInfo.titleColor}" name="titleColor" id="titleColor"  class="style-input fixwidth-s"   />
			</p>
			<div class="blank10"></div>
			<p class="form-line">
					<label for="subtitle">副标题：</label>	
					<input type="text" value="${articleInfo.subtitle}"  name="subtitle" id="subtitle" class="style-input fixwidth-s"  />
					<span class="form-item mlm">
						来源：
						<input type="text" value="${articleInfo.source}" name="source" id="source" class="style-input fixwidth-s" />
					</span>
					<span class="form-item mlm">作者：
						<input type="text" value="${articleInfo.author}" name="author" id="author" class="style-input fixwidth-s" />
					</span>
			</p>
			<div class="blank10"></div>
			<p class="form-line">
				 <iframe height="40" src="" 
						frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="yes"
						style="width:100%;overflow:hidden;scroll:no;display:block; " id="uploadHeadPicIframe"
						onload="this.height=this.contentWindow.document.documentElement.scrollHeight" >
				</iframe>
			</p>
  </div>	
	
 <div class="set-mod">
			 <div class="blank10"></div>
			 <div class="set-box-ctn clearfix">
			 		<div class="set-box">
			 			<textarea  name="digest" id="digest"  placeholder="摘要："  >${articleInfo.digest}</textarea>
			 		</div>
	 				<div class="set-box">
						<textarea name="diy1" id="diy1" placeholder="自定义1:">${articleInfo.diy1}</textarea>
					</div>
					<div class="set-box">
						<textarea name="diy2" id="diy2"  placeholder="自定义2:">${articleInfo.diy2}</textarea>
					</div>
					<div class="set-box">
						<textarea name="diy3" id="diy3" placeholder="自定义3:">${articleInfo.diy3}</textarea>
					</div>
					<div class="set-box">
						<textarea name="diy4" id="diy4"  placeholder="自定义4:">${articleInfo.diy4}</textarea>
					</div>
					<div class="set-box">
						<textarea name="diy5" id="diy5"  placeholder="自定义5:">${articleInfo.diy5}</textarea>
					</div>
			 </div>
		</div>
		
	<div class="blank10"></div>
	<div class="center">
		<span class="ui-button" style="display: none;"><a href="javascript:parent.deleteBatch()">彻底删除</a></span>
		<span class="ui-button"><a href="javascript:parent.submitArticle4TagList()">确定修改</a></span>
	</div>
</div>
	<!--修改文章弹出框内容 end-->

</form>

<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>   
<script type="text/javascript"  src="${ROOT}/resource/js/article/article_common.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/plugins/bigcolorpicker/js/jquery.bigcolorpicker.js"></script>
<script type="text/javascript">
var root = '${ROOT}';//发布器项目根目录
var isLegalTitle = true;
$(function (){
	$('#uploadHeadPicIframe').attr('src' , '/article/toUploadHeadPicIframe.do?channelId=${channelId }&articleId=${articleId}&preHeadPic='+encodeURIComponent('${articleInfo.pictureUrl}') );
	$("#titleColor").bigColorpicker("titleColor","L",10);
});

function submit(){
	if(!validateForm()){
		return 'false';
	}
	$('#pictureUrl').val(document.getElementById("uploadHeadPicIframe").contentWindow.getHeadPicFile());
	$('#editorForm').submit();
}

//根据频道id和文章id批量删除文章列表数据
function deleteByChannelIdAndArticleId(){
	location.href = '/article/deleteByChannelIdAndArticleId.do?channelId=${channelId}&articleId=${articleId}';
}


function validateForm(){
	if($('#titleColor').length>10){
		alert('标题颜色不能超过十位');
		return ;
	}
	return validateFormCommonPart();
}

</script>

</body>
</html>