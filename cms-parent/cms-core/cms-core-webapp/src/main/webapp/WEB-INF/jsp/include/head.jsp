<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript" src="${ROOT }/resource/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ROOT }/resource/js/udb.js"></script>
<script>
	function logout(){
	if(confirm('确定退出?')){
		var username = Navbar.getUsername();
		if(username != null){
			Navbar.logout("${ROOT }/logout.do");
		}else{
			parent.location.href = '/logout.do';
		}
	}
} 
</script>
<!--头部动作条 start-->
		<div class="top-toolbar clearfix">
			<div class="mod-crumbs fl">
			    <a class="back-to-index" href="/article/articleList.do?channelId=${channelInfo.id}">${channelInfo.name}</a><span style="color:black">&gt;&gt;</span>${tagTreeUr}
			</div>
			<ul class="work-list fr">
				<!-- 首页|评论|发表|滚动|标签|模板|热词|管理|退出 -->
				<!-- 
				<li class="list-item"><a class="list-name" href="">评论管理</a></li>
				 -->
				<li class="list-item">
					<a class="list-name" href="/channel/toChannelMainPage.do?channelId=${channelInfo.id}">首页</a>
				</li>
				<li class="list-item">
					<a class="list-name" href="#">评论</a>
				</li>
				<li class="list-item">
					<c:if test="${empty currentTag}">
						<a class="list-name" href="/article/toEditArticlePage.do?channelId=${channelInfo.id}&userId=${userInfo.userId}">发表</a>
					</c:if>
					<c:if test="${not empty currentTag}">
						<a class="list-name" href="/article/toEditArticlePage.do?channelId=${channelInfo.id}&userId=${userInfo.userId}&currentTag=${currentTag}">发表</a>
					</c:if>
				</li>
				<li class="list-item">
					<a class="list-name" href="/article/articleRoll.do?channelId=${channelInfo.id}">滚动</a>
				</li>
				<li class="list-item">
					<a class="list-name" href="/tag/tagManage.do?channelId=${channelInfo.id}">标签</a>
				</li> 
				<li class="list-item">
					<a class="list-name" href="/template/templateList.do?channelId=${channelInfo.id}">模板</a>
				</li> 
				<li class="list-item">
					<a class="list-name" href="/hotword/hotwordManage.do?channelId=${channelInfo.id}">热词</a>
				</li>
				<li class="list-item">
					<a class="list-name" href="/user/userManage.do?channelId=${channelInfo.id}">管理</a>
				</li>
				<!-- 
				<li class="list-item">
					<a class="list-name" href="#">投票管理</a>
					<ul class="dropdown">
						<li><a href="#">独立投票</a></li>
						<li><a href="#">文章投票</a></li>
					</ul>
				</li>
				 -->
				<li class="list-item">
					<a class="list-name" href="/worklog/logList.do"  target="contentIframe">工作日志</a>
					<ul class="dropdown">
						<li><a href="/worklog/todayStatistics.do">当天工作统计</a></li>
						<li><a href="/worklog/userStatistics.do">用户流水统计</a></li>
						<li><a href="/worklog/channelStatistics.do">频道流水统计</a></li>
						<li><a href="/worklog/historyStatistics.do">历史流水统计</a></li>
					</ul>
				</li>
				<li class="list-item"><a class="list-name" href="#">其他操作</a>
					<ul class="dropdown">
						<li><a href="/channel/flushAll.do">常用刷新</a></li>
						<c:if test="${userInfo.admin}">
						<li><a href="/template/autoFlushTemplateList.do?channelId=${channelInfo.id}">自动刷新模版管理</a></li>
						</c:if>
					</ul>
				</li>
				<li class="list-item">
					<a class="list-name" href="javascript:logout()">退出</a>
				</li>
			</ul>
		</div>
		<!--头部动作条 end-->