<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>CMS - 用户登录 - Login</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<style>
		html,body{background-color: #F7F7F7;}
	</style>
<script type="text/javascript" src="${ROOT }/resource/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ROOT }/resource/js/udb.js"></script>
<script type="text/javascript">
if(window.parent != window){
			window.parent.location = window.location;	
}
var errorMessage = '${errorMessage}';
if(errorMessage != ''){
	alert(errorMessage);
}
	var key = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b",
			"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
			"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B",
			"C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" ];
	function rsalogin() {
		var result = document.getElementById("password").value;
		if (result == null || result == "") {
			alert("请您输入密码!");
			return;
		}
		result = changePass(result);
		document.getElementById("password").value = result;
		var loginForm = document.getElementById("loginForm");
		loginForm.action = "/login.do";
		loginForm.submit();
	}

	function changePass(src) {
		if (src == null || src == "") {
			return;
		}
		var len = src.length;
		if (len == 128) {
			return src;
		}
		var pass = "";
		var index = Math.floor(Math.random() * 60 + 1);
		for (i = 0; i < 126; i++) {
			if (i == index) {
				pass += src;
				i += len - 1;
			} else {
				var ran = Math.floor(Math.random() * 60);
				pass = pass + key[ran];
			}
		}
		pass = pass + key[index] + key[len];
		return pass;
	}
	document.onkeydown = keyListener;
	function keyListener(e) {
		e = e ? e : event;
		if (e.keyCode == 13) {
			rsalogin();
			return false;
		}
	}
	
	<c:if test="${innerIp}">
	var udblogin = "${ROOT }/login/udbLogin.do";
	$(function(){
		var username = Navbar.getUsername();
		if(username != null && errorMessage.indexOf("udb") == -1){
			$.ajax({
				   type: 'GET',
				   url: '/login/checkUdbName.do?udbName=' + username,
				   async: false,
				   cache: false,
				   dataType: 'text',
				   success: function(data){
					   		if('true' == data){
					   			location.href = udblogin;
					   		} else {
					   			Navbar.login(udblogin);
					   		}
				   }
				});
		}else{
			Navbar.login(udblogin);
		}
	});
	</c:if>
</script>



</head>

<body class="login-page">
	<!--登陆页登录框 start-->
	<div class="login-page">
		<div class="login-box">
			<div class="system-note">
				<iframe src="/login/siteNote.do" width="460" height="376" frameborder="0">
					
				</iframe>
			</div>
			<div class="login-form">
				<h1>多玩发布器</h1>
				<form id="loginForm" name="loginForm" action="" method="get">
					<p class="form-line">
						<label for="username">用户名</label><input type="text" name="userId" id="userId" placeholder="你的名称">
					</p>
					<p class="form-line">
						<label for="password">密码</label><input type="password" name="password" id="password" placeholder="你的密码">
					</p>
					<p class="form-line">
						<span class="ui-button"><a href="javascript:rsalogin();">登录系统</a></span>
					</p>
					<p class="form-line">
						<span class="note">申请新频道、注册账号请联系管理员</span>
					</p>
				</form>
			</div>
		</div>
		<div class="copy-right">
			<p>copyright2013,duowan.com,all rights reserved</p>
		</div>
	</div>	
	<!--登陆页登录框 end-->
</body>
</html>

