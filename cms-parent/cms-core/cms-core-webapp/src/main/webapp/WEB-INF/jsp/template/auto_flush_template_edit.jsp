<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title></title>
</head>
<body>
<!--头部动作条 end-->
<form id="autoFlushTemplateForm" action="/template/publishAutoFlushTemplate.do" method="post">
<input type="hidden"  name="id"  id="id" value="${autoFlushTemplateInfo.id }" />
<div class="edit-mod">
	<table class="layout-table">
		<tbody>
			<tr>
				<td><label for="">频道id：</label></td>
				<td><input class="style-input fixwidth-75p" type="text"  id="channelId" name="channelId" value="${channelInfo.id }" readonly="readonly"/></td>
			</tr>
			<tr>
				<td><label for="">频道名称：</label></td>
				<td><input class="style-input fixwidth-75p" type="text"  id="channelName" name="channelName" value="${channelInfo.name }" readonly="readonly"/></td>
			</tr>
			<tr>
				<td><label for="">模板id：</label></td>
				<td><input class="style-input fixwidth-75p" type="text"  id="templateId" name="templateId" value="${autoFlushTemplateInfo.templateId }" /></td>
			</tr>
			<tr>
				<td nowrap="nowrap"><label for="">刷新间隔时间(s)：</label></td>
				<td><input class="style-input fixwidth-75p" type="text"  id="interval" name="interval" value="${autoFlushTemplateInfo.interval }" /></td>
			</tr>
			<tr>
				<td><label for="">描述：</label></td>
				<td>
					<textarea rows="5" cols="30"  id="description"  name="description">${autoFlushTemplateInfo.description }</textarea>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</form>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>    
<script type="text/javascript">

function submit(){
	$('#autoFlushTemplateForm').submit();
	
}

</script>
</body>
</html>