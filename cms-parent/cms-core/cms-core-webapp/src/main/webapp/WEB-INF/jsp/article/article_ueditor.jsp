<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="editor">
	<div id="edui1" class="edui-editor " style="z-index: 999;">
		<div id="edui1_toolbarbox" class="edui-editor-toolbarbox">
			<div id="edui1_toolbarboxouter" class="edui-editor-toolbarboxouter">
				<div class="edui-editor-toolbarboxinner">
					<div id="edui2" class="edui-toolbar  "
						onselectstart="return false;"
						onmousedown="return $EDITORUI[&quot;edui2&quot;]._onMouseDown(event, this);"
						style="-webkit-user-select: none;">
						<div id="edui159" class="edui-box edui-button edui-for-fullscreen">
							<div id="edui159_state"
								onmousedown="$EDITORUI[&quot;edui159&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui159&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui159&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui159&quot;].Stateful_onMouseOut(event, this);"
								class=" ">
								<div class="edui-button-wrap">
									<div id="edui159_body" unselectable="on" title="全屏"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui159&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui3" class="edui-box edui-button edui-for-source">
							<div id="edui3_state"
								onmousedown="$EDITORUI[&quot;edui3&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui3&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui3&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui3&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui3_body" unselectable="on" title="源代码"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui3&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui4" class="edui-box edui-button edui-for-undo">
							<div id="edui4_state"
								onmousedown="$EDITORUI[&quot;edui4&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui4&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui4&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui4&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui4_body" unselectable="on" title="撤销"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui4&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui5" class="edui-box edui-button edui-for-redo">
							<div id="edui5_state"
								onmousedown="$EDITORUI[&quot;edui5&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui5&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui5&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui5&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui5_body" unselectable="on" title="重做"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui5&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui6" class="edui-box edui-separator "></div>
						<div id="edui7" class="edui-box edui-button edui-for-removeformat">
							<div id="edui7_state"
								onmousedown="$EDITORUI[&quot;edui7&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui7&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui7&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui7&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui7_body" unselectable="on" title="清除格式"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui7&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui8" class="edui-box edui-button edui-for-formatmatch">
							<div id="edui8_state"
								onmousedown="$EDITORUI[&quot;edui8&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui8&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui8&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui8&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui8_body" unselectable="on" title="格式刷"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui8&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui9"
							class="edui-box edui-splitbutton edui-for-autotypeset">
							<div title="自动排版" id="edui9_state"
								onmousedown="$EDITORUI[&quot;edui9&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui9&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui9&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui9&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-splitbutton-body">
									<div id="edui9_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui9&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui9&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui12" class="edui-box edui-button edui-for-pasteplain">
							<div id="edui12_state"
								onmousedown="$EDITORUI[&quot;edui12&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui12&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui12&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui12&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui12_body" unselectable="on" title="纯文本粘贴模式"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui12&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui13" class="edui-box edui-separator "></div>
						<div id="edui14" class="edui-box edui-button edui-for-pagebreak">
							<div id="edui14_state"
								onmousedown="$EDITORUI[&quot;edui14&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui14&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui14&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui14&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui14_body" unselectable="on" title="分页"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui14&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui21" class="edui-box edui-button edui-for-insertimage">
							<div id="edui21_state"
								onmousedown="$EDITORUI[&quot;edui21&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui21&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui21&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui21&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui21_body" unselectable="on" title="图片"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui21&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui26" class="edui-box edui-button edui-for-insertvideo">
							<div id="edui26_state"
								onmousedown="$EDITORUI[&quot;edui26&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui26&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui26&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui26&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui26_body" unselectable="on" title="视频"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui26&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui31" class="edui-box edui-button edui-for-insertframe">
							<div id="edui31_state"
								onmousedown="$EDITORUI[&quot;edui31&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui31&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui31&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui31&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui31_body" unselectable="on" title="插入Iframe"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui31&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui36" class="edui-box edui-button edui-for-link">
							<div id="edui36_state"
								onmousedown="$EDITORUI[&quot;edui36&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui36&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui36&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui36&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui36_body" unselectable="on" title="超链接"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui36&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui37" class="edui-box edui-button edui-for-unlink">
							<div id="edui37_state"
								onmousedown="$EDITORUI[&quot;edui37&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui37&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui37&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui37&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui37_body" unselectable="on" title="取消链接"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui37&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui38" class="edui-box edui-button edui-for-horizontal">
							<div id="edui38_state"
								onmousedown="$EDITORUI[&quot;edui38&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui38&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui38&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui38&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui38_body" unselectable="on" title="分隔线"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui38&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui43" class="edui-box edui-button edui-for-anchor">
							<div id="edui43_state"
								onmousedown="$EDITORUI[&quot;edui43&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui43&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui43&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui43&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui43_body" unselectable="on" title="锚点"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui43&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui46" class="edui-box edui-button edui-for-spechars">
							<div id="edui46_state"
								onmousedown="$EDITORUI[&quot;edui46&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui46&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui46&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui46&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui46_body" unselectable="on" title="特殊字符"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui46&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui51" class="edui-box edui-button edui-for-background">
							<div id="edui51_state"
								onmousedown="$EDITORUI[&quot;edui51&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui51&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui51&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui51&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui51_body" unselectable="on" title="背景"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui51&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui52" class="edui-box edui-separator "></div>
						<div id="edui53" class="edui-box edui-button edui-for-justifyleft">
							<div id="edui53_state"
								onmousedown="$EDITORUI[&quot;edui53&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui53&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui53&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui53&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-checked">
								<div class="edui-button-wrap">
									<div id="edui53_body" unselectable="on" title="居左对齐"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui53&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui54"
							class="edui-box edui-button edui-for-justifycenter">
							<div id="edui54_state"
								onmousedown="$EDITORUI[&quot;edui54&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui54&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui54&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui54&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui54_body" unselectable="on" title="居中对齐"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui54&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui55"
							class="edui-box edui-button edui-for-justifyright">
							<div id="edui55_state"
								onmousedown="$EDITORUI[&quot;edui55&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui55&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui55&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui55&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui55_body" unselectable="on" title="居右对齐"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui55&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui56"
							class="edui-box edui-button edui-for-justifyjustify">
							<div id="edui56_state"
								onmousedown="$EDITORUI[&quot;edui56&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui56&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui56&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui56&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui56_body" unselectable="on" title="两端对齐"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui56&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui57" class="edui-box edui-button edui-for-imagenone">
							<div id="edui57_state"
								onmousedown="$EDITORUI[&quot;edui57&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui57&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui57&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui57&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui57_body" unselectable="on" title="默认"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui57&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui58" class="edui-box edui-button edui-for-imageleft">
							<div id="edui58_state"
								onmousedown="$EDITORUI[&quot;edui58&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui58&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui58&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui58&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui58_body" unselectable="on" title="左浮动"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui58&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui59" class="edui-box edui-button edui-for-imageright">
							<div id="edui59_state"
								onmousedown="$EDITORUI[&quot;edui59&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui59&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui59&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui59&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui59_body" unselectable="on" title="右浮动"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui59&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui60" class="edui-box edui-button edui-for-imagecenter">
							<div id="edui60_state"
								onmousedown="$EDITORUI[&quot;edui60&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui60&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui60&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui60&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui60_body" unselectable="on" title="居中"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui60&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
										<div class="edui-box edui-label"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui61"
							class="edui-box edui-menubutton edui-for-rowspacingbottom">
							<div title="段后距" id="edui61_state"
								onmousedown="$EDITORUI[&quot;edui61&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui61&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui61&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui61&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-menubutton-body">
									<div id="edui61_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui61&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui61&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui68"
							class="edui-box edui-menubutton edui-for-rowspacingtop">
							<div title="段前距" id="edui68_state"
								onmousedown="$EDITORUI[&quot;edui68&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui68&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui68&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui68&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-menubutton-body">
									<div id="edui68_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui68&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui68&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui75"
							class="edui-box edui-menubutton edui-for-lineheight">
							<div title="行间距" id="edui75_state"
								onmousedown="$EDITORUI[&quot;edui75&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui75&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui75&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui75&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-menubutton-body">
									<div id="edui75_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui75&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui75&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui84"
							class="edui-box edui-menubutton edui-for-insertunorderedlist">
							<div title="无序列表" id="edui84_state"
								onmousedown="$EDITORUI[&quot;edui84&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui84&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui84&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui84&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-menubutton-body">
									<div id="edui84_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui84&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui84&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui89"
							class="edui-box edui-menubutton edui-for-insertorderedlist">
							<div title="有序列表" id="edui89_state"
								onmousedown="$EDITORUI[&quot;edui89&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui89&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui89&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui89&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-menubutton-body">
									<div id="edui89_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui89&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui89&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui96" class="edui-box edui-button edui-for-bold">
							<div id="edui96_state"
								onmousedown="$EDITORUI[&quot;edui96&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui96&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui96&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui96&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui96_body" unselectable="on" title="加粗"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui96&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui97" class="edui-box edui-button edui-for-italic">
							<div id="edui97_state"
								onmousedown="$EDITORUI[&quot;edui97&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui97&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui97&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui97&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui97_body" unselectable="on" title="斜体"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui97&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui98" class="edui-box edui-button edui-for-underline">
							<div id="edui98_state"
								onmousedown="$EDITORUI[&quot;edui98&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui98&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui98&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui98&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui98_body" unselectable="on" title="下划线"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui98&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui99"
							class="edui-box edui-button edui-for-strikethrough">
							<div id="edui99_state"
								onmousedown="$EDITORUI[&quot;edui99&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui99&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui99&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui99&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui99_body" unselectable="on" title="删除线"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui99&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui100" class="edui-box edui-combox edui-for-fontsize">
							<div title="字号" id="edui100_state"
								onmousedown="$EDITORUI[&quot;edui100&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui100&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui100&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui100&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-combox-body">
									<div id="edui100_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui100&quot;]._onButtonClick(event, this);">15.555556297302246px</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui100&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui111" class="edui-box edui-combox edui-for-fontfamily">
							<div title="字体" id="edui111_state"
								onmousedown="$EDITORUI[&quot;edui111&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui111&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui111&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui111&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-combox-body">
									<div id="edui111_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui111&quot;]._onButtonClick(event, this);">宋体</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui111&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui124"
							class="edui-box edui-splitbutton edui-for-forecolor edui-colorbutton">
							<div title="字体颜色" id="edui124_state"
								onmousedown="$EDITORUI[&quot;edui124&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui124&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui124&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui124&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-splitbutton-body">
									<div id="edui124_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui124&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
										<div id="edui124_colorlump" class="edui-colorlump"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui124&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui127"
							class="edui-box edui-splitbutton edui-for-backcolor edui-colorbutton">
							<div title="背景色" id="edui127_state"
								onmousedown="$EDITORUI[&quot;edui127&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui127&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui127&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui127&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-splitbutton-body">
									<div id="edui127_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui127&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
										<div id="edui127_colorlump" class="edui-colorlump"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui127&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui130" class="edui-box edui-button edui-for-indent">
							<div id="edui130_state"
								onmousedown="$EDITORUI[&quot;edui130&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui130&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui130&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui130&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-button-wrap">
									<div id="edui130_body" unselectable="on" title="首行缩进"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui130&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui131" class="edui-box edui-combox edui-for-paragraph">
							<div title="段落格式" id="edui131_state"
								onmousedown="$EDITORUI[&quot;edui131&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui131&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui131&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui131&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-combox-body">
									<div id="edui131_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui131&quot;]._onButtonClick(event, this);">段落</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui131&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui140" class="edui-box edui-separator "></div>
						<div id="edui145"
							class="edui-box edui-splitbutton edui-for-inserttable">
							<div title="表格" id="edui145_state"
								onmousedown="$EDITORUI[&quot;edui145&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui145&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui145&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui145&quot;].Stateful_onMouseOut(event, this);">
								<div class="edui-splitbutton-body">
									<div id="edui145_button_body" class="edui-box edui-button-body"
										onclick="$EDITORUI[&quot;edui145&quot;]._onButtonClick(event, this);">
										<div class="edui-box edui-icon"></div>
									</div>
									<div class="edui-box edui-splitborder"></div>
									<div class="edui-box edui-arrow"
										onclick="$EDITORUI[&quot;edui145&quot;]._onArrowClick();"></div>
								</div>
							</div>
						</div>
						<div id="edui148"
							class="edui-box edui-button edui-for-splittorows">
							<div id="edui148_state"
								onmousedown="$EDITORUI[&quot;edui148&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui148&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui148&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui148&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui148_body" unselectable="on" title="拆分成行"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui148&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui149"
							class="edui-box edui-button edui-for-splittocols">
							<div id="edui149_state"
								onmousedown="$EDITORUI[&quot;edui149&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui149&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui149&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui149&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui149_body" unselectable="on" title="拆分成列"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui149&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui150"
							class="edui-box edui-button edui-for-splittocells">
							<div id="edui150_state"
								onmousedown="$EDITORUI[&quot;edui150&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui150&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui150&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui150&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui150_body" unselectable="on" title="完全拆分单元格"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui150&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui151" class="edui-box edui-button edui-for-mergecells">
							<div id="edui151_state"
								onmousedown="$EDITORUI[&quot;edui151&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui151&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui151&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui151&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui151_body" unselectable="on" title="合并多个单元格"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui151&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui152" class="edui-box edui-button edui-for-mergeright">
							<div id="edui152_state"
								onmousedown="$EDITORUI[&quot;edui152&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui152&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui152&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui152&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui152_body" unselectable="on" title="右合并单元格"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui152&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui153" class="edui-box edui-button edui-for-mergedown">
							<div id="edui153_state"
								onmousedown="$EDITORUI[&quot;edui153&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui153&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui153&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui153&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui153_body" unselectable="on" title="下合并单元格"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui153&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui154" class="edui-box edui-button edui-for-insertcol">
							<div id="edui154_state"
								onmousedown="$EDITORUI[&quot;edui154&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui154&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui154&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui154&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui154_body" unselectable="on" title="前插入列"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui154&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui155" class="edui-box edui-button edui-for-insertrow">
							<div id="edui155_state"
								onmousedown="$EDITORUI[&quot;edui155&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui155&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui155&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui155&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui155_body" unselectable="on" title="前插入行"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui155&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui156" class="edui-box edui-button edui-for-deletecol">
							<div id="edui156_state"
								onmousedown="$EDITORUI[&quot;edui156&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui156&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui156&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui156&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui156_body" unselectable="on" title="删除列"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui156&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui157" class="edui-box edui-button edui-for-deleterow">
							<div id="edui157_state"
								onmousedown="$EDITORUI[&quot;edui157&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui157&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui157&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui157&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui157_body" unselectable="on" title="删除行"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui157&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
						<div id="edui158"
							class="edui-box edui-button edui-for-insertparagraphbeforetable">
							<div id="edui158_state"
								onmousedown="$EDITORUI[&quot;edui158&quot;].Stateful_onMouseDown(event, this);"
								onmouseup="$EDITORUI[&quot;edui158&quot;].Stateful_onMouseUp(event, this);"
								onmouseover="$EDITORUI[&quot;edui158&quot;].Stateful_onMouseOver(event, this);"
								onmouseout="$EDITORUI[&quot;edui158&quot;].Stateful_onMouseOut(event, this);"
								class=" edui-state-disabled">
								<div class="edui-button-wrap">
									<div id="edui158_body" unselectable="on" title="表格前插行"
										class="edui-button-body" onmousedown="return false;"
										onclick="return $EDITORUI[&quot;edui158&quot;]._onClick();">
										<div class="edui-box edui-icon"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="edui1_toolbarmsg" class="edui-editor-toolbarmsg"
				style="display: none;">
				<div id="edui1_upload_dialog" class="edui-editor-toolbarmsg-upload"
					onclick="$EDITORUI[&quot;edui1&quot;].showWordImageDialog();">点击上传</div>
				<div class="edui-editor-toolbarmsg-close"
					onclick="$EDITORUI[&quot;edui1&quot;].hideToolbarMsg();">x</div>
				<div id="edui1_toolbarmsg_label"
					class="edui-editor-toolbarmsg-label"></div>
				<div style="height: 0; overflow: hidden; clear: both;"></div>
			</div>
		</div>
		<div id="edui1_iframeholder" class="edui-editor-iframeholder"
			style="height: 320px; overflow: hidden;">
			<iframe id="baidu_editor_0" width="100%" height="100%" scroll="no"
				frameborder="0"></iframe>
		</div>
		<div id="edui1_bottombar" class="edui-editor-bottomContainer">
			<table>
				<tbody>
					<tr>
						<td id="edui1_elementpath" class="edui-editor-bottombar" style=""><div
								class="edui-editor-breadcrumb" onmousedown="return false;">
								元素路径: <span unselectable="on"
									onclick="$EDITORUI[&quot;edui1&quot;].editor.execCommand(&quot;elementpath&quot;, &quot;0&quot;);">body</span>
								&gt; <span unselectable="on"
									onclick="$EDITORUI[&quot;edui1&quot;].editor.execCommand(&quot;elementpath&quot;, &quot;1&quot;);">p</span>
							</div></td>
						<td id="edui1_autoSave" class="edui-editor-autoSave"><span
							id="autoSaveTips"><font color="blue">正在自动保存.....
									&nbsp;&nbsp;</font></span><span id="autoSave"><span id="openAutoSave"
								style="display: none"><a href="javascript:openAutoSave()">开启自动保存</a>
									&nbsp; | &nbsp;</span><span id="closeAutoSave"><a
									href="javascript:closeAutoSave()">关闭自动保存</a> &nbsp; | &nbsp; <a
									href="javascript:saveData()">保存</a> &nbsp; | &nbsp;<a
									href="javascript:restoreData()">恢复</a> &nbsp; | &nbsp; <a
									href="javascript:releaseData()">清空</a> &nbsp; | &nbsp;</span></span></td>
						<td id="edui1_wordcount" class="edui-editor-wordcount" style="">当前已输入
							8 个字符，您还可以输入99992 个字符</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>