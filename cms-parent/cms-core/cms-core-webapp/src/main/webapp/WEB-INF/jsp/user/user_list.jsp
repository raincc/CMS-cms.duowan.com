<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<title>用户列表</title>
</head>
<body>
	<div class="main-content">
		
		<div class="blank10"></div>
		<!--所有用户列表 start-->
		<div class="user-list">
			<div class="action-bar">
			<form name="search_tag" action="/user/userList.do" method="get" >
				<label for="">用户名：</label>
						<input type="text" name="searchkey" value="${searchkey}" class="style-input fixwidth-l mlm"/>
						<input type="submit" class="go-search-bt c-green-bt mlm" value="搜索" />
						<span class="mlm">共有<span class="c-red-font">${userInfoPage.totalCount}</span>用户</span>
				</form>
			</div>
			<div class="blank10"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>用户名</th>
						<th>加入时间</th>
						<th>最后登录时间</th>
						<th>最后登录IP</th>
						<th>最后登录代理IP</th>
						<th>邮箱</th>
						<th>状态</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="userInfo" items="${userInfoPage.data}" varStatus="idx" begin="0">
					 <c:if test="${idx.index%2==0}">
				<tr  class="odd">
					 </c:if>
					<c:if test="${idx.index%2 !=0}">
				<tr >
					 </c:if>
						<td><a href="">${userInfo.userId}</a></td>
						<td>${userInfo.createTimeStr}</td>
						<td>${userInfo.lastModifyTimeStr}</td>
						<td>${userInfo.userIp}</td>
						<td>${userInfo.proxyIp}</td>
						<td>${userInfo.mail}</td>
						<td>
						 <c:if test="${userInfo.userStatus.value == 1}">
								<span class="c-black-font">
									${userInfo.userStatus.display}
								</span>
							</c:if>	
								<c:if test="${userInfo.userStatus.value  == 0}">
									<span class="c-red-font">
										${userInfo.userStatus.display}
									</span>
								</c:if>
						</td>
						<td>
							<c:if test="${!userInfo.admin}">
								<a class="mhs"  href="javascript:confirmMessage('确定删除该用户？' ,  '/user/deleteUser.do?userId=${userInfo.userId}')">删除</a>
								|
							    <c:if test="${userInfo.userStatus.value == 1}">
									<a href="javascript:confirmMessage('确定冻结该用户？' ,  '/user/freeze.do?userId=${userInfo.userId}')"   >冻结</a>
								</c:if>
								<c:if test="${userInfo.userStatus.value  == 0}">
									<a href="javascript:confirmMessage('确定解冻该用户？' ,  '/user/unFreeze.do?userId=${userInfo.userId}') "  >解冻</a>
								</c:if>
							    |
							    <a class="mhs" href="javascript:resetPassword('${userInfo.userId}', '${userInfo.mail}')">重设密码</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${userInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${userInfoPage.pageSize}"/>
						<jsp:param name="total" value="${userInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/user/userList.do?pageNo=:pageNo${queryStr}"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/user/userList.do?pageNo=1${queryStr}"/>
						<jsp:param name="totalPageCount" value="${userInfoPage.totalPageCount}"/>
			</jsp:include>
		</div>
		<!--所有用户列表 end-->
		
	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>	
<script type="text/javascript"  src="${ROOT}/resource/js/user/resetPassword.js"></script>	
<script type="text/javascript">
function confirmMessage(message , url){
	if(confirm(message)){
		location.href =url ;
	}
}
</script>
</body>
</html>