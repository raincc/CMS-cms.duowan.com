<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>刷新页面</title>
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/bootstrap/css/bootstrap.min.css" media="all">
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/bootstrap/css/bootstrap-responsive.min.css" media="all">
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
<script type="text/javascript"  src="${ROOT}/resource/plugins/My97DatePicker/WdatePicker.js"></script>
</head>
<body>
	<div  class="main-content">
	<jsp:include page="/head.do"></jsp:include>
	
	<div class="blank20"></div>
		<!--常用操作 start-->
		<div class="comment-action">
				<div class="updata-file">
				<h2>刷新cdn</h2>
				<table>
					<tbody>
						<tr>
							<td>刷新URL</td>
							<td><input type="text" id="flushUrl" class="style-input fixwidth-400 mls"></td>
							<td class="last-col"><input type="button" onclick="flushCdnByUrl()" class="u-inputBtn" value="刷新"></td>
						</tr>
						<tr>
							<td>刷新目录</td>
							<td><input type="text" id="flushDir" class="style-input fixwidth-400 mls"></td>
							<td class="last-col"><input type="button" onclick="flushCdnByDir()" class="u-inputBtn" value="刷新"></td>
						</tr>


					</tbody>
				</table>
			</div>
			<div class="blank10"></div>
			<!-- 
			<div class="updata-file">
				<h2>同步文件</h2>
				<table>
					<tbody>
						<tr>
							<td>同步文件到测试页面</td>
							<td>无任务</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="同步"></td>
						</tr>
						<tr>
							<td>同步文件到测试页面</td>
							<td>无任务</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="同步"></td>
						</tr>
						<tr>
							<td>同步文件到测试页面</td>
							<td>无任务</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="同步"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="blank10"></div>
			 -->
			 <!--c:if test="${userInfo.admin}"  -->
			<div class="flash-channal">
				<h2>刷新整个频道</h2>
				<table>
					<tbody>
						<tr>
							<td>刷新本频道所有tag：<font color="red">(有效)</font></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新" onclick="flushAllTagInChannel()"></td>
						</tr>
						<tr>
							<td>刷新本频道所有模版：<font color="red">(有效)</font></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新" onclick="flushAllTemplateInChannnel()"></td>
						</tr>
						<tr>
							<td>刷新本频道所有文章静态页：<font color="red">(有效)</font></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新" onclick="flushAllArticleInChannelId()"></td>
						</tr>
						<tr>
							<td>刷新本频道指定日期后的所有文章静态页： <font color="red">(有效)</font>
							<input type="text" class="style-input fixwidth-l" name="flushArticle_Date"  id="flushArticle_Date" autocomplete="off" 
									onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" /><span class="mlm">(格式：20007-11-24 14:33:22)</span></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新" onclick="flushArticleAfterDateInChannelId()"></td>
						</tr>
						<tr>
							<td>刷新本频道使用指定最终页模板的所有文章静态页： <font color="red">(有效)</font>
							<input type="text" class="style-input fixwidth-l" name="flushByArticleTemplateId"  id="flushByArticleTemplateId"  /><span class="mlm">(格式：模板id)</span></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新" onclick="flushByArticleTemplateId()"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="blank10"></div>
			<!--  /c:if-->
			<!-- 
			<div class="tag-manage">
				<h2>tag管理</h2>
				<table>
					<tbody>
						<tr>
							<td>校正tag：</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新"></td>
						</tr>
						<tr>
							<td>刷新本频道指定tag：<input type="" class="style-input fixwidth-l"></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新"></td>
						</tr>
						<tr>
							<td>刷新指定tag下所有文章：<input type="" class="style-input fixwidth-l"></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新"></td>
						</tr>
						<tr>
							<td>合并本频道tag： <span class="mls">把tag</span> <input type="" class="style-input fixwidth-l"><span class="mls">合并到</span><input type="" class="style-input fixwidth-l mls"><span class="mlm"></span></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新"></td>
						</tr>
						<tr>
							<td><span class="mls">把tag</span> <input type="" class="style-input fixwidth-l"><span class="mls">复制到</span><input type="" class="style-input fixwidth-l mls"><span class="mlm"></span></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
						<tr>
							<td><span class="mls">把tag</span> <input type="" class="style-input fixwidth-l"><span class="mls">复制到</span><input type="" class="style-input fixwidth-l mls"><span class="mlm"></span></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
						<tr>
							<td>查看tagtree：</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
						<tr>
							<td>批量修改指定tag下所有文章的模板id: <span class="mls">tag：</span><input type="" class="style-input fixwidth-l"><span class="mlm">模版id：</span><input type="" class="style-input fixwidth-l"></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="blank10"></div>
			<div class="other-manage">
				<h2>其他操作</h2>
				<table>
					<tbody>
						<tr>
							<td>刷新本频道所有评论的html文件:</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="刷新"></td>
						</tr>
						<tr>
							<td>删除本频道指定类型的所有模板:<input type="" class="style-input fixwidth-l mlm"></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="删除"></td>
						</tr>
						<tr>
							<td>删除本频道指定合作的所有模板:<input type="" class="style-input fixwidth-l mlm"></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="删除"></td>
						</tr>
						<tr>
							<td>把本频道的所有模板复制为指定cooperate类型到本频道: <input type="" class="style-input fixwidth-l mls"><span class="mlm">目标模板:</span><input type="" class="style-input fixwidth-l mls"><span class="mlm">合作名:</span><input type="" class="style-input fixwidth-l mls"></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
						<tr>
							<td>生成json文件</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
						<tr>
							<td>对本频道所有文章中的热词进行处理:</td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
						<tr>
							<td>查看list:<input type="" class="style-input fixwidth-l mls"></td>
							<td class="last-col"><input type="button" class="u-inputBtn" value="提交"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="blank20"></div>
		</div>
		 -->
		<!--常用操作 end-->
	
	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>    
<script type="text/javascript"  src="${ROOT}/resource/bootstrap/js/bootstrap.min.js"></script>    
<script type="text/javascript">

function flushCdnByUrl(){
	var flushUrl = $('#flushUrl').val();
	if(flushUrl == ''){
		alert('请填入需要刷新的URL!');
		return;
	}
	if(confirm('确定要刷新CDN？')){
		$.get('/channel/flushCdnByUrl.do', { 'flushUrl': flushUrl  },
				  function(data){
				    alert(data);
				  }
		);
	}
}

function flushCdnByDir(){
	var flushDir = $('#flushDir').val();
	if(flushDir == ''){
		alert('请填入需要刷新的目录!');
		return;
	}
	if(confirm('确定要刷新CDN目录？')){
		$.get('/channel/flushCdnByDir.do', { 'flushDir': flushDir  },
				  function(data){
				    alert(data);
				  }
		);
	}
}

//刷新本频道所有tag
function flushAllTagInChannel(){
	if(confirm('确定要刷新所有tag？')){
		$.get('/channel/flushAllTagInChannel.do', { 'channelId': '${channelId}' },
				  function(data){
				    alert('刷新本频道所有tag成功');
				  }
		);
	}
}
//刷新本频道所有模板
function flushAllTemplateInChannnel(){
	if(confirm('确定要刷新所有模版？')){
		$.get('/channel/flushAllTemplateInChannnel.do', { 'channelId': '${channelId}' },
				  function(data){
				    alert('刷新本频道所有模版成功');
				  }
		);
	}
}

//刷新本频道所有文章静态页
function flushAllArticleInChannelId(){
	if(confirm('确定要刷新所有文章静态页？')){
		$.get('/channel/flushAllArticleInChannelId.do', { 'channelId': '${channelId}' },
				  function(data){
				    alert('刷新本频道所有文章静态页成功');
				  }
		);
	}
}

function flushArticleAfterDateInChannelId(){
	
	var dateStr = $("#flushArticle_Date").val().replace(/^\s+|\s+$/g,"");
	if(dateStr == ''){
		alert('日期不能为空');
	};
	if(confirm('确定要刷新' + dateStr + '时间后的文章静态页？')){
		$.get('/channel/flushArticleAfterDateInChannelId.do', { 'channelId': '${channelId}', 'dateStr': dateStr },
				  function(data){
				    alert('刷新' + dateStr + '时间后的文章静态页成功');
				  }
		);
	}
}

function flushByArticleTemplateId(){
	var articleTemplateId = $("#flushByArticleTemplateId").val().replace(/^\s+|\s+$/g,"");
	if(articleTemplateId == ''){
		alert('模板id不能为空');
	};
	if(confirm('确定要刷新模板id为' + articleTemplateId + '的文章静态页？')){
		$.get('/channel/flushByArticleTemplateId.do', { 'channelId': '${channelId}', 'articleTemplateId': articleTemplateId },
				  function(data){
				    alert('刷新模板id为' + articleTemplateId + '的文章静态页成功');
				  }
		);
	}
}

</script>	
</body>
</html>