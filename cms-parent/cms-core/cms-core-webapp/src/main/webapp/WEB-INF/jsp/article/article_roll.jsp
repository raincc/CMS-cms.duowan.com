<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>滚动新闻</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body >
	<div class="main-content">
	<jsp:include page="/head.do"></jsp:include>

		<!--滚动文章搜索结果 start-->
		<div class="scroll-artical">
			<div class="blank10"></div>
			<div class="action-bar">
			<form name="search_article" action="/article/articleRoll.do" method="get" >
				<input type="hidden"  name="channelId" value="${channelInfo.id}" />
				<input type="hidden"  name="status" value="${status}" />
				<input type="text" autocomplete="off"   value="${searchkey}"  name="searchkey"   
						onclick="clickSearchKey()"   id="searchkey" class="style-input fixwidth-l">

				<select class="style-select fixwidth-s" name="type" id="type" >
						<option value="title">标题</option>
						<option value="userid">编辑</option>
						<option value="author">作者</option>
						<option value="source">来源</option>
						<option value="posttime">日期</option>
				</select>
				<a href="javascript:document.search_article.submit();"  class="go-search-bt c-green-bt mlm">搜索</a>
				<input type="button" class="u-inputBtn" value="滚动文章"  onclick="redirectPage('/article/articleRoll.do?channelId=${channelInfo.id}')"  />
				<input type="button" class="u-inputBtn" value="预定发布文章" onclick="redirectPage('/article/articleRoll.do?channelId=${channelInfo.id}&status=1')"  />
				<input type="button" class="u-inputBtn" value="标签为空文章" onclick="redirectPage('/article/articleRoll.do?channelId=${channelInfo.id}&status=2')"  />
			</form>
			</div>

			<div class="blank10"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>最新滚动文章</th>
						<th>编辑</th>
						<th>来源</th>
						<c:if test="${!isPrepublish}">
							<th>发布时间</th>
						</c:if>
						 <c:if test="${isPrepublish}">
							<th>预定发布时间</th>
						</c:if>
						<th class="tag-list">标签</th>
					</tr>
				</thead>
				<tbody>
			<c:forEach var="articleInfo" items="${articleInfoPage.data}" varStatus="idx" begin="0">
				<tr >
						<td class="name">
								<a href="${articleInfo.urlOnLine}" target="_blank">★</a>
									<a href="/article/toEditArticlePage.do?articleId=${articleInfo.id}&channelId=${channelInfo.id}">${articleInfo.title}</a>
									<c:if test="${articleInfo.isHeadPic == 'yes'}">
													<a href="${articleInfo.pictureUrl}" target="_blank"><span class="c-red-font showImage" key="${articleInfo.pictureUrl}">(图)</span></a>
										</c:if>
								<a href="${articleInfo.urlOnCms}" target="_blank" class="showTip" >☆</a>
						</td>
						<td ><a href="/article/articleRoll.do?channelId=${channelInfo.id}&status=&searchkey=${articleInfo.userId}&type=userid">${articleInfo.userId}</a></td>
						<td ><a href="/article/articleRoll.do?channelId=${channelInfo.id}&status=&searchkey=${articleInfo.source}&type=source">${articleInfo.source}</a></td>
						<c:if test="${!isPrepublish}">
							<td ><span class="desc"><a href="/article/articleRoll.do?channelId=${channelInfo.id}&status=&searchkey=${fn:substring(articleInfo.publishTimeStr, 0, 10)}&type=posttime">${articleInfo.publishTimeStr}</a></span></td>
						</c:if>
						<c:if test="${isPrepublish}">
							<td >${articleInfo.prePublishTimeStr}</td>
						</c:if>
						<td class="tag-list">
						<c:forEach var="singleTag" items="${articleInfo.tagArr}" varStatus="status" begin="0">
							<a href="/article/articleList.do?tag=${singleTag}&channelId=${channelInfo.id}" class="mhs"  target="contentIframe">${singleTag}</a>
						</c:forEach>
						</td>
					</tr>
					
					</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${articleInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${articleInfoPage.pageSize}"/>
						<jsp:param name="total" value="${articleInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/article/articleRoll.do?channelId=${channelInfo.id}&status=${status}&pageNo=:pageNo${queryStr}"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/article/articleRoll.do?channelId=${channelInfo.id}&status=${status}&pageNo=1${queryStr}"/>
						<jsp:param name="totalPageCount" value="${articleInfoPage.totalPageCount}"/>
			</jsp:include>
			
		</div>
		<!--滚动文章搜索结果 end-->

	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>		
<script type="text/javascript"  src="${ROOT}/resource/plugins/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
var type = '${type}';
$(function (){
	if(type != ''){
		$('#type option[value='+type+']').attr('selected', true); 
	}
	$(".showTip").mouseover(function(e){
			//创建数据显示div元素
			var tipDIV = "<div id='preViewTip'>预览</div>";
			//追加到文档中
			$("body").append(tipDIV);
			$("#preViewTip").css({
				"top": (e.pageY + 10) + "px",
				"left": (e.pageX + 10) + "px",
				"position":"absolute",
				"border":"1px solid #333",
				"background":"#f7f5d1",
				"padding":"1px",
				"color":"#333",
				"display":"none"
			}).show("fast");
			
		}).mouseout(function(){
			$("#preViewTip").remove();//移除
		}).mousemove(function(e){
			$("#preViewTip").css({
				"top": (e.pageY + 10) + "px",
				"left": (e.pageX + 10) + "px",//跟随光标移动
			})
		});
	
	$(".showImage").mouseover(function(e){
			var imgSrc = $(this).attr('key');
			//创建数据显示div元素
			var tipDIV = "<div id='preViewTip'><img src='"+imgSrc+"' height='90' width='130'></div>";
			//追加到文档中
			$("body").append(tipDIV);
			$("#preViewTip").css({
				"top": (e.pageY - 90) + "px",
				"left": (e.pageX + 10) + "px",
				"position":"absolute",
			}).show("fast");
			
		}).mouseout(function(){
			$("#preViewTip").remove();//移除
		}).mousemove(function(e){
			$("#preViewTip").css({
				"top": (e.pageY - 90) + "px",
				"left": (e.pageX + 10) + "px",//跟随光标移动
			})
		});
	
});
//点击查询输入框
function clickSearchKey(){
	if ($('#type').val() == 'posttime'){
		$('#searchkey').click(function (){
			WdatePicker({dateFmt:'yyyy-MM-dd'});
		});
	}else{
		$('#searchkey').unbind('click' );
	}
}

function redirectPage(url){
	location.href = url;
}
</script>	
</body>
</html>