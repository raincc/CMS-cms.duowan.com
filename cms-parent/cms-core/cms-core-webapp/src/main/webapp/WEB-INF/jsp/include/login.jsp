<%@page contentType="text/html; charset=UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
<title>CMS - 用户登录 - Login</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="${ROOT }/resource/css/global.css" media="all">
</style>

<script type="text/javascript">
if(window.parent != window){
			window.parent.location = window.location;	
}
var errorMessage = '${errorMessage}';
if(errorMessage != ''){
	alert(errorMessage);
}
	var key = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b",
			"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
			"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B",
			"C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" ];
	function rsalogin() {
		var result = document.getElementById("password").value;
		if (result == null || result == "") {
			alert("请您输入密码!");
			return;
		}
		result = changePass(result);
		document.getElementById("password").value = result;
		var loginForm = document.getElementById("loginForm");
		loginForm.action = "/login.do";
		loginForm.submit();
	}

	function changePass(src) {
		if (src == null || src == "") {
			return;
		}
		var len = src.length;
		if (len == 128) {
			return src;
		}
		var pass = "";
		var index = Math.floor(Math.random() * 60 + 1);
		for (i = 0; i < 126; i++) {
			if (i == index) {
				pass += src;
				i += len - 1;
			} else {
				var ran = Math.floor(Math.random() * 60);
				pass = pass + key[ran];
			}
		}
		pass = pass + key[index] + key[len];
		return pass;
	}
	document.onkeydown = keyListener;
	function keyListener(e) {
		e = e ? e : event;
		if (e.keyCode == 13) {
			rsalogin();
			return false;
		}
	}
</script>
</head>

<body>
	<div align="center" style="margin: 150px auto;">
		<span style="color: #61B0E9;font:30px/1.5 tahoma,'微软雅黑'">多玩发布器</span><br/><br/>
		<form id="loginForm" name="loginForm" action="" method="get">
			<span >用户名</span>:&nbsp;<input id="userId" name="userId" type="text" size="15" /><br />
			&nbsp;&nbsp;<span >密 码</span>:&nbsp;<input id="password" name="password" type="password" size="15" /><br /> <br /> 
			<a href="javascript:rsalogin();" class="c-green-bt go-search-bt mls">登 录 </a>
		</form>
		<br /> Powered By: <strong>Content Management System</strong> v 1.0
		Beta <br /> <br /> Copyright &copy; 2012, <strong><a
			href="">DuoWan.com</a> </strong> all rights reserved.
	</div>
</body>

</html>

