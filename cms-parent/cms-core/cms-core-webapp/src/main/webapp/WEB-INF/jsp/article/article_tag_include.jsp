<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   			
		
	<div class="tag-area">
				<div class="blank10"></div>			
			<!-- 常用标签 -->
			<div class="tag-list" id="useMostTagArea">
			</div>
			<div class="blank10"></div>
			<p class="form-line">
				<select name="tagLevel1" id="tagLevel1" class="style-select"  onchange="article_tagLevel_changer.changeTagLevel1()"   >
					<option value="">----请选择分类----</option>
				</select>
				<select name="tagLevel2" id="tagLevel2" style="display: none;" class="style-select mls"  onchange="article_tagLevel_changer.changeTagLevel2()">
					<option value="">----请选择分类----</option>
				</select>
				<select name="tagLevel3" id="tagLevel3" style="display: none;" class="style-select mls" onchange="article_tagLevel_changer.changeTagLevel3()">
					<option value="">----请选择分类----</option>
				</select>
				<select name="tagLevel4" id="tagLevel4" style="display: none;" class="style-select mls" onchange="article_tagLevel_changer.changeTagLevel4()">
					<option value="">----请选择分类----</option>
				</select>
				<select name="tagLevel5" id="tagLevel5" style="display: none;" class="style-select mls" onchange="article_tagLevel_changer.changeTagLevel5()">
					<option value="">----请选择分类----</option>
				</select>
				<c:if test="${isCurrentTagEmpty }">
					<input type="text"  name="tags"  id="tags" value="${articleInfo.tags}" class="style-input fixwidth-${width }">
				</c:if>
				
				<c:if test="${!isCurrentTagEmpty }">
					<input type="text"  name="tags"  id="tags" value="${currentTag}," class="style-input fixwidth-${width }">
				</c:if>
				<em class="note-num c-red-font"  >
					 <font id="tagTips"></font>
				</em>
			</p>
			<div class="blank10"></div>
			<div class="tag-list">
				特殊标签：
				<c:forEach var="specialTag" items="${specialTagList}" varStatus="idx" begin="0">
						<input type="button" class="u-inputBtn" value="${specialTag.name}"  onclick="article_tag.clickTagButton('${specialTag.name}')"  />
				</c:forEach>
			</div>
			
			<!-- 二级标签用按钮形式展示的区域 -->
			<div class="form-line" id="tagLevel2ButtonArea">
			</div>
			
			<!-- 三级标签用按钮形式展示的区域 -->
			<div class="form-line" id="tagLevel3ButtonArea">
			</div>
		</div>	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script> 			
<script type="text/javascript">
var root = '${ROOT}';
</script>			
<script type="text/javascript"  src="${ROOT}/resource/js/load_util.js"></script>
<!-- 动态设置"发布器普通标签"的下拉框内容 -->
<script type="text/javascript"  src="${ROOT}/resource/js/article/article_tag.js"></script>