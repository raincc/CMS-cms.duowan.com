<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑用户个人信息</title>
<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
<style type="text/css">
.mod-tab-hd{overflow:hidden;padding:0 10px;height:32px;line-height:32px;background:#61b0e9;}
.mod-tab-hd .tab-nav{float:left;}
.mod-tab-hd li{float:left;cursor:pointer;text-align:center;width:80px;height:32px;background:none;color:#FFF;}
.mod-tab-hd li.selected{font-weight:700;background:#D9D9D9;color:#474A4B;}
.mod-tab-hd li.hover{background:#EFEFEF;color:#474A4B;}
</style>

</head>
<body>

<div class="mod-tab-hd">
	<ul class="tab-nav" key="head_info">
        <li class="selected" key="personal_info">个人信息</li>
		<li key="change_name">修改用户名</li>
		<li key="change_mail">修改邮箱</li>
		<li key="change_password">修改密码</li>
		<li key="binding_udb">绑定udb</li>
	</ul>
</div>


<div class="main-content">
	
	<div key="personal_info_div">
		<div class="edit-mod">
			<p class="form-line">
				<label for="">用户名：</label>
				<input type="text" value="${userInfo.userId}" class="style-input fixwidth-l" readonly="readonly" style="color:grey">
				<div class="blank15"></div>
				<label for="">&nbsp;&nbsp;&nbsp;邮箱：</label>
				<input type="text" id="personalinfo_mail" value="${userInfo.mail}"  class="style-input fixwidth-l" readonly="readonly" style="color:grey">
				<div class="blank15"></div>
			</p>
		</div>
		<span class="ui-button"><a href="javascript:resetPassword('${userInfo.userId}', '${userInfo.mail}')">重置密码</a></span>
	</div>
	<div key="change_name_div" style="display:none">
		<form action="/user/updatePersonalName.do"  method="post"  id="updatePersonalNameForm" >
			<div class="edit-mod">
				<p class="form-line">
					<label for="">原用户名：</label>
					<input type="text" value="${userInfo.userId}" class="style-input fixwidth-l" disabled="disabled">
					<div class="blank15"></div>
					<label for="">新用户名：</label>
					<input type="text"  name="userId"  id="userId" onchange="checkUserId()"  class="style-input fixwidth-l">
						<em class="note-num c-red-font"  >
							 <font id="userId_check" ></font>
						</em>
				</p>
			</div>
			<span class="ui-button"><a href="javascript:updatePersonalName()">确定</a></span>
		</form>
	</div>
	<div key="change_mail_div" style="display:none">
		<form action="/user/updatePersonalMail.do"  method="post"  id="updatePersonalMailForm" >
			<div class="edit-mod">
				<p class="form-line">
					<label for="">&nbsp;&nbsp;&nbsp;原邮箱：</label>
					<input type="text" id="changemail_mail" value="${userInfo.mail}"  class="style-input fixwidth-l" disabled="disabled">
					<div class="blank15"></div>
					<label for="">&nbsp;&nbsp;&nbsp;新邮箱：</label>
					<input type="text"  name="mail"  id="mail" onchange="checkmail()"  class="style-input fixwidth-l">
						<em class="note-num c-red-font"  >
							 <font id="mail_check" ></font>
						</em>
				</p>
			</div>
			<span class="ui-button"><a href="javascript:updatePersonalMail()">确定</a></span>
		</form>
	</div>
	<div key="change_password_div" style="display:none">
		<form action="/user/updatePersonalPassword.do"  method="post"  id="updatePersonalPasswordForm" >
			<div class="edit-mod">
				<p class="form-line">
					<label for="">&nbsp;&nbsp;&nbsp;&nbsp;原密码：</label>
					<input type="password" name="oldpassword"  id="oldpassword"  class="style-input fixwidth-l" onchange="checkPersonalPassword()">
						<em class="note-num c-red-font"  >
							 <font id="password_check" ></font>
						</em>
					<div class="blank15"></div>
					<label for="">&nbsp;&nbsp;&nbsp;&nbsp;新密码：</label>
					<input type="password" name="newpassword"  id="newpassword"  class="style-input fixwidth-l" onchange="checkNewPassword()">
						<em class="note-num c-red-font"  >
							 <font id="newpassword_check" ></font>
						</em>
					<div class="blank15"></div>
					<label for="">确认密码：</label>
					<input type="password" name="repassword"  id="repassword"  class="style-input fixwidth-l" onchange="checkRePassword()">
						<em class="note-num c-red-font"  >
							 <font id="repassword_check" ></font>
						</em>
					<div class="blank15"></div>
				</p>
			</div>
			<span class="ui-button"><a href="javascript:updatePersonalPassword()">确定</a></span>
		</form>
	</div>
	<div key="binding_udb_div" style="display:none">
		<form action="/user/bindingUdb.do"  method="post"  id="bindingUdbForm" >
			<div class="edit-mod">
				<p class="form-line">
					<label for="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户名：</label>
					<input type="text" value="${userInfo.userId}"  class="style-input fixwidth-l" disabled="disabled">
					<div class="blank15"></div>
					<label for="">通行证账号：</label>
					<input type="text" name="passport" id="passport" onchange="checkPassport()" value="${userInfo.udbName}" class="style-input fixwidth-l">
						<em class="note-num c-red-font"  >
							 <font id="passport_check" ></font>
						</em>
				</p>
			</div>
			<span class="ui-button"><a href="javascript:bindingUdb()">确定</a></span>
		</form>
	</div>
		
</div>	
	
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery.form.js"></script>
<script type="text/javascript"  src="${ROOT}/resource/js/user/resetPassword.js"></script>
<script type="text/javascript">
var root = '${ROOT}';
$(function(){
	$("[key=head_info] > [key]").click(function(){
		$(this).addClass("selected").siblings("[key]").removeClass("selected");
		var prefix = $(this).attr("key");
		$("[key="+prefix+"_div]").show().siblings("div").hide();
		$("#passport").select();
	});
})
//更改用户名部分----------------------
function checkUserId(){
	document.getElementById('userId_check').innerHTML='正在检测用户名是否可用....';
    var userId=document.getElementById('userId').value;
    
    var reg = new RegExp("^[\\u4E00-\\u9FFF]{1,4}$");
	if(!reg.test(userId)){
		$("#userId").css({"border-color":"red"}).select().focus();
		$("#userId_check").html("只允许汉字，且不超过4个汉字!");
		return ;
	}
    
	$.ajax({
	   type: 'GET',
	   url: '/user/checkUserId.do?userId=' + userId,
	   async: false,
	   cache: false,
	   dataType: 'text',
	   success: function(data){
		   		if('addUser' == data){
		   			$("#userId").css({"border-color":""})
		   			$("#userId_check").html("该用户名可以使用.");
		   		} else {
		   			$("#userId").css({"border-color":"red"}).select().focus();
		   			$("#userId_check").html("用户名已存在,请更换.");
		   		}
	   }
	});
}

function updatePersonalName(){
	var tip = $("#userId_check").html();
	if(tip != "该用户名可以使用."){
		alert("请检查新用户名!");
		$("#userId").css({"border-color":"red"}).select().focus();
		return ;
	}
	$('#updatePersonalNameForm').submit();
}
//--------------------------

//更改邮箱部分----------------------------------
function checkmail(){
	var mail_value = $("#mail").val().trim();
	if(mail_value.indexOf("@") == -1){
		$("#mail_check").html("请检查新邮箱格式!");
		$("#mail").css({"border-color":"red"}).select();
	}else if(mail_value == $("#changemail_mail").val().trim()){
		$("#mail_check").html("新邮箱与原邮箱一样，请检查");
		$("#mail").css({"border-color":"red"}).select();
	}else {
		$("#mail").css({"border-color":""});
		$("#mail_check").html("新邮箱格式正确");
	}
	
}

function updatePersonalMail(){
	if($("#mail_check").html() != "新邮箱格式正确"){
		$("#mail").css({"border-color":"red"}).select();
		alert("请检查新邮箱!");
		return ;
	}
	var mail_value = $("#mail").val().trim();
	var options = { 
        url:'/user/updatePersonalMail.do', //提交给哪个执行
        type:'POST', 
        dataType: 'text',
        success: function(data){
        		if(data.indexOf('success') != -1){
        			$("#changemail_mail").val(mail_value);
        			$("#personalinfo_mail").val(mail_value);
        			$("#mail").val("");
        			alert('修改成功！');
        		}else {
        			alert('修改失败，请稍后重试');
        		}
        }, //显示操作提示
        error: function(){
        	alert('服务器连接超时，请稍后重试');
        },   
        timeout:5000 
	};
	$('#updatePersonalMailForm').ajaxSubmit(options); 
	
}
//----------------------------

//更改密码部分-------------------------
function checkPersonalPassword(){
	$("#password_check").html('正在检测密码是否正确...');
	$.ajax({
	   type: 'POST',
	   url: '/user/checkPersonalPassword.do',
	   async: false,
	   cache: false,
	   dataType: 'text',
	   data:{"password":$("#oldpassword").val()},
	   success: function(data){
		   		if('success' == data){
		   			$("#oldpassword").css({"border-color":""});
		   			$("#password_check").html("密码正确");
		   		} else {
		   			$("#oldpassword").css({"border-color":"red"}).select().focus();
		   			$("#password_check").html("密码错误");
		   		}
	   }
	});
}

function checkNewPassword(){
	var value = $("#newpassword").val();
	if(value.length < 6 || value.length > 20 || isIntegerNumber(value)){
		$("#newpassword").css({"border-color":"red"}).select().focus();
		$("#newpassword_check").html("密码必须6-20个字符,且不能全为数字!");
	}else{
		$("#newpassword").css({"border-color":""});
		$("#newpassword_check").html("密码符合规范");
	}
}

function isIntegerNumber(value){
	var n_value = parseInt(value);
	if(isNaN(n_value)){
		return false;
	}
	if(value.length > (n_value + "").length){
		return false;
	}
	return true;
}

function checkRePassword(){
	
	if($("#newpassword_check").html() != "密码符合规范"){
		$("#newpassword").css({"border-color":"red"}).select().focus();
		$("#newpassword_check").html("密码必须6-20个字符,且不能全为数字!");
		return ;
	}
	
	if($("#newpassword").val() != $("#repassword").val()){
		$("#repassword_check").html("两次密码不一样");
	}else{
		$("#repassword_check").html("重复密码通过");
	}
	
}

function updatePersonalPassword(){
	if($("#password_check").html() != "密码正确"){
		alert("请重新输入原密码进行验证");
		$("#oldpassword").css({"border-color":"red"}).select().focus();
		return ;
	}
	checkRePassword();//调用重复密码检查
	if($("#repassword_check").html() != "重复密码通过"){
		alert("新密码或者重复密码没通过，请检查");
		$("#newpassword").css({"border-color":"red"}).select().focus();
		return ;
	}
	var options = { 
        url:'/user/updatePersonalPassword.do', //提交给哪个执行
        type:'POST', 
        dataType: 'text',
        success: function(data){
        		if(data.indexOf('success') != -1){
        			alert('修改成功!');
        			location.href=root;
        		}else {
        			alert('修改失败!' + data);
        		}
        }, //显示操作提示
        error: function(){
        	alert('修改失败，请稍后重试');
        },   
        timeout:5000 
	};
	$('#updatePersonalPasswordForm').ajaxSubmit(options); 
}

//绑定udb通行证----------------------------
function checkPassport(){
	
	var passport = $("#passport").val().trim();
	
	if("dw_" != passport.substring(0,3) || passport.length < 4 || passport.length > 16){
		$("#passport_check").html("通行证必须以'dw_'开头，4-16个字符!");
		$("#passport").select();
	}else{
		$("#passport_check").html("正在检测通行证是否已存在于系统...");
	
		$.ajax({
			   type: 'GET',
			   url: '/login/checkUdbName.do?udbName=' + passport,
			   async: false,
			   cache: false,
			   dataType: 'text',
			   success: function(data){
				   		if('true' == data){
				   			$("#passport_check").html("该通行证已经存于系统!");
				   		} else {
				   			$("#passport_check").html("该通行证可以使用!");
				   		}
			   }
			});
	}
}

function bindingUdb(){
	if($("#passport_check").html() != "该通行证可以使用!"){
		alert("请检查通行证!");
		$("#passport").css({"border-color":"red"}).select().focus();
		return ;
	}
	var options = { 
        url:'/user/bindingUdb.do', //提交给哪个执行
        type:'POST', 
        dataType: 'text',
        success: function(data){
        		if(data.indexOf('success') != -1){
        			alert('绑定成功!');
        		}else {
        			alert('绑定失败!' + data);
        		}
        }, //显示操作提示
        error: function(){
        	alert('绑定失败，请稍后重试');
        },   
        timeout:5000 
	};
	$('#bindingUdbForm').ajaxSubmit(options); 
}

</script>
</body>
</html>