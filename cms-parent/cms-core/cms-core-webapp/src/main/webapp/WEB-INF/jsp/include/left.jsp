<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<style>
		html,body{background-color: #F7F7F7;}
	</style>
	<script src="http://www.duowan.com/public/assets/sys/js/jquery.js?02131514.js"></script>
</head>
<body >
<div class="main-slide" id="mainSlide" >
		<p class="user-name">${userInfo.userId}， <br />欢迎您！
			<a title="您拥有${channelAmount }个频道的权限">(${channelAmount})</a>
		</p>
		<div class="main-nav">
			<h2><a href="">首页</a></h2>
			<ul class="parent-list J-tree">
				<c:forEach var="userPowerInfo" items="${userPowerInfoList}" varStatus="idx" begin="0">
					<c:if test="${idx.first == true || (userPowerInfoList[idx.index-1].sort != userPowerInfo.sort)}">
						<li class="nav-item subText">${userPowerInfo.sort}</li>
					</c:if>	
				<li class="nav-item">
					<p class="subject-title parent-title">
						<a href="${ROOT }/channel/toChannelMainPage.do?channelId=${userPowerInfo.channelId}"  target="contentIframe">${userPowerInfo.channelInfo.name}</a>
						<i class="handle"></i>
					</p>
					<ul class="son-list">
						<li><a href="/article/toEditArticlePage.do?channelId=${userPowerInfo.channelId}&userId=${userInfo.userId}" target="contentIframe">发表文章</a></li>
						<li><a href="/article/articleRoll.do?channelId=${userPowerInfo.channelId}" target="contentIframe">滚动新闻</a></li>
						<li><a href="/article/articleList.do?channelId=${userPowerInfo.channelId}" target="contentIframe">文章列表</a></li>
						<!-- 
						<li><a href="/article/toEditEmptyLinkArticlePage.do?channelId=${userPowerInfo.channelId}&userId=${userInfo.userId}" target="contentIframe">发表空链接</a></li>
						<li><a href="/article/articleRoll.do?channelId=${userPowerInfo.channelId}&status=1" target="contentIframe">预定发布文章</a></li>
						<li><a href="/article/articleRoll.do?channelId=${userPowerInfo.channelId}&status=2" target="contentIframe">标签为空文章</a></li>
						<li><a href="/article/articleRoll.do?channelId=${userPowerInfo.channelId}&status=3" target="contentIframe">抓取文章</a></li>
						 -->
						<li><a href="/tag/tagManage.do?channelId=${userPowerInfo.channelId}" target="contentIframe">标签</a></li>
						<li><a href="/template/templateList.do?channelId=${userPowerInfo.channelId}" target="contentIframe">模板</a></li>
						<li><a href="/file/fileManage.do?channelId=${userPowerInfo.channelId}" target="contentIframe">文件上传</a></li>
					</ul>
				</li>
			</c:forEach>
				
			</ul>
		</div>
		<div class="action-bar">
			<ul>
				<c:if test="${userInfo.isAdmin}">
				<li><a href="/user/userList.do"  target="contentIframe">用户列表</a></li>
				<li><a href="/channel/channelManage.do"  target="contentIframe">频道管理</a></li>
				</c:if>
				<li><a href="/user/toEditUserPrivateInfoPage.do"  target="contentIframe">用户个人信息</a></li>
				<!-- 
				<li><a href="" target="contentIframe">用户名修改</a></li>
				<li><a href="" target="contentIframe">邮箱修改</a></li>
				<li><a href="" target="contentIframe">获取新密码串</a></li>
				 -->
				<li><a href="javascript:logout()" >退出</a><!-- target="contentIframe" --></li>
			</ul>
		</div>
	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>	
<script type="text/javascript" src="${ROOT }/resource/js/udb.js"></script>
<script type="text/javascript">

function logout(){
	if(confirm('确定退出?')){
		var username = Navbar.getUsername();
		if(username != null){
			Navbar.logout("${ROOT }/logout.do");
		}else{
			parent.location.href = '/logout.do';
		}
		
	}
} 

$(function(){
	var j_root = $("ul.J-tree");
	j_root.find('.handle').toggle(function() {
		$(this).parent().addClass('active').next('.son-list').slideDown();
	}, function() {
		$(this).parent().removeClass('active').next('.son-list').slideUp();
	});
});

</script>
</body>
</html>