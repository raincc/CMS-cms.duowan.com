<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>${tag}--文章列表页</title>
	<link rel="stylesheet" href="http://assets.dwstatic.com/b=lego/1.0.0/css&f=mod.pop.css">
	<script src="http://www.duowan.com/public/assets/sys/js/jquery.js?02131514.js"></script>
	<script src="http://assets.dwstatic.com/b=lego/1.0.0/js&f=lego.popupbox.js"></script>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
<div class="main-content">
<jsp:include page="/head.do"></jsp:include>
		<!--文章列表 start-->
		<div class="artical-list">
			<table class="normal-table">
				<tbody>
				<c:forEach var="tagInfo" items="${tagInfoList}" varStatus="idx" begin="0">
					 <c:if test="${ idx.index%10==0 ||  idx.index == 0 }">
							<tr  class="odd">
						 </c:if>
					<c:if test="${ (idx.index%5 ==0 && idx.index%10 !=0) }">
							<tr >
					</c:if>
						<td><a href="/article/articleList.do?tag=${tagInfo.name}&channelId=${channelInfo.id}"  target="_self">${tagInfo.name}</a></td>
					<c:if test="${ ( (idx.index+1)%5 ==0) ||  idx.last == true  }">
							</tr>
					</c:if>
				</c:forEach>
				</tbody>
			</table>
			<div class="blank10"></div>
			<div class="action-bar">
			<c:if test="${not empty param.tag}">
			<form name="search_article" action="/article/articleList.do" method="get" >
				<input type="text"  autocomplete="off" name="searchkey" id="searchkey" value="${searchkey}"  onclick="clickSearchKey()"   class="style-input fixwidth-l">
				<input type="hidden"  name="channelId" value="${channelInfo.id}" />
				<input type="hidden"  name="tag" value="${tag}" />
				<select class="style-select mls" name="type" id="type"  onchange="changeType()">
						<option value="title">标题</option>
						<option value="userid">编辑</option>
						<option value="author">作者</option>
						<option value="source">来源</option>
						<option value="posttime">日期</option>
				</select>
				<a href="javascript:document.search_article.submit();" class="c-green-bt go-search-bt mls">搜索</a>
				<span class="btn-group mll">
					<input class="u-inputBtn" type="button"  onclick="toEditArticlePage()"  value="发表文章">
					<c:if test="${!isArticleTagRoot}">
						<input class="u-inputBtn" id="post-link-Btn" type="button" value="发表空链接">
						<input class="u-inputBtn" type="button" onclick="toYudingList()" value="预定列表">
					 </c:if>	
				</span>
				</form>
			</c:if>
			</div>
			<div class="blank10"></div>
			<table class="normal-table">
				<thead>
					<tr>
						<th>文章标题</th>
						<th>编辑</th>
						<th>作者</th>
						<th>来源</th>
						<th>发布时间</th>
						<th>权重</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="articleInfo" items="${articleInfoPage.data}" varStatus="idx" begin="0">
						 <c:if test="${articleInfo.isEmpltyLink}">
							<tr  class="odd"  title="空链接" >
						 </c:if>
						<c:if test="${!articleInfo.isEmpltyLink}">
							<tr >
						 </c:if>
						<td>	
									<c:if test="${!articleInfo.isEmpltyLink}">
										<a href="${articleInfo.urlOnLine}" target="_blank">★</a>
										<a href="/article/toEditArticlePage.do?articleId=${articleInfo.id}&channelId=${channelInfo.id}">
											${articleInfo.title} 
										</a>
										<c:if test="${articleInfo.isHeadPic == 'yes'}">
													<a href="${articleInfo.pictureUrl}" target="_blank"><span class="c-red-font showImage" key="${articleInfo.pictureUrl}">(图)</span></a>
											</c:if>
										<a href="${articleInfo.urlOnCms}" target="_blank" class="showTip">☆</a>
									</c:if>
									<c:if test="${articleInfo.isEmpltyLink}">
										<a href="${articleInfo.urlOnLine}" target="_blank">★</a>
										${articleInfo.title}
										<c:if test="${articleInfo.isHeadPic == 'yes'}">
													<a href="${articleInfo.pictureUrl}" target="_blank"><span class="c-red-font showImage" key="${articleInfo.pictureUrl}">(图)</span></a>
										</c:if>
										<a href="${articleInfo.urlOnLine}" target="_blank" class="showTip">☆</a>
									</c:if>
									
						</td>
						<td><a href="articleList.do?channelId=${channelInfo.id}&tag=${tag}&searchkey=${articleInfo.userId}&type=userid">${articleInfo.userId}</a></td>
						<td><a href="articleList.do?channelId=${channelInfo.id}&tag=${tag}&searchkey=${articleInfo.author}&type=author">${articleInfo.author}</a></td>
						<td><a href="articleList.do?channelId=${channelInfo.id}&tag=${tag}&searchkey=${articleInfo.source}&type=source">${articleInfo.source}</a></td>
						<!--lastUpdateTimeStr  ${fn:substring(articleInfo.publishTimeStr, 0, 10)}-->
						<td><a href="articleList.do?channelId=${channelInfo.id}&tag=${tag}&searchkey=${fn:substring(articleInfo.publishTimeStr, 0, 10)}&type=posttime">${articleInfo.publishTimeStr}</a></td>
						<td> ${articleInfo.dayNum}[<a href="javascript:updatePower('${articleInfo.power}'  , '${articleInfo.id}' )">${articleInfo.power}</a>]</td>
						<td>
							<a class="mhs" href="javascript:upDayNum('${articleInfo.id}')">顶</a>|
							<a class="mhs" href="javascript:downDayNum('${articleInfo.id}')">降</a>|
							<a class="mhs" href="javascript:resetDayNum('${articleInfo.id}')">还原</a>|
							<a class="mhs" href="javascript:deleterArticle('${articleInfo.id}')">删</a>|
							<a class="mhs reedit-artical" href="" rel="${articleInfo.id}">修</a>
						</td>
					</tr>
					
					</c:forEach>
				</tbody>
			</table>
			<jsp:include page="../include/pager.jsp">
						<jsp:param name="pageNo" value="${articleInfoPage.currentPageNo}"/>
						<jsp:param name="pageSize" value="${articleInfoPage.pageSize}"/>
						<jsp:param name="total" value="${articleInfoPage.totalCount}"/>
						<jsp:param name="pageUrl" value="http://${header.host}${pageContext.request.contextPath}/article/articleList.do?channelId=${channelInfo.id}&tag=${tag}&pageNo=:pageNo${queryStr}"/>
						<jsp:param name="firstPageUrl" value="http://${header.host}${pageContext.request.contextPath}/article/articleList.do?channelId=${channelInfo.id}&tag=${tag}&pageNo=1${queryStr}"/>
						<jsp:param name="totalPageCount" value="${articleInfoPage.totalPageCount}"/>
			</jsp:include>
		</div>
<!--发表空连接弹出框 start-->
	<div id="post-link" class="pop-box post-link-popubox">
		<div class="box">
			<div class="box-hd">
				<h3 class="title">发表空连接</h3>
				<div class="act">
					<a href="#" class="btn-close J_btnClose" title="关闭">关闭</a>
				</div>
			</div>
			<div class="box-bd pal">
				<!--发表空连接 start-->
				<iframe src=""  id="publishEmptylinkDialogIframe" name="publishEmptylinkDialogIframe" frameborder="0" width="860" height="380"></iframe>
				<!--发表空连接 end-->
			</div>
		</div>
	</div>
	<!--发表空连接弹出框 end-->
	<!--修改文章弹出框 start-->
	<div class="pop-box" id="edit-artical-popubox">
		<div class="box">
			<div class="box-hd">
				<h3 class="title">修改文章</h3>
				<div class="act">
					<a href="#" class="btn-close J_btnClose" title="关闭">关闭</a>
				</div>
			</div>
			<div class="box-bd pal">
				<iframe src=""  id="dialogIframe"  name="dialogIframe"  frameborder="0" width="860" height="430"></iframe>
			</div>
		</div>
	</div>
		
		<!--文章列表 end-->
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>		
<script type="text/javascript"  src="${ROOT}/resource/plugins/jquery_ui/jquery-ui-1.9.2.custom.min.js"></script>   
<script type="text/javascript"  src="${ROOT}/resource/plugins/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
var _value;

$(function (){
	$(".showTip").mouseover(function(e){
			//创建数据显示div元素
			var tipDIV = "<div id='preViewTip'>预览</div>";
			//追加到文档中
			$("body").append(tipDIV);
			$("#preViewTip").css({
				"top": (e.pageY + 10) + "px",
				"left": (e.pageX + 10) + "px",
				"position":"absolute",
				"border":"1px solid #333",
				"background":"#f7f5d1",
				"padding":"1px",
				"color":"#333",
				"display":"none"
			}).show("fast");
			
		}).mouseout(function(){
			$("#preViewTip").remove();//移除
		}).mousemove(function(e){
			$("#preViewTip").css({
				"top": (e.pageY + 10) + "px",
				"left": (e.pageX + 10) + "px",//跟随光标移动
			})
		});
	
	$(".showImage").mouseover(function(e){
			var imgSrc = $(this).attr('key');
			//创建数据显示div元素
			var tipDIV = "<div id='preViewTip'><img src='"+imgSrc+"' height='90' width='130'></div>";
			//追加到文档中
			$("body").append(tipDIV);
			$("#preViewTip").css({
				"top": (e.pageY - 90) + "px",
				"left": (e.pageX + 10) + "px",
				"position":"absolute"
			}).show("fast");
			
		}).mouseout(function(){
			$("#preViewTip").remove();//移除
		}).mousemove(function(e){
			$("#preViewTip").css({
				"top": (e.pageY - 90) + "px",
				"left": (e.pageX + 10) + "px",//跟随光标移动
			})
		});
	
});

function clickSearchKey(){
	if ($('#type').val() == 'posttime'){
		$('#searchkey').click(function (){
			WdatePicker({dateFmt:'yyyy-MM-dd'});
		});
	}else{
		$('#searchkey').unbind('click' );
	}
}

function toYudingList(){
	location.href = '/article/articleList.do?channelId=${channelInfo.id}&tag=${tag}&status=1';
}

//跳转到发表文章页面
function toEditArticlePage(){
	location.href = '/article/toEditArticlePage.do?channelId=${channelInfo.id}&userId=${userInfo.userId}&currentTag=${currentTag}';
}

//注册“发表空连接弹出框”
LEGO.popupBox("#post-link-Btn", {boxSelector: "#post-link"});
$("#post-link-Btn").on("click",function(){
	publishEmptylink();
})
//发表空链接
function publishEmptylink(){
	$('#publishEmptylinkDialogIframe').attr('src' , '/article/toEditEmptyLinkArticlePage.do?channelId=${channelInfo.id}&userId=${userInfo.userId}&tag=${tagParam}');
}

//注册“修改文章弹出框”
LEGO.popupBox(".reedit-artical", {boxSelector: "#edit-artical-popubox"});
$(".reedit-artical").on("click",function(){
	editArticle4TagList($(this).attr("rel"));
})
//修
function editArticle4TagList(articleId){
	$('#dialogIframe').attr('src' , '/article/toEditArticle4TagListPage.do?articleId=' + articleId+'&channelId=${channelInfo.id}&tag=${tag}');
}
//彻底删除
function deleteBatch(){
	   document.getElementById("dialogIframe").contentWindow.deleteByChannelIdAndArticleId();
	   closeDialog();
	   window.setTimeout(function() { 
			 location.reload();
		  }, 500);
}
//提交Article4TagList对象
function submitArticle4TagList(){
		var result = document.getElementById("dialogIframe").contentWindow.submit();//子窗口的iframe方法
	   if(result !== 'false'){
		   closeDialog();
		   window.setTimeout(function() { 
				 location.reload();
		   }, 500);
	   }
}
//提交发表空链接
function submitEmptyLink(){
	  var result = document.getElementById("publishEmptylinkDialogIframe").contentWindow.submit();//子窗口的iframe方法
	  if(result !== 'false'){
		   closeDialog();
		   window.setTimeout(function() { 
				 location.reload();
		   }, 500);
	  }
}

//
function closeDialog(){
	$('.J_btnClose').click();
}

//更新权重
function updatePower(power , articleId){
	_value = prompt('请输入新的权重值,原权重为'+power,power);
	if(_value==null || _value =='' )return;
	if(_value > 127){
		_value = 127;
	}else if(_value < 0){
		_value = 0;
	}
	location.href = '/article/updatePower.do?articleId=' + articleId + '&channelId=${channelInfo.id}&tag=${tag}&power=' + _value;
}

//顶
function upDayNum(articleId){
	_value = prompt("你想要把优先级上浮几天，请输入一个数值：","");
	if(_value==null || _value =='' )return;
	location.href = '/article/updateDayNum.do?articleId=' + articleId + '&channelId=${channelInfo.id}&tag=${tag}&num=' + _value;
}
//降
function downDayNum(articleId){
	_value=prompt("你想要把优先级下浮几天，请输入一个数值：","");
	if(_value==null || _value =='' )return;
	location.href = '/article/updateDayNum.do?articleId=' + articleId + '&channelId=${channelInfo.id}&tag=${tag}&num=-' + _value;
}
//还原
function resetDayNum(articleId){
	_value=confirm("你想要还原原有优先级么？");
	if(_value){
		location.href = '/article/resetDayNum.do?articleId=' + articleId + '&channelId=${channelInfo.id}&tag=${tag}';
	}
}
//删
function deleterArticle(articleId){
	_value=confirm("是否确认删除，这个操作不可恢复？");
	if(_value){
		location.href = '/article/deleteArticle4TagList.do?articleId=' + articleId + '&channelId=${channelInfo.id}&tag=${tag}';
	}
}


</script>
<script type="text/javascript">
var type = '${type}';

$(function (){
	if(type != ''){
		$('#type option[value='+type+']').attr('selected', true); 
	}

});

</script>

</div>
</body>
</html>