<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>
		<c:if test="${type == 'add'}">添加用户</c:if>
		<c:if test="${type != 'add'}">编辑用户</c:if>
	</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
</head>
<body>
	<div class="main-content">
	<jsp:include page="/head.do"></jsp:include>
	
	<div class="blank10"></div>
	
		<!--编辑用户 start-->
	<form action="/user/updateUser.do"  method="post"  id="updateUserForm" >
		<input type="hidden"  name="selectPower" id="selectPower" />	
		<input type="hidden"  name="type" id="type" />	
		<input type="hidden"  name="channelId" id="channelId"  value="${channelId}" />	
		<div class="edit-user-info">
			<c:if test="${type == 'add'}">
				<h2 class="clearfix"><em>添加新用户</em></h2>
			</c:if>
			<c:if test="${type != 'add'}">
				<h2 class="clearfix"><em>修改用户信息</em></h2>
			</c:if>
			<div class="blank20"></div>
			<table class="layout-table">
				<tbody>
					<tr>
						<td><label class="desc" for="userId">用户名：</label></td>
						<td>
						    <!-- //由于userInfo是存放在session的，所以新增时需要清空userId和mail对应的input输入框 -->
						    <c:if test="${type != 'add'}">
						    	<input type="text"  name="userId"  id="userId" value="${userInfo.userId}" readonly="readonly" class="style-input fixwidth-l">
						    </c:if>
						     <c:if test="${type == 'add'}">
						     	<input type="text"  name="userId"  id="userId" onchange="checkUserId()"  class="style-input fixwidth-l">
						     </c:if>
						     <span class="c-red-font wrong-note" id="userId_check"></span>
						</td>
					</tr>
					<tr class="addUserArea">
						<td><label class="desc" for="passport">通行证账号：</label></td>
						<td>
							<input type="text" name="udbName" id="passport" onchange="checkPassport()" class="style-input fixwidth-l">
							<span class="c-red-font wrong-note" id="passport_check"></span>
						</td>
					</tr>
					<tr class="addUserArea">
						<td><label class="desc" for="main">邮箱：</label></td>
						<td>
							<c:if test="${type != 'add'}">
								<input type="text"  name="mail"  id="mail"  value="${userInfo.mail}" class="style-input fixwidth-l">
							</c:if>
							<c:if test="${type == 'add'}">
								<input type="text"  name="mail"  id="mail" onchange="checkMail()" value="新系统用户必须输入邮箱" style="color:#c0c0c0;" class="style-input fixwidth-l">
								<span class="c-red-font wrong-note" id="mail_check"></span>
							</c:if>
						</td>
					</tr>
					<tr class="addUserArea">
						<td><label class="desc" for="password">密码：</label></td>
						<td>
							<input type="password" name="password"  id="password" onchange="checkPassword()" class="style-input fixwidth-l">
							<span class="c-red-font wrong-note" id="password_check"></span>
						</td>
					</tr>

					<tr >
						<td><label class="desc" for="">角色：</label></td>
						<td>
							<!-- 
							<input type="radio" class="mrs" name="role" id=""  value="a" onclick="clickRole('admin')">
							<label for="">系统管理员</label>
							 -->
							<c:if test="${sessionScope.userInfo.admin}">
								<input type="radio" class="mrs" name="role" id="" value="d"  onclick="clickRole('artist')">
								<label for="">美术组</label>
								<input type="radio" class="mll mrs" name="role" id="" value="h" onclick="clickRole('mainEditor')">
								<label for="">主编辑</label>
							</c:if> 
							
							<input type="radio" class="mll mrs" name="role" id="" value="o" onclick="clickRole('editor')">
							<label for="">编辑</label>
							<input type="radio" class="mll mrs" name="role" id="" value="b" onclick="clickRole('beginner')">
							<label for="">实习生/兼职</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><label for="">详细权限:</label><input type="checkbox" class="mls"  name="defaultRolePower"  id="defaultRolePower" onchange="exchangeRightSet()" /></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div class="right-set" id="rightSet" >
								<h3>权限：</h3>
								<c:set var="times" scope="request" value="0"/>
								<c:forEach var="power"  items="${allPowerInfoList}" varStatus="idx" begin="0">
									<c:if test="${times%5 ==0 && !empty power.name}">
										<p class="form-line">
									</c:if>
									<c:if test="${!empty power.name}">
										<span class="sub-item">
											<input type="checkbox" name="${power.id}" id="${power.id}"  />
											<label class="mls" for="">${power.name}</label>
										</span>
										<c:set var="times" scope="request" value="${times+1}"/>
									</c:if>				
									<c:if test="${ ((times)%5 ==0 &&!empty power.name) || idx.last == true}">
										</p>
									</c:if>
								</c:forEach>
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div class="blank20"></div>
							<c:if test="${type == 'add'}">
							<span class="ui-button"><a href="javascript:submit()">添加</a></span>
							</c:if>
							<c:if test="${type != 'add'}">
							<span class="ui-button"><a href="javascript:submit()">确定</a></span>
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		</form>
		<!--编辑用户 end-->

	</div>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script>	
<script type="text/javascript">
var isAddUserToChannel = false;
var type  = ( '${type}' == 'add' ?  true  : false) ;
$(function (){
	
	//邮箱,密码在浏览器保存密码时有值,应清空
	$("#mail").focus(function(){         // 地址框获得鼠标焦点
			var txt_value = $(this).val();   // 得到当前文本框的值
			if(txt_value==this.defaultValue){  
                $(this).val("").css("color","#000000");              // 如果符合条件，则清空文本框内容
			} 
	  }).blur(function(){		  // 地址框失去鼠标焦点
	  	    var txt_value =  $(this).val();   // 得到当前文本框的值
			if(txt_value==""){
                $(this).val(this.defaultValue).css("color","#c0c0c0");// 如果符合条件，则设置内容
			} 
	  })
	
	if(type){
		$('#type').val('add');
	}else{
		$('.addUserArea').hide();
		isAddUserToChannel = true;//若是修改，则表示修改频道用户
	}
	hideNoUsePermission();
	$('input[name=role][value="${userPowerInfo.userRole.value}"] ').attr('checked', 'checked');
	<c:forEach var="ownPower"  items="${userPowerInfo.ownPowers}" varStatus="idx" begin="0">
		$('#' + ${ownPower.id} ).attr('checked', 'checked');
	</c:forEach>
});
//----------检测用户名-------------------
function checkUserId(){
	
	if(!type){
		return;
	}
	
	var userId = $('#userId').val().trim();
	
	var reg = new RegExp("^[\\u4E00-\\u9FFF]{1,4}$");
	if(!reg.test(userId)){
		$("#userId").select();
		$("#userId_check").html("只允许汉字，且不超过4个汉字!");
		return ;
	}
	
	$.get('/user/checkUserId.do' ,{'userId':userId , channelId : '${channelId}'},
			function (data){
                if( data.indexOf('exist') > -1){
                	$('#userId_check').html('用户名已存在,请更换用户名.');
                   $('#userId').focus();
                 }else if(data.indexOf('addUserToChannel') > -1 ){
                	$('#userId_check').html('用户名存在于系统,但未加入该频道,可加入.');
                	$('.addUserArea').hide();
                	isAddUserToChannel = true;
                 }else if(data.indexOf('addUser') > -1){
                	 $('#userId_check').html('用户名未加入系统,可加入.');
                	 isAddUserToChannel = false;
                 }
		    },'text'
	);
}
//-----------------------------
//----------检测通行账号-------------------
function checkPassport(){
	
	var passport = $("#passport").val().trim();
	
	if("dw_" != passport.substring(0,3) || passport.length < 4 || passport.length > 16){
		$("#passport_check").html("通行证必须以'dw_'开头，4-16个字符!");
		$("#passport").select();
	}else{
		$("#passport_check").html("正在检测通行证是否已存在于系统...");
	
		$.ajax({
			   type: 'GET',
			   url: '/login/checkUdbName.do?udbName=' + passport,
			   async: false,
			   cache: false,
			   dataType: 'text',
			   success: function(data){
				   		if('true' == data){
				   			$("#passport_check").html("该通行证已经存于系统!");
				   		} else {
				   			$("#passport_check").html("该通行证可以使用!");
				   		}
			   }
			});
	}
}
//----------检测邮箱-------------------
function checkMail(){
	
	var mail_value = $("#mail").val().trim();
	if(mail_value.indexOf("@") == -1){
		$("#mail_check").html("请检查新邮箱格式!");
		$("#mail").select();
		return ;
	}
	
	$("#mail_check").html('正在检测邮箱是否已存在于系统...');
	
	$.ajax({
	   type: 'POST',
	   url: '/user/checkExistMail.do',
	   async: false,
	   cache: false,
	   dataType: 'text',
	   data:{"mail":mail_value},
	   success: function(data){
		   		if('true' == data){
		   			$("#mail_check").html("此邮箱已存在于系统，请更换!");
		   			$("#mail").select();
		   		} else if('false' == data){
		   			$("#mail_check").html("此邮箱不存在于系统，可加入.");
		   		} else {
		   			
		   		}
	   }
	});
	
}
//----------检测密码-------------------
function checkPassword(){
	var value = $("#password").val();
	if(value.length < 6 || value.length > 20 || isIntegerNumber(value)){
		$("#password").select();
		$("#password_check").html("密码必须6-20个字符,且不能全为数字!");
	}else{
		$("#password_check").html("密码符合规范");
	}
}
function isIntegerNumber(value){
	var n_value = parseInt(value);
	if(isNaN(n_value)){
		return false;
	}
	if(value.length > (n_value + "").length){
		return false;
	}
	return true;
}
//-----------------------------

//隐藏新版发布器没实现的权限
function hideNoUsePermission(){
	$('#16').parent().hide();//添加关键字
	$('#17').parent().hide();//关键字修删
	$('#18').parent().hide();//关键字搜看
}

//互换id=rightSet的div的显示状态
function exchangeRightSet(){
	if($('#defaultRolePower').attr('checked')== 'checked'){
		$('#rightSet').fadeIn();
	}else{
		$('#rightSet').fadeOut();
	}
}
var errorMessage = '';
function check(){
	
	if(!type)
		return true;
	
	if($('#userId_check').html().trim() == '')
		checkUserId();
	if($('#userId_check').html().indexOf('可加入') == -1){
		errorMessage += '用户名不合法\n';
	}
	
	if($('#mail_check').html().trim() == '' && !isAddUserToChannel)
		checkMail();
	
	if($('#mail_check').html().indexOf('可加入') == -1 && !isAddUserToChannel){//只有增加新用户才校验，增加已有用户到本频道，不作检查
		errorMessage += '邮箱格式不合法\n';
	}
	var hasSelectRole = false;
	$('input[name=role]').each(function (){
		if($(this).attr('checked')== 'checked'){
			hasSelectRole = true;
		}
	});
	if(!hasSelectRole){
		errorMessage += '角色不能为空\n';
	}
	if($('#password_check').html().trim() == '' && !isAddUserToChannel)
		checkPassword();
	if($('#password_check').html().indexOf('符合规范') == -1 && !isAddUserToChannel){
		errorMessage += '密码不符合规范\n';
	}
	if(errorMessage != '' ){
		alert(errorMessage);
	}
	return errorMessage != '' ? false : true;
}
//用于表单提交时，设置选中的权限值到隐藏的input框，
function setSelectPower(){
	var selectPower = '' ;
	$('input[type=checkbox]').not('#defaultRolePower').each(function (){
		if($(this).attr('checked') =='checked'){
			selectPower += $(this).attr('name') + ',';
		}
	});
	$('#selectPower').val(selectPower);
}

//提交表单
function submit(){
	if(!check()){
		errorMessage = '';
		return ;
	}
	setSelectPower();
	if(isAddUserToChannel){
		$('#updateUserForm').attr('action' , '/user/updateUserToChannel.do');
	}
	$('#updateUserForm').submit();
}
//点击角色的radio框，动态设置权限
function clickRole(roleName){
	//管理员:全部权限都有
	$('input[type=checkbox]').not('#defaultRolePower').each(function (){
		$(this).attr('checked','checked');
	});
	//主编 ：所有权限都有。
	if(roleName == 'mainEditor' ){

	}
	//编辑 :除了“上传文件到频道图片域名” ， “抓取管理” ， “用户搜看” ,"最终文章页模板增删改"， 其他权限都有。
	if(roleName == 'editor' ){
		$('#4').removeAttr('checked');
		$('#29').removeAttr('checked');
		$('#30').removeAttr('checked');
		$('#31').removeAttr('checked');//编辑没有"用户修删加"权限
		$('#6').removeAttr('checked');
	}
	//美术：除了"用户搜看"和"用户修删加" ，其他权限都有
	if(roleName == 'artist' ){
		$('#30').removeAttr('checked');
		$('#31').removeAttr('checked');
	}
	//实习/兼职：只有"文章修删"和"添加文章"
	if(roleName == 'beginner' ){
		$('input[type=checkbox]').not('#defaultRolePower').each(function (){
			$(this).removeAttr('checked');
		});
		$('#1').attr('checked','checked');
		$('#2').attr('checked','checked');
	}
}

</script>
</body>
</html>