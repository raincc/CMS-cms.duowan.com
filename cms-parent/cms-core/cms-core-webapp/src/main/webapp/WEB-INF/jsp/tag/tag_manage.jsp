<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>      
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chorme=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>标签管理</title>
	<link rel="stylesheet" type="text/css" href="${ROOT }/resource/css/global.css" media="all">
	<script src="http://www.duowan.com/public/assets/sys/js/jquery.js?02131514.js"></script>
</head>
<body>
	<div class="main-content">
		<jsp:include page="/head.do"></jsp:include>

  <form action=""  id="addForm"  name="addForm">
  		<input type="hidden" name="type" id="type" />
  		<input type="hidden" name="parentName" id="parentName" />
  		<input type="hidden" name="parentTree" id="parentTree" />
  		<input type="hidden" name="tagName" id="tagName" />
  		<input type="hidden" name="channelId" id="channelId"  value="${channelInfo.id}" />
	</form>
		<!--创建模版 start-->
		<div class="creat-mod">
			<div class="action-bar">
				<a href=""  id="toggleAllSonTag" class="ui-link-bt">全部展开</a>
				<span class="fr copy-bar">
					<input type="text"  name="tagInput" id="tagInput" class="style-input">
					<input class="u-inputBtn" type="button" onclick="javascript:addTag(0,'' , '' ,'tagInput' )" value="增加">
					<input class="u-inputBtn" type="button" onclick="javascript:addTag(1,'' , '','tagInput' )" value="增加特殊">
					<input class="u-inputBtn" type="button" onclick="javascript:pasteTag('' , '' , '0')"value="粘贴">
					<!-- a class="c-blue-bt" href="javascript:addTag(2,'', '','tagInput' )">增加投稿</a> -->
				</span>
		
			</div>
			<div class="blank20"></div>
			<table class="normal-table tag-manage-table">
				<thead>
					<tr>
						<th>tag</th>
						<th>模板</th>
						<th>别名</th>
						<th>管理</th>
					</tr>
				</thead>
				<tbody>
				<!-- 把投稿标签去掉：之前的投稿标签逻辑暂时保留，以防需求再变化 -->
					<c:forEach var="tagInfo" items="${tagInfoList}" varStatus="idx" begin="0">
					
<c:if test="${tagInfo.category.display != '用户投稿'}">					
					 <c:if test="${tagInfo.category.display == '普通'}">
						<tr rel="${tagInfo.tagDepth}"   <c:if test="${tagInfo.tagDepth > 0 }">class="tag-lev-${tagInfo.tagDepth}"</c:if>  style="display:none;">
					</c:if>
					<c:if test="${tagInfo.category.display == '用户投稿'}">
						<tr rel="${tagInfo.tagDepth}"   <c:if test="${tagInfo.tagDepth > 0 }">class="tag-lev-${tagInfo.tagDepth}"</c:if>  title="投稿标签"  style="display:none;">
					</c:if>
					
					<c:if test="${tagInfo.category.display == '特殊'}">
						<tr rel="${tagInfo.tagDepth}"   class="special-tag <c:if test="${tagInfo.tagDepth > 0 }">tag-lev-${tagInfo.tagDepth}</c:if>" title="特殊标签" style="display:none;">
					 </c:if>
					  <!-- onclick="javascript:toArticleListPage('${tagInfo.name}','${channelInfo.id}')" -->
							<td class="name" >
								<c:if test="${idx.last==false && tagInfoList[idx.index+1].tagDepth > tagInfo.tagDepth }">
										<i class="tag-manage-title tag-manage-active"></i>
										 <c:if test="${!tagInfo.isEmptyUrlOnLine }">
												<a href="${tagInfo.urlOnLine}" target="_blank">★</a>
										</c:if>	
											<a href="/article/articleList.do?tag=${fn:escapeXml(tagInfo.name)}&channelId=${channelInfo.id}">${fn:escapeXml(tagInfo.name)}</a>
											 <c:if test="${!tagInfo.isEmptyUrlOnCms }">
													<a href="${tagInfo.urlOnCms}" target="_blank">☆</a>
											</c:if>	
								 </c:if>
								<c:if test="${ !(idx.last==false && tagInfoList[idx.index+1].tagDepth > tagInfo.tagDepth)}">
										<i class="tag-manage-title"></i>
										 <c:if test="${!tagInfo.isEmptyUrlOnLine }">
											<a href="${tagInfo.urlOnLine}" target="_blank">★</a>
											</c:if>		
										<a href="/article/articleList.do?tag=${fn:escapeXml(tagInfo.name)}&channelId=${channelInfo.id}">${fn:escapeXml(tagInfo.name)}</a>
										 <c:if test="${!tagInfo.isEmptyUrlOnCms }">
											<a href="${tagInfo.urlOnCms}" target="_blank">☆</a>
										</c:if>	
								 </c:if>	
								<c:if test="${idx.first == true }">
									<c:set  var="parentTree" value=""></c:set>
								</c:if>	
								<c:if test="${idx.first != true }">
									<c:set  var="parentTree" value="${tagInfoList[idx.index-1].tree}"></c:set>
								</c:if>
							</td>
								<td>
										${tagInfo.relatedTemplateCategory.display}
								</td>
								<td></td>
								<td class="action">
									<input type="text"   id="tagInput_${idx.index}"  name="tagInput_${idx.index}" class="style-input fixwidth-input">
							<!-- 只有普通标签才可以增加子标签 -->	<!-- 只有小于五级的标签才可以增加子标签 -->			
							<c:if test="${tagInfo.category.display == '普通' && (tagInfo.tagDepth < 5) }">
									<!-- a href="javascript:addTag(0,  '${tagInfo.name}' ,'${tagInfo.tree}', 'tagInput_${idx.index}')">增</a> -->
									<input type="button" class="u-inputBtn" onclick="javascript:addTag(0,  '${fn:escapeXml(tagInfo.name)}' ,'${tagInfo.tree}', 'tagInput_${idx.index}')" value="增">
							 </c:if>
							 		
									<!--a href="javascript:editTag( '${tagInfo.name}', 'tagInput_${idx.index}')">修</a>-->
									<input type="button" class="u-inputBtn" onclick="javascript:editTag( '${fn:escapeXml(tagInfo.name)}', 'tagInput_${idx.index}')" value="修">
									<!-- start :不是最低级标签不能“删”和 "彻删" -->
							<c:if test="${ (idx.last==false && (!(tagInfoList[idx.index+1].tagDepth > tagInfo.tagDepth)) ) || idx.last==true  }">
									<!-- a href="javascript:deleteTag('${tagInfo.name}')">删</a> -->
									<input type="button" class="u-inputBtn" onclick="javascript:deleteTag('${fn:escapeXml(tagInfo.name)}')" value="删">
									<!-- a href="javascript:removeTagAllMsg('${tagInfo.name}')">彻底删除</a> -->
									<input type="button" class="u-inputBtn" onclick="javascript:removeTagAllMsg('${fn:escapeXml(tagInfo.name)}')" value="彻删">
							 </c:if>
									<!-- end :不是最低级标签不能“删”和 "彻删" -->
									<c:if test="${tagInfo.category.display != '特殊'}">
										<!-- a href="javascript:cutTag('${tagInfo.name}' ,'cutTag_${idx.index}' ) "  id="cutTag_${idx.index}">剪切</a> -->
										<input type="button" class="u-inputBtn" onclick="javascript:cutTag('${fn:escapeXml(tagInfo.name)}' ,'cutTag_${idx.index}' ) " id="cutTag_${idx.index}" value="剪切">
											<c:if test="${tagInfo.tagDepth < 5 }">
												<!-- a href="javascript:pasteTag('${tagInfo.name}', '${tagInfo.tree}' , '${tagInfo.tagDepth }')">粘贴</a> -->
												<input type="button" class="u-inputBtn" onclick="javascript:pasteTag('${fn:escapeXml(tagInfo.name)}', '${tagInfo.tree}' , '${tagInfo.tagDepth }')" value="粘贴">
											</c:if>
									 </c:if>
									<!-- 
									<a href="javascript:addAlias('${tagInfo.name}' , 'tagInput_${idx.index}' )">别名</a>
									 -->
									 <c:if test="${tagInfo.category.display != '特殊'}">
												<c:if test="${!tagInfo.isTagImage }">
												<!-- a href="javascript:exchangeTagImage( '${tagInfo.name}','true')">图</a> -->
												<input type="button" class="u-inputBtn" onclick="javascript:exchangeTagImage( '${fn:escapeXml(tagInfo.name)}','true')" value="图">
												 </c:if>
												 <c:if test="${tagInfo.isTagImage }">
												 <!-- a href="javascript:exchangeTagImage('${tagInfo.name}','false')">非图</a> -->
												 <input type="button" class="u-inputBtn" onclick="javascript:exchangeTagImage('${fn:escapeXml(tagInfo.name)}','false')" value="非图">
												  </c:if>
									   </c:if>
								</td>
						</tr>
 </c:if>						
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!--创建模版 end-->

	</div>
<script src="${ROOT }/resource/js/jquery-1.7.2.min.js"></script>	
<script src="${ROOT }/resource/js/tag_util.js"></script>	
<script type="text/javascript"  src="${ROOT}/resource/js/cookie.js"></script>   
<script type="text/javascript">
var isExistTag = false;
function checkTagIsExist(tagName){
	//注意：这里的ajax是同步的,由于success的返回在闭包外，所以需要定义全局的变量isExistTag
	$.ajax({
		 type: 'GET',
		 url : '/tag/isExist.do' ,
		 data: 'tagName='+tagName+'&channelId=${channelInfo.id}',
		 success: function(data){
				if(data == 'true'){
					isExistTag = true;
				}else{
					isExistTag = false;
				}
		 },
		 dataType:'text',
		 async:false
	});
	return isExistTag;
}
//默认标签不能超过五级,超过五级返回false,反之返回true
function checkTagIsLegal(parentTree){
	if(parentTree.split(',').length > 6){//parentTree以,开头；因此计算数组长度要加1,
		return false;
	}
	return true;
}

function exchangeTagImage(tagName , isTagImage){
	$('#tagName').val(tagName);
	$('#parentName').val(isTagImage);
	$('#addForm').attr('action' , '/tag/exchangeTagImage.do' );
	$('#addForm').submit();
}

//增加别名
function addAlias(tagName , tagInput){
	if($('#'+tagInput).val() == ''){
		//$('#errorMessageDiv').html('请输入标签名后再点击修改');
		alert('请输入标签名后再点击修改');
		return;
	}
	$('#tagName').val(tagName);//需要增加别名的标签
	$('#parentName').val($('#'+tagInput).val());//需要增加的别名
	$('#addForm').attr('action' , '/tag/addAlias.do' );
	$('#addForm').submit();
	
}

//点击“标签”后进入“标签”对应的文章列表
function toArticleListPage(tag , channelId){
	location.href = '/article/articleList.do?tag='+tag+'&channelId=' + channelId ;
}

function pasteTag(tagName , tagTree , tagDepth){
	$.get('/tag/pasteTag.do' ,
			{'tagName':tagName , 'channelId' : '${channelInfo.id}' , 'tagTree' : tagTree , 'tagDepth' : tagDepth},
			function (data){
				if(data.indexOf('error:') > -1){
					alert(data);
				}else{
					location.href = '/tag/tagManage.do?channelId=${channelInfo.id}' ;
				}
		    },'text'
	);

}
function cutTag(tagName , id ){

	$.get('/tag/cutTag.do' ,
			{'tagName':tagName , 'channelId' : '${channelInfo.id}'},
			function (data){
				if(data.indexOf('error:') > -1){
					alert(data);
				}else{
					//$('.cuttingTips').remove();
					//$('#'+id).after('&nbsp;&nbsp;<font color = "red" class="cuttingTips">剪切中</font>');
					$(":button[disabled=disabled]").val("剪切").removeAttr("disabled");;
					$('#'+id).val("剪切中").attr("disabled","disabled");
				}
		    },'text'
	);
}

function deleteTag(tagName){
	if(confirm('删除标签但不删除标签下的文章？')){
		$('#tagName').val(tagName);
		$('#addForm').attr('action' , '/tag/deleteTag.do' );
		$('#addForm').submit();
	}
}
function removeTagAllMsg(tagName){
	if(confirm('删除标签并删除标签下的文章？')){
		$('#tagName').val(tagName);
		$('#addForm').attr('action' , '/tag/removeTagAllMsg.do' );
		$('#addForm').submit();
	}
}

function addTag(type,parentTag , parentTree,tagInput){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&;|{}【】‘；：”“'。，、？]") ;
	var result;
	if ((result = pattern.exec(  $('#'+tagInput).val() )) != null)  {
		alert('添加标签失败,原因：标签含特殊字符' + result[0]);
		return ;
	}
	
	if(!validateTag($('#'+tagInput).val())){
		return;
	}
	if(!checkTagIsLegal(parentTree)){
		//$('#errorMessageDiv').html('添加标签失败，原因：标签的层数超过五层。默认标签不能超过五级');
		alert('添加标签失败，原因：标签的层数超过五层。默认标签不能超过五级');
		return;
	}
	$('#type').val(type);
	$('#parentName').val(parentTag);
	$('#parentTree').val(parentTree);
	$('#tagName').val($('#'+tagInput).val());
	$('#addForm').attr('action' , '/tag/addTag.do' );
	$('#addForm').submit();
}

function validateTag(tagValue){
	tagValue = tagValue.replace(/(^\s*)|(\s*$)/g,'');//String.trim()
	if( tagValue == ''){
		alert('请输入标签名');
		return false;
	}
	if(checkTagIsExist(tagValue)){
		alert('标签已存在，请不要重复提交同名标签');
		return false;
	}
	return true;
}

function editTag(editedTag,tagInput){
	if(!validateTag($('#'+tagInput).val())){
		return;
	}
	$('#parentTree').val(editedTag);
	$('#tagName').val($('#'+tagInput).val());
	$('#addForm').attr('action' , '/tag/editTag.do' );
	$('#addForm').submit();
}

</script>

	<script type="text/javascript">
var channelId = '${channelInfo.id}';
//标签展示效果	
$(function (){
	TagUtil.showRootTag();
	
	var isShowAllSonTag = cookieUtil.getCookie(channelId + '_showAllSonTag');
	if(!isShowAllSonTag || isShowAllSonTag==''){
		hideAllSonTag();
	}else{
		showAllSonTag();
	}
	
	$('#toggleAllSonTag').toggle(function (){
		if('全部收起' == $(this).html()){
			hideAllSonTag();
		}else{
			showAllSonTag();
		}
	},function (){
		if('全部收起' == $(this).html()){
			hideAllSonTag();
		}else{
			showAllSonTag();
		}
	});
	
	
});
//显示所有子级Tag
function showAllSonTag(){
	$('#toggleAllSonTag').html('全部收起');
	TagUtil.showAllSonTag();
	TagUtil.changeTagActiveOpen();//修改标签图标为打开
	cookieUtil.setCookie( channelId + '_showAllSonTag' , 'showAll' );
}
//隐藏所有子级Tag
function hideAllSonTag(){
	$('#toggleAllSonTag').html('全部展开');
	TagUtil.hideAllSonTag();
	TagUtil.changeTagActiveClose();//修改标签图标为关闭
	cookieUtil.setCookie( channelId + '_showAllSonTag' , '' );
}


$('.tag-manage-title').toggle(function() {
	if($(this).hasClass("tag-manage-active")){
		TagUtil.showSonTag(this);
		return;
	}
	if($(this).hasClass('tag-manage-actived')){
		TagUtil.hideSonTag(this);
	}
}, function() {
	if($(this).hasClass('tag-manage-actived')){
		TagUtil.hideSonTag(this);
		return;
	}
	if($(this).hasClass("tag-manage-active")){
		TagUtil.showSonTag(this);
	}
});
	</script>
</body>
</html>