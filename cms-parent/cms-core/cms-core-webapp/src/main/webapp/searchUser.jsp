<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.duowan.cms.service.user.UserService"%>
<%@page import="com.duowan.cms.dto.user.UserInfo"%>
<%@page import="java.util.List"%>
<%@page import="com.duowan.cms.dto.user.UserPowerInfo"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.duowan.cms.common.webapp.content.SpringApplicationContextHolder"%>
<%@page import="com.duowan.cms.service.ip.IpService"%>
<%@page import="com.duowan.cms.common.util.StringUtil"%>
<%@page import="org.apache.log4j.Logger"%>

<%! 
static Logger logger = Logger.getLogger("searchUser.jsp");

String getRealIp(HttpServletRequest req) {

    String ip = req.getHeader("X-Forwarded-For");
    if(StringUtil.isEmpty(ip)) {
        return req.getRemoteAddr();
    }
    ip = ip.split(", ")[0].trim();
    if("127.0.0.1".equals(ip)) {
        return req.getRemoteAddr();
    }
    return ip;
}

%>
    
<%
IpService ipService = SpringApplicationContextHolder.getBean("ipService", IpService.class);
String ip = getRealIp(request);
if(!ipService.isInnerIP(ip)){
    out.println("ip被拒绝!");
    return;
}else{
    if("post".equalsIgnoreCase(request.getMethod())){
		String userId = request.getParameter("userId");
		String udbName = request.getParameter("udbName");
		UserService userService = SpringApplicationContextHolder.getBean("userService", UserService.class);
		UserInfo searchUserInfo = null;
		if(!StringUtil.isEmpty(userId)){
		    searchUserInfo = userService.getById(userId);
		}else if(!StringUtil.isEmpty(udbName)){
		    searchUserInfo = userService.getByUdbName(udbName);
		}
		
		if(null != searchUserInfo){
		  	//为userInfo添加权限信息
		  	pageContext.setAttribute("type", "one");
	        List<UserPowerInfo> userPowerInfoList = userService.getAllPowerByUyserId(searchUserInfo.getUserId());
	        searchUserInfo.setAllChannelPowerInfo(userPowerInfoList);
	        pageContext.setAttribute("searchUserInfo", searchUserInfo);
		}else{
		   pageContext.setAttribute("type", "none");
		}
		
	}
}


%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>查询用户</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="">
	<meta http-equiv="description" content="This is search user page">
	<link rel="stylesheet" type="text/css" href="/resource/css/global.css">

  </head>
  
  <body>
    <form action="" method="post" id="searchUser">
    <table align="center" width="60%" border="0">
    	<tbody>
    		<tr>
    			<td align="right">用户名：</td>
    			<td>
    				<input type="text" name="userId">
    				<div class="blank15"></div>
    			</td>
    		</tr>
    		<tr>
    			<td align="right">udb账号：</td>
    			<td>
    				<input type="text" name="udbName"><div class="blank15"></div>
   				</td>
    		</tr>
    		<tr>
			  	<td colspan="2" align="center">
			  		<span class="ui-button"><a href="javascript:submitSearch()">查询</a></span>
			  	</td>
			  </tr>
    	</tbody>
    </table>
    </form>
    <c:if test="${type == 'one'}">
    <hr />
    <br />
    <table class="normal-table">
    	<thead>
			<tr>
				<th>属性</th>
				<th>信息</th>
			</tr>
		</thead>
		<tbody>
			<tr class="odd">
				<td>用户名</td>
				<td>${searchUserInfo.userId }</td>
			</tr>
			<tr>
				<td>状态</td>
				<td><font color="red">${searchUserInfo.userStatus.display}</font></td>
			</tr>
			<tr class="odd">
				<td>邮箱</td>
				<td>${searchUserInfo.mail }</td>
			</tr>
			<tr>
				<td>udb账号</td>
				<td>${searchUserInfo.udbName}</td>
			</tr>
			<tr class="odd">
				<td>创建时间</td>
				<td>${searchUserInfo.createTimeStr }</td>
			</tr>
			<tr>
				<td>最后更新时间</td>
				<td>${searchUserInfo.lastModifyTimeStr }</td>
			</tr>
			<tr class="odd">
				<td>用户ip</td>
				<td>${searchUserInfo.userIp }</td>
			</tr>
			<tr>
				<td>代理ip</td>
				<td>${searchUserInfo.proxyIp }</td>
			</tr>
			<tr  class="odd">
				<td>是否管理员</td>
				<td>${searchUserInfo.admin }</td>
			</tr>
		</tbody>
    </table>
    	<hr />
    	用户权限：拥有<font color="red" key="channelNum"></font>个频道<br />
    	
    	<table class="normal-table">
			<thead>
				<tr>
					<th>频道名</th>
					<th>权限</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="singleUserPower" items="${searchUserInfo.allChannelPowerInfo}" varStatus="idx" begin="0">
				 <c:if test="${idx.index%2==0}">
					<tr  class="odd" key="channelCell">
				 </c:if>
				<c:if test="${idx.index%2 !=0}">
					<tr key="channelCell">
				 </c:if>
						<td>${singleUserPower.channelInfo.name}</td>
						<td class="right-name">
							<span class="c-black-font">${singleUserPower.userRole.display}</span>
							 <c:if test="${singleUserPower.addedPowerSize > 0}">
								<span class="c-green-font">&nbsp; + &nbsp; ${singleUserPower.addedPowerStr }</span>
							 </c:if>
							 <c:if test="${singleUserPower.removedPowerSize > 0}">
							 	<span class="c-red-font">&nbsp; - &nbsp; ${singleUserPower.removedPowerStr}</span>
							 </c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
    </c:if>
    <c:if test="${type == 'none'}">
    <hr />
    <br />
    	查无此用户!
    </c:if>
<script type="text/javascript"  src="${ROOT}/resource/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
	$(function(){
		$("[key=channelNum]").html($("tr[key=channelCell]").length);
	})
	
	function submitSearch(){
		$("#searchUser").submit();
	}
</script>
  </body>
</html>
