package com.duowan.cms.action.article;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;
import com.duowan.cms.dto.article.ArticleSearchCriteria.ArticleOrderBy;
import com.duowan.cms.dto.article.ArticleStatus;
import com.duowan.cms.dto.article.SimpleArticleInfo;
import com.duowan.cms.service.article.ArticleService;
/**
 * 处理与文章滚动和空链接相关的页面请求
 * <p>作　　者：黄均杨
 * <p>完成日期：2012-12-28
 */
@Controller
public class ArticleRollController {

	@Autowired
	private ArticleService articleService;
	
	/**
	 * 滚动文章
	 */
	@RequestMapping(value = "/article/articleRoll", method = RequestMethod.GET)
	public String articleRoll(HttpServletRequest request, HttpServletResponse response)
				throws IOException, BaseCheckedException {
		this.configArticleRoll(request);
		return "/article/article_roll";
	}
	
	/**
	 * 空链接列表
	 */
	@RequestMapping(value = "/article/articleEmptylinkList", method = RequestMethod.GET)
	public String articleEmptylinkList(HttpServletRequest request, HttpServletResponse response)
				throws IOException, BaseCheckedException {
		this.configArticleRoll(request);
		return "/article/article_emptylink_list";
	}
	
	private void configArticleRoll(HttpServletRequest request) throws BaseCheckedException {
		
		this.setArticleRoll(request);//设置滚动列表中的文章信息
		this.setSearchInfo(request);//设置"上一个请求的查询条件"到页面
	}
	
	
	/**
	 * 设置滚动列表中的文章信息
	 */
	private void setArticleRoll(HttpServletRequest request) throws BaseCheckedException{
		ArticleSearchCriteria articleSearchCriteria = this.setArticleSearchCriteria(request);
		//Page<ArticleInfo> articleInfoPage = articleService.pageSearch(articleSearchCriteria);
		Page<SimpleArticleInfo> articleInfoPage = articleService.pageSearch4Show(articleSearchCriteria);
		request.setAttribute("articleInfoPage", articleInfoPage);
	}
	
	/**
	 * 设置"上一个请求的查询条件"到页面
	 */
	private void setSearchInfo(HttpServletRequest request) {
		String searchkey = request.getParameter("searchkey");
		String type = request.getParameter("type");

		request.setAttribute("searchkey", "null".equals(searchkey) ? "" : searchkey);
		request.setAttribute("type", "null".equals(type) ? "" : type);
		request.setAttribute("queryStr", "&searchkey=" + searchkey + "&type=" + type);
		request.setAttribute("isPrepublish", "1".equals(request.getParameter("status")));//是否预发布
	}
	

	/**
	 * 设置articleRoll方法的ArticleSearchCriteria设值代码
	 */
	private ArticleSearchCriteria setArticleSearchCriteria(HttpServletRequest request) throws BaseCheckedException {
		String channelId = request.getParameter("channelId");
		String searchkey = request.getParameter("searchkey");
		String type = request.getParameter("type");
		ArticleSearchCriteria articleSearchCriteria = new ArticleSearchCriteria();
		articleSearchCriteria.setChannelId(channelId);
		if ( !StringUtil.isEmpty(searchkey) )  {// 作非空判断，防止NullpointException
			searchkey = searchkey.trim();
		}
		if ("title".equals(type)) {// 标题
			articleSearchCriteria.setTitle(searchkey);
		} else if ("userid".equals(type)) {// 编辑
			articleSearchCriteria.setUserId(searchkey);
		} else if ("author".equals(type)) {// 作者
			articleSearchCriteria.setAuthor(searchkey);
		} else if ("source".equals(type)) {// 来源
			articleSearchCriteria.setSource(searchkey);
		} else if ("posttime".equals(type)) {// 日期
			try {
			    Date searchDate = DateUtil.parse(searchkey, "yyyy-MM-dd");
			    articleSearchCriteria.setUpdateTimeStart(searchDate);
			    articleSearchCriteria.setUpdateTimeEnd(DateUtil.getNextDate(searchDate));
			} catch (ParseException e) {
				throw new BaseCheckedException("" , "输入的日期不合法，请输入合法格式:yyyy-MM-dd");
			}
		}
		String pageNo = request.getParameter("pageNo");
		if (!StringUtil.isEmpty(pageNo)) {
			articleSearchCriteria.setPageNo(Integer.parseInt(pageNo));
		}
		String status = request.getParameter("status"); //文章状态
		if (!StringUtil.isEmpty(status)) {
			articleSearchCriteria.addStatus(ArticleStatus.getInstance(status));
			request.setAttribute("status", status);
		}else{
			//预定发布的文章不在滚动列表和文章列表中出现
			articleSearchCriteria.addStatus(ArticleStatus.NORMAL);
			articleSearchCriteria.addStatus(ArticleStatus.NO_TAG);
			articleSearchCriteria.addStatus(ArticleStatus.CATCHED);
			articleSearchCriteria.addStatus(ArticleStatus.USER_DELIVERY);
			request.setAttribute("status", "");
		}
		//设置排序字段
		
//      为了性能考虑
//      articleSearchCriteria.addOrderBy(ArticleOrderBy.UPDATE_TIME_DESC)
//      .addOrderBy(ArticleOrderBy.DAY_NUM_DESC)
//      .addOrderBy(ArticleOrderBy.POWER_DESC);
		articleSearchCriteria.addOrderBy(ArticleOrderBy.UPDATE_TIME_DESC);

		return articleSearchCriteria;
	}
	
	
}
