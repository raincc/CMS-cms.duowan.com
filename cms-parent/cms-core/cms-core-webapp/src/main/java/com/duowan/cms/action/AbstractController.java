package com.duowan.cms.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.Validator;
import org.springframework.web.bind.support.WebBindingInitializer;




public  class AbstractController {
    // 由子类按需要设置
    protected WebBindingInitializer webBindingInitializer; 
    protected Validator validator;


    
    /**
     * 绑定页面参数到实体对象
     * @param request
     * @param command
     * @return
     */
    protected boolean validate(HttpServletRequest request, Object command) {
        return true;
    }
}
