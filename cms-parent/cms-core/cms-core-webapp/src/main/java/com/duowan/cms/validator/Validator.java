package com.duowan.cms.validator;


public interface Validator<T> {
	public  String validate(T bean) ;
}
