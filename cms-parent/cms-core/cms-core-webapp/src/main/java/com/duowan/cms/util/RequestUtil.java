package com.duowan.cms.util;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.duowan.cms.common.util.DateUtil;

public class RequestUtil {

	/**
	 * 获取用户的代理ip
	 * @param request
	 * @return
	 */
	public static String getProxyIp(HttpServletRequest request){
		return  request.getRemoteAddr();
	}
	
	public static String getUserIp(HttpServletRequest request){
		String proxyIp =RequestUtil.getProxyIp(request);
		String userIp = request.getHeader("X-Forwarded-For");
		userIp = (  (userIp  != null && userIp.length() > 0) ? userIp : proxyIp);
		return userIp;
	}
	
	public static String getRequestLog(HttpServletRequest request){
		String userId = request.getParameter("userId");
		 StringBuffer logBuffer = new StringBuffer();
		 logBuffer.append(userId).append("于").append(DateUtil.format(new Date(), DateUtil.exportXlsDateCreateTimeFormat));
		 logBuffer.append("发表  IP").append(RequestUtil.getUserIp(request))
		 			   .append("(").append(RequestUtil.getProxyIp(request)).append( ")<br>\n");
		 return logBuffer.toString();
	}
	
}
