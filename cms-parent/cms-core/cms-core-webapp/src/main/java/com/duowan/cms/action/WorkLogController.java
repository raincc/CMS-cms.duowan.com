package com.duowan.cms.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.user.UserInfo;
import com.duowan.cms.dto.user.UserPowerInfo;
import com.duowan.cms.dto.worklog.WorkLogInfo;
import com.duowan.cms.dto.worklog.WorkLogSearchCriteria;
import com.duowan.cms.service.worklog.WorkLogService;

@Controller
public class WorkLogController {

	@Autowired
	private WorkLogService workLogService;
	
	@RequestMapping(value = "/worklog/logList", method = RequestMethod.GET)
	public String logList(HttpServletRequest request,HttpServletResponse response ,
				@ModelAttribute("workLogSearchCriteria") WorkLogSearchCriteria workLogSearchCriteria ) throws IOException, BaseCheckedException {
	    return listData(request, response, workLogSearchCriteria);
	}
	
	@RequestMapping(value = "/worklog/historyStatistics", method = RequestMethod.GET)
    public String historyStatistics(HttpServletRequest request,HttpServletResponse response ,
                @ModelAttribute("workLogSearchCriteria") WorkLogSearchCriteria workLogSearchCriteria ) throws IOException, BaseCheckedException {
        return listData(request, response, workLogSearchCriteria);
    }
	
	private String listData (HttpServletRequest request,HttpServletResponse response ,WorkLogSearchCriteria workLogSearchCriteria ) throws IOException, BaseCheckedException{
	    
	    UserInfo userInfo = (UserInfo) request.getSession().getAttribute("userInfo");
        ChannelInfo channelInfo = (ChannelInfo) request.getSession().getAttribute("channelInfo");
        //取出权限
        List<UserPowerInfo> list = userInfo.getAllChannelPowerInfo();
        String mode = request.getParameter("mode");
        if(StringUtil.isEmpty(mode))
            mode = "currentChannel";
        if("currentChannel".equals(mode) && null != channelInfo){
            workLogSearchCriteria.setChannelName(channelInfo.getName());
        }else if("allChannel".equals(mode) && userInfo.isAdmin()){
            workLogSearchCriteria.setChannelName(null);
        }else if("givenChannel".equals(mode) && hasPower(workLogSearchCriteria.getChannelName(), list)){
            //String channelName = request.getParameter("channelName");
            //workLogSearchCriteria.setChannelName(channelName);
        }else{
            return "redirect:/index.do";
        }
        //userService.getAllPowerByUyserId(userInfo.getUserId());
        request.setAttribute("userChannelList", list);
        Page<WorkLogInfo> workLogInfoPage = workLogService.pageSearch(workLogSearchCriteria);
        request.setAttribute("workLogInfoPage", workLogInfoPage);
        request.setAttribute("workLogSearchCriteria", workLogSearchCriteria);
        request.setAttribute("queryStr", "&startDateStr=" + workLogSearchCriteria.getStartDateStr()
            + "&endDateStr=" +workLogSearchCriteria.getEndDateStr() + "&userId="
            +workLogSearchCriteria.getUserId()+"&channelName="+workLogSearchCriteria.getChannelName()+"&mode=" + mode);
        
        return "/worklog/log_list";
	}
	
	@RequestMapping(value = "/worklog/userStatistics", method = RequestMethod.GET)
    public String userStatistics(HttpServletRequest request,HttpServletResponse response, @ModelAttribute("workLogSearchCriteria") WorkLogSearchCriteria workLogSearchCriteria) throws IOException, BaseCheckedException {
        
        Page<WorkLogInfo> workLogInfoPage = workLogService.userStatistics(workLogSearchCriteria);
        request.setAttribute("workLogInfoPage", workLogInfoPage);
        request.setAttribute("workLogSearchCriteria", workLogSearchCriteria);
        request.setAttribute("queryStr", "&startDateStr=" + workLogSearchCriteria.getStartDateStr()
            + "&endDateStr=" +workLogSearchCriteria.getEndDateStr() + "&userId="+workLogSearchCriteria.getUserId());
        
        return "/worklog/userStatistics";
    }
	
	@RequestMapping(value = "/worklog/todayStatistics", method = RequestMethod.GET)
    public String todayStatistics(HttpServletRequest request,HttpServletResponse response) throws IOException, BaseCheckedException {
	    
	    UserInfo userInfo = (UserInfo) request.getSession().getAttribute("userInfo");
	    
	    ChannelInfo channelInfo = (ChannelInfo) request.getSession().getAttribute("channelInfo");
	    //自己的工作量
	    List<WorkLogInfo> listSelf = workLogService.todayStatistics(userInfo);
	    //其他编辑的工作量
	    List<WorkLogInfo> listChannel = workLogService.todayStatistics(channelInfo.getId());
        
	    //总数让客户端计算
	    request.setAttribute("listSelf", listSelf);
	    request.setAttribute("listChannel", listChannel);
	    
        return "/worklog/todayStatistics";
    }
	
	@RequestMapping(value = "/worklog/channelStatistics", method = RequestMethod.GET)
    public String channelStatistics(HttpServletRequest request,HttpServletResponse response, @ModelAttribute("workLogSearchCriteria") WorkLogSearchCriteria workLogSearchCriteria) throws IOException, BaseCheckedException {
        
	    UserInfo userInfo = (UserInfo) request.getSession().getAttribute("userInfo");
        ChannelInfo channelInfo = (ChannelInfo) request.getSession().getAttribute("channelInfo");
        //取出权限
        List<UserPowerInfo> list = userInfo.getAllChannelPowerInfo();
        String mode = request.getParameter("mode");
        if(StringUtil.isEmpty(mode))
            mode = "currentChannel";
        if("currentChannel".equals(mode) && null != channelInfo){
            workLogSearchCriteria.setChannelName(channelInfo.getName());
        }else if("allChannel".equals(mode) && userInfo.isAdmin()){
            workLogSearchCriteria.setChannelName(null);
        }else if("givenChannel".equals(mode) && hasPower(workLogSearchCriteria.getChannelName(), list)){
            //String channelName = request.getParameter("channelName");
            //workLogSearchCriteria.setChannelName(channelName);
        }else{
            return "redirect:/index.do";
        }
        //userService.getAllPowerByUyserId(userInfo.getUserId());
        request.setAttribute("userChannelList", list);
        Page<WorkLogInfo> workLogInfoPage = workLogService.channelStatistics(workLogSearchCriteria);
        request.setAttribute("workLogInfoPage", workLogInfoPage);
        request.setAttribute("workLogSearchCriteria", workLogSearchCriteria);
        request.setAttribute("queryStr", "&startDateStr=" + workLogSearchCriteria.getStartDateStr()
                + "&endDateStr=" +workLogSearchCriteria.getEndDateStr()+"&channelName="+workLogSearchCriteria.getChannelName()+"&mode=" + mode);
        
        return "/worklog/channelStatistics";
    }

	private boolean hasPower(String channelName, List<UserPowerInfo> list){
	    for(UserPowerInfo upi : list){
	        if(upi.getChannelInfo().getName().equals(channelName))
	            return true;
	    }
	    return false;
	}
	
}
