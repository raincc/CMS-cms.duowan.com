package com.duowan.cms.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommonController {

	@RequestMapping(value = "/pager", method = RequestMethod.GET)
	public String pager(HttpServletRequest request,HttpServletResponse response) throws IOException {
		return "/include/pager";
	}
}
