package com.duowan.cms.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.hotword.HotWordInfo;
import com.duowan.cms.dto.hotword.HotWordSearchCriteria;
import com.duowan.cms.service.hotword.HotWordService;

@Controller
public class HotwordController extends AbstractController {

    @Autowired
    private HotWordService hotWordService;

    @RequestMapping(value = "/hotword/hotwordManage", method = RequestMethod.GET)
    public String hotwordManage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        setListInfo2Request(request);

        // 设置"上一个请求的查询条件"到页面
        String searchkey = request.getParameter("searchkey");
        searchkey = StringUtil.clearHTML(searchkey);
        request.setAttribute("searchkey", "null".equals(searchkey) ? "" : searchkey);
        request.setAttribute("queryStr", "&searchkey=" + searchkey);

        return "/hotword/hotword_manage";
    }
    
    private void setListInfo2Request(HttpServletRequest request){
        String channelId = request.getParameter("channelId");
        String searchkey = request.getParameter("searchkey");
        String pageNo = request.getParameter("pageNo");
        
        // 设置列表信息
        HotWordSearchCriteria searchCriteria = new HotWordSearchCriteria();
        searchCriteria.setChannelId(channelId);
        if (!StringUtil.isEmpty(searchkey)) {
            if (searchkey.indexOf("http:") > -1)
                searchCriteria.setHref(searchkey);
            else
                searchCriteria.setName(searchkey);
        }
        if (!StringUtil.isEmpty(pageNo)) {
            searchCriteria.setPageNo(Integer.parseInt(pageNo));
        }
        Page<HotWordInfo> hotWordInfoPage = hotWordService.pageSearch(searchCriteria);
        request.setAttribute("hotWordInfoPage", hotWordInfoPage);
    }
    
    @RequestMapping(value = "/hotword/isExistHotword", method = RequestMethod.GET)
    public void isExistHotword(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        String channelId = request.getParameter("channelId");
        String name = request.getParameter("name");
        if(StringUtil.hasOneEmpty(channelId, name)){
            response.getWriter().write("fail");
            return ;
        }
        
        if(null == hotWordService.getByChannelIdAndName(channelId, name)){
            response.getWriter().write("false");
        }else{
            response.getWriter().write("true");
        }
    }

    @RequestMapping(value = "/hotword/addHotword", method = RequestMethod.POST)
    public String addHotword(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("hotWordInfo") HotWordInfo hotWordInfo) throws IOException {
        
        if(null != hotWordInfo && !StringUtil.hasOneEmpty(hotWordInfo.getChannelId(), hotWordInfo.getName()))
            hotWordService.addHotWord(hotWordInfo);
        return "redirect:/hotword/hotwordManage.do?channelId=" + hotWordInfo.getChannelId();
    }
    
    @RequestMapping(value = "/hotword/updateHotWord", method = RequestMethod.POST)
    public void updateHotWord(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("hotWordInfo") HotWordInfo hotWordInfo) throws IOException {
        
        if(StringUtil.hasOneEmpty(hotWordInfo.getChannelId(), hotWordInfo.getName())){
            response.getWriter().write("fail");
            return ;
        }
        hotWordService.updateHotWord(hotWordInfo);
        response.getWriter().write("success");
    }

    @RequestMapping(value = "/hotword/deleteHotword", method = RequestMethod.GET)
    public String deleteHotword(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("hotWordInfo") HotWordInfo hotWordInfo) throws IOException {

        hotWordService.deleteHotWord(hotWordInfo);

        return "redirect:/hotword/hotwordManage.do?channelId=" + hotWordInfo.getChannelId();
    }

}
