package com.duowan.cms.action;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;
import com.duowan.cms.dto.template.AutoFlushTemplateSearchCriteria;
import com.duowan.cms.service.channel.ChannelService;
import com.duowan.cms.service.template.AutoFlushTemplateService;

@Controller
public class AutoFlushTemplateController extends AbstractController  {

	@Autowired
	private AutoFlushTemplateService autoFlushTemplateService;
	
	@Autowired
	private ChannelService channelService;
	
	/**
	 * 文章列表
	 */
	@RequestMapping(value = "/template/autoFlushTemplateList", method = RequestMethod.GET)
	public String articleList(HttpServletRequest request, HttpServletResponse response)
				throws IOException, BaseCheckedException {
	    
	    ChannelInfo channelInfo = (ChannelInfo) request.getSession().getAttribute("channelInfo");
	    String templateId = request.getParameter("templateId");
	    
		String pageNo = request.getParameter("pageNo");
		AutoFlushTemplateSearchCriteria searchCriteria  = new AutoFlushTemplateSearchCriteria();
		searchCriteria.setPageSize(10);
		searchCriteria.setChannelId(channelInfo.getId());
		if(StringUtil.isNumber(templateId)){
		    searchCriteria.setTemplateId(Long.parseLong(templateId));
        }
		if (!StringUtil.isEmpty(pageNo)) {
			searchCriteria.setPageNo(Integer.parseInt(pageNo));
		}
		Page<AutoFlushTemplateInfo> autoFlushTemplateInfoPage = autoFlushTemplateService.pageSearch(searchCriteria);
		request.setAttribute("autoFlushTemplateInfoPage", autoFlushTemplateInfoPage);
		return "/template/auto_flush_template_list";
	}
	
	@RequestMapping(value = "/template/publishAutoFlushTemplate", method = RequestMethod.POST)
	public void publish(HttpServletRequest request, HttpServletResponse response,
				@ModelAttribute("autoFlushTemplateInfo") AutoFlushTemplateInfo autoFlushTemplateInfo) throws IOException,
				BaseCheckedException, ParseException {
		String channelName = request.getParameter("channelName");
		ChannelInfo channelInfo = channelService.getByName(channelName);
		autoFlushTemplateInfo.setChannelInfo(channelInfo);
		autoFlushTemplateService.save(autoFlushTemplateInfo);
	}
	
	@RequestMapping(value = "/template/toPublishAutoFlushTemplatePage", method = RequestMethod.GET)
	public String toPublishAutoFlushTemplatePage(HttpServletRequest request, HttpServletResponse response) throws IOException,
				BaseCheckedException, ParseException {
		String id = request.getParameter("id");
		if (!StringUtil.isEmpty(id)) {
			AutoFlushTemplateInfo autoFlushTemplateInfo = autoFlushTemplateService.getById(id);
			request.setAttribute("autoFlushTemplateInfo", autoFlushTemplateInfo);
		}
		return "/template/auto_flush_template_edit";
	}
	
	@RequestMapping(value = "/template/deleteAutoFlushTemplate", method = RequestMethod.GET)
	public String deleteAutoFlushTemplate(HttpServletRequest request, HttpServletResponse response)  {
		String id = request.getParameter("id");
		autoFlushTemplateService.delete(id);
		return "redirect:/template/autoFlushTemplateList.do";
	}
	
}
