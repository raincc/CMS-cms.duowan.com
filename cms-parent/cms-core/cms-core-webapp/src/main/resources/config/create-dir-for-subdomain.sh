
subdomain_prefix=$1  # subdomain name is "$subdomain_prefix.duowan.com"
base_dir=$2          # without trailing '/'
bind_base_dir=$3     # without trailing '/'


if [ -z "$base_dir" -o -z "$subdomain_prefix" ]
then
    echo "usage: $0 subdomain_prefix base_dir  bind_base_dir"
    exit 1
fi

mkdir -p "$base_dir/$subdomain_prefix"
mkdir -p "$bind_base_dir/$subdomain_prefix.duowan.com"
mount --bind "$base_dir/$subdomain_prefix" "$bind_base_dir/$subdomain_prefix.duowan.com"
