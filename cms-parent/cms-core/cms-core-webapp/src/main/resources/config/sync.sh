
# code

# 保存上次函数的返回值。没有返回值的函数不应该修改这个对象。
last_result=''

local_sync_dir=/data1/www/cms.duowan.com/

# 从类似qqxl/index.html的文件名中得到对应url的Host。比如
# qqxl/index.html对应的Host是qqxl.duowan.com
function get_subdomain(){
	filename=$1
	last_result=${filename%%/*}.duowan.com
}

# 从类似qqxl/index.html的文件名中得到对应url的path。比如
# qqxl/index.html对应的path是/index.html
function get_path(){
	filename=$1
	left=${filename%%/*}
	num=${#left}
	path=${filename:num}
	last_result=$path
}

# Nginx机器检查失败后的处理函数
# 参数：$2 ip
#	$1 url (lol.duowan.com/index.html)
function nginx_fail_handler(){
	url=$1	
	ip=$2

        work_dir=`pwd`
        cd $local_sync_dir
        command="rsync -vrz -R $url release@$ip::release_code/ --password-file=/etc/rsyncd_users_tomcat"
        $command
        cd $work_dir
}



# 检查文件的同步情况，通过md5sum判断文件是否一致。
# 参数：$1 filename 比如：qqxl/index.html
function check_file(){
	filename=$1
	get_subdomain $filename
	subdomain=$last_result
	get_path $filename
	path=$last_result

	for ip in $nginx_servers
	do
		nginx_fail_handler "$subdomain$path" "$ip"
	done
}


nginx_servers="221.204.223.154
113.108.228.198
183.61.6.23
119.188.90.76
115.238.171.143
218.60.98.93
115.238.171.183
"

filename=$1
check_file $filename
