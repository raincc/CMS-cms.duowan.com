/**
 * 
 */
package com.duowan.cms.core.rmi.client.channel;

import java.util.List;

import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 * 频道RMI服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-8
 * <br>==========================
 */
public interface ChannelRemoteService  extends RemoteService {
    
    //异常码：该专区不存在
    public final static String CHANNEL_IS_NOT_EXITS = ChannelRemoteService.class.getName().concat("001");
    
    /**
     * 根据ID获取频道信息
     * @param id
     * @return
     */
    public ChannelInfo getById(String id);
    
    /**
     * 根据名字获取频道信息
     * @param cname
     * @return
     */
    public ChannelInfo getByName(String cname);
    /**
     * 获取所有的频道信息
     * @return
     */
    public List<ChannelInfo> getAllChannel();
    
    /**
     *  获取某个编辑拥有的所有专区
     * @return
     */
    public List<ChannelInfo> getChannelByUserId(String userId);
    
}
