/**
 * 
 */
package com.duowan.cms.core.rmi.client.article;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.Article4TagListSearchCriteria;

/**
 * 文章 RMI 服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-8
 * <br>==========================
 */
public interface Article4TagListRemoteService  extends RemoteService {
    
    /**
     *  根据查询条件获取文章列表
     * @param searchCriteria
     * @return
     */
    public List<Article4TagListInfo> listSearch(Article4TagListSearchCriteria searchCriteria);
    
    public Page<Article4TagListInfo> pageSearch(Article4TagListSearchCriteria searchCriteria);
    
    /**
     * 获取channelId频道下time时间内更新过的文章tag（一般是通过修改文章的关联tags , 所引起的变化的tags）,返回多个tag的列表
     * @param time
     * @return
     */
    public List<String> getRecentEffectTagsInArticle(String channelId , int minute);
    
    /**
     * 获取文章数，根据频道id与tag
     * @param channelId
     * @param tag
     * @return
     */
    public Integer getArticleCountByChannelIdAndTag(String channelId, String tag);
}
