/**
 * 
 */
package com.duowan.cms.core.rmi.client.gametest;

import java.util.List;

import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.gametest.GameTestInfo;
import com.duowan.cms.dto.gametest.GameTestSearchCriteria;

/**
 * 频道RMI服务接口
 */
public interface GameTestRemoteService  extends RemoteService {
    
    public List<GameTestInfo> listSearch(GameTestSearchCriteria searchCriteria);
    
}
