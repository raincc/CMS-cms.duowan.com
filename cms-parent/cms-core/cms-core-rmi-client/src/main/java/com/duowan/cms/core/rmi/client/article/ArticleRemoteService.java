/**
 * 
 */
package com.duowan.cms.core.rmi.client.article;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.article.Article4TagListInfo;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.article.ArticleSearchCriteria;

/**
 * 文章 RMI 服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-8
 * <br>==========================
 */
public interface ArticleRemoteService  extends RemoteService {
    
    //异常码：该专区下文章不存在
    public final static String ARTICLE_IS_NOT_EXITS = ArticleRemoteService.class.getName().concat("001");
    //异常码：文章不是普通类型
    public final static String ARTICLE_IS_NOT_NORNAL = ArticleRemoteService.class.getName().concat("002");
    
    /**
     * 根据文章ID获取某个频道下的文章信息
     * 
     * @param channelId 频道ID
     * @param articleId 文章ID
     * @return
     */
    public ArticleInfo getByChannelIdAndArticleId(String channelId, Long articleId);
    /**
     *  根据查询条件获取文章列表
     * @param searchCriteria
     * @return
     */
    public List<ArticleInfo> listSearch(ArticleSearchCriteria searchCriteria);
    
    
    public Page<ArticleInfo> pageSearch(ArticleSearchCriteria searchCriteria);
    
    
    /**
     *  获取预定发表的文章
     */
    public List<ArticleInfo>  getPrepublishArticle();
    
    /**
     *  获取预定发布的空链接
     */
    public void handlePrepublishEmptyLink() throws BaseCheckedException;
    
    /**
     * 获取channelId频道下time时间内更新的文章
     * @param time
     * @return
     */
    public List<ArticleInfo> getRecentUpdateArticle(String channelId , int minute);
    
    
    
    /**
     * 发布文章接口
     * @param articleInfo
     * @throws BaseCheckedException
     */
    public void publish(ArticleInfo articleInfo) throws BaseCheckedException;
    
    /**
     * 获得某频道某文章的访问地址url
     * @param channelId
     * @param articleId
     * @return
     */
    public String getArticleUrl(String channelId, Long articleId);
    
    /**
     * 检查题目是否已经存在
     * @param channelId
     * @param title
     * @return
     */
    public boolean checkTitleIsExisted(String channelId, String articleId, String title);
    
}
