package com.duowan.cms.core.rmi.client.template;

import java.util.List;

import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.template.AutoFlushTemplateInfo;

/**
 * 
 * 自动刷新模板 RMI 服务
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2013-3-8
 * <br>==========================
 */
public interface AutoFlushTemplateRemoteService extends RemoteService {

    /**
     * 创建自动刷新模板对象/修改自动刷新模板对象
     * @param templateInfo
     */
    public void save(AutoFlushTemplateInfo autoFlushTemplateInfo); 
    
    /**
     * 通过主键获取自动刷新模板对象
     * @param id
     * @return
     */
    public AutoFlushTemplateInfo  getById(String id);
    
    /**
     * 获取所有自动刷新模板对象
     * @return
     */
    public List<AutoFlushTemplateInfo> getAllAutoFlushTemplate();
    
}
