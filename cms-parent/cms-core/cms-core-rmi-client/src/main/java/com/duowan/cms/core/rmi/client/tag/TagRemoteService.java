/**
 * 
 */
package com.duowan.cms.core.rmi.client.tag;

import java.util.List;

import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.tag.TagInfo;

/**
 * Tag RMI服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-09
 * <br>==========================
 */
public interface TagRemoteService  extends RemoteService {
    
    /**
     * 根据频道id和名字获取tag
     * @param channelId
     * @param name
     * @return
     */
    public TagInfo getByChannelIdAndName(String channelId, String name);
    
    /**
     * 获取parent的一级子标签
     * @param channelId
     * @param parent
     * @return
     */
    public List<TagInfo> getChildren(String channelId, String parent);
    
    /**
     * 获取time时间内更新的标签
     * @param time
     * @return
     */
    public List<TagInfo> getRecentUpdateTag(String channelId,int minute);

    /**
     * 获取某频道某tag的后代，包含自身
     * @param channelId
     * @param parent
     * @return
     */
    public List<TagInfo> getDescendantIncludeSelf(String channelId, String parent);
    
    /**
     * 获取某频道下整体tag树，包括所有子孙tag（需要按照“tree”属性排序，方便页面展示）
     * @return
     */
    public List<TagInfo> getAllTagsInChannel(String channelId);
}
