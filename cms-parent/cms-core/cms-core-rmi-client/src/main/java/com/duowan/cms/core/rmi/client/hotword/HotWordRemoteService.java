/**
 * 
 */
package com.duowan.cms.core.rmi.client.hotword;

import java.util.List;

import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.hotword.HotWordInfo;

/**
 * 频道RMI服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：yzq
 * <br> 版本：1.0
 * <br> 创建时间：2012-12-20
 * <br>==========================
 */
public interface HotWordRemoteService  extends RemoteService {
    
    public List<HotWordInfo> getByChannelId(String channelId);
    
}
