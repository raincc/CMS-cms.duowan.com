package com.duowan.cms.core.rmi.client.template;

import java.util.List;

import com.duowan.cms.common.domain.Page;
import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.template.TemplateCategory;
import com.duowan.cms.dto.template.TemplateInfo;
import com.duowan.cms.dto.template.TemplateSearchCriteria;

/**
 * 模板RMI服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-8
 * <br>==========================
 */
public interface TemplateRemoteService  extends RemoteService {
    
    //异常码：该模板在该专区下不存在
    public final static String TEMPLATE_IS_NOT_EXIST_IN_CHANNEL = TemplateRemoteService.class.getName()+"001";
    
    /**
     * 在专区下，根据模板名字获取模板
     * @param channelId
     * @param templateName
     * @return
     */
    public TemplateInfo getByChannelIdAndTemplateName(String channelId, String templateName);
    
    /**
     * 在专区下，根据模板ID获取模板
     * @param channelId
     * @param templateId
     * @return
     */
    public TemplateInfo getByChannelIdAndTemplateId(String channelId, Long templateId);
    
    /**
     *  根据查询条件获取模板列表(慎用，结果集过大的时候，为了不影响性能，请使用pageSearch分页查询)
     * @param searchCriteria
     * @return
     */
    public List<TemplateInfo> listSearch(TemplateSearchCriteria searchCriteria);
    
    /**
     *  根据查询条件分页获取模板列表
     * @param searchCriteria
     * @return
     */
    public Page<TemplateInfo> pageSearch(TemplateSearchCriteria searchCriteria);
    
    /**
     * 根据channelid和类别获取模板，用于刷tag
     * @param channelId
     * @param category
     * @return
     */
    public List<TemplateInfo> getByChannelIdAndCategoryOrderByCooperater(String channelId, TemplateCategory category);

    /**
     * 标识某个模板解析有误（把改模板的状态标识为“解析有误”）
     * @param channelId
     * @param templateId
     */
    public void markParserFail(String channelId, Long templateId);
    
    /**
     * 标识某个模板解析有误（把改模板的状态标识为“解析有误”）,同时更新失败原因到更新日志
     * @param channelId
     * @param templateId
     * @param reason
     */
    public void markParserFail(String channelId, Long templateId, String reason);
    
    /**
     * 标识某个模板解析成功（把改模板的状态标识为“普通”）
     * @param channelId
     * @param templateId
     */
    public void markParserSuccess(String channelId, Long templateId);
    
    /**
     *  获取“需要自动刷新的模板”的列表
     * @return
     */
    public List<TemplateInfo> listAutoFlushTemplates();
}
