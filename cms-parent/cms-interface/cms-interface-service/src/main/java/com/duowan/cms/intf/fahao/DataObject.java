package com.duowan.cms.intf.fahao;

import java.util.List;

public class DataObject{
	private List<FahaoInfo> day;
	private List<FahaoInfo> week;
	private List<FahaoInfo> month;
	private List<FahaoInfo> all;
	
	public List<FahaoInfo> getDay() {
		return day;
	}
	public void setDay(List<FahaoInfo> day) {
		this.day = day;
	}
	public List<FahaoInfo> getWeek() {
		return week;
	}
	public void setWeek(List<FahaoInfo> week) {
		this.week = week;
	}
	public List<FahaoInfo> getMonth() {
		return month;
	}
	public void setMonth(List<FahaoInfo> month) {
		this.month = month;
	}
	public List<FahaoInfo> getAll() {
		return all;
	}
	public void setAll(List<FahaoInfo> all) {
		this.all = all;
	}


}
