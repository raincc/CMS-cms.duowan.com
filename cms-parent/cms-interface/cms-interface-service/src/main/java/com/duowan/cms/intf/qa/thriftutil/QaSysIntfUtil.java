package com.duowan.cms.intf.qa.thriftutil;

import org.apache.log4j.Logger;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 * 问答系统Thrift接口工具类
 */
public class QaSysIntfUtil {

	private static Logger logger = Logger.getLogger(QaSysIntfUtil.class);
	
	
    private static boolean initialized;

    public static TSocket socket = null;
    public static QAW2YProxy.Client client = null;

    public static int remotePort = 13300;
    public static String remoteHost = "121.9.221.236";

    static {
        initialized = false;
        init();
        System.out.println("Thrift client started.");
    }

    public synchronized static void init() {
        if (!initialized) {
            initialized = true;
            if (client == null) {
                startClient();
            }
        }
    }

    private static void ensureClient() {
        if (socket == null || !socket.isOpen()) {
            startClient();
        }
    }

    private static boolean ensureSocket() {
        return socket != null && socket.isOpen();
    }

    private static void startClient() {
        System.out.println("starting Thrift client to " + remoteHost + ":"
                + remotePort + ".");
        try {
            socket = new TSocket(remoteHost, remotePort);
            TTransport transport = new TFramedTransport(socket);
            TProtocol protocol = new TBinaryProtocol(transport);
            client = new QAW2YProxy.Client(protocol);
            socket.open();
        } catch (Exception ex) {
            socket = null;
        }
    }

    /**
     * 提供新闻主题
     * @param req {m_areaKey:频道ID,m_title:标题,m_content:内容}
     * @return
     */
    public static synchronized boolean postWebNewsTopic(WebNewsTopic req) {
        if (req == null || !isFine(req.getM_areaKey())
                || !isFine(req.getM_title()) || !isFine(req.getM_content())) {
            return false;
        }
        boolean flag = false;
        ensureClient();
        try {
            if (ensureSocket()) {
            	logger.info("同步频道[channelid="+req.getM_areaKey()+"]文章[title="+req.getM_title()+"]的文章到问答系统。");
                flag = client.postWebNewsTopic(req);
            }
        } catch (Exception e) {
            if (socket != null) {
                socket.close();
                socket = null;
            }
        }
        return flag;
    }

    public static boolean isFine(String str) {
        return str == null || str.trim().length() <= 0 ? false : true;
    }

    public static void main(String[] args) {
        WebNewsTopic topic = new WebNewsTopic();
        topic.setM_title("英雄联盟谁最强?");
        topic.setM_content("没有最强的英雄，只有更强的玩家!");
        topic.setM_areaKey("lol");
        boolean flag = QaSysIntfUtil.postWebNewsTopic(topic);
        System.out.println(flag);
    }
}
