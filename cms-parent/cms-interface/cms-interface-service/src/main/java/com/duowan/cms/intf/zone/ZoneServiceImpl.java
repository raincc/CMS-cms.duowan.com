package com.duowan.cms.intf.zone;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.NetUtil;
import com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder;
import com.duowan.cms.intf.Constants;
@Service("zoneRemoteService")
public class ZoneServiceImpl implements ZoneRemoteService {

	private static Logger logger = Logger.getLogger(ZoneServiceImpl.class);
    /**
     * 同步有爱有密码配置
     */
    private final static String ZONE_PASSWORD = "allow_pass";
    /**
     * 同步文章到有爱
     * @param userName 关联的账号
     * @param pass 约定的密码
     * @param articleUrl 文章的外网URL
     * @param title 文章的title
     * @param tags 文章的tags
     * @throws BaseCheckedException 
     */
	@Override
	public void publish(String userName, String articleUrl, String title, String tags , 
				String source , String  author) throws BaseCheckedException {
		Map<String, String> postParamsMap = new HashMap<String, String>(); 
		postParamsMap.put("userName",userName);
		String pass = PropertiesHolder.get(ZONE_PASSWORD, "7fVZLdUBiebeM3O");
		postParamsMap.put("pass",pass);
		postParamsMap.put("url",articleUrl);
		postParamsMap.put("tags",tags);
		postParamsMap.put("title",title);
		postParamsMap.put("userName","userName");
		postParamsMap.put("from",source);//参数暂无，所以注释
		postParamsMap.put("author",author);
		  //ExecutorService threadPool = ThreadUtil.getThreadPool();
		NetUtil.postHttpRequest(postParamsMap, Constants.ZONE_PUBLISH_URL, Constants.EXPIRE_SECONDS);
		logger.info("同步文章["+title+"]到有爱成功");
	}

}
