/**
 * 
 */
package com.duowan.cms.intf.qa;

import org.springframework.stereotype.Service;

import com.duowan.cms.intf.qa.thriftutil.QaSysIntfUtil;
import com.duowan.cms.intf.qa.thriftutil.WebNewsTopic;

/**
 * Y世界（问答系统）服务实现
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-9
 * <br>==========================
 */
@Service("qaRemoteService")
public class QaServiceImpl implements QaRemoteService {

    @Override
    public void publish(String channelId, String title, String content) {
        WebNewsTopic topic = new WebNewsTopic();
        topic.setM_title(title);
        topic.setM_content(content);
        topic.setM_areaKey(channelId);
        boolean isSuccess = QaSysIntfUtil.postWebNewsTopic(topic);
        if(isSuccess)
            logger.info("频道"+channelId+"下的文章[title="+title+"]同步到问答系统成功。");
        else
            logger.info("频道"+channelId+"下的文章[title="+title+"]同步到问答系统失败。");
    }

}
