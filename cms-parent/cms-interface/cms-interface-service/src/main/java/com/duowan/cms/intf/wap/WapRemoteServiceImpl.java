package com.duowan.cms.intf.wap;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.duowan.cms.intf.Constants;
import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.DateUtil;
import com.duowan.cms.common.util.NetUtil;
import com.duowan.cms.common.util.StringUtil;
import com.duowan.cms.dto.article.ArticleInfo;

@Service("wapRemoteService")
public class WapRemoteServiceImpl implements WapRemoteService {
	private static Logger logger = Logger.getLogger(WapRemoteServiceImpl.class);
	/**
	 * 把文章发到Wap接口
	 * Subject:完整标题 
     *	Source:原始来源
     *	ClassName:分类（多级分类以”,”相隔）
     *	Body:具体内容(内含图片链接)
     *	AddTime:添加时间
     *	LinkUrl:原始链接
     *	ChannelId：频道id
     *	(注：任一参数为空将不会入库)
	 */
	@Override
	public void sendArticleToWap(ArticleInfo finalArticleInfo) throws BaseCheckedException {
		logger.info("开始调用Wap接口:");
		Map<String, String> postParamsMap = this.buildParamMap(finalArticleInfo);
		NetUtil.postHttpRequest(postParamsMap, Constants.WAP_URL, Constants.EXPIRE_SECONDS);
		logger.info("调用Wap接口结束!");
	}
	
	private  Map<String, String> buildParamMap(ArticleInfo finalArticleInfo){
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("Subject", finalArticleInfo.getTitle());
		parameters.put("Source", finalArticleInfo.getSource() );
		parameters.put("ClassName", finalArticleInfo.getTags() );
		parameters.put("LinkUrl", finalArticleInfo.getUrlOnLine());
		parameters.put("AddTime", DateUtil.getCurrentTimeStr());
		parameters.put("ChannelId", finalArticleInfo.getChannelInfo().getId());
		String body = finalArticleInfo.getContent();
 		parameters.put("Body", StringUtil.htmlToWeb(body));
 		logger.info("Wap接口参数: wap接口URL -->" +Constants.WAP_URL+
 			",Subject -->" + finalArticleInfo.getTitle() +
 			",Source -->"+ finalArticleInfo.getSource() +
 			",ClassName -->"+ finalArticleInfo.getTags()+
 			",LinkUrl -->"+  finalArticleInfo.getUrlOnLine() +
 			",AddTime -->"+ DateUtil.getCurrentTimeStr() +
 			",ChannelId -->"+ finalArticleInfo.getChannelInfo().getId() +
 			",Body -->"+ StringUtil.htmlToWeb(body) );
 		return parameters;
	}

}
