package com.duowan.cms.intf.fahao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.JsonUtil;
import com.duowan.cms.common.util.NetUtil;
import com.duowan.cms.intf.Constants;

public class FahaoRankHelper {
	/**
	 * 获取新游戏测试
	 * 
	 * @return
	 * @throws BaseCheckedException
	 * @throws Exception
	 */
	public synchronized Map<String, List<FahaoInfo>> getMapFromNewGameTest(String gameNames)
				throws BaseCheckedException {

		Map<String, List<FahaoInfo>> map = new HashMap<String, List<FahaoInfo>>();

		Map<String, String> postParamsMap = new HashMap<String, String>();// 参数map
		postParamsMap.put(Constants.FAHAO_ACTION, Constants.FAHAO_API_NEWGAME);
		postParamsMap.put("gameName", gameNames);
		String data = NetUtil.getHttpRequest(postParamsMap, Constants.FAHAO_URL, Constants.EXPIRE_SECONDS);
		if("0".equals(data)) return map;
		List<FahaoInfo> list = JsonUtil.jsonString2JavaBeanList(FahaoInfo.class, FahaoRankHelper
					.trimRepCode(data));
		if (null != list && !list.isEmpty()) {
			for (FahaoInfo fahaoInfo : list) {
				String key = fahaoInfo.getGameName();
				List<FahaoInfo> listTemp = new ArrayList<FahaoInfo>();
				listTemp.add(fahaoInfo);
				map.put(key, listTemp);
			}
		}
		return map;
	}

	/**
	 *  根据不同的接口，获取(周月总)的数据
	 */
	public synchronized Map<String, List<FahaoInfo>> getMap(String faHaoAction)
				throws BaseCheckedException {
		Map<String, String> postParamsMap = new HashMap<String, String>();// 参数map
		postParamsMap.put(Constants.FAHAO_ACTION, faHaoAction);
		String data = NetUtil.postHttpRequest(postParamsMap, Constants.FAHAO_URL,
			Constants.EXPIRE_SECONDS);
		
		JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(FahaoRankHelper
                .getJsonFromResponseData(data).replaceAll("null", "\"\""));

		Map<String, List<FahaoInfo>> map = new HashMap<String, List<FahaoInfo>>();
		try{
		    JSONArray week = jsonObject.getJSONArray("week");
	        if (null != week && !week.isEmpty()) {
	            map.put("week", JsonUtil.jsonArray2JavaBeanList(FahaoInfo.class, week));
	        }
		}catch(Exception e){
		    
		}
		try{
		    JSONArray month = jsonObject.getJSONArray("month");
	        if (null != month && !month.isEmpty()) {
	            map.put("month", JsonUtil.jsonArray2JavaBeanList(FahaoInfo.class, month));
	        }
		}catch(Exception e){
		    
		}
		
		try{
		    JSONArray all = jsonObject.getJSONArray("all");
	        if (null != all && !all.isEmpty()) {
	            map.put("all", JsonUtil.jsonArray2JavaBeanList(FahaoInfo.class, all));
	        }
        }catch(Exception e){
            
        }

		return map;
	}

	/**
	 * 去除返回字符传中的http代码
	 * 
	 * @param data
	 * @return
	 */
	public static String trimRepCode(String data) {
		if (data != null && data.startsWith("200")) {
		    Pattern p = Pattern.compile("\\[.*\\]");
		    Matcher m = p.matcher(data);
		    if(m.find()){
		        data = m.group();
		    }
		}
		return data;
	}
	
	public static String getJsonFromResponseData(String data) {
        if (data != null && data.startsWith("200")) {
            Pattern p = Pattern.compile("\\{.*\\}");
            Matcher m = p.matcher(data);
            if(m.find()){
                data = m.group();
            }
        }
        return data;
    }
	
}
