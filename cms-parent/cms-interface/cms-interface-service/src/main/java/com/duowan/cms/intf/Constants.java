package com.duowan.cms.intf;

import com.duowan.cms.common.webapp.property.ExtendedPropertyPlaceholderConfigurer.PropertiesHolder;

public class Constants {

	/**发号系统api*/
    public static String FAHAO_URL = PropertiesHolder.get("fahao_url");
	/**接口类别参数api*/
	public static final String FAHAO_ACTION = "action";
	/**新游测试*/
	public static final String FAHAO_API_NEWGAME = "newGameTest";
	/**网站预订排行(周月总)*/
	public static final String FAHAO_API_BOOK = "book";
	/**网站发号排行(周月总)*/   
	public static final String FAHAO_API_SEND = "send";
	/**ka最新活动*/
	public static final String FAHAO_API_ACTIVE = "newActs";
	/**游戏特权*/
	public static final String FAHAO_YXTQ_ACTIVE = "yxtq";
	/**http连接过期时间*/
	public static final int EXPIRE_SECONDS  = 10;
	/**图库*/
	public static final String TU_API_URL = PropertiesHolder.get("tuku_url");
	/**游戏库所有游戏信息接口*/
	public static final String GAME_API_URL = PropertiesHolder.get("game_api_url");
	/**游戏库游戏卡信息接口*/
	public static final String GAME_CARD_URL = PropertiesHolder.get("game_card_url");
	
	/**有爱发布文章接口*/
    public static final String ZONE_PUBLISH_URL = PropertiesHolder.get("zone_publish_url");
	
	/**wap接口*/
    public static final String WAP_URL = PropertiesHolder.get("wap_url");
	
	//---------------------------- 投票接口 -----------------------------------------
    public static final String VOTE_URL = PropertiesHolder.get("vote_url");
    public static final String VOTE_OR_LOAD_URL = PropertiesHolder.get("vote_or_load_url");
    
    //---------------------------- 通用评论相关接口------------------------------------------
    public static final String COMMENT3_GETCOMMENT_URL = PropertiesHolder.get("comment3_getcomment_url", "http://comment3.duowan.com/index.php");
    public static final String COMMENT3ADMIN_URL = PropertiesHolder.get("comment3admin_url", "http://comment3admin.duowan.com/index.php");
    
    
    //----------------------------新游测试信息接口------------------------------------------
    public static final String GAMETESTINFO_URL = PropertiesHolder.get("gametestinfo_url", "http://ka.duowan.com/api/newGameTest.do");
    
	
}
