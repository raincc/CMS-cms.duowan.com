package com.duowan.cms.intf.gamelib;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.exception.BaseCheckedException;

public class GameLibServiceImplTest {

	private BeanFactory factory;
	private GameLibRemoteService gameLibService = null;
	@Before
	public void setUp() throws Exception {
		factory = new ClassPathXmlApplicationContext("config/application-context.xml");
		gameLibService =  (GameLibRemoteService) factory.getBean("gameLibRemoteService");
	}
	@Test
	public void testGetAll() throws BaseCheckedException {
		List<GameInfo> gameInfoList = gameLibService.getAll();
		Assert.assertNotNull(gameInfoList);
	}

	@Test
	public void testGetByName() throws BaseCheckedException {
		GameInfo gameInfo = gameLibService.getGameInfoByName("魔兽争霸RPG");
		Assert.assertNotNull(gameInfo);
	}
	@Test
	public void testGetCardInfoByName() throws BaseCheckedException {
		String gameCardInfo = gameLibService.getGameCardInfo("魔兽争霸RPG");
		System.out.println(gameCardInfo);
		Assert.assertNotNull(gameCardInfo);
	}

	@Test
	public void testSendArticle() throws BaseCheckedException {
		gameLibService.sendArticle("剑灵",
			"中国内地首富之子也爱剑灵 凌晨微博求码", "http://newgame.duowan.com/1208/209748325302.html");
	}

}
