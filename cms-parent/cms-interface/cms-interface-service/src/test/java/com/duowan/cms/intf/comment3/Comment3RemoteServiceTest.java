package com.duowan.cms.intf.comment3;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.util.IdManager;
import com.duowan.cms.common.util.MD5;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.channel.ChannelInfo;

public class Comment3RemoteServiceTest {

	private BeanFactory factory;
	private Comment3RemoteService comment3Service = null;
	@Before
	public void setUp() throws Exception {
		factory = new ClassPathXmlApplicationContext("config/application-context.xml");
		comment3Service =  (Comment3RemoteService) factory.getBean("comment3RemoteService");
	}
	@Test
	public void testGetCommentCount() throws BaseCheckedException {
	    int commentCount = comment3Service.getCommentCount(buildArtilceInfo());
	    System.out.println(commentCount);
	}
	
	@Test
    public void testGetCommentCountMore() throws BaseCheckedException {
//	    String[] ids = {"231432791748", "231432768039", "231431702157", "231431184943", "231430608729",
//                        "231430349724", "231430039819", "231429658063", "231421540779", "231421446704"};
	    String[] ids = {"231430039819","231419633760","231432768039","231431702157","231431184943",
	                    "231430608729","231429658063","231421446704","231421008654","231418426993",
	                    "231417261048","231415718571","231415056034","231413291410","231329059648",
	                    "231342745551","231342300580","231342029544","231331434520","231346107304",
	                    "231335495909","231335303484","231334405287","231331087293","231328399823"};
        Set<String> uniqids = new HashSet<String>();
        MD5 md5 = new MD5();
        String domain = "lol.duowan.com";
        for(String id : ids){
            String uniqid = "http://" + domain + "/" + IdManager.formatIdToYM(Long.parseLong(id)) + "/" + id + ".html";
            System.out.println(uniqid);
            System.out.println(md5.get(uniqid).toLowerCase());
            uniqids.add(md5.get(uniqid).toLowerCase());
        }
        Map<String,String> map = comment3Service.getMoreCommentCount(domain, uniqids, 8);
        System.out.println(map.size());
        for(String str : map.keySet()){
            System.out.println(str + ":" + map.get(str));
        }
    }
	@Test
	public void testParseResponseData(){
	    
	}

	
	
	private ArticleInfo buildArtilceInfo(){
	    ArticleInfo a = new ArticleInfo();
	    ChannelInfo c = new ChannelInfo();
	    c.setId("lol");
	    c.setDomain("http://lol.duowan.com");
	    a.setChannelInfo(c);
	    a.setId(231342745551L);
	    a.setThreadId(231342745551L);
	    a.setUrlOnLine("http://lol.duowan.com/1305/231342745551.html");
	    
	    
	    
	    return a;
	}

}
