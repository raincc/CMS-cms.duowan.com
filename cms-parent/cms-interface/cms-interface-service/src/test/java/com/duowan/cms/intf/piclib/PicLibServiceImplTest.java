package com.duowan.cms.intf.piclib;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.duowan.cms.common.exception.BaseCheckedException;

public class PicLibServiceImplTest {

	private PicLibRemoteService picLibService = new PicLibServiceImpl();

	@Test
	public void testGetList() throws BaseCheckedException {
		 List<TujiInfo> tijiInfoList = picLibService.getList("wow", 1, 50, 2, "");
		Assert.assertNotNull(tijiInfoList);
	}

}
