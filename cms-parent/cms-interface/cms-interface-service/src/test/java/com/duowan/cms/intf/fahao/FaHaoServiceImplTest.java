package com.duowan.cms.intf.fahao;


import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.duowan.cms.intf.Constants;
import com.duowan.cms.common.exception.BaseCheckedException;

public class FaHaoServiceImplTest {

	private  FahaoRemoteService fahaoService = new FahaoServiceImpl();
	@Test
	public void testGetByGameName() throws UnsupportedEncodingException, BaseCheckedException {
		FahaoInfo fahaoInfo = fahaoService.getByGameName("倩女幽魂");
		Assert.assertNotNull(fahaoInfo);
	}
	
	@Test
	public void testGetFahaoRankFromNewGameTest() throws BaseCheckedException {
		Map<String, List<FahaoInfo>> map = fahaoService.getFahaoRank(Constants.FAHAO_API_NEWGAME, "倩女幽魂");
		Assert.assertNotNull(map);
	}
	/**
	 * 网站预订排行(周月总)
	 */
	@Test
	public void testGetFahaoRankFromBookRank() throws BaseCheckedException {
		Map<String, List<FahaoInfo>> map = fahaoService.getFahaoRank(Constants.FAHAO_API_BOOK, "倩女幽魂");
		Assert.assertNotNull(map);
		Assert.assertNotNull(map.get("week"));
	}

}
