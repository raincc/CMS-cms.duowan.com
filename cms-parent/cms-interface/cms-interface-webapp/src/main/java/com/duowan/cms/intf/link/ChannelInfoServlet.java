/*
 * @(#) ChannelInfoServlet.java   1.0, Dec 8, 2010
 * Copyright 2005-2010 duowan.com.
 * All rights reserved.
 */
package com.duowan.cms.intf.link;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.duowan.cms.common.webapp.content.SpringApplicationContextHolder;
import com.duowan.cms.core.rmi.client.channel.ChannelRemoteService;
import com.duowan.cms.dto.channel.ChannelInfo;
import com.duowan.cms.intf.util.IPFilter;

/**
 * @author yzq
 * @Description: 获取频道信息的接口
 */
public class ChannelInfoServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = 3519772235207010297L;

    private static final String FILE_SEPARATOR = System.getProperty("line.separator");

    private static Logger logger = Logger.getLogger(ChannelInfoServlet.class);

    private static final String CHANNELINFO_ALLOWEDIP = "ip_getChannelInfo";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        
        // 添加IP限制
        String ip = request.getHeader("X-Forwarded-For");
        if ((ip == null) || ("".equals(ip))) {
            ip = request.getRemoteAddr();
        }
        ip = ip.split(", ")[0].trim();
        if ("127.0.0.1".equals(ip)) {
            ip = request.getRemoteAddr();
        }
        //IPFilter.isLegalIp(ip, CHANNELINFO_ALLOWEDIP) 两者都可以
        //IPManager读取并解析文件，每5分钟读一次;IPFilter利用Properties类进行读取properties文件,每次都需load一次
        if (!IPFilter.isLegalIp(ip, CHANNELINFO_ALLOWEDIP)) {
            logger.warn("未授权ip访问频道信息：" + ip);
            out.println("访问IP未授权，请向管理人员提出申请。");
        } else {
            //LinkRemoteService linkService = SpringApplicationContextHolder.getBean("linkRemoteService", LinkRemoteService.class);
            ChannelRemoteService channelRemoteService = SpringApplicationContextHolder.getBean("channelRemoteService", ChannelRemoteService.class);
            List<ChannelInfo> list = channelRemoteService.getAllChannel();
            StringBuffer sb = new StringBuffer();
            for (ChannelInfo c : list) {
                // channelInfoes[i][2].substring(7) 去掉前面的http://
                sb.append(c.getId()).append(",").append(c.getName()).append(",").append(c.getDomain().substring(7)).append(FILE_SEPARATOR);
            }
            out.println(sb.toString());
        }

        out.flush();
        out.close();
    }

    /**
     * The doPost method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to post.
     * 
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
