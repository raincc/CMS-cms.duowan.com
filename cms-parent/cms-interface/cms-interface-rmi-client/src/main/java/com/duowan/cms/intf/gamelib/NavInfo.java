package com.duowan.cms.intf.gamelib;

import java.io.Serializable;

public class NavInfo implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1886109268760426878L;

    private Integer gameID;
    
    private Integer navID;
    
    private String title;
    
    private String url;
    
    private String used;
    
    private String weight;

    public Integer getGameID() {
        return gameID;
    }

    public void setGameID(Integer gameID) {
        this.gameID = gameID;
    }

    public Integer getNavID() {
        return navID;
    }

    public void setNavID(Integer navID) {
        this.navID = navID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
    
    
}
