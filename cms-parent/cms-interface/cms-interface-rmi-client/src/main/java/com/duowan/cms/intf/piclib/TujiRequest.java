package com.duowan.cms.intf.piclib;

public class TujiRequest {

    private String alias;//专题名称，若为空，则所有专题
    
    private Integer page;//第几页，默认第一页
    
    private Integer sort;//排序：0-->最新，1-->热门（按评论数），默认0-->最新
    
    private Integer num;//每页多少，默认50
    
    private String tag;//图集标签：美女，若为空，则所有标签

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

}
