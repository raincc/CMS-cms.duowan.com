package com.duowan.cms.intf.piclib;

import java.io.Serializable;

/**
 * 
 * 图集对象
 * 
 * @author Allen
 *
 */
public class TujiInfo implements Serializable{
    
    private String g_id;        //图集id
    private String g_title;     //图集名
    private String g_created;   //图集创建时间戳
    private String g_sum;       //图片张数
    private String cover_url;   //封面url
    private String cover_width; //封面宽度
    private String cover_height;//封面高度
    private String click;       //浏览数
    private String tags;        //标签名列表
    private String user_id;     //上传者id
    private String user_name;   //上传者昵称
    private String description; //图集说明前50字
    
    public String getG_id() {
        return g_id;
    }
    public void setG_id(String g_id) {
        this.g_id = g_id;
    }
    public String getG_title() {
        return g_title;
    }
    public void setG_title(String g_title) {
        this.g_title = g_title;
    }
    public String getG_created() {
        return g_created;
    }
    public void setG_created(String g_created) {
        this.g_created = g_created;
    }
    public String getG_sum() {
        return g_sum;
    }
    public void setG_sum(String g_sum) {
        this.g_sum = g_sum;
    }
    public String getCover_url() {
        return cover_url;
    }
    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }
    public String getCover_width() {
        return cover_width;
    }
    public void setCover_width(String cover_width) {
        this.cover_width = cover_width;
    }
    public String getCover_height() {
        return cover_height;
    }
    public void setCover_height(String cover_height) {
        this.cover_height = cover_height;
    }
    public String getClick() {
        return click;
    }
    public void setClick(String click) {
        this.click = click;
    }
    public String getTags() {
        return tags;
    }
    public void setTags(String tags) {
        this.tags = tags;
    }
    public String getUser_id() {
        return user_id;
    }
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getUser_name() {
        return user_name;
    }
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
