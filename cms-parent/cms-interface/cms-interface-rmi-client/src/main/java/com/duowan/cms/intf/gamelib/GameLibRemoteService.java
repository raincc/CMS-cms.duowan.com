package com.duowan.cms.intf.gamelib;

import java.util.List;

import org.apache.log4j.Logger;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.intf.duowanvote.DuowanVoteRemoteService;


/**
 * 游戏库系统服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-5
 * <br>==========================
 */
public interface GameLibRemoteService extends RemoteService {
    
    public static Logger logger = Logger.getLogger(GameLibRemoteService.class);
    
    /**
     * 获取所有的游戏信息
     * @return
     */
    public List<GameInfo> getAll() throws BaseCheckedException ;

    /**
     * 根据游戏名获取游戏信息
     * @param name
     * @return
     */
    public GameInfo getGameInfoByName(String gameName)throws BaseCheckedException ;
    /**
     * 根据游戏名获取卡信息
     * @param gameName
     * @return
     * @throws BaseCheckedException
     */
    public String getGameCardInfo(String gameName)throws BaseCheckedException ;
    /**
     * 发送游戏的文章 title 和文章 url
     * @param articleTitle
     * @param articleUrl
     */
    public void sendArticle(String gameName,String articleTitle, String articleUrl) throws BaseCheckedException;
    
}
