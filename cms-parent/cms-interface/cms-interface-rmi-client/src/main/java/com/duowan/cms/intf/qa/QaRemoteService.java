/**
 * 
 */
package com.duowan.cms.intf.qa;

import org.apache.log4j.Logger;

import com.duowan.cms.common.service.RemoteService;

/**
 * Y世界(问答系统)服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-9
 * <br>==========================
 */
public interface QaRemoteService extends RemoteService {
    
    public static Logger logger = Logger.getLogger(QaRemoteService.class);

    public void publish(String channelId , String title, String content);
    
}
