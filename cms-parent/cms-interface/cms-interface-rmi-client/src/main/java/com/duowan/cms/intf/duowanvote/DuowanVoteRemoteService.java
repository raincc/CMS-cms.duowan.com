/**
 * 
 */
package com.duowan.cms.intf.duowanvote;

import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import com.duowan.cms.common.service.RemoteService;

/**
 *  多玩投票(表情投票,文章流量统计)系统服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：yzq
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-5
 * <br>==========================
 */
public interface DuowanVoteRemoteService extends RemoteService {
    
    public static Logger logger = Logger.getLogger(DuowanVoteRemoteService.class);

    public LinkedHashMap<Long, Long> getVoteRankDataByArticles(DuowanVoteSearchCriteria criteria);
    
    public Integer getVoteNum(String articleId, String channelId, String title, String url);
}
