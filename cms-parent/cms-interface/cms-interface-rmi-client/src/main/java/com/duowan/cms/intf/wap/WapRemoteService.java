package com.duowan.cms.intf.wap;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.dto.article.ArticleInfo;

public interface WapRemoteService {

	/**
	 * 把文章发到Wap接口
	 * Subject:完整标题 
     *	Source:原始来源
     *	ClassName:分类（多级分类以”,”相隔）
     *	Body:具体内容(内含图片链接)
     *	AddTime:添加时间
     *	LinkUrl:原始链接
     *	ChannelId：频道id
     *	(注：任一参数为空将不会入库)
	 */
	public void sendArticleToWap(ArticleInfo finalArticleInfo) throws BaseCheckedException;
}
