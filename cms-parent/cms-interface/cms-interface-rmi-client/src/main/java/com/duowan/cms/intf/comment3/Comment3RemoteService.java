/**
 * 
 */
package com.duowan.cms.intf.comment3;

import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.log4j.Logger;

import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.article.ArticleInfo;
import com.duowan.cms.dto.channel.ChannelInfo;

/**
 *  多玩通用评论系统服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：yzq
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-5
 * <br>==========================
 */
public interface Comment3RemoteService extends RemoteService {
    
    public static Logger logger = Logger.getLogger(Comment3RemoteService.class);

    public Integer getCommentCount(ArticleInfo articleInfo);
    
    public void openCommentSite(ChannelInfo channelInfo);
    
    public LinkedHashMap<String, String> getMoreCommentCount(String domain, Set<String> uniqids, Integer length);
}
