/**
 * 
 */
package com.duowan.cms.intf.zone;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.RemoteService;

/**
 * 有爱服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-9
 * <br>==========================
 */
public interface ZoneRemoteService extends RemoteService {

    /**
     * 同步文章到有爱
     * @param userName 关联的账号
     * @param pass 约定的密码
     * @param articleUrl 文章的外网URL
     * @param title 文章的title
     * @param tags 文章的tags
     */
    public void publish(String userName, String articleUrl, String title, String tags ,
    			String source , String  author) throws BaseCheckedException;
    
}
