/**
 * 
 */
package com.duowan.cms.intf.fahao;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.RemoteService;
import com.duowan.cms.dto.gametest.GameTestInfo;

/**
 *  发号系统服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-5
 * <br>==========================
 */
public interface FahaoRemoteService extends RemoteService {

    /**
     * 通过游戏名字获取游戏的发号信息
     * @param gameName
     * @return
     */
    public FahaoInfo getByGameName(String gameName)  throws BaseCheckedException, UnsupportedEncodingException;
    
    /**
     * 获得获取发号数据信息列表
     * @param gameName 游戏名字
     * @return
     */
    public Map<String, List<FahaoInfo>> getFahaoRank(String action, String gameName) throws BaseCheckedException;
    
    
    public List<GameTestInfo> listSearchGameTestInfo(Map<String, String> paramsMap);
    
}
