package com.duowan.cms.intf.fahao;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import com.duowan.cms.common.dto.DataTransferObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 发号对象
 */
public class FahaoInfo implements DataTransferObject {
    
	private static Logger logger = Logger.getLogger(FahaoInfo.class);
	
	private String id = "";//游戏id
	private String actState = "";//状态
	private String gameName = "";//游戏名
	private String url = "";//对应URL连接
	private String count = "";//数量
	private String actName = "";//活动名称
	private String gotcount ;//为该活动领号数量
	private String hadbook ;//为该游动预订数量
	private String taocount ;//为该活动淘号数量
	private String gameUrl;//卡中心游戏预定的最终页
	private String oprateCrop;
    private String kuUrl = ""; //对应游戏名在产品库的URL 2011-03-18 hc 增加 (排行榜里的游戏名需要链接到产品库)
	
	public FahaoInfo(String id, String actState, String gameName, String url,
			String count) {
		super();
		this.id = id;
		this.actState = actState;
		this.gameName = gameName;
		this.url = url;
		this.count = count;
	}
	public FahaoInfo(){
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getActState() {
		if(StringUtils.isNotBlank(actState)){
			try {
				actState = URLDecoder.decode(actState, "utf-8");
			} catch (UnsupportedEncodingException e) {
				logger.error("getActState(),URLDecoder.decode happen exception:"+e.getMessage());
			} 
		}else{
			actState = "暂无";
		}
		return actState;
	}
	public void setActState(String actState) {
		this.actState = actState;
	}
	public String getGameName() {
		if(StringUtils.isNotBlank(gameName)){
			try {
				gameName = URLDecoder.decode(gameName, "utf-8");
			} catch (UnsupportedEncodingException e) {
				logger.info("getActState(),URLDecoder.decode happen exception:"+e.getMessage());
			} 
		}
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
	public String getActName() {
		return actName;
	}
	public void setActName(String actName) {
		this.actName = actName;
	}
	
	public String getOprateCrop() {
        return oprateCrop;
    }
    public void setOprateCrop(String oprateCrop) {
        this.oprateCrop = oprateCrop;
    }
    //	public String getKuUrl() {
//		if(getGameName()!=null){
//    		try{
//    			kuUrl = KuService.getGameUrl(getGameName());
//    		}catch (Exception e) {
//    			logger.warn("getGameUrl error,gameName="+getGameName(),e);
//    		}
//		}
//		
//		if(kuUrl==null||kuUrl.trim().equals("")){
//			kuUrl = url;
//		}
//		return kuUrl;
//	}
	public void setKuUrl(String kuUrl) {
		this.kuUrl = kuUrl;
	}
	
	public String getGotcount() {
		return gotcount;
	}
	public void setGotcount(String gotcount) {
		this.gotcount = gotcount;
	}
	public String getHadbook() {
		return hadbook;
	}
	public void setHadbook(String hadbook) {
		this.hadbook = hadbook;
	}
	public String getTaocount() {
		return taocount;
	}
	public void setTaocount(String taocount) {
		this.taocount = taocount;
	}
	   public String getGameUrl() {
	        return gameUrl;
	    }
	    public void setGameUrl(String gameUrl) {
	        this.gameUrl = gameUrl;
	    }
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";
	    
	    String retValue = "";
	    
	    retValue = "Fahao ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "actState = " + this.actState + TAB
	        + "gameName = " + this.gameName + TAB
	        + "url = " + this.url + TAB
	        + "count = " + this.count + TAB
	        + "actName = " + this.actName + TAB
	        + "kuUrl = " + this.kuUrl + TAB
	        + "gotcount = " + this.gotcount + TAB
	        + "hadbook = " + this.hadbook + TAB
	        + "taocount = " + this.taocount + TAB
	        + "gameUrl = " + this.gameUrl + TAB//--add by yzq 20121009 
	        + " )";
	
	    return retValue;
	}
	
	public String getKuUrl() {
        return kuUrl;
    }
    public static void main(String [] args){
		//String d = KuService.getGameUrl("雷电");
		//System.out.println(d);
	}
}
