package com.duowan.cms.intf.fahao;

public class FahaoRequest {

    private String action;
    private String gameName;
    
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getGameName() {
        return gameName;
    }
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
}
