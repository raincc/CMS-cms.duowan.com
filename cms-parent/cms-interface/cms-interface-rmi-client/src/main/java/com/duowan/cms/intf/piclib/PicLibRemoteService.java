/**
 * 
 */
package com.duowan.cms.intf.piclib;

import java.util.List;

import com.duowan.cms.common.exception.BaseCheckedException;
import com.duowan.cms.common.service.RemoteService;

/**
 * 图库系统服务接口
 *
 * <br>==========================
 * <br> 公司：欢聚时代
 * <br> 开发：qiusidi@yy.com
 * <br> 版本：1.0
 * <br> 创建时间：2012-11-5
 * <br>==========================
 */
public interface PicLibRemoteService  extends RemoteService {

    /**
     * 根据子站名字获取图库图集信息
     * @param alias 子站名字
     * @param pageNo 当前页，默认是1
     * @param pageSize 每页条数,默认为50，最多100
     * @param sort  排序方式,0为最新,1为热门， 默认为0
     * @param tag 标签名,默认无
     * @return
     */
    public List<TujiInfo> getList(String alias, Integer pageNo, Integer pageSize, Integer sort, String tag)throws BaseCheckedException;
}
