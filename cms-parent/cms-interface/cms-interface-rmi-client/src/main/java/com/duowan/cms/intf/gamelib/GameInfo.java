package com.duowan.cms.intf.gamelib;

/**
 游戏库游戏信息(需设置默认值)
 */
public class GameInfo {

    private String gameId="";     //游戏ID
    private String gameName="";   //游戏名称
    private String urlId="http://ka.duowan.com";      //访问游戏的url
    private String picUrl="http://ka.duowan.com";     //访问游戏图片的url
    private String videoUrl="http://ka.duowan.com";   //访问游戏视频的url
    private String mesgUrl="http://ka.duowan.com";    //访问游戏资讯的url
    
    private int    picUpdateNum;  //游戏图片24小时内的更新数量
    private int    picTotalNum;   //游戏图片总数目
    private int    videoUpdateNum;//游戏视频24小时内的更新数量
    private int    videoTotalNum; //游戏视频总数目
    private int    mesgUpdateNum; //游戏资讯24小时内的更新数量
    
    public String getGameId() {
        return gameId;
    }
    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
    public String getGameName() {
        return gameName;
    }
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
    public String getUrlId() {
        return urlId;
    }
    public void setUrlId(String urlId) {
        this.urlId = urlId;
    }
    public String getPicUrl() {
        return picUrl;
    }
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
    public String getVideoUrl() {
        return videoUrl;
    }
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
    public String getMesgUrl() {
        return mesgUrl;
    }
    public void setMesgUrl(String mesgUrl) {
        this.mesgUrl = mesgUrl;
    }
    public int getPicUpdateNum() {
        return picUpdateNum;
    }
    public void setPicUpdateNum(int picUpdateNum) {
        this.picUpdateNum = picUpdateNum;
    }
    public int getPicTotalNum() {
        return picTotalNum;
    }
    public void setPicTotalNum(int picTotalNum) {
        this.picTotalNum = picTotalNum;
    }
    public int getVideoUpdateNum() {
        return videoUpdateNum;
    }
    public void setVideoUpdateNum(int videoUpdateNum) {
        this.videoUpdateNum = videoUpdateNum;
    }
    public int getVideoTotalNum() {
        return videoTotalNum;
    }
    public void setVideoTotalNum(int videoTotalNum) {
        this.videoTotalNum = videoTotalNum;
    }
    public int getMesgUpdateNum() {
        return mesgUpdateNum;
    }
    public void setMesgUpdateNum(int mesgUpdateNum) {
        this.mesgUpdateNum = mesgUpdateNum;
    }
    
    @Override
    public String toString() {
        return "{gameId:"+gameId+","
              +"gameName:"+gameName+","
              +"urlId:"+urlId+","
              +"picUrl:"+picUrl+","
              +"videoUrl:"+videoUrl+","
              +"mesgUrl:"+mesgUrl+","
              +"picUpdateNum:"+picUpdateNum+","
              +"picTotalNum:"+picTotalNum+","
              +"videoUpdateNum:"+videoUpdateNum+","
              +"videoTotalNum:"+videoTotalNum+","
              +"mesgUpdateNum:"+mesgUpdateNum
              +"}";
    }
}
