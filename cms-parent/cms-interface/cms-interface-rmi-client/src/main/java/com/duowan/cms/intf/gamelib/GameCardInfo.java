package com.duowan.cms.intf.gamelib;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

public class GameCardInfo implements Serializable{
	/**
     * 
     */
    private static final long serialVersionUID = -7228951281742363362L;

    private String gameName;
	
	private Integer actStateId;
	
	private String bbsUrl;
	
	private String channelUrl;
	
	private String clientSize;
	
	private String clientUrl;
	
	private String cnName;
	
	private String deCo;
	
	private List<String> deCoList;
	
	private String evaluationUrl;
	
	private Integer gameID;
	
	private String homePageUrl;
	
	private String kaInfo;
	
	private String kaStatus;
	
	private String kaUrl;
	
	private List<NavInfo> navInfos;
	
	private String opCo;
	
	private List<String> opCoList;
	
	private Integer picNums;
	
	private String picUrl;
	
	private String region;
	
	private Integer starNum;
	
	private String statusInfo;
	
	private String subject;
	
	private List<String> subjectList;
	
	private String type;
	
	private List<String> typeList;
	
	private String urlID;
	
	private Integer videoNums;
	
	private String visual;
	
	private List<String> visualList;

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

    public Integer getActStateId() {
        return actStateId;
    }

    public void setActStateId(Integer actStateId) {
        this.actStateId = actStateId;
    }

    public String getBbsUrl() {
        return bbsUrl;
    }

    public void setBbsUrl(String bbsUrl) {
        this.bbsUrl = bbsUrl;
    }

    public String getChannelUrl() {
        return channelUrl;
    }

    public void setChannelUrl(String channelUrl) {
        this.channelUrl = channelUrl;
    }

    public String getClientSize() {
        return clientSize;
    }

    public void setClientSize(String clientSize) {
        this.clientSize = clientSize;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getDeCo() {
        return deCo;
    }

    public void setDeCo(String deCo) {
        this.deCo = deCo;
    }

    public List<String> getDeCoList() {
        return deCoList;
    }

    public void setDeCoList(List<String> deCoList) {
        this.deCoList = deCoList;
    }

    public String getEvaluationUrl() {
        return evaluationUrl;
    }

    public void setEvaluationUrl(String evaluationUrl) {
        this.evaluationUrl = evaluationUrl;
    }

    public Integer getGameID() {
        return gameID;
    }

    public void setGameID(Integer gameID) {
        this.gameID = gameID;
    }

    public String getHomePageUrl() {
        return homePageUrl;
    }

    public void setHomePageUrl(String homePageUrl) {
        this.homePageUrl = homePageUrl;
    }

    public String getKaInfo() {
        return kaInfo;
    }

    public void setKaInfo(String kaInfo) {
        this.kaInfo = kaInfo;
    }

    public String getKaStatus() {
        return kaStatus;
    }

    public void setKaStatus(String kaStatus) {
        this.kaStatus = kaStatus;
    }

    public String getKaUrl() {
        return kaUrl;
    }

    public void setKaUrl(String kaUrl) {
        this.kaUrl = kaUrl;
    }

    public List<NavInfo> getNavInfos() {
        return navInfos;
    }

    public void setNavInfos(List<NavInfo> navInfos) {
        this.navInfos = navInfos;
    }

    public String getOpCo() {
        return opCo;
    }

    public void setOpCo(String opCo) {
        this.opCo = opCo;
    }

    public List<String> getOpCoList() {
        return opCoList;
    }

    public void setOpCoList(List<String> opCoList) {
        this.opCoList = opCoList;
    }

    public Integer getPicNums() {
        return picNums;
    }

    public void setPicNums(Integer picNums) {
        this.picNums = picNums;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getStarNum() {
        return starNum;
    }

    public void setStarNum(Integer starNum) {
        this.starNum = starNum;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<String> subjectList) {
        this.subjectList = subjectList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }

    public String getUrlID() {
        return urlID;
    }

    public void setUrlID(String urlID) {
        this.urlID = urlID;
    }

    public Integer getVideoNums() {
        return videoNums;
    }

    public void setVideoNums(Integer videoNums) {
        this.videoNums = videoNums;
    }

    public String getVisual() {
        return visual;
    }

    public void setVisual(String visual) {
        this.visual = visual;
    }

    public List<String> getVisualList() {
        return visualList;
    }

    public void setVisualList(List<String> visualList) {
        this.visualList = visualList;
    }
    
    public Object get(String propertyName){
        
        Class clazz = this.getClass();
        try {
            Field field = clazz.getDeclaredField(propertyName);
            return field.get(this);
        } catch (Exception e) {
            return "";
        }
    }

}
