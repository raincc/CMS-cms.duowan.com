package com.duowan.cms.intf.duowanvote;

import com.duowan.cms.common.dto.SearchCriteria;


/**
 * 多玩投票系统的查询条件
 * @author yzq
 *
 */
public class DuowanVoteSearchCriteria implements SearchCriteria{

    /**
     * 
     */
    private static final long serialVersionUID = 6840564580837123586L;

    private String channelId;
    
    private String tag;
    
    private Integer status;
    
    private Integer type;
    
    private Integer start;
    
    private Integer size;
    
    private String startDate;
    
    private String endDate;
    
    private String queryType;
    
    private String articleIds;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getArticleIds() {
        return articleIds;
    }

    public void setArticleIds(String articleIds) {
        this.articleIds = articleIds;
    }
    
    
    
}
