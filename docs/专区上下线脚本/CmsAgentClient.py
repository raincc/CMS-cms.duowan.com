#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
 发送消息给CmsAgent
 用法
   CmsAgentClient.py {start|stop} 专区名
'''

import sys
import socket

# config
servers = [
  ('115.238.171.183', 20000),
  ('221.204.223.154', 20000),
  ('182.118.1.97', 20000),
  ('113.108.228.198', 20000),
  ('183.61.6.23', 20000),
  ('119.188.90.76', 20000),
  ('115.238.171.143', 20000),
  ('218.60.98.93', 20000),
]

def request(address, message):
  print 'request', address, message
  connection = socket.create_connection(address)
  connection.send(message)
  buffer = connection.recv(1024)
  if buffer == 'ok':
    return 0
  else:
    return 2

def request_all(message):
  for address in servers:
    result = request(address, message)
    print address, message, result

def start_subdomain(subdomain):
  message = 'start ' + subdomain
  request_all(message);

def stop_subdomain(subdomain):
  message = 'stop ' + subdomain
  request_all(message)

if __name__ == '__main__':
  command = sys.argv[1]
  subdomain = sys.argv[2]
  print command, subdomain
  if command == 'start':
    exit(start_subdomain(subdomain))
  elif command == 'stop':
    exit(stop_subdomain(subdomain))
  else:
    exit(1)
