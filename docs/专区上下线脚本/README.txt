CmsAgent

服务器
  1. 修改CmsAgentConfig.py，设置好监听的本地端口（注意Iptables）、允许的客户端ip和Nginx配置文件目录。
  2. 服务器上以root权限运行CmsAgent.py：
    CmsAgent.py CmsAgentConfig.py

客户端
  1. 修改CmsAgentClient.py中的servers变量，将全部服务器的ip和端口加入servers中。
  2. 开专区：
    CmsAgentClient.py start 专区名
  3. 关闭专区：
    CmsAgentClient.py stop 专区名
