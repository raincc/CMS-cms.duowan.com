START TRANSACTION;

alter table list_tzcq add (isdelete tinyint(4) default 0, diy4 varchar(2000) default null, diy5 varchar(2000) default null, title_color varchar(10) DEFAULT NULL);
alter table list_ifkcw add (isdelete tinyint(4) default 0, diy4 varchar(2000) default null, diy5 varchar(2000) default null, title_color varchar(10) DEFAULT NULL);
CREATE INDEX articleid ON list_tzcq (articleid);
CREATE INDEX articleid ON list_ifkcw (articleid);
COMMIT;