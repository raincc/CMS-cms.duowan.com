START TRANSACTION;

alter table list_tkzw add (isdelete tinyint(4) default 0, diy4 varchar(2000) default null, diy5 varchar(2000) default null, title_color varchar(10) DEFAULT NULL);
alter table list_zjsj add (isdelete tinyint(4) default 0, diy4 varchar(2000) default null, diy5 varchar(2000) default null, title_color varchar(10) DEFAULT NULL);
CREATE INDEX articleid ON list_tkzw (articleid);
CREATE INDEX articleid ON list_zjsj (articleid);
COMMIT;