CREATE TABLE `autoflushtag` (
  `id` bigint(13) NOT NULL AUTO_INCREMENT,
  `channelid` char(18) NOT NULL,
  `tagname`  varchar(20) NOT NULL,
  `createtime` datetime DEFAULT NULL,
  `flushtime` datetime DEFAULT NULL,  
  PRIMARY KEY (`id`),
KEY `channel_tag` (`channelid`,`tagname`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;