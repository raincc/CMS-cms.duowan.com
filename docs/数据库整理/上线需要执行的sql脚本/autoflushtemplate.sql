/*
MySQL Data Transfer
Source Host: 172.19.103.15
Source Database: cms
Target Host: 172.19.103.15
Target Database: cms
Date: 2013-03-08 15:09:26
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for autoflushtemplate
-- ----------------------------
DROP TABLE IF EXISTS `autoflushtemplate`;
CREATE TABLE `autoflushtemplate` (
  `channelid` char(18) NOT NULL DEFAULT '',
  `templateid` bigint(13) NOT NULL,
  `flushinterval` int(11) DEFAULT NULL,
  `lastflushtime` datetime DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `flushid` bigint(13) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`flushid`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `autoflushtemplate` VALUES ('wow', '212035943397', '600', null, null, '1');
INSERT INTO `autoflushtemplate` VALUES ('3ds', '141089009968', '600', null, null, '16');
INSERT INTO `autoflushtemplate` VALUES ('c9', '168883879666', '600', null, null, '17');
INSERT INTO `autoflushtemplate` VALUES ('c9', '196179685835', '600', null, null, '18');
INSERT INTO `autoflushtemplate` VALUES ('c9', '197043203344', '600', null, null, '19');
INSERT INTO `autoflushtemplate` VALUES ('d2', '215802364595', '600', null, null, '20');
INSERT INTO `autoflushtemplate` VALUES ('d3', '183035810768', '600', null, null, '21');
INSERT INTO `autoflushtemplate` VALUES ('df', '158895690233', '600', null, null, '22');
INSERT INTO `autoflushtemplate` VALUES ('df', '166639769728', '600', null, null, '23');
INSERT INTO `autoflushtemplate` VALUES ('df', '193425484329', '600', null, null, '24');
INSERT INTO `autoflushtemplate` VALUES ('dn', '138906883550', '600', null, null, '25');
INSERT INTO `autoflushtemplate` VALUES ('dn', '138907939701', '600', null, null, '26');
INSERT INTO `autoflushtemplate` VALUES ('dn', '198772801716', '600', null, null, '27');
INSERT INTO `autoflushtemplate` VALUES ('dn', '198772999515', '600', null, null, '28');
INSERT INTO `autoflushtemplate` VALUES ('dzs', '198249353282', '600', null, null, '29');
INSERT INTO `autoflushtemplate` VALUES ('dzs', '199107608644', '600', null, null, '30');
INSERT INTO `autoflushtemplate` VALUES ('dzs', '199110132873', '600', null, null, '31');
INSERT INTO `autoflushtemplate` VALUES ('dzs', '199109953133', '600', null, null, '32');
INSERT INTO `autoflushtemplate` VALUES ('dzs', '199110207164', '600', null, null, '33');
INSERT INTO `autoflushtemplate` VALUES ('ipad', '211804980978', '600', null, null, '34');
INSERT INTO `autoflushtemplate` VALUES ('ipad', '211805196760', '600', null, null, '35');
INSERT INTO `autoflushtemplate` VALUES ('ipad', '211808301518', '600', null, null, '36');
INSERT INTO `autoflushtemplate` VALUES ('ipad', '211804568175', '600', null, null, '37');
INSERT INTO `autoflushtemplate` VALUES ('ipad', '213615024833', '600', null, null, '38');
INSERT INTO `autoflushtemplate` VALUES ('ipad', '218828635403', '600', null, null, '39');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201275916419', '600', null, null, '40');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205428084098', '600', null, null, '41');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205350883299', '600', null, null, '42');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205350630263', '600', null, null, '43');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205350498271', '600', null, null, '44');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205350257122', '600', null, null, '45');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205350054475', '600', null, null, '46');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205349764308', '600', null, null, '47');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205349597087', '600', null, null, '48');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205348873654', '600', null, null, '49');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205348719203', '600', null, null, '50');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205517372459', '600', null, null, '51');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205517507495', '600', null, null, '52');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205345260573', '600', null, null, '53');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205431318188', '600', null, null, '54');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205342089927', '600', null, null, '55');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205344872826', '600', null, null, '56');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205346609840', '600', null, null, '57');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205431450286', '600', null, null, '58');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205427660665', '600', null, null, '59');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205427474341', '600', null, null, '60');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205427230254', '600', null, null, '61');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205427321233', '600', null, null, '62');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205426213746', '600', null, null, '63');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205424332146', '600', null, null, '64');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205425840299', '600', null, null, '65');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205348584035', '600', null, null, '66');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205348428441', '600', null, null, '67');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205348231368', '600', null, null, '68');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205347137591', '600', null, null, '69');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205347929685', '600', null, null, '70');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205346378655', '600', null, null, '71');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205346784727', '600', null, null, '72');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205346232329', '600', null, null, '73');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205345109051', '600', null, null, '74');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205344796348', '600', null, null, '75');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205344263032', '600', null, null, '76');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201026565508', '600', null, null, '77');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201087216718', '600', null, null, '78');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201086892890', '600', null, null, '79');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '205353088509', '600', null, null, '80');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '158254090284', '600', null, null, '81');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201805560080', '600', null, null, '82');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201118691031', '600', null, null, '83');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '166463366820', '600', null, null, '84');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201089437995', '600', null, null, '85');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '166459959933', '600', null, null, '86');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '220559254886', '600', null, null, '87');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201086124262', '600', null, null, '88');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201087130672', '600', null, null, '89');
INSERT INTO `autoflushtemplate` VALUES ('iphone', '201086804176', '600', null, null, '90');
INSERT INTO `autoflushtemplate` VALUES ('kan', '219852743637', '600', null, null, '91');
INSERT INTO `autoflushtemplate` VALUES ('kan', '220294823394', '600', null, null, '92');
INSERT INTO `autoflushtemplate` VALUES ('kan', '220357732113', '600', null, null, '93');
INSERT INTO `autoflushtemplate` VALUES ('kan', '220357260842', '600', null, null, '94');
INSERT INTO `autoflushtemplate` VALUES ('kan', '220356495369', '600', null, null, '95');
INSERT INTO `autoflushtemplate` VALUES ('ldj', '170782040072', '600', null, null, '96');
INSERT INTO `autoflushtemplate` VALUES ('lol', '178064635030', '600', null, null, '97');
INSERT INTO `autoflushtemplate` VALUES ('lol', '179158597172', '600', null, null, '98');
INSERT INTO `autoflushtemplate` VALUES ('lol', '179162045303', '600', null, null, '99');
INSERT INTO `autoflushtemplate` VALUES ('lol', '179162151812', '600', null, null, '100');
INSERT INTO `autoflushtemplate` VALUES ('lol', '195848602985', '600', null, null, '101');
INSERT INTO `autoflushtemplate` VALUES ('mhp3', '141324229456', '600', null, null, '102');
INSERT INTO `autoflushtemplate` VALUES ('nba2k', '212168076376', '600', null, null, '103');
INSERT INTO `autoflushtemplate` VALUES ('nba2k', '212163577352', '600', null, null, '104');
INSERT INTO `autoflushtemplate` VALUES ('nba2k', '212163614214', '600', null, null, '105');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112371259817', '600', null, null, '106');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112217595133', '600', null, null, '107');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112035534144', '600', null, null, '108');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112120106012', '600', null, null, '109');
INSERT INTO `autoflushtemplate` VALUES ('nds', '113418168134', '600', null, null, '110');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112298349322', '600', null, null, '111');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112298310239', '600', null, null, '112');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112297863525', '600', null, null, '113');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112298254396', '600', null, null, '114');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112292120829', '600', null, null, '115');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112359393069', '600', null, null, '116');
INSERT INTO `autoflushtemplate` VALUES ('nds', '112292081627', '600', null, null, '117');
INSERT INTO `autoflushtemplate` VALUES ('news', '184268553355', '600', null, null, '118');
INSERT INTO `autoflushtemplate` VALUES ('news', '184589551893', '600', null, null, '119');
INSERT INTO `autoflushtemplate` VALUES ('news', '184589698514', '600', null, null, '120');
INSERT INTO `autoflushtemplate` VALUES ('news', '184589775419', '600', null, null, '121');
INSERT INTO `autoflushtemplate` VALUES ('news', '184269684953', '600', null, null, '122');
INSERT INTO `autoflushtemplate` VALUES ('psp', '113417947158', '600', null, null, '123');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112650629906', '600', null, null, '124');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112651848212', '600', null, null, '125');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112655297753', '600', null, null, '126');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112660946469', '600', null, null, '127');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112650843838', '600', null, null, '128');
INSERT INTO `autoflushtemplate` VALUES ('psp', '135745125742', '600', null, null, '129');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112652148470', '600', null, null, '130');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112652766990', '600', null, null, '131');
INSERT INTO `autoflushtemplate` VALUES ('psp', '112652257231', '600', null, null, '132');
INSERT INTO `autoflushtemplate` VALUES ('qqxj', '194886545477', '600', null, null, '133');
INSERT INTO `autoflushtemplate` VALUES ('qqxl', '201008585142', '600', null, null, '134');
INSERT INTO `autoflushtemplate` VALUES ('rift', '218741009681', '600', null, null, '135');
INSERT INTO `autoflushtemplate` VALUES ('rift', '218741371748', '600', null, null, '136');
INSERT INTO `autoflushtemplate` VALUES ('webgame', '177686568922', '600', null, null, '137');
INSERT INTO `autoflushtemplate` VALUES ('wot', '192821021540', '600', null, null, '138');
INSERT INTO `autoflushtemplate` VALUES ('wot', '191860274581', '600', null, null, '139');
INSERT INTO `autoflushtemplate` VALUES ('wot', '191860376453', '600', null, null, '140');
INSERT INTO `autoflushtemplate` VALUES ('wot', '192814130079', '600', null, null, '141');
INSERT INTO `autoflushtemplate` VALUES ('wulin2', '221423467305', '600', null, null, '142');
INSERT INTO `autoflushtemplate` VALUES ('www', '157115657547', '600', null, null, '143');
INSERT INTO `autoflushtemplate` VALUES ('xunxian', '169140388121', '600', null, null, '144');
INSERT INTO `autoflushtemplate` VALUES ('xyx', '197228239957', '600', null, null, '145');
INSERT INTO `autoflushtemplate` VALUES ('xyx', '197227821987', '600', null, null, '146');
INSERT INTO `autoflushtemplate` VALUES ('zx', '186663790558', '600', null, null, '147');
INSERT INTO `autoflushtemplate` VALUES ('zx', '165408050051', '600', null, null, '148');
